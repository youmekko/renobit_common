import Vue from "vue";
import * as puremvc from "puremvc";


export default class ExternalWindowMediator  extends puremvc.Mediator {
      public static NAME: string = "ExternalWindowMediator";
      private static _count:number = 0;

      public static getCount(){
            ExternalWindowMediator._count++;
            return ExternalWindowMediator._count;
      }
      constructor(name: string, viewComponent: any, private notiList:string[]=[]) {
            super(name, viewComponent);
      }


      public getView(): any {
            return this.viewComponent;
      }

      public onRegister() {



      }


      public listNotificationInterests(): string[] {
            console.log("@@ external notilist ", this.notiList);
           return this.notiList;
      }


      /*
	배경 값의 경우 바인딩으로 사용하기 때문에 따로 뷰에 업데이트해줄 필요가 없읎ㅇ.ㅁ
	 */
      public handleNotification(note: puremvc.Notification): void {
            console.log("@@ external handleNotification note1 = ", note);
            this.getView().handleNotification(note);
      }

}
