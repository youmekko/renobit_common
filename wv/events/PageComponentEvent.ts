export class PageComponentEvent{


      public static readonly LOADED:string = "loaded";

      public type:string;
      public target:string;
      public data:any;


      constructor( type:string, target:any, data?:any ){
            this.type = type;
            this.target = target;
            this.data = data;
      }
}
