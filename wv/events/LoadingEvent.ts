
	export namespace event{
		export class LoadingEvent{
			public static readonly LOADING_EVENT:string = "LoadingEvent";
			public static readonly LOAD_START:string = "LoadingEvent.loadStart";
                  public static readonly CREATE_COMPONENT:string = "LoadingEvent.createComponent";
			public static readonly PROGRESS:string = "LoadingEvent.progress";
			public static readonly COMPLETED:string = "LoadingEvent.completed";

			public type:string;
			public target:string;
			public data:any;


			constructor( type:string, target:any, data?:any ){
				this.type = type;
				this.target = target;
				this.data = data;
			}
		}
	}
