export default class FPSManager {
      private static _instance = null;
      private _stats:any = null;
      private _show:boolean=false;
      private _requestAnimationID:number =0;

      constructor(){
            this.createElement();
      }


      static getInstance():FPSManager {
            if(FPSManager._instance==null){
                  FPSManager._instance = new FPSManager();
            }

            return FPSManager._instance;
      }

      private createElement(){
            if(this._stats==null) {
                  this._stats = new Stats();
            }
      }

      private removeElement(){

      }
      public toggle(){
            if(this._show){
                  this.hide();
            }else {
                  this.show();
            }

      }



      public show(){
            this._show=true;
            $(document.body).append(this._stats.dom);

            $(this._stats.dom).css({zIndex: 80000, right:"10px", bottom:0,position:"absolute", top:"auto", left:"auto"});
            var objThis = this;
            if(this._requestAnimationID==0){
                  (function localRender() {
                        objThis._requestAnimationID = requestAnimationFrame(localRender);
                        objThis._stats.update();
                  }());
            }


      }


      public hide(){
            this._show=false;
            $(this._stats.dom).remove();
      cancelAnimationFrame(this._requestAnimationID);
            this._requestAnimationID=0;

      }


      /*
      기존 소스 내용.

        this._stats = new Stats();
            if( window.location.hash.indexOf("_3debug") > -1 ){
                  this._$debugContainer = $("<div class='renderer-info'></div>");
                  this._$debugContainer.css({"position":"absolute", top:0, left:0, "z-index":"18000", color:"white" });
                  $(this._element).append(this._$debugContainer);

                  this.showStatsPanel();
            }


            showStatsPanel(){
            document.body.appendChild(this._stats.dom);
            $(this._stats.dom).css({zIndex:80000});
      }


      protected updateDebugInfo(){
            this._$debugContainer.empty();
            this._$debugContainer.html(`<p>geometries : ${this._renderer.info.memory.geometries}</p><p>textures : ${this._renderer.info.memory.textures}</p><p>triangles : ${this._renderer.info.render.triangles}</p><p>child : ${this._scene.children.length}</p><p>programs:${this._renderer.info.programs.length}</p>`);
      }
      */


}
