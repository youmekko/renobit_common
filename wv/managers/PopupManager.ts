import {IComponentMetaProperties, IWVComponent} from "../../core/component/interfaces/ComponentInterfaces";
import PopupWindow, {PopupWindowOptions} from "../components/PopupWindow";
import {ObjectUtil} from "../../utils/Util";
import WV2DComponent from "../../core/component/2D/WV2DComponent";
import Vue from "vue";
import {WORK_LAYERS} from "../layer/Layer";
import {TwoLayerCore} from "../layer/TwoLayerCore";


export default class PopupManager {

      private _popupInfoMap:Map<string, IWVComponent> = new Map();

      constructor() {
            this._popupInfoMap = new Map();
      }


      /*
	 페이지가 없어지는 경우 실행
	 call : OpenPageCommand()에서 호출
	 */
      public clear() {
            // 마스터레이어에 적용한 내용을 제거한다.
            this.closedAllPopup();
      }


      private _bInit:boolean=false;
      private _initGlobalBusEvent(){
            if(this._bInit==false){
                  this._bInit=true;

                  /*
	            팝업에서 닫기 버튼이 눌리는 경우

		 */

                  window.wemb.$globalBus.$on(PopupManagerEvent.CLOSED_POPUP_EVENT, (popupInstance:PopupWindow) => {
                        if(popupInstance && popupInstance.appendElement) {
                              // 팝업.name == popupId와 동일
                              this._closedPopup(popupInstance.name);
                        }
                  });
            }
      }


      /*

      페이지를 팝업으로 호출하기


      1. 팝업 존재유무 확인(페이지 이름으로)
      2. 팝업 컴포넌트 인스턴스 생성
      3. 팝업을 2D Layer에 추가
       */
      open(pageName:string, options:PopupWindowOptions) {
            var locale_msg = Vue.$i18n.messages.wv;

            /**
             * 현재 유저가 팝업 페이지에 대한 권한이 있는지 pageName 으로 체크하기
             */
            var accessiblePage = window.wemb.userInfo._accessiblePages.find(x=>x.name === pageName);
            if(!accessiblePage){
                Vue.$message.error(locale_msg.page.notHaveAssigned);
                return;
            }

            /*
            페이지 로딩 후
            닫기 버튼 활성화 처리를 위한 이벤트 등록
             */

            /*
            2019.02.24, ckkim
            options이 없는 경우 기본 값 추가 .
             */
            if(!options)
                  options = {};


            /* popupId가 없는 경우 pageName을 popupId로 설정하기. */
            if(options.popupId == null){
                  options.popupId = pageName;
            }



            //1. 팝업 존재유무 확인(페이지 이름으로)
            if(this._popupInfoMap.has(options.popupId)==false){
                  // 팝업이 닫히는 경우 닫히는 이벤트 리스너 활성화
                  this._initGlobalBusEvent();

                  // 페이지 존재 유무
                  if(window.wemb.pageManager.hasPageInfoByName(pageName)==false){
                        // 메시지를 내부에 출력해야함.
                        Vue.$message(locale_msg.page.noPageByName.replace("{pageName}",pageName));
                        return;

                  }

                  let pageSimpleInfo = window.wemb.pageManager.getPageInfoByName(pageName);
                  // 페이지에 존재하는 페이지 인지 확인하기
                  if(window.wemb.pageManager.isHavePage(pageName)==true){
                        Vue.$message(locale_msg.page.nestedPageWarning);
                        return;
                  }




                  /**
                   □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
                   생성할 페이지 팝업윈두오 인스턴스 메타 프로퍼티 만들기.
                   □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
                   **/


                  let tempInitMetaProperties:any = {
                        props: {
                              setter: {
                                    popup: "",
                                    depth: 0,
                                    width:0,
                                    height:0,
                                    borderRadius:{
                                          topLeft:0,
                                          topRight:0,
                                          bottomLeft:0,
                                          bottomRight:0
                                    },
                                    customStyle: false,
                                    header: '',
                                    close: '',
                                    x:0,
                                    y:0,
                                    title:"",
                                    contentOverflow:"hidden",
                                    memory:false
                              },
                              styleInfo:{
                                    className:""
                              },
                              pageComponentMetaProperties:{
                                    pageInfo:{
                                          pageName:"",
                                          usingProgress:false
                                    },
                                    params:{}
                              }

                        }
                  };

                  /* width, height 값이 없는 경우 읽어들이는 페이지 크기로 설정 */
                  if(options.hasOwnProperty("width")==false){
                        options.width = parseInt(pageSimpleInfo.props.setter.width);
                  }

                  if(options.hasOwnProperty("height")==false){
                        options.height = parseInt(pageSimpleInfo.props.setter.height);
                  }

                  if (options.hasOwnProperty("borderRadius") == false) {
                        options.borderRadius = pageSimpleInfo.props.setter.borderRadius;
                  }


                  /* x,y 값이 없는 경우 가운데로 설정 */
                  if(options.hasOwnProperty("x")==false){
                        let currentWidth = window.wemb.pageManager.currentPageInfo.props.setter.width;
                        options.x  = (currentWidth-options.width)/2;

                  }
                  if(options.hasOwnProperty("y")==false){
                        let currentHeight = window.wemb.pageManager.currentPageInfo.props.setter.height;
                        options.y  = (currentHeight-options.height)/2;
                  }

                  if(options.hasOwnProperty("customStyle")===false){
                        options.customStyle = pageSimpleInfo.props.setter.popup.customStyle;
                  }

                  if (options.hasOwnProperty("header") === false) {
                        options.header = pageSimpleInfo.props.setter.popup.instance.header;
                  }

                  if (options.hasOwnProperty("close") === false) {
                        options.close = pageSimpleInfo.props.setter.popup.instance.close;
                  }

                  /* options 값을 setter에 적용하기 */
                  $.extend(true, tempInitMetaProperties.props.setter, options);



                  if(options.hasOwnProperty("className")){
                        tempInitMetaProperties.props.styleInfo.className = options.className;
                  }

                  if(options.hasOwnProperty("params")){
                        tempInitMetaProperties.props.pageComponentMetaProperties.params = options.params;
                  }


                  /* progress 사용 유무 */
                  if(options.hasOwnProperty("usingProgress")){
                        tempInitMetaProperties.props.pageComponentMetaProperties.pageInfo.usingProgress = options.usingProgress;
                  }

                  // 페이지 이름 설정
                  tempInitMetaProperties.props.pageComponentMetaProperties.pageInfo.pageName = pageName;



                  //////////////
                  // init meta properties 생성
                  let initMetaProperties:IComponentMetaProperties =  $.extend(true, {
                        id: ObjectUtil.generateID(),
                        componentName:PopupWindow.NAME,
                        name: options.popupId,
                        layerName: WORK_LAYERS.MASTER_LAYER,

                  }, tempInitMetaProperties);

                  initMetaProperties.props.setter.depth=  this._getMaxZIndexToMasterLayer();


                  //Custom popup 인데 dragIns, CloseIns 가 옵션에 없으면 error
                  if (options.customStyle) {
                        if (!options.header || !options.close) {
                              Vue.$message.warning('Handler not specified. You cannot drag the popup.');
                        }
                  }

                  //2. 팝업 윈도우 인스턴스 생성
                  let comInstance:PopupWindow = window.wemb.componentLibraryManager.createComponentInstance(PopupWindow.NAME);
                  // 팝업 닫히는 처리를 위해 전역 버스 사용.
                  comInstance.setGlobalBus(window.wemb.$globalBus);
                  // 스크립트 이벤트 처리를 위해 이벤트 버스 전달.
                  comInstance.executeViewerMode();


                  // properties와 element 생성
                  comInstance.create(initMetaProperties);

                  // MasterLayer에 컴포넌트에 추가. (추가 후 immediateUpdate()와 onAddContainer() 호출
                  window.wemb.mainPageComponent.addComInstance(comInstance);


                  // 생성한 인스턴스를 맵에 등록
                  // 페이지 이동시 기존 마스터 내용은 그대로 둔채, 신규로 추가된 팝업 내용을 지울때도 사용됨.
                  this._popupInfoMap.set(options.popupId, comInstance)

                  return comInstance;
            }else {
                  // 기존에 생성한 팝업 인스턴스가 있는 경우 재 사용하기
                  let comInstance2:PopupWindow = this._popupInfoMap.get(options.popupId) as PopupWindow;
                  if(comInstance2) {

                        // 메모리가 true인 경우 기존 내용을 그대로 사용하기 위해서 활성화 처리
                        if(comInstance2.memory){
                              comInstance2.show();
                        }else {
                              comInstance2.pageComponent.openPageByName(pageName);
                        }
                        return comInstance2;
                  }
            }

            return null;
      }

      public closed(popupId:string){
            /*
            1. viewerMainContainer에서 팝업 컴포넌트 삭제
                  viewerMainContainer에서는 레이어를 찾아 레이어서 존재하는 컴포넌트를 찾아 삭제
             */
            let popupInstance = this._popupInfoMap.get(popupId) as PopupWindow;
            // 직접 삭제하는게 아니라 이벤트를 발생시켜 삭제되게 해야함.
            if(popupInstance){
                  popupInstance.closed();
            }
      }



      /*
      memory와 상관 없이 무조건 제거
       */
      public immediateClosed(popupId:string){
            let popupInstance = this._popupInfoMap.get(popupId);
            if(popupInstance) {

                  window.wemb.mainPageComponent.removeComInstance(popupInstance);
                  this._popupInfoMap.delete(popupId);
            }
      }


      public closedAllPopup(){
            let popupIdList =Array.from(this._popupInfoMap.keys());

            for (let popupId of popupIdList) {
                  this.immediateClosed(popupId);
            }

            this._popupInfoMap.clear();

      }


      /*
      메모리가 true인 경우 제거 하지 않기
       */
      private _closedPopup(popupId:string) {
            let popupInstance = this._popupInfoMap.get(popupId) as PopupWindow;
            if(popupInstance) {
                  if(popupInstance.memory==false) {
                        window.wemb.mainPageComponent.removeComInstance(popupInstance);
                        this._popupInfoMap.delete(popupId);
                  }else {
                        popupInstance.hide();
                  }
            }
      }




      private getPopupInstance(popupId:string){
            let popupInstance = this._popupInfoMap.get(popupId);
            if(popupInstance) {
                  return popupInstance;
            }

            return null;
      }


      public hasPopupInstance(popupId:string){
            return this._popupInfoMap.has(popupId);
      }

      // master 레이어에서 컴포넌트 인스턴스의 최대 zindex값 구하기
      private _getMaxZIndexToMasterLayer() {

            let layer:TwoLayerCore=window.wemb.mainPageComponent.masterLayer;
            let childLength:number = layer.numChildren;
            let zIndex:number = 30000;
            for(let i=0;i<childLength;i++){
                  let currentIndex = (layer.getChildAt(i) as WV2DComponent).depth;
                  if(zIndex<currentIndex)
                        zIndex=currentIndex;
            }

            return zIndex + 1;

      }


      get popupInfoMap() {
            return this._popupInfoMap;
      }


}

export const PopupManagerEvent = {
      UPDATE_POPUP_LIST_EVENT: "event/updatePopupList",
      CLOSED_POPUP_EVENT: "event/closedPopup",
      CREATED_POPUP: "event/createdPopup"
};
