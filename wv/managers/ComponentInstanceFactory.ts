import Vue from "vue";
import {event} from "../events/LoadingEvent";
import LoadingEvent = event.LoadingEvent;

import WVEvent from "../../core/events/WVEvent";
import ComponentLibraryManager from "./ComponentLibraryManager";
import WVComponent from "../../core/component/WVComponent";
import {IDisplayObjectProperties} from "../../core/display/interfaces/DisplayInterfaces";
import CPNotification from "../../core/utils/CPNotification";
import {WORK_LAYERS} from "../layer/Layer";
import {EXE_MODE} from "../data/Data";
import {IComponentInstanceLoaderContainer} from "./ComponentInstanceLoader";



/*
컴포넌트 라이브러리 메니저를 활용해 여러 개의 컴포넌트 인스턴스를 생성하는 기능 담당.
call : 주로 pageManager에서 사용됨.
 */
export default class ComponentInstanceFactory {

      public static readonly  ON_LOAD_PAGE_METHOD:string = "onLoadPage";
      public static count:number =0;
      private _$bus = new Vue();
      private _loadIndex =0;
      private _componentInfoList:Array<any> = [];
      private _currentComponentInstance:WVComponent = null;
      private _loadingSW=true;

      //hong
      private instanceCount = 0;

      constructor(private _libraryManager:ComponentLibraryManager, private _loaderPageContainer:IComponentInstanceLoaderContainer, private _exeMode:EXE_MODE=EXE_MODE.EDITOR){

            ComponentInstanceFactory.count++;
      }


      /*
      ComponentInstanceFactory에서 발생하는 이벤트 등록 처리
       */
      public $on(eventName:string, data:any){
            this._$bus.$on(eventName, data);
      }

      //////////////////////////////////////////////////

      public clear(){
            this._componentInfoList = null;
            this._$bus.$off();

            this._currentComponentInstance = null;
      }


      /*
      Factory 멈추기
       */
      public stopAndClearFactory(){


            this._loadingSW=false;
            console.log("ComponentInstanceFactory2 stopAndClearFactory ", ComponentInstanceFactory.count, this._loadingSW, this._loadIndex);
            if(this._currentComponentInstance){
                  this._currentComponentInstance.destroy();
                  this._currentComponentInstance= null;
            }


            this.clear();

      }

      //////////////////////////////////////////////////





      //////////////////////////////////////////////////
      public startFactory(comInstanceInfoList:Array<any>){
            this._componentInfoList = comInstanceInfoList;
            this.instanceCount = this._componentInfoList.length;
            if(comInstanceInfoList==null || comInstanceInfoList.length==0){
                  let event = new LoadingEvent(LoadingEvent.LOAD_START, this, {
                        loadLength:0,
                        currentIndex:0
                  });

                  this._$bus.$emit(LoadingEvent.LOAD_START, event);
                  this._completedLoad();

                  return;
            }


            this._executeStartLoading();
      }

      private _executeStartLoading(){
            let event = new LoadingEvent(LoadingEvent.LOAD_START, this, {
                  loadLength:this._componentInfoList.length,
                  currentIndex:0
            });
            this._$bus.$emit(LoadingEvent.LOAD_START, event);

            //FAST LOAD (홍건희)
            var that = this;
            this._componentInfoList.forEach(function(comInstanceInfo, index){
                  let comInstance:WVComponent = that._libraryManager.createComponentInstance(comInstanceInfo.componentName);
                  that._currentComponentInstance= comInstance;

                  if(comInstance==null){
                        CPNotification.getInstance().notifyMessage("인스턴스 생성 실패 ", [`componentName=${comInstanceInfo.componentName}`, `instanceName=${comInstanceInfo.name}`]);
                        // that._failedLoadComponentAndNextLoad();
                        that.instanceCount--;
                        if(that.instanceCount > 0) {
                              let loadEvent = new LoadingEvent(LoadingEvent.PROGRESS, that,{
                                    loadLength:that._componentInfoList.length,
                                    currentIndex:that._loadIndex+1,
                                    comInstanceInfo:null,
                                    comInstance:null,
                                    comInstanceCount : that.instanceCount
                              });
                              that._$bus.$emit(LoadingEvent.PROGRESS, loadEvent);
                        } else {
                              that._completedLoad();
                        }

                        return;
                  }


                  /*
                  컴포넌트가 Container(Layer)에 붙는 경우 발생하는 ADDED_CONTAINER이벤트를 활용해
                  컴포넌트 로딩 처리
                   */

                  function onAddContainer(event:WVEvent){
                        comInstance.removeEventListener(WVEvent.ADDED_CONTAINER, onAddContainer);
                        if(that._loadingSW==false)
                              return;

                        that.instanceCount--;
                        if(that.instanceCount > 0)
                        {
                              let loadEvent = new LoadingEvent(LoadingEvent.PROGRESS, that,{
                                    loadLength:that._componentInfoList.length,
                                    currentIndex: index+1,
                                    comInstanceInfo:comInstanceInfo,
                                    comInstance:comInstance,
                                    comInstanceCount : that.instanceCount
                              });
                              that._$bus.$emit(LoadingEvent.PROGRESS, loadEvent);
                        }else{
                              that._completedLoad();
                        }
                        // that._nextLoad();
                  }


                  function onCreated(event:WVEvent){
                        that._currentComponentInstance =null;
                        comInstance.removeEventListener(WVEvent.CREATED, onCreated);


                        if(that._loadingSW==false)
                              return;


                        //console.log("### onCreated");
                        //console.log("### container에 component 추가하기 이후 ADD이벤트 발생", comInstance.name);

                        let loadEvent = new LoadingEvent(LoadingEvent.CREATE_COMPONENT, that,{
                              loadLength:that._componentInfoList.length,
                              currentIndex:index+1,
                              comInstanceInfo:comInstanceInfo,
                              comInstance:comInstance
                        });

                        that._$bus.$emit(LoadingEvent.CREATE_COMPONENT, loadEvent);


                        /*
                        만약 CREATED 이벤트와 ADDED_CONTAINER 이벤트가 발생 전에
                        취소가 되면 _currentComponentInstance에 등록된 이벤트를 모두 제거해야 한다.
                         */


                  }


                  comInstance.addEventListener(WVEvent.CREATED, onCreated);
                  comInstance.addEventListener(WVEvent.ADDED_CONTAINER, onAddContainer);


                  /*
              컴포넌트 기능 추가 및 설정
               */
                  if (comInstanceInfo.layerName == WORK_LAYERS.THREE_LAYER) {
                        comInstance.setExtensionElements(that._loaderPageContainer.getThreeExtensionElements());
                  }

                  /* WScript 처리를 위한 코드 실행, 뷰어 모드 일때만 실행  */
                  if(that._exeMode==EXE_MODE.VIEWER) {
                        comInstance.executeViewerMode();
                  }

                  // 인스턴스 생성 후, 프로퍼티와 Element 생성을 위해 create()실행
                  // create가 정상적으로 실행되는 경우 ADDED_CONTAINER이벤트 발생
                  comInstance.create(comInstanceInfo as IDisplayObjectProperties);
            });

            //구버전
            // this._loadAt(0);
      }

      private _loadAt(index:number){

            if(this._loadingSW==false)
                  return;


            let comInstanceInfo:ComponentInstanceInfoInPage = this._componentInfoList[index];
            console.log("\n\n\n생성 인스턴스 시작", index+1, "/", this._componentInfoList.length, "comInstanceInfo.componentName", comInstanceInfo.componentName, comInstanceInfo.name)
            // 컴포넌트 인스턴스 생성
            let comInstance:WVComponent = this._libraryManager.createComponentInstance(comInstanceInfo.componentName);
            this._currentComponentInstance= comInstance;


            // 컴포넌트 인스턴스가 컨테이너에 붙은 경우 다음 내용 실행.
            let that = this;

            if(comInstance==null){
                  CPNotification.getInstance().notifyMessage("인스턴스 생성 실패 ", [`componentName=${comInstanceInfo.componentName}`, `instanceName=${comInstanceInfo.name}`]);
                  that._failedLoadComponentAndNextLoad();

                  return;
            }


            /*
            컴포넌트가 Container(Layer)에 붙는 경우 발생하는 ADDED_CONTAINER이벤트를 활용해
            컴포넌트 로딩 처리
             */
            function onAddContainer(event:WVEvent){
                  comInstance.removeEventListener(WVEvent.ADDED_CONTAINER, onAddContainer);
                  if(that._loadingSW==false)
                        return;


                  // 리소스를 사용하지 않는 경우

                  let loadEvent = new LoadingEvent(LoadingEvent.PROGRESS, that,{
                        loadLength:that._componentInfoList.length,
                        currentIndex:index+1,
                        comInstanceInfo:comInstanceInfo,
                        comInstance:comInstance
                  });

                  that._$bus.$emit(LoadingEvent.PROGRESS, loadEvent);


                  that._nextLoad();
            }


            function onCreated(event:WVEvent){
                  that._currentComponentInstance =null;
                  comInstance.removeEventListener(WVEvent.CREATED, onCreated);


                  if(that._loadingSW==false)
                        return;


                  //console.log("### onCreated");
                  //console.log("### container에 component 추가하기 이후 ADD이벤트 발생", comInstance.name);

                  let loadEvent = new LoadingEvent(LoadingEvent.CREATE_COMPONENT, that,{
                        loadLength:that._componentInfoList.length,
                        currentIndex:index+1,
                        comInstanceInfo:comInstanceInfo,
                        comInstance:comInstance
                  });

                  that._$bus.$emit(LoadingEvent.CREATE_COMPONENT, loadEvent);


                  /*
                  만약 CREATED 이벤트와 ADDED_CONTAINER 이벤트가 발생 전에
                  취소가 되면 _currentComponentInstance에 등록된 이벤트를 모두 제거해야 한다.
                   */


            }


            comInstance.addEventListener(WVEvent.CREATED, onCreated);
            comInstance.addEventListener(WVEvent.ADDED_CONTAINER, onAddContainer);


            /*
		컴포넌트 기능 추가 및 설정
		 */
            if (comInstanceInfo.layerName == WORK_LAYERS.THREE_LAYER) {
                  comInstance.setExtensionElements(this._loaderPageContainer.getThreeExtensionElements());
            }

            /* WScript 처리를 위한 코드 실행, 뷰어 모드 일때만 실행  */
            if(this._exeMode==EXE_MODE.VIEWER) {
                  comInstance.executeViewerMode();
            }

            // 인스턴스 생성 후, 프로퍼티와 Element 생성을 위해 create()실행
            // create가 정상적으로 실행되는 경우 ADDED_CONTAINER이벤트 발생
            comInstance.create(comInstanceInfo as IDisplayObjectProperties);
      }




      /*
	 컴포넌트 생성이 실패하는 경우
	 */
      private _failedLoadComponentAndNextLoad(){
            if(this._loadingSW==false)
                  return;

            let loadEvent = new LoadingEvent(LoadingEvent.PROGRESS, this,{
                  loadLength:this._componentInfoList.length,
                  currentIndex:this._loadIndex+1,
                  comInstanceInfo:null,
                  comInstance:null

            });

            this._$bus.$emit(LoadingEvent.PROGRESS, loadEvent);
            this._nextLoad();
      }

      private _nextLoad(){
            if(this._loadingSW==false)
                  return;


            this._loadIndex++;
            if(this._componentInfoList.length<=this._loadIndex){
                  this._completedLoad();
            }else {
                  this._loadAt(this._loadIndex);
            }
      }

      private _completedLoad(){
            if(this._loadingSW==false)
                  return;

            console.log("################# 11 컴포넌트 파일 로딩 완료 . ");
            console.log("\n\n\n");



            let event = new LoadingEvent(LoadingEvent.COMPLETED, this);
            this._$bus.$emit(LoadingEvent.COMPLETED, event);

            this.clear();
      }




}

class ComponentInstanceInfoInPage {
      componentName:string;
      id:string;
      name:string;
      layerName:string
      props: {
            setter?:any;
      }
}
