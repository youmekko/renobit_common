import Vue from "vue";
import sessionApi from "../../../common/api/sessionApi";

export class SessionCheckingManager {
      private static _instance = null;

      private _timerID: number = 0;
      private _time: number = 10 * 1000;

      static getInstance():SessionCheckingManager{
            if(SessionCheckingManager._instance==null){
                  SessionCheckingManager._instance = new SessionCheckingManager();
            }

            return SessionCheckingManager._instance;
      }


      constructor() {

      }

      public setTime(minute: number) {
            // 최소 5분 이상
            minute = Math.max(1,minute);
            this._time = minute * 1000;
      }

      public stopChecking() {
            if (this._timerID != 0) {
                  clearTimeout(this._timerID);
                  this._timerID = 0;
            }
      }


      public startChecking() {
            if (this._timerID == 0) {
                  this._timerID = setTimeout(() => {
                        this.tick();
                  }, this._time);

            }
      }


      private async tick() {
            let resultInfo: any = await sessionApi.prolongSession();
            if (resultInfo.success == false || resultInfo.error) {
                  console.warn(resultInfo.message);
                  this.stopChecking();

            } else {
                  //console.log("session 연장 정상적으로 처리 완료");
                  this.stopChecking();
                  this.startChecking();
            }
      }


}
