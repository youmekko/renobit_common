import Vue from "vue";
import configApi from "../../../common/api/configApi";
import sendErrorMessage from "../../../common/api/sendErrorMessage";
import {WORK_LAYERS} from "../layer/Layer";
import {CONFIG_MODE, ERROR_TYPE, EXE_MODE} from "../data/Data";

import {http} from "../../../wemb/http/Http";
import api = http.api;
import {SessionCheckingManager} from "./SessionCheckingManager";

/*
version 1.3.2.22(2019.02.28)
-배경패널 textarea 이벤트 처리 추가
-메뉴컴포넌트 active 메소드추가 등

version 1.3.2.21(2019.02.25)

version 1.3.2.10(2019.02.13)
 -포소드 기능 다수 수정
 -에디터 기능 다수 수정

version 1.3.2.9(2019.02.12)
 -포소드 기능 다수 수정


version 1.3.2.8(2019.02.08)
 -포소드 기능 다수 수정


version 1.3.2.7(2019.02.01)
 -CCTV 컴포넌트 수정

version 1.3.2.6(2019.01.30)
 -CCTV 컴포넌트 수정


version 1.3.2.5(2019.01.28)
 -POSODEventBrower 수정
 -ADMIn 권한 삭제 오류 수정
 -로그인 세션 처리 수정 중

version 1.3.2.3(2019.01.11)
 - DCIM, POSOD 버그 수정

version 1.3.2.2(2019.01.09)
 - Pack 다국어 기능 추가

version 1.3.1.2(2019.01.07)
 - RollingComponent 로딩 방식 변경

version 1.3.1.0(2018.12.27)
 - 2D라벨 BackgroundColor, Padding, Font-weight 기능 추가

version 1.3.0.3(2018.12.17)
version 1.3.0.0(2018.11.28)

version 1.2.12.1(2018.10.30)
- 세션 DB화 적용
- 세션 클러스터링 적용
- 로그 DB 자동삭제 스케줄러(30일기준) 적용
- 나침반 visible처리 오류 수정
- XLSX 전역객체 추가



만들고 나서 아직 적용하지 않음.
version 1.2.8.12(2018.10.10)
- java 버전 테스트를 위한 기능 수정
- Viewer에서 오른쪽 마우스 버튼 막기
- viewer에서 메인 메뉴가 빈 영역을 영역으로 차지하는 문제 해결
- Snippets 팝업호출시 전달 인자 key정보 수정 param=>params

version 1.2.6.10(2018.09.20)
      - 메뉴셋 기능 추가 및 수정
      - 에디터에서 토글 버튼으로 나침반 토글 처리
      - config값으로 뷰어에서 나침반 보기이 숨기기
      - 초기 페이지에 저장한  카메라 위치로 이동 하는 기능
            - page.resetCameraPosition()

version 1.2.1.5 ( 2018.09.18)
      - 데이터 셋 내보내기 버튼 활성화
      - 메뉴 셋 기능 추가
            - 팝업, 등등
            - 팝업 활성화 /비활성화시 키보드 명령어 락/풀기

version 1.2.0.4 (2018.09.14)
      - 3D 컴포넌트 기능 추가 삭제
          - boundingEnable  기능 삭제
              mouse over/out시 하이라이트 기능 사용시 문제 점 발생

version 1.2.0.3 (2018.09. 14)

      Admin 페이지
            - 시작 페이지 설정되지 않는  문제 해결

      편집기 기능 추가
       - RestAPI DataSet 타입 추가

      컴포넌트 기능 추가 및 개선 사항
       - 동적으로 WScript Event 등록 기능 추가
           - 이벤트 등록방법:
                 컴포넌트인스턴스.onWScriptEvent("이벤트명", 리스너)
                (첨부한 3d_test_page.json (샘플 페이지) 참고)

       - 롤링 컴포넌트 오토 플레이 되지 않는 문제 해결

       - 3D 컴포넌트 기능 추가
          - boundingEnable 속성 추가
              mouse over/out시 하이라이트 기능 사용 유무 설정
              초기값은 속성 창에서 설정 가능
              (첨부한 3d_test_page.json (샘플 페이지) 참고)

          - 3D Label x,y 위치 속성 추가
              속성 패널에서 설정 가능


version 1.1.2.1
-컴포넌트 추가
      - EmptyComponent
      - UpdownStatusComponnet
- 컴포넌트 수정
      - MenuComponent
            - Event parameter 추가

- Editor/Viewer 기능 추가
      - wemb.logout()

 */



type ConfigInfo = {
      mode: string;
      remote: {
            server_path: string;
            data_manager_socket_url: string;
            data_manager_socket_port: string;
            server_type:string;
      },
      layer:{
            using_three_layer:boolean
      },
      loading: {
            text_display: string;
      },
      viewer: {
            main_menu_display: boolean;
            session_checking:{
                  using:boolean,
                  duration:number
            }
      },
      locale: string;
}

export enum CONFIG_PROPERTY_GROUP_NAME {
      MODE = "mode",
      REMOTE = "remove",
      LOADING = "loading",
      VIEWER = "viewer",
      EXTENSION = "extension"
}


export default class ConfigManager {
      private static _instance: ConfigManager = null;
      private _test: boolean = false;
      private _hostUrl = "";

      private _mode: CONFIG_MODE = CONFIG_MODE.REMOTE;

      public get version():string{
            return VERSION;
      }

      public get configMode() {
            return this._configInfo.mode;
      }

      public get serverType():string {
            return this._configInfo.remote.server_type;
      }

      private _remotePath = "";
      private _removeWorkURL = "";
      private _offlineWorkURL = "static/mockup_data";


      private _startPageId: string = "";
      private _startPageName = "";

      private _startWorkLayerName: WORK_LAYERS = WORK_LAYERS.TWO_LAYER;
      private _exeMode: EXE_MODE = EXE_MODE.EDITOR;
      /*
	@_init 초기화 완료 유무 상태 변수
	 */
      private _init = false;

      private _dataManagerSocketPort = 0;
      private _dataManagerSocketUrl = "";
      private _log = false;

      private _configInfo: any;
      public get configInfo(){
            return this._configInfo;
      }

      private _queryParams: any;

      private _fonts: Array<string> = [];

      private _userId: string;
      private _hasAdminRole: boolean = false;
      private _maxActiveTime: number = 30;

      private resolve;
      private _configPromise;

      constructor(exeMode = EXE_MODE.EDITOR) {
            /*
		삭제예정:  2018.05.31
		exeMode를 제거
		exeMode로 받을 예정
		 */
            this._exeMode = exeMode;
      }

      private _pack_info: any;

      public get pack_info() {
            return this._pack_info;
      }

      public set pack_info(v) {
            this._pack_info = v;
      }

      private _context:string;

      get context() {
            return this._context;
      }

      set context(value) {
            this._context = value;
      }


      public get sessionTime(): number {
            return wemb.userInfo.maxActiveTime;
      }

      public static getInstance(): ConfigManager {
            if (ConfigManager._instance == null) {
                  ConfigManager._instance = new ConfigManager();
            }

            return ConfigManager._instance;
      }


      /*
            환경설정 정보가 로드 되기 전에 먼저 실행됨.
		call :
			ViewerMainComponent.create()에서 호출
		 */
      public initConfigValue(queryParams) {


            this._queryParams = queryParams;

            if (queryParams.hasOwnProperty("pid") == true) {
                  this._startPageId = queryParams.pid;
            }


            if (queryParams.hasOwnProperty("pname") == true) {
                  this.startPageName = queryParams.pname;
            }

            if (queryParams.hasOwnProperty("layer") == true) {

                  switch (queryParams.layer) {
                        case "0" :
                              this.startWorkLayerName = WORK_LAYERS.MASTER_LAYER;
                              break;
                        case "1" :
                              this.startWorkLayerName = WORK_LAYERS.TWO_LAYER;
                              break;
                        case "2" :
                              this.startWorkLayerName = WORK_LAYERS.THREE_LAYER;
                              break;

                  }
            }

            if (queryParams.hasOwnProperty("log") == true) {
                  if (queryParams.log == "true" || queryParams.log == "1") {
                        this._log = true;
                  } else {
                        this._log = false;
                  }
            }
      }


      public async startLoading(): Promise<boolean> {
            let configInfo = await configApi.loadConfig();
            // 테스트 유무 구하기
            if (this._getParameterByName("test") == true) {
                  this._test = true;
            }
            else {
                  this._test = false;
            }

            return this._checkConfigValue(configInfo);
      }
      async getFontList () : Promise<any> {
            let fontsList = {
                  id: "adminService.getFontInfo",
                  params: {}
            };

            // 초기 default font
            let selected_font = "NanumGothic";

            await api.post(this._getFullPathName() + "/admin.do", fontsList).then((result) => {
                  this._fonts = JSON.parse(result.data.data.fonts);
                  selected_font = result.data.data.selected_font;
            });

            let font_family = this._fonts.filter((font)=> font.hasOwnProperty('fontface')).map((f)=>f.fontface).join('\n');
            font_family += font_family + " .viewer .view-area-main *, .editor .edit-area-main * { font-family: " + selected_font + ", -apple-system, Roboto, Helvetica Neue, Arial, sans-serif;}"

            var head = document.head || document.getElementsByTagName('head')[0];
            var style = document.createElement('style');
            head.appendChild(style);
            style.type = 'text/css';
            if (style.styleSheet){
                  // This is required for IE8 and below.
                  style.styleSheet.cssText = font_family;
            } else {
                  style.appendChild(document.createTextNode(font_family));
            }
      }
      checkFontExist(font : string){
            return this.fonts.some((data)=>{
                  return data.label === font
            });
      }

      // 로딩 텍스트 출력 유무 판단.
      public get loadingTextDisplay(): boolean {
            if (this._configInfo.hasOwnProperty("loading")) {
                  if (this._configInfo.loading.hasOwnProperty("text_display"))
                        return this._configInfo.loading.text_display;

            }

            return true;
      }

      get queryParams() {
            return this._queryParams;
      }

      get viewerMainMenuDisplay() {
            if (this._configInfo.hasOwnProperty("viewer")) {
                  if (this._configInfo.viewer.hasOwnProperty("main_menu_display"))
                        return this._configInfo.viewer.main_menu_display;

            }
            return true;
      }

      get viewerDirectionView() {
            if (this._configInfo.hasOwnProperty("viewer")) {
                  if (this._configInfo.viewer.hasOwnProperty("direction_view"))
                        return this._configInfo.viewer.direction_view;

            }
            return false;
      }


      /*
      2018.11.04(ckkim)
      3d 사용 유무

       */

      get using3D(){
            if (this._configInfo.hasOwnProperty("layer")) {
                  if (this._configInfo.layer.hasOwnProperty("using_three_layer"))
                        return this._configInfo.layer.using_three_layer;

            }


            return true;
      }



      get userMultilingualPath() {
            let path = this._configInfo.multilingual_path || '';
            return path;
      }

      /*
      1. 폰트 체크
      2. url 체크
      3. 서버타입(java,node) 체크
      4. data_manager_socket_port 설정(현재 사용하지 않음)
      5.

       */
      _checkConfigValue(config: any) {
            if(config==null){
                  alert("config error");
                  return;
            }

            var self = this;
            this._configInfo = config;



            if (config.hasOwnProperty("fonts") == false) {
                  this._fonts = [];
            } else {
                  this.getFontList();
            }

            // 모드가 없는 경우, 기본 remote로 설정
            if (config.hasOwnProperty("mode") == false) {
                  config.mode = CONFIG_MODE.LOCAL;
            }




            if (config.mode == CONFIG_MODE.LOCAL) {
                  this._remotePath = this._getFullPathName();
                  this._removeWorkURL = this._remotePath + "/editor.do";
                  this._dataManagerSocketUrl = "ws://" + location.hostname;

            } else {
                  // 설정값이 remote가 아닌 경우 local로 간주
                  if (config.hasOwnProperty("remote") == false) {
                        sendErrorMessage.sendCriticalErrorCommand({
                              type: ERROR_TYPE.PARSING_ERROR,
                              message: "설정값이 remote인 경우 config.json에 remote 정보가 있어야 합니다. 관리자에 문의해주세요.'"
                        });
                        return false;
                  }


                  if (config.remote.hasOwnProperty("server_path") == false) {
                        sendErrorMessage.sendCriticalErrorCommand({
                              type: ERROR_TYPE.PARSING_ERROR,
                              message: "설정값이 remote인 경우 config.json에 remote.server_path 정보가 있어야 합니다. 관리자에 문의해주세요."
                        });


                        return false;
                  }

                  if (config.remote.hasOwnProperty("data_manager_socket_url") == false) {
                        sendErrorMessage.sendCriticalErrorCommand({
                              type: ERROR_TYPE.PARSING_ERROR,
                              message: "설정값이 remote인 경우 config.json에 remote.data_manager_socket_url 정보가 있어야 합니다. 관리자에 문의해주세요."
                        });


                        return false;
                  }

                  this._remotePath = config.remote.server_path;
                  this._removeWorkURL = this._remotePath + "/editor.do";
                  this._dataManagerSocketUrl = config.remote.data_manager_socket_url;
            }


            /*
            기본 값 설정하기.
             */
            if (config.remote.hasOwnProperty("server_type") == false) {
                  config.remote.server_type="java"
            }else {
                  var value:string = config.remote.server_type;
                  if(value.toLowerCase()!="java" || value.toLowerCase()!="node")
                        config.remote.server_type=="java";
            }



            if (config.remote.hasOwnProperty("data_manager_socket_port") == false) {

                  sendErrorMessage.sendCriticalErrorCommand({
                        type: ERROR_TYPE.PARSING_ERROR,
                        message: "설정값이 remote인 경우 config.json에 remote.data_manager_socket_port 정보가 있어야 합니다. 관리자에 문의해주세요."
                  });

                  return false;
            }


            this._dataManagerSocketPort = config.remote.data_manager_socket_port;

            /*
		디버깅용

		console.log("_remotePath = ", this._remotePath);
		console.log("_removeWorkURL = ", this._removeWorkURL);
		console.log("_dataManagerSocketUrl = ", this._dataManagerSocketUrl);
		*/
            this._init = true;
            window.__serverUrl__ = this.serverUrl;//dataset관리자 window에서 접근시 사용 config객체 접근시 ie에서 늦어지기 때문에..
            this.context = this._getContext();

            //this.resolve(config);
            return true;
      }

      /*
		현재 접근한 최종 경로만 구하기
		location.origin + pathname
		 */
      private _getFullPathName() {
            return location.origin + this._getContext();
      }

      /**
       * context 값 설정
       * @private
       */

      private _getContext() {
            let pathName = location.pathname;

            //pathName에 '/' (슬래쉬) 가 한 개만 들어가면 Context가 Root임
            let isContextRoot = pathName.match(/\//ig).length === 1;

            if (isContextRoot) {
                  pathName = ""
            } else {
                  pathName = pathName.split("/")[1];
                  pathName = "/" + pathName;
            }

            return pathName;
      }


      get fonts() {
            return this._fonts;
      }

      get serverUrl() {
            if (this._test) {
                  return this._offlineWorkURL;
            } else {
                  return this._remotePath
            }
      }


      get workUrl() {
            if (this._test) {
                  return this._offlineWorkURL;
            } else {
                  return this._removeWorkURL
            }
      }


      set test(testMode) {
            this._test = testMode;
      }

      get test() {
            return this._test;
      }

      get startPageId() {
            return this._startPageId;
      }

      set startPageId(id) {
            this._startPageId = id;
      }


      get startPageName() {
            return this._startPageName;
      }

      set startPageName(name) {
            this._startPageName = name;
      }

      get exeMode() {
            return this._exeMode;
      }

      set exeMode(_exeMode: EXE_MODE) {
            this._exeMode = _exeMode;
      }

      get isEditorMode() {
            return this._exeMode == EXE_MODE.EDITOR;
      }


      set startWorkLayerName(value) {
            this._startWorkLayerName = value;
      }

      get startWorkLayerName() {
            return this._startWorkLayerName;
      }


      get dataManagerSocketUrl() {
            return this._dataManagerSocketUrl;
      }

      get dataManagerSocketPort() {
            return this._dataManagerSocketPort;
      }

      set log(value) {
            this._log = value;
      }

      get log() {
            return this._log;
      }


      get locale() {
            return null;
      }


      get languages() {
            if (this._configInfo.hasOwnProperty("languages")) {
                  return this._configInfo.languages;
            }

            return [];
      }

      set userId(value) {
            this._userId = value;
      }

      get userId() {
            return wemb.userInfo.userId;
      }

      set hasAdminRole(value) {
            this._hasAdminRole = value;
      }

      get hasAdminRole() {
            return wemb.userInfo.hasAdminRole;
      }

      set maxActiveTime(value) {
            this._maxActiveTime = value;
      }

      get maxActiveTime() {
            return wemb.userInfo.maxActiveTime;
      }


      getConfigProperties(groupName: string): any {
            if (this._configInfo.hasOwnProperty(groupName) == false)
                  return null;


            return this._configInfo[groupName];
      }



      async setUserConfigInfo() {
            if (this._test) {
                  this._configInfo['maxActiveTime'] = 30;
                  return true;
            }


            try {
                  let payload = {
                        id: "configManageService.configInitInfo",
                        params: {}
                  };

                  let result = await api.post(this.serverUrl + "/config.do", payload);

                  this._startPageId = result.data["startPageId"];
                  return true;
            } catch (error) {
                  return false;
            }


      }


      _getParameterByName(name, url = null): any {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                  results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      /**
       * 2019-07-03 // add by jj
       * [REN-319] v2.1.0 사용자 초기 페이지 설정
       * @param id : initPage_id
       */
      setStartPageId(id){
            this._startPageId = id;
      }

      getStartPageId() {

            if(window.wemb.pageManager.hasPageInfoBy(this._startPageId)==true)
            {
                  return  this._startPageId;
            }


            if(window.wemb.pageManager.getPageLength()==0){
                  return null;
            }

            /*
            if(window.wemb.configManager.configMode == CONFIG_MODE.LOCAL)
                  return null;
            */

            // dev mode시에만 실행.
            return window.wemb.pageManager.getPageInfoAt(0).id;
      }



      /*
      viewer에서 자동으로 연장되게
      config.using = true인 경우에만 실행.
       */
      startSessionCheckingManager(){
            if(this._exeMode==EXE_MODE.VIEWER && this.configMode==CONFIG_MODE.LOCAL) {
                  let using = true;
                  let time=10;
                  try {
                        using = this._configInfo.viewer.session_checking.using;
                        time=this._configInfo.viewer.session_checking.duration;

                  }catch(error){
                        using= true;
                        time=10;
                  }


                  if(using){

                        SessionCheckingManager.getInstance().setTime(time);
                        SessionCheckingManager.getInstance().startChecking();
                  }
            }
      }

}
