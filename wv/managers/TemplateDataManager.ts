import Vue from "vue";

import templateApi from "../../../common/api/templateApi"
import {ObjectUtil} from "../../utils/Util"
import FileManager from "./FileManager";

export default class TemplateDataManager {

      public static _instance: TemplateDataManager;

      public _customData: Array<any> = [];
      public _customCategories: any = null;
      public _customItems: object = {};

      public _defaultData: Array<any> = [];
      public _defaultCategories: any = null;
      public _defaultItems: object = {};

      private _selectedItem: object = {};

      private _$bus: Vue;

      private _concatData: Array<any> = [];

      public locale_msg = Vue.$i18n.messages.wv;


      constructor() {
            this._$bus = new Vue();

      }


      public static getInstance() {
            if (TemplateDataManager._instance == null) {
                  TemplateDataManager._instance = new TemplateDataManager();
            }
            return TemplateDataManager._instance;
      }


      public startLoading(): Promise<boolean> {
            return new Promise((resolve, reject) => {
                  this.loadDefaultData().then(() => {
                        this.loadCustomData().then(() => {
                              resolve(true);
                              return true;
                        }, () => {
                              resolve(false);
                        })
                  })

            })
      }


      get $bus() {
            return this._$bus;
      }

      get $on() {
            return this._$bus.$on;
      }

      get $emit() {
            return this._$bus.$emit;
      }

      get selectedItem() {
            return this._selectedItem;
      }

      get concatData() {
            return this._defaultData.concat(this._customData);
      }

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////


      //Custom 데이터 로드
      async loadCustomData() {
            let result = await templateApi.getTemplateCustomData();
            let data = result || [];
            this.editCustomData(data);
      }

      //Custom Data에서 카테고리 정보
      public editCustomData(data) {

            //전체 데이터 캐싱
            this._customData = data;

            //카테고리 데이터 캐싱
            this._customCategories = data.filter((x) => x.type == "group");

            //아이템 데이터 캐싱
            this._customItems = {};

            for (let index in this._customCategories) {
                  let type = this._customCategories[index];
                  this._customItems[type.id] = data.filter(x => x.parent == type.id);
            }

            console.log("Total reloaded CUSTOM data", this._customData);

      }


      //Renobit 데이터 로드
      async loadDefaultData() {
            let result = await templateApi.getTemplateDefaultData();

            let data = result || [];

            this.editDefaultData(data);
      }

      //Custom Data에서 카테고리 정보
      public editDefaultData(data) {

            //전체 데이터 캐싱
            this._defaultData = data;

            //카테고리 데이터 캐싱
            this._defaultCategories = data.filter((x) => x.type == "group");

            //아이템 데이터 캐싱
            this._defaultItems = {};

            for (let index in this._defaultCategories) {
                  let type = this._defaultCategories[index];
                  this._defaultItems[type.id] = data.filter(x => x.parent == type.id);
            }

            console.log("Total reloaded DEFAULT data", this._defaultData);
      }


      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////


      //새로운 카테고리를 추가하는 로직
      //새로운 카테고리 이름을 스트링으로 받고
      public addNewCategory(newCategory) {

            //빈 객체를 하나 생성 한 후에
            let _newCategoryObject = {};

            //이름이 빈문자열이 아니면
            if (newCategory !== "") {

                  //빈 객체를 이용해 새로운 카테고리 객체를 만든다.
                  _newCategoryObject['id'] = ObjectUtil.generateID();
                  _newCategoryObject['text'] = newCategory;
                  _newCategoryObject['parent'] = "wv_root";
                  _newCategoryObject['type'] = "group";

                  //새로운 객체를 현재 전체 데이터 필드에 푸쉬해준다.
                  this._customData.push(_newCategoryObject);

                  //새로운 전체 데이터를 JSON으로 다시 쓴다.
                  this.rewriteJsonData(this._customData);

                  Vue.$message.success(this.locale_msg.templateManager.sucAddNewCate);
            }
      }

      //카테고리명 변경을 위한 로직
      //해당 객체 찾아서 text키 value를 변경해주고 JSON UPDATE
      public editCategoryName(id, name) {
            this._customData.find(x => x.id === id).text = name;
            this.rewriteJsonData(this._customData);
      }


      //카테고리 삭제를 위한 로직
      //Param 삭제할 카테고리 아이디
      public deleteCategory(categoryId) {
            let removeCategory = this._customData.find(x => x.id === categoryId);
            let removeCategoryIndex = this._customData.indexOf(removeCategory);

            //일단 해당 카테고리 부터 삭제
            this._customData.splice(removeCategoryIndex, 1);

            let removeItems = this._customData.filter(x => x.parent === categoryId);
            let removeItemIndex = null;
            for (var index in removeItems) {
                  removeItemIndex = this._customData.indexOf(removeItems[index]);
                  this._customData.splice(removeItemIndex, 1);
            }
            //카테고리 + 아이템들 삭제후 JSON다시 쓰기
            this.rewriteJsonData(this._customData);
            Vue.$message.success(this.locale_msg.templateManager.sucDelCate);
      }


      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////


      //새로운 아이템을 추가하는 로직
      public addNewItem(id) {
            let _newItemObject = {};

            _newItemObject['id'] = ObjectUtil.generateID();
            _newItemObject['type'] = "item";
            _newItemObject['htmlStr'] = "";
            _newItemObject['styleStr'] = "";
            _newItemObject['dataStr'] = "";
            _newItemObject['text'] = "New Item";
            _newItemObject['parent'] = id;

            //새로운 아이템을 만들고 _customData에 push한 다음
            this._customData.push(_newItemObject);

            //새로운 전체 데이터를 JSON으로 다시 쓴다.
            this.rewriteJsonData(this._customData);

            //새로운 아이템을 리턴해준다.
            return _newItemObject;
      }


      public itemSaveAs(item, parentId, templateName) {
            let _newItemObject = {};

            _newItemObject['id'] = ObjectUtil.generateID();
            _newItemObject['type'] = "item";
            _newItemObject['htmlStr'] = item.htmlStr;
            _newItemObject['styleStr'] = item.styleStr;
            _newItemObject['dataStr'] = item.dataStr;
            _newItemObject['text'] = templateName;
            _newItemObject['parent'] = parentId;

            this._customData.push(_newItemObject);

            this.rewriteJsonData(this._customData);

            return _newItemObject;
      }

      /////////////////////////////////////////////////////////////

      //데이터 변경 후 JSON 다시 쓰기
      public rewriteJsonData(data) {
            templateApi.setTemplateCustomData(data).then((result) => {
                  if (result) {
                        //JSON 데이터가 다시 써지고 나면
                        //모델 객체의 필드들을 다시 캐싱해준다.
                        this.loadCustomData();

                        //Model에서 jsonUpdate라는 이벤트를 emit한다.
                        this.$bus.$emit("template_manager_update", this.concatData);

                        console.log("Success JSON Update");
                  } else {
                        console.log("Failed JSON Update");
                  }
            });

      }

      ///////////////////////////////////////////////////////////////////

      //아이템 목록에서 선택한 아이템 데이터 찾기
      //Param 목록에서 선택한 아이템의 아이디
      public customSelectedItem(id) {
            //현재 데이터 중 param과 동일한 id를 가진 객체를 찾아서 리턴 or 빈객체
            return this._selectedItem = this._customData.find(x => x.id === id) || {};
      }


      public defaultSelectedItem(id) {
            //현재 데이터 중 param과 동일한 id를 가진 객체를 찾아서 리턴 or 빈객체
            return this._selectedItem = this._defaultData.find(x => x.id === id) || {};
      }


      //아이템 저장하는 로직
      //Param
      //Save버튼을 클릭하면서 데이터를 넘겨주거나
      //다른 아이템 선택하면서 기존에 수정중이던 데이터를 넘겨주거나
      public saveItem(data, templateName) {
            let savedData = this._customData.find(customData => customData.id === data.id);

            savedData.htmlStr = data.htmlStr;
            savedData.styleStr = data.styleStr;
            savedData.dataStr = data.dataStr;
            savedData.text = templateName;

            //새로운 전체 데이터를 JSON으로 다시 쓴다.
            this.rewriteJsonData(this._customData);

            Vue.$message.success(this.locale_msg.common.successSave);

            //저장한 데이터 리턴해주기
            return savedData;
      }

      public checkCssTemplate(data) {
           return data.htmlStr.split('<').filter(x => x.indexOf('>') !== -1 ).some(str => {
                  let checkStr = str.substring(0, str.indexOf('>'));
                  return checkStr.includes('class="$') || checkStr.includes('style="$')
            })
      }

      //아이템 삭제 로직
      //Param 삭제할 아이템 아이디
      public deleteItem(itemId) {

            //삭제할 아이템의 index를 찾고
            let removeIndex = this._customData.map(x => x.id).indexOf(itemId);

            let confirmAnswer = window.confirm(this.locale_msg.common.confirmDelete);

            if (confirmAnswer) {
                  //지운다.
                  this._customData.splice(removeIndex, 1);
                  Vue.$message.success(this.locale_msg.templateManager.sucDelTem);
            }

            this.rewriteJsonData(this._customData);
      }


      public getItemById(id) {
            return this._customData.find(x => {
                  if (x.id === id) {
                        return x
                  }
            });
      }


      public exportTemplateAsFile(jsonStr) {
            let fileManager = new FileManager();
            fileManager.saveTextFile("templateList.json", jsonStr);
            fileManager.onDestroy();
      }


      public importTemplateFromFile(file: any) {
            var locale_msg = Vue.$i18n.messages.wv;
            var reader = new FileReader();

            if (file.name.search(/.json$/) === -1) {
                  Vue.$message.error(locale_msg.common.errorFormat);
            } else {
                  reader.onloadend = (evt: any) => {
                        try {
                              let data = JSON.parse(evt.target.result).template;
                              let valid = this.getValidateTemplate(data);
                              if (valid.length) {
                                    this.mergeCustomTemplate(valid);
                              }
                              Vue.$message.success(locale_msg.common.successImport);
                        } catch (error) {
                              Vue.$message.error(this.locale_msg.common.failImport);
                        }
                  };

                  reader.readAsText(file);
            }
      }


      public getValidateTemplate(list) {
            if (Array.isArray(list)) {
                  return list.filter((x) => {
                        if (x.id && x.text && x.type && x.parent) {
                              return x;
                        }
                  })
            } else {
                  return [];
            }
      }

      public mergeCustomTemplate(list) {
            let locale_msg = Vue.$i18n.messages.wv;
            let isConfirmOverwrite: boolean = false;

            //기존 데이터에서 새로 업로드한 데이터에 없는 것들만  doubleCheckList로 할당
            let noDuplicatedList = this._customData.filter((x) => {
                  if (!list.find(y => y.id == x.id)) {
                        return x;
                  }
            })

            //겹치는 데이터가 있으면 overwrite 할지 묻고
            if (this._customData.length > noDuplicatedList.length) {
                  if (confirm(locale_msg.common.overwrite)) {
                        isConfirmOverwrite = true;
                  }
            }

            let mergeList;
            if (isConfirmOverwrite) {
                  mergeList = noDuplicatedList.concat(list);
            } else {
                  //중복된 데이터가 없는 경우에는
                  //업로드한 템플릿의  id와 기존 데이터의 id가 일치하지 않는 것만 찾아서
                  let _list = list.filter((x) => {
                        if (!this._customData.find(y => y.id == x.id)) {
                              return x
                        }
                  })

                  //기존 데이터에 더해준다.
                  mergeList = this._customData.concat(_list);
            }

            mergeList.forEach((x) => {
                        if (x.type === 'group') {
                              delete x['children']
                        }
                  }
            )

            this.rewriteJsonData(mergeList);
      }
}
