import Vue from "vue";

/**
 * singleton
 * 에디터에서 window 창이 별도로 열리는 Manager들이 메뉴를 두번, 세번 호출 했을 때, 창이 여러번 뜨지 않게 관리하는 객체,
 * 그밖에 기본 사이즈 설정 및 열려있는 창을 재 호출할 경우 focus시키는 기능을 포함한다.
 * window url이 필요하므로 router에 미리 등록되어 있어야한다. src/router/index.ts
 *
 * 관리객체
 * DATASET_MANAGER:
 * SCRIPT_EDITOR: 스크립트 관리자
 * MULTILINGUAL_MANAGER: 다국어 편집기
 *
 */
export const POPUP = {
    DATASET_MANAGER: {//데이터셋 관리자
        NAME: "dataset_manager",//관리하기 위한 key
        URL: "datasetMananger",//router path
        SIZE: "width=1100,height=600"//winow size
    },

    SCRIPT_EDITOR: {
        NAME: "script_editor",
        URL:  "scriptEditor",
        SIZE: "width=1200,height=650"
    },
    MULTILINGUAL_MANAGER: {
        NAME: "multilingual_manager",
        URL:  "multilingualEditor",
        SIZE: "width=1200,height=650"
    },
    CODE_COMPILER: {
          NAME: "codeCompiler",
          URL: "codeCompiler",
          SIZE: "width=1200,height=659"
    }
};

export default class WindowManager {
    private static _instance:WindowManager = null;
    private url: string = '';
    private popupWindows:any = {};
    constructor() {
        this.popupWindows = {}; //{"name": window}
        this.setUrl();
        let self = this;
        this.bindBeforeUnloadEvent( window, ()=>{
            self.closeAllPopup();
        });
    }

    public static getInstance():WindowManager {
          if(WindowManager._instance==null){
                WindowManager._instance = new WindowManager();
          }

          return WindowManager._instance;
    }

    /*window 창을 관리하기 위하여 창이 열리고 닫히는 시점을 정확히 알아야하는데, ie에서 창이 닫힐때 이벤트 처리 코드가 추가로 필요하여 작성되었다*/
    private bindBeforeUnloadEvent(win, callback ){
        let self = this;
        if(win == null) return;
        let myEvent = win.attachEvent || win.addEventListener;
        let chkevent = win.attachEvent ? 'onbeforeunload' : 'beforeunload'; /// make IE7, IE8 compitable
        myEvent(chkevent, function(e) { // For >=IE7, Chrome, Firefox
            callback();
        });
    }

    private setUrl(){
        let url = window.location.href;
        let urlIndex = url.indexOf("#/");
        if (urlIndex == -1) {
            url += "#/";
        } else {
            url = url.split("#/")[0] + "#/";
        }

        this.url = url;
    }

    public open(data:any) {
        if(data) {
            let popup = this.getOpendPopup(data.NAME);
            if (popup && popup.location && popup.location.href ) {
                popup.focus();
            } else {
                this.closedPopup(data.NAME);
                popup = window.open((data.ORIGIN ? location.origin : this.url)+data.URL, "_blank", "left=0,top=0,scrollbars=1,resizable=1,"+data.SIZE);
                //  popup = window.open("http://localhost:4200/ScriptEditor.html", "_blank", "left=0,top=0,scrollbars=1,resizable=1,"+data.SIZE);
                this.popupWindows[data.NAME] = popup;
                let self = this;
                this.bindBeforeUnloadEvent( popup, ()=>{

                      self.closedPopup(data.NAME);
                })
            }

            return popup;
        }
    }


    public closeAllPopup() {
        for (let data in this.popupWindows) {
            let _window = this.popupWindows[data];
            if (_window && _window.close) {
                _window.close();
            }
        }
    }

    public getOpendPopup(name:string) {
        let _window = this.popupWindows[name];
        return _window;
    }

    public closedPopup(name:string) {
        let popup = this.popupWindows[name];
        if (popup) {
            /*
			   2018.10.02(ckkim)
			   닫기전 closed 이벤트 발생
			    */
            $(popup).trigger("closed");
            delete this.popupWindows[name];
        }
    }
}
