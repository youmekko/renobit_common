class HookElement {
      public cmd:string="";
      public callback:Function=null;
      public params:any=null;
      public priority:number=1;

      public async execute(params=null){
            if(params){
                  return await this.callback(params);
            }


            if(this.params)
                  return await this.callback(this.params);
            else
                  return await this.callback();
      }

}
export default class HookManager {
      public static readonly HOOK_BEFORE_SAVE_PAGE:string = "hook/beforeSavePage";
      public static readonly HOOK_AFTER_SAVE_PAGE:string = "hook/afterSavePage";
      // 페이지 정보 로딩 전 호출
      public static readonly HOOK_BEFORE_LOAD_PAGE:string = "hook/beforeLoadPage";
      // 페이지
      public static readonly HOOK_AFTER_LOAD_PAGE:string = "hook/afterLoadPage";

      public static readonly HOOK_BEFORE_ACITVE_PAGE:string = "hook/beforeActivePage";
      public static readonly HOOK_AFTER_ACITVE_PAGE:string = "hook/afterActivePage";

      // 컴포넌트 인스턴스를 생성하기 전, 즉, 컴포넌트 생성 정보를 변경하고 싶을때 사용.
      public static readonly HOOK_BEFORE_ADD_COMPONENT_INSTANCE:string = "hook/before_AddComponentInstance";


      public static readonly  HOOK_BEFORE_PASTE_COMPONENT_INSTANCE:string="hoook/before_pasteComponentInstance";

      public static readonly HOOK_BEFORE_OPEN_PAGE:string = "hook/before_open_page";


      private static _instance:HookManager;
      /*
      2018.11.07(ckkim)

       */

      private _actionHookMap:Map<string, HookElement[]> = new Map();

      constructor(){

      }

      public static getInstance():HookManager{
            if(HookManager._instance==null){
                  HookManager._instance = new HookManager();
            }

            return HookManager._instance;
      }


      //////////////////////////////////////////////////
      /*
      2018.11.07(ckkim)
       */

      addAction(cmd:string, callback:Function, params:any=null, priority:number=1){
            if(callback==null)
                  return;

            //console.log("@@ action callback 3 ", callback instanceof Function);
            if(callback instanceof Function ==false){
                  return;
            }

            let hookList = null;
            if(this._actionHookMap.has(cmd)==false){
                  this._actionHookMap.set(cmd, []);
            }
            hookList = this._actionHookMap.get(cmd);




            let hookElement:HookElement = new HookElement();
            hookElement.cmd = cmd;
            hookElement.callback = callback;
            hookElement.params = params;
            hookElement.priority = priority;


            hookList.push(hookElement);
      }
      async executeAction(cmd:string, params:any=null){

            //console.log("action hook executeAction ", cmd);
            if(this._actionHookMap.has(cmd)==false){
                  return true;
            }


            let hookList = this._actionHookMap.get(cmd);
            if(hookList.length==0){
                  return true;
            }

            // 소팅
            let sortingHookList =  hookList.sort((a:HookElement,b:HookElement)=>{
                  return a.priority<b.priority
            })

            //console.log("@@ action hookList ", cmd, " length = ", hookList.length)

            for(let i=0;i<sortingHookList.length;i++){
                  let hookElement:HookElement = sortingHookList[i];
                  let result = await hookElement.execute(params);
                  if(result==false){
                        return false;
                  }
            }

            return true;

      }

}

window.HookManager = HookManager;
