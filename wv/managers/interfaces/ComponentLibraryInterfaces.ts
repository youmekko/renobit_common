
/*
파일 구조
{
    "list": [{
        "name": "SVGBrokenLineComponent",
        "file_name": "static/components/2D/SVGBrokenLineComponent/SVGBrokenLineComponent.js"
    }, {
        "name": "SVGCurveComponent",
        "file_name": "static/components/2D/SVGCurveComponent/SVGCurveComponent.js"
    }, {
        "name": "SVGCircleComponent",
        "file_name": "static/components/2D/SVGCircleComponent/SVGCircleComponent.js"
    }, {
        "name": "SVGLineComponent",
        "file_name": "static/components/2D/SVGLineComponent/SVGLineComponent.js"
    }, {
        "name": "SVGRectComponent",
        "file_name": "static/components/2D/SVGRectComponent/SVGRectComponent.js"
    }, {
        "name": "BasicGridComponent",
        "file_name": "static/components/2D/BasicGridComponent/BasicGridComponent.js",
        "style_name": "static/components/2D/BasicGridComponent/BasicGridComponent.css",
        "dependencies": {
            "libs": [{
                "file_name": "static/libs/jquery-fixed-header-table/jquery.CongelarFilaColumna.js",
                "mode": "both"
            }],
            "styles": [{
                "file_name": "static/libs/jquery-fixed-header-table/ScrollTabla.css",
                "mode": "both"
            }]
        }
    }]
}

 */

/////////////////////////////////////////////////////////////////////
// component_file_list.json에 있는 json,css 파일 정보 구조와 구조를 읽어 동적으로 추가하는 부분

/*
컴포넌트 라이브러리 구조

예)
{
  "name": "BasicGridComponent",
  "file_name": "static/components/2D/BasicGridComponent/BasicGridComponent.js",
  "style_name": "static/components/2D/BasicGridComponent/BasicGridComponent.css",
  "dependencies": {
      "libs": [{
          "file_name": "static/libs/jquery-fixed-header-table/jquery.CongelarFilaColumna.js",
          "mode": "both"
      }],
      "styles": [{
          "file_name": "static/libs/jquery-fixed-header-table/ScrollTabla.css",
          "mode": "both"
      }]
  }
}
 */

export type ComponentLibraryFileInfo = {
      name: string;
      instanceName: string;
      file_name: string;
      style_name: string;
      type:string;
      dependencies?: ComponentLibraryDependencies;
      loadingFileLength:number;
}

export type ComponentLibraryDependencies = {
      libs?: Array<ComponentLibraryDependenceFileInfo>;
      styles?: Array<ComponentLibraryDependenceFileInfo>;
      core?: Array<ComponentLibraryDependenceFileInfo>;
}


/*
라이브러리 파일 하나의 정보 구조
 */
export type ComponentLibraryDependenceFileInfo ={
      file_name: string;
      mode?: string;
}



export enum EXTENSION_TYPE {
      COMPONENT="component",
      PANEL="panel",
      TOOLBAR="toolbar",
      PLUGIN="plugin",
      MENU="menu"
}

