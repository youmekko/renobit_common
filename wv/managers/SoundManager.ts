 export class SoundManager {
    private static _instance = null;
    private _src = "";
    private _loop = true;
    private _enabled = true;
    constructor() {
    
    }

    public static getInstance():SoundManager {
        if (SoundManager._instance == null) {
            SoundManager._instance = new SoundManager();
        }

        return SoundManager._instance;
    }


    getAudioEl(){
        let $audio = $("#__renobit_audio__");
        if($audio.length){
            return $audio;
        }else{
            $("body").append('<div style="display:none" controls><audio id="__renobit_audio__"></audio></div>');
        }

        return $("#__renobit_audio__");
    }

    public set src(src){
        this._src = src;
        let $audio = this.getAudioEl();
        $audio.empty();
        src = src.replace(".ogg", "");
        src = src.replace(".mp3", "");
        let tag = `<source src="${src}.ogg" type="audio/ogg"> 
                   <source src="${src}.mp3" type="audio/mpeg">`;
        $audio.append(tag);
    }

    public get src(){
        return this._src;
    }

    public play(){
        if(!this._enabled){return;}
        this.getAudioEl().get(0).play(); 
    }

    public pause(){
        if(!this._enabled){return;}
        this.getAudioEl().get(0).pause(); 
    }

    public set loop( bvalue ){
        this.getAudioEl().get(0).loop = bvalue;
    }

    public get loop(){
        return this.getAudioEl().get(0).loop;
    }

    public enable(){
        this._enabled = true;
    }

    public disable(){
        this._enabled = false;
    }
 }
