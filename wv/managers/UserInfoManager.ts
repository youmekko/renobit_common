import api = http.api;
import {http} from "../../http/Http";

export default class UserInfoManager {
      private static _instance: UserInfoManager = null;
      private _userId:string;
      private _hasAdminRole:boolean = false;
      private _maxActiveTime: number = 1800;
      private _clientIp:string;
      private _loginHist:Array<any>;
      private _accessiblePages:Array<any>;
      private _grades:Array<any>;

      set userId(value) {
            this._userId = value;
      }

      get userId() {
            return this._userId;
      }

      set hasAdminRole(value) {
            this._hasAdminRole = value;
      }

      get hasAdminRole() {
            return this._hasAdminRole;
      }

      set maxActiveTime(value) {
            this._maxActiveTime = value;
      }

      get maxActiveTime() {
            return this._maxActiveTime;
      }

      set clientIp(value) {
            this._clientIp = value;
      }

      get clientIp() {
            return this._clientIp;
      }

      set loginHist(value) {
            this._loginHist = value;
      }

      get loginHist() {
            return this._loginHist;
      }

      set accessiblePages(value) {
            this._accessiblePages = value;
      }

      get accessiblePages() {
            return this._accessiblePages;
      }

      get grades() {
            return this._grades;
      }

      set grades(value) {
            this._grades = value;
      }

      public static getInstance(): UserInfoManager {
            if (UserInfoManager._instance == null) {
                  UserInfoManager._instance = new UserInfoManager();
            }

            return UserInfoManager._instance;
      }

      async setUserConfigInfo() {
            try {
                  let payload = {
                        id: "configManageService.configInitInfo",
                        params: {}
                  };

                  let result = await api.post(wemb.configManager.serverUrl + "/config.do", payload);

                  this.userId = result.data["userId"];
                  this.hasAdminRole = result.data["hasAdminRole"];
                  this.maxActiveTime = result.data["maxActiveTime"];
                  this.clientIp = result.data["clientIp"];
                  this.loginHist = result.data["loginHist"];
                  this.accessiblePages = result.data["accessiblePages"];
                  this.grades = result.data["grades"];
                  wemb.configManager.startPageId = wemb.configManager.startPageId ? wemb.configManager.startPageId : result.data["startPageId"];
                  return true;
            } catch (error) {
                  return false;
            }
      }


}
