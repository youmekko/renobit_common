import Vue from "vue";

export default class FileManager {

      private _bIng: boolean = false;
      private _fileName: string = "";
      private _data: any = null;

      constructor() {

      }

      saveTextFile(fileName: string, data: any) {
            if (this._bIng == true) {
                  alert(Vue.$i18n.messages.wv.common.processing);
                  return;
            }

            if (window.navigator.msSaveBlob) {
                  let textFileAsBlob = new Blob([data], {type: 'text/plain'});
                  window.navigator.msSaveBlob(textFileAsBlob, fileName);

            } else {
                  let textFileAsBlob = new Blob([data], {type: 'text/plain'});
                  let downloadLink = document.createElement("a");
                  downloadLink.download = fileName;
                  downloadLink.innerHTML = "Download File";

                  downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
                  downloadLink.onclick = (event: any) => {
                        document.body.removeChild(event.target);
                        Vue.$message(Vue.$i18n.messages.wv.fileManager.successExport);
                  };

                  downloadLink.style.display = "none";
                  document.body.appendChild(downloadLink);
                  downloadLink.click();
            }
      }


      loadTextFile(callback: Function) {
            let $importFile: any = $("#importFile");

            if ($importFile.length == 0) {
                  callback(false, Vue.$i18n.messages.wv.fileManager.noInputTag);
                  return;
            }


            $importFile.attr("accept", ".json, json/*");
            // 기존 저장된 이벤트 제거
            $importFile.off();
            $("#importFile").val("");

            // 생성되는 경우
            $importFile.on("change", (event) => {
                  if ($importFile[0].files.length == 0) {
                        callback(false, Vue.$i18n.messages.wv.fileManager.noFile);
                        return;
                  }

                  var fileToLoad: any = $importFile[0].files[0];

                  var fileReader: FileReader = new FileReader();

                  fileReader.onload = function (fileLoadedEvent: any) {
                        var textFromFileLoaded = fileLoadedEvent.target.result;
                        callback(true, textFromFileLoaded);
                  };

                  fileReader.onerror = function () {
                        callback(false, Vue.$i18n.messages.wv.fileManager.occurError);
                  };

                  fileReader.readAsText(fileToLoad, "UTF-8");
            });

            $importFile.trigger("click");
      }

      loadJsonFile(callback: Function) {
            let $importFile: any = $("#importFile");

            if ($importFile.length == 0) {
                  callback(false, Vue.$i18n.messages.wv.fileManager.noInputTag);
                  return;
            }


            $importFile.attr("accept", ".json, json/*");
            // 기존 저장된 이벤트 제거
            $importFile.off();
            $("#importFile").val("");

            // 생성되는 경우
            $importFile.on("change", (event) => {
                  if ($importFile[0].files.length == 0) {
                        callback(false, Vue.$i18n.messages.wv.fileManager.noFile);
                        return;
                  }

                  if ( Array.from($importFile[0].files).some(file => {
                        return file.name.search(/.json$/) === -1 })
                  ) {
                        Vue.$message.error(Vue.$i18n.messages.wv.common.errorFormat);
                        return;
                  }

                  var fileToLoad: any = $importFile[0].files[0];

                  var fileReader: FileReader = new FileReader();

                  fileReader.onload = function (fileLoadedEvent: any) {
                        var textFromFileLoaded = fileLoadedEvent.target.result;
                        callback(true, textFromFileLoaded);
                  };

                  fileReader.onerror = function () {
                        callback(false, Vue.$i18n.messages.wv.fileManager.occurError);
                  };

                  fileReader.readAsText(fileToLoad, "UTF-8");
            });

            $importFile.trigger("click");
      }

      loadZipFile(callback: Function) {
            let $importFile: any = $("#importFile");

            if ($importFile.length == 0) {
                  callback(false, Vue.$i18n.messages.wv.fileManager.noInputTag);
                  return;
            }

            $importFile.attr("accept", ".zip, zip/*");
            $importFile.val("");

            $importFile.off().on("change", (event) => {
                  if ( Array.from($importFile[0].files).some(file => {
                        return file.name.search(/.zip$/) === -1 })
                  ) {
                        Vue.$message.error(Vue.$i18n.messages.wv.common.errorFormat);
                        return;
                  }

                  Vue.$loading({});
                  let formData = new FormData();
                  formData.append("uploadType", "importRes");
                  formData.append("file", event.target.files[0]);

                  let url = window.wemb.configManager.serverUrl + "/fileUpload";
                  window.wemb.$http.post(url, formData).then(result => {
                        if (typeof result.data.error === "boolean" && result.data.error) {
                              Vue.$message.error(Vue.$i18n.messages.wv.fileManager.errorResourceUpload);
                        } else if (result.data.length) {
                              Vue.$message.success(Vue.$i18n.messages.wv.fileManager.successResourceAdd);
                        }

                        Vue.$loading({}).close();

                        Vue.nextTick(() => {
                              $(".el-loading-mask.is-fullscreen").remove();
                        });
                  }, (error) => {
                        Vue.$loading({}).close();
                        Vue.$message.error(Vue.$i18n.messages.wv.fileManager.errorResourceUpload);
                        console.log("loadZipFile ERROR", error);

                        Vue.nextTick(() => {
                              $(".el-loading-mask.is-fullscreen").remove();
                        });
                  });

            });

            $importFile.trigger("click");
      }

      onDestroy() {

      }
}
