import mainMenuApi from "../../../common/api/mainMenuApi";

export default class MainMenuManager {

	private _menuItemList:Array<object>;
	constructor(){
		this._menuItemList = [];
	}
	reset(){
		this._menuItemList = [];
	}

	getMenuItemList(){
		return this._menuItemList;
	}

	startLoading():Promise<boolean>{
		return mainMenuApi.loadFile().then((list:Array<object>)=>{

                  // 3D 메뉴 활성화 처리
		      this.attach3DMenu(list);
			this._menuItemList = list;
			return true;

		})
	}

	/*
	2018.11.04(ckkim)
	3D 메뉴 활성화 처리
	 */
	private attach3DMenu(list:any[]) {

            try {

                  let editMenuItem = list.find((item) => {
                        return item.key == "edit"
                  })
                  let editModeItem = editMenuItem.children.find((subItem) => {
                        return subItem.key == "editMode"
                  })

                  let threeModeItem = editModeItem.children.find((modeItem) => {
                        return modeItem.key == "3dMode"
                  })
                  threeModeItem.show = wemb.configManager.using3D;

                  let editAreaItem = editMenuItem.children.find((subItem) => {
                        return subItem.key == "showEditArea"
                  })

                  let threeLayerItem = editAreaItem.children.find((modeItem) => {
                        return modeItem.key == "3dLayer"
                  })
                  threeLayerItem.show = wemb.configManager.using3D;
            }catch (error) {
                  console.log("@@ error ", error);
            }

      }
}
