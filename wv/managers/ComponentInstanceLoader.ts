import ComponentInstanceFactory from "./ComponentInstanceFactory";
import {IProgressbar} from "../components/LoadingProgressbar";
import {event} from "../events/LoadingEvent";
import LoadingEvent = event.LoadingEvent;
import WVComponent from "../../core/component/WVComponent";
import {EXE_MODE} from "../data/Data";
import {IThreeExtensionElements} from "../../core/component/interfaces/ComponentInterfaces";
import LoadingProgressbar from "../components/LoadingProgressbar";

/*

ComponentInstanceLoader를 사용하는 곳에서
컴포넌트 인스턴스 생성 후
proxy 또는 PageComponent로 생성한 한 인스턴스를 추가할때
사용하는 인터페이스

주로, 에디터 proxy, 뷰어 proxy, PageComponent에서 사용하게 됨.
 */
export interface IComponentInstanceLoaderContainer {
      // 인스턴스 생성 후 컴포넌트가 로드 될때
      addComInstance(comInstance:WVComponent):boolean;
      // 모든 컴포넌트가 로드 된 후
      loadCompleted():void;

      getThreeExtensionElements():IThreeExtensionElements;

}



export default class ComponentInstanceLoader{


      constructor() {

      }
      /*
      주의!!!!

      컴포넌트를 생성하고 인스턴스만 화면에 추가만 함
      이때 리소스 등이 로드되지 않은 시점임.
       */
      public static async startInstanceLoader(comInstanceInfoList:Array<any>, progressBar:IProgressbar, loaderPageContainer:IComponentInstanceLoaderContainer, exeMode:EXE_MODE=EXE_MODE.EDITOR):Promise<boolean>{



            window.perf_time = performance.now();
            console.time('ComponentInstanceLoad')
            return new Promise<boolean>((resolve,reject)=>{


                  /*
                        window.wemb.componentLibraryManager : 라이브러리 관리자
                        loaderPageContainer: 컴포넌트 인스턴스 생성 후 인스턴스를 추가할 레이어(container),
                        exeMode : 실행 모드.
                   */
                  let comInstanceFactory = new ComponentInstanceFactory(window.wemb.componentLibraryManager, loaderPageContainer, exeMode);
                  // 레이어별 컴포넌트 생성
                  progressBar.setMaxLength(comInstanceInfoList.length);
                  //progressBar.show("");
                  comInstanceFactory.$on(LoadingEvent.LOAD_START, (event)=>{
                        //console.log("\t\t cominstance 생성 시작 ", LoadingEvent.LOAD_START);
                        progressBar.addMessage("\t\t   -라이브러리 로딩 시작", false);
                        progressBar.addMessage(`    -라이브러리 로딩 개수  ${event.data.loadLength}`, false);
                  })


                  comInstanceFactory.$on(LoadingEvent.CREATE_COMPONENT, (event:LoadingEvent)=>{
                        //console.log("\t\tcominstance 생성 컴포넌트 뷰에 추가 시작");
                        // comInstance가 null일 경우도 있기 때문에 반드시 확인해야함.
                        if(event.data.comInstance) {
                              // 컴포넌트 하나가 로드될때마다 editroxy로 보내기
                              loaderPageContainer.addComInstance(event.data.comInstance);
                        }
                  })


                  comInstanceFactory.$on(LoadingEvent.PROGRESS, (event:LoadingEvent)=>{
                        //console.log("\t\tcominstance 생성 완료", event.data.currentIndex);

                        progressBar.setProgress(event.data.currentIndex);
                        if(event.data.comInstanceInfo)
                              progressBar.addMessage(`    -${event.data.currentIndex}. ${event.data.comInstanceInfo.name} 로딩 완료`, false);
                        else
                              progressBar.addMessage(`    -${event.data.currentIndex}  로딩 완료`, false);
                  })

                  comInstanceFactory.$on(LoadingEvent.COMPLETED, (event)=>{
                        console.timeEnd('ComponentInstanceLoad');
                        //console.log("페이지의 모든 컴포넌트 인스턴스 생성 완료 ");
                        //console.log("#############################################\n#############################################\n\n\n\n\n");
                        progressBar.completed();
                        progressBar.addMessage("컴포넌트 라이브러리 로딩 완료 ",false);

                        if(progressBar)
                              progressBar.hide();

                        // 이벤트 처리가 가능하도록 레이어 활성화
                        // 에디터 모드에서는 비활성화 처리
                        if(loaderPageContainer)
                              loaderPageContainer.loadCompleted();

                        resolve(true);

                        /*
                        삭제 예정
                        setTimeout(()=>{

                              /!*
                               참고 : progressBar = LoadingProgressbar 동일

                               *!/
                              if(progressBar)
                                    progressBar.hide();

                              // 이벤트 처리가 가능하도록 레이어 활성화
                              // 에디터 모드에서는 비활성화 처리
                              if(loaderPageContainer)
                                    loaderPageContainer.loadCompleted();
                              resolve(true);
                        },1000)*/
                  })

                  //console.log("#############################################");
                  //console.log("\t\t 인스턴스 생성 실행");
                  comInstanceFactory.startFactory(comInstanceInfoList);

            });
      }

}

/*
2018.11.04(ckkim)
viewer,editor main page component에서는
ComponentInstanceLoader를 사용

PageComponent에서는
ComponentInstanceLoader2를 사용.

ComponentInstanceLoader2와 ComponentInstanceLoader와의 차이점
ComponentInstanceLoader2에는
      - ajax을 통한 페이지 정보 읽기 취소
      - resource 로딩 읽기 취소 등의 기능이 추가되어 있음.

 */
export class ComponentInstanceLoader2{


      private _comInstanceFactory:ComponentInstanceFactory;
      private _completedTimerID = 0;
      constructor() {
            this._comInstanceFactory = null;
      }

      public destroy(){
            this.stopClearInstanceLoader();


      }

      public stopClearInstanceLoader(){
            if(this._comInstanceFactory){
                  this._comInstanceFactory.stopAndClearFactory();
                  this._comInstanceFactory = null;
            }

            if(this._completedTimerID){
                  clearInterval(this._completedTimerID);
                  this._completedTimerID = 0;
            }
      }
      /*
      주의!!!!

      컴포넌트를 생성하고 인스턴스만 화면에 추가만 함
      이때 리소스 등이 로드되지 않은 시점임.
       */
      public async startInstanceLoader(comInstanceInfoList:Array<any>, progressBar:IProgressbar, loaderPageContainer:IComponentInstanceLoaderContainer, exeMode:EXE_MODE=EXE_MODE.EDITOR):Promise<boolean>{



            return new Promise<boolean>((resolve,reject)=>{


                  this.stopClearInstanceLoader();
                  /*
                        window.wemb.componentLibraryManager : 라이브러리 관리자
                        loaderPageContainer: 컴포넌트 인스턴스 생성 후 인스턴스를 추가할 레이어(container),
                        exeMode : 실행 모드.
                   */
                  this._comInstanceFactory = new ComponentInstanceFactory(window.wemb.componentLibraryManager, loaderPageContainer, exeMode);
                  // 레이어별 컴포넌트 생성
                  progressBar.setMaxLength(comInstanceInfoList.length);
                  progressBar.show("");
                  this._comInstanceFactory.$on(LoadingEvent.LOAD_START, (event)=>{
                        //console.log("\t\t cominstance 생성 시작 ", LoadingEvent.LOAD_START);
                        progressBar.addMessage("\t\t   -1라이브러리 로딩 시작", false);
                        progressBar.addMessage(`    -11라이브러리 로딩 개수  ${event.data.loadLength}`, false);
                  })


                  this._comInstanceFactory.$on(LoadingEvent.CREATE_COMPONENT, (event:LoadingEvent)=>{
                        //console.log("\t\tcominstance 생성 컴포넌트 뷰에 추가 시작");

                        // comInstance가 null일 경우도 있기 때문에 반드시 확인해야함.
                        if(event.data.comInstance) {
                              // 컴포넌트 하나가 로드될때마다 editroxy로 보내기
                              loaderPageContainer.addComInstance(event.data.comInstance);
                        }
                  })


                  this._comInstanceFactory.$on(LoadingEvent.PROGRESS, (event:LoadingEvent)=>{
                        //console.log("\t\tcominstance 생성 완료", event.data.currentIndex, event.data.comInstance);

                        progressBar.setProgress(event.data.currentIndex);
                        if(event.data.comInstanceInfo)
                              progressBar.addMessage(`    -${event.data.currentIndex}. ${event.data.comInstanceInfo.name} 로딩 완료`, false);
                        else
                              progressBar.addMessage(`    -${event.data.currentIndex}  로딩 완료`, false);
                  })

                  this._comInstanceFactory.$on(LoadingEvent.COMPLETED, (event)=>{
                        //console.log("페이지의 모든 컴포넌트 인스턴스 생성 완료 ");
                        //console.log("#############################################\n#############################################\n\n\n\n\n");
                        progressBar.completed();
                        progressBar.addMessage("컴포넌트 라이브러리 로딩 완료 ",false);
                        progressBar.hide();
                        loaderPageContainer && loaderPageContainer.loadCompleted();
                        resolve(true);
                  });

                  //console.log("#############################################");
                  //console.log("\t\t 인스턴스 생성 실행");
                  this._comInstanceFactory.startFactory(comInstanceInfoList);

            });
      }
}
