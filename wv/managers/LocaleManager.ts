import multilingualApi from "../../../common/api/multilingualApi";
import Vue from 'vue'
import ConfigManager from "./ConfigManager";

import {http} from "../../../wemb/http/Http";
import api = http.api;
import { WebGLShadowMap } from "three";

export default class LocaleManager {
    public static readonly prefix:string = "__locale__.";
	private static _instance:LocaleManager = null;
    private localeItems:any = {};

    constructor(){

      }

    public static getInstance():LocaleManager{
        if( LocaleManager._instance == null ){
            LocaleManager._instance = new LocaleManager();
        }

        return LocaleManager._instance;
    }


    public async load(){
          let result = null;
          if(ConfigManager.getInstance().serverType=="java") {
                var packs = await multilingualApi.getPackageInfo();
                ConfigManager.getInstance().pack_info = packs.packNames;
                console.log('pack info write', ConfigManager.getInstance().pack_info);
                var pack_locales = await multilingualApi.getPackLanguageData(this.locale, packs);
                result = await multilingualApi.getLanguageData(this.locale, pack_locales);
          }else {
                result = await multilingualApi.getLanguageDataToNode(this.locale);
          }

        //  let result = await multilingualApi.getLanguageData(this.locale);
        Vue.$i18n.setLocaleMessage('wv', result);


        let result2;
        if( ConfigManager.getInstance().userMultilingualPath ){
            result2 = await multilingualApi.getUserMultilingualData()
        }else{
            result2 = await multilingualApi.getMultilingualData()
        }

        this.localeItems = result2 || {};

        return true;
    }

      public set locale(key:string){
            if(ConfigManager.getInstance().languages.find(x=>x.locale == key || x.user_locale == key)){
                  localStorage.setItem("__renobit_locale__", key);
                  location.reload();
            }
      }

    public get locale(){
        return localStorage.getItem("__renobit_locale__") || '';
    }

    public translate( key:string, locale:string="" ){
        if( !key ){return "";}
        let str = "";
        let item = this.localeItems[key];

        if( item ){
            if( !locale ){
                str = item[this.locale] || '';
            }else{
                str = item[locale] || '';
            }
        }

        return str;
    }

    public translatePrefixStr(prefixStr){
        if( !prefixStr ){return '';}
        prefixStr = prefixStr.toString();
        let result = prefixStr;
        if( prefixStr.indexOf( LocaleManager.prefix ) === 0 ){
            let key = prefixStr.replace( LocaleManager.prefix, "");
            let value = this.translate(key)
            if( value ){
                result = value;
            }
        }
        return result;
    }



      /*
	localStorage의 __renobit_locale__ 키 적용하기

	1. 기본값 설정하기

	2. 서버에 적용되어 있는  locale 값 구하기

	3. localStorage __renobit_locale__ 키 값 적용하기

		 경우01: 파라메터에 lang가 존재하는 경우
		 경우02: 기존 설정된 __renobit_locale__키 값 적용하기
		 경우03: serverLocale 적용하기
	  */
      public async updateLocaleToLocalStorage() {

            /*
            2018.09.26(ckkim(
            - 읽어들인 페이지 정보를 localStorage에 저장하기
            - 이를 위해서 초기 시작시에 저장한 정보를 모두 지우기
            */
            var locale_cache = localStorage.getItem('__renobit_locale__');
            if(window.localStorage) localStorage.clear();
            localStorage.setItem('__renobit_locale__', locale_cache);


            let config = ConfigManager.getInstance().configInfo;


            // 1. 기본값 설정하기
            let serverLocale = "ko-KR";


            // 2. 서버에 적용되어 있는  locale 값 구하기
            if (ConfigManager.getInstance().test) {
                  serverLocale = 'ko-KR';
            }else {
                  try {

                        let payload = {
                              id: "adminService.getPolicyInfo",
                              params: {}
                        };

                        let result = await api.post(ConfigManager.getInstance().serverUrl + "/admin.do", payload);

                        /*
                        result의 LOCALE 정보
                        {
                          "config_id": "LOCALE",
                          "name": "Locale Setting",
                          "value": "ko-KR",
                          "description": "DefaultLanguage Setting",
                          "seq": 10
                        }
                         */
                        var locale_config = result.data.data.find(function (d) {
                              return d.config_id == "LOCALE"
                        });

                        /*
                        경우에 따라 ko_KR식으로 로케일 정보가 존재하게 됨
                        이때 -가 아닌 _가 존재하는 경우 -으로 수정
                         */
                        serverLocale = locale_config ? locale_config.value.replace("_", "-") : 'ko-KR';

                  } catch (error) {
                        console.warn(error);
                        return false;
                  }
            }



            //3. localStorage __renobit_locale__ 키 값 적용하기

            /*
                  파라메터로 넘어온 lang==locale정보 구하기
                  locale, user_locale 중 하나사용
                        "locale": "ko-KR",
                        "user_locale": "KOR"

            */
            var regex = new RegExp('lang=([^&]*)');
            var match_result = location.href.match(regex);
            var lang = match_result !== null ? match_result[1] : null;

            /*
            경우01: 파라메터에 lang가 존재하는 경우
             */
            if (lang) {

                  var valid_locale = config.languages.find(function (d) {
                        return (d.user_locale === lang || d.locale === lang)
                  });
                  if (valid_locale) {
                        localStorage.setItem("__renobit_locale__", valid_locale.locale);
                  }else {
                        localStorage.setItem("__renobit_locale__", serverLocale);
                  }
            } else {
                  //경우02: 기존 설정된 __renobit_locale__키 값 적용하기

                  var temp_locale = localStorage.getItem('__renobit_locale__');
                  if (config.languages.find(function (d) {
                        return (d.locale === temp_locale)
                  })) {
                        localStorage.setItem("__renobit_locale__", temp_locale)
                  } else {

                        //경우03. 파라메터 lang X, lcalstore lang X 가 없는 경우 confif.ocal정보 사용하기
                        localStorage.setItem("__renobit_locale__", serverLocale);
                        console.log("ex 03. locale");
                  }
            }
            return true;
      }

}
