
import Vue from "vue";
import { http } from "../../http/Http";
import api = http.api;
import EventDispatcher from "../../core/events/EventDispatcher";
import { ObjectUtil } from "../../utils/Util";
import { dto } from "../../../editor/windows/datasetManager/model/dto/DatasetDTO";
import DatasetItemDTO = dto.DatasetItemDTO;
import DatasetInstDTO = dto.DatasetInstDTO;
import { Dataset } from "../../../editor/windows/datasetManager/const/Dataset";
import DatasetEvent from "../../core/events/DatasetEvent";
import datasetApi from "../../../common/api/datasetApi";




/**
 * DataService - Singleton
 * WScript 접근시 페이지 프로퍼티로 접근하는데 실제 프로퍼티에 등록하는 객체는 InstanceManagerOnPageViewer이다.
 * 데이터셋의 정보를 이용해 폴링 데이터를 처리하는 클래스(Push는 아직 지원하지 않음) * 
 * PageId별로 Dataset을 호출하는 worker들을 관리함 
 * Dataset Type = Query, TIM, REST API
 *  - Query, TIM은 datast.do를 통해 호출
 *  - REST  API는 jQuery ajax를 통해 호출
 * 사용자가 Dataset 호출시 두번째 파라메터 인자로 override할 수 있는 항목
 * {param:{데이터셋 파라메터}, delay: 첫 호출시 delay타임(초), interval: polling 주기(초)}
 */


export default class DataService {
    private static _instance = null;
    private workersByPageId: any = {};//pageId를 key값으로 worker들을 배열로 관리함
    private datasetList: Array<DatasetItemDTO> = [];//전체 데이터셋 리스트
    //private socket = Vue.prototype.$socket;

    constructor(){
        setTimeout(()=>{
            datasetApi.getDatasetList('').then((result)=>{
                this.datasetList = result.data;
            })
        }, 1000 );
    }

    public initialize(){
        return new Promise((resolve,reject) => {
            datasetApi.getDatasetList('').then((result)=>{
                  this.datasetList = result.data;
                  resolve();
            }).catch((err) => {
                  reject(err);
            })
        });
    }
    public static getInstance(){
        if( DataService._instance == null ){
            DataService._instance = new DataService();
        }

        return DataService._instance;
    }

    /*pagid를 인자로 받아 해당 페이지에서 생성된 worker리스트를 반환*/    
    private getWorkerListByPageId( id:string ):Array<DatasetWorker>{
        if( !this.workersByPageId[id] ){
            this.workersByPageId[id] = [];
        }

        return  this.workersByPageId[id];
    }

    /*private setSocket(){
        if (Vue.prototype.$socket) {
            this.socket.onmessage = (messageEvent) => {}
        }
    }*/

    public initInfo( datasetList: Array<DatasetItemDTO> ){
        this.datasetList = datasetList;
    }

    private getDatasetInstById(id:string, param:any={}):DatasetInstDTO{
        //데이터셋 파라미터 정보를 변경
        let findItem:DatasetItemDTO = this.datasetList.find(x=>x.dataset_id == id);
        let datasetInst:DatasetInstDTO;
        if(findItem){
           datasetInst = this.getDatasetInstByDataset(findItem, param);
        }

        return datasetInst;
    }

    private getDatasetInstByName(name:string, param: any={}):DatasetInstDTO{
        let findItem:DatasetItemDTO = this.datasetList.find(x=>x.name == name);
        let datasetInst:DatasetInstDTO;
        if(findItem){
           datasetInst = this.getDatasetInstByDataset(findItem, param);
        }

        return datasetInst;
    }

    private getDatasetInstByDataset(dataset: DatasetItemDTO, param:any={}){
        let datasetInst  = new DatasetInstDTO();
        datasetInst.id = dataset.dataset_id;
        datasetInst.interval = parseInt(dataset.interval);
        datasetInst.data_type = dataset.data_type;
        datasetInst.delivery_type = dataset.delivery_type;
        datasetInst.query = dataset.query;
        datasetInst.rest_api = dataset.rest_api;
        datasetInst.tim = dataset.tim;
        datasetInst.param = this.getParamData( dataset, param );
        datasetInst.query_type = dataset.query_type;

        if (dataset.datasource) {
              datasetInst.datasource =  dataset.datasource;
              datasetInst.dbType =  dataset.db_type;
        }
        return datasetInst;
    }

    /*TODO 동적으로 데이터셋 정보를 만들어서 호출하고자 할 때 사용. 현재 사용하지는 않음*/
    public execute(pageId: string, dataset:any):DatasetWorker{
       let datasetInst:DatasetInstDTO;
       //TOD param 처리...
       if( dataset && typeof dataset == "object"){
            datasetInst = DatasetValidator.convertToDataset(dataset);

            if( datasetInst.data_type == Dataset.DATA_TYPE.SQL ){
                //등록된 데이터셋은 기본값이 있기 때문에 동적 생성 데이터셋에서만 파람 체크를 함
                if( datasetInst.query && !DatasetValidator.syncParam( datasetInst.query, datasetInst.param ) ){
                    console.warn("query정보와 param정보가 일치하지 않습니다.");
                    return new DatasetWorker(null);
                }
            }
        }

        return this.createWorker(pageId, datasetInst);
    }

    private createWorker( pageId: string, datasetInst:DatasetInstDTO ):DatasetWorker{

        let workersList = this.getWorkerListByPageId( pageId );
        let worker = null;
        //console.log("@@ createWorker", dataset, DatasetValidator.validation(dataset))
        if( datasetInst && DatasetValidator.validation(datasetInst)){
            worker = new DatasetWorker( datasetInst );
            workersList.push( worker );//유효한  워커만 pull에서 관리
        }else{
            worker = new DatasetWorker(null);
        }

        return worker;
    }

    /*데이터셋 parameter를 override 정보까지 적용하여 반환하는 메소드*/
    private getParamData(dataset:DatasetItemDTO, param:any={}){
        let paramData = {};
        if(dataset.data_type != Dataset.DATA_TYPE.REST ){
            if( dataset && dataset.param_info){
                for( let i:number=0; i<dataset.param_info.length; i++ ){
                    let info = dataset.param_info[i];
                    paramData[info.param_name] = param[info.param_name] || info.default_value;
                }
            }
        }else{
            let restApi = JSON.parse(dataset.rest_api);
            if( restApi.data || param ){
                let _data = $.extend(true, null, restApi.data);
                paramData = $.extend(false, _data, param); 
            }            
        }

        return paramData;
    }

    /*데이터셋을 데이터셋 이름으로 호출, 실제로는 DelegateDataService객체에서 호출(pageid를 자동으로 넣어줌)*/
    public call(pageId: string, datasetName:string, param:any={}):DatasetWorker{
        let datasetInst:DatasetInstDTO = this.getDatasetInstByName(datasetName, param.param);
        return this.callRefDataset(pageId, datasetInst, param);
    }

    /*데이터셋을 데이터셋 id로 호출, 실제로는 DelegateDataService객체에서 호출(pageid를 자동으로 넣어줌)*/
    public callById(pageId: string, id:string, param:any={}):DatasetWorker{
        let datasetInst:DatasetInstDTO = this.getDatasetInstById(id, param.param);
        return this.callRefDataset(pageId, datasetInst, param);
    }

    private callRefDataset(pageId: string, datasetInst:DatasetInstDTO, param:any={}):DatasetWorker{
        if( datasetInst ){
            datasetInst.interval = param.interval || datasetInst.interval;
            datasetInst.delay = param.delay || datasetInst.delay;
        }
        return this.createWorker(pageId, datasetInst);
    }

    /*인자로 전달받은 pageId의 worker clear처리*/
    public clear( pageId: string, param:any ){
        let workers = this.getWorkerListByPageId(pageId);
        let findItem = null;
        if( typeof param == "string" ){
            ///clear by id
            findItem = workers.find(x=>x.info.id == param);
        }else{
            findItem = workers.find(x=>x==param);
        }

        if( findItem ){
            findItem.clear();
            let index = workers.indexOf( findItem );

            workers.splice( index, 1 );
        }else{
            console.warn("DatasetWorker 객체를 찾을 수 없습니다.");
        }
    }

    /*인자로 전달받은 pageId의 전체 worker clear 처리*/
    public clearAllPageWorkers(pageId:string){
        let workers = this.getWorkerListByPageId(pageId);
        for(let i=0, len=workers.length; i<len; i++){
            let item: DatasetWorker = workers.shift();
            item.clear();
        }
    }

    /*인자로 전달받은 pageId 실행중인 worker 개수 반환*/
    public getPageServiceCount(pageId:string):Number{
        let workers = this.getWorkerListByPageId(pageId);
        return workers.length;
    }
}

/**
 * 데이터셋 유효성 체크를 해주는 클래스
 */
export class DatasetValidator{
    constructor(){}

    /*TODO 상용자 데이터셋 동적 생성 호출시 유효성 체크하기 위해 사용하는 메소드 현재 사용하지 않음*/
    public static convertToDataset( item:any ):DatasetInstDTO{
        let dataset: DatasetInstDTO = $.extend(true, {}, item);
        let deliveryType = dataset.delivery_type.toLowerCase();
        if( deliveryType == "polling"){
            dataset.delivery_type = Dataset.DELIVERY_TYPE.POLLING;
        }else if( deliveryType == "push" ){
            dataset.delivery_type = Dataset.DELIVERY_TYPE.PUSH;
        }

        if( dataset.data_type == Dataset.DATA_TYPE.TIM ){
            if( dataset.tim ){
                dataset.rest_api = dataset.tim;//tim일경우.. 외부 입력시 tim으로 받을 수 있음
            }
        }

        let id = ObjectUtil.generateID();
        dataset.id = id;
        return dataset;
    }

    public static syncParam(query:string, param:any={}):any{
        let params:Array<string> = DatasetValidator.parseQueryParam(query);
        let syncParam: any = {};
        if( params.length ){
            for( let i:number=0; i<params.length; i++ ){
                let p = params[i];
                if( param && param[p] != undefined ){
                    syncParam[p] = param[p];
                }else{
                    return false;
                }
            }

            return true;
        }else{
            return false;
        }
    }



    public static parseQueryParam(str:string):Array<string> {
        let a = DatasetValidator.__getParseQueryParam(str, "#");
        let b = DatasetValidator.__getParseQueryParam(str, "$");
        return a.concat(b);
    }

    private static __getParseQueryParam(str:string, prefix:string):Array<string> {
        let ary:Array<string> = [];
        let startIndex = str.indexOf(prefix+"{");
        str = str.substr(startIndex);

        let closeStrIndex = str.indexOf("}");

        if (startIndex == -1 || closeStrIndex == -1) { return ary; }

        str = str.substr(2);

        ary = str.split(prefix+"{");
        ary = ary.map(x => x.split("}")[0].trim())
            .reduce((a, b) => {
                if (a.indexOf(b) < 0) {
                    a.push(b);
                }
                return a;
            }, []);
        return ary;
    }

    public static validation(dataset:DatasetInstDTO){
        if(!DatasetValidator.validationDataType(dataset.data_type)){
            return false;
        }

        if(!DatasetValidator.validationDeliveryType(dataset.delivery_type)){
            return false;
        }

        let result = true;
        switch( dataset.data_type ){
            case Dataset.DATA_TYPE.SQL:
                if(  !dataset.query || dataset.query.length < 2 ){
                    console.warn("요청한 데이터셋의 [query]정보가 정확하지 않습니다..", dataset.query);
                    result = false;
                }

            break;
            case Dataset.DATA_TYPE.REST:
            case Dataset.DATA_TYPE.TIM:
                if( !dataset.rest_api || dataset.rest_api.length < 5 ){
                    console.warn("요청한 데이터셋의 [url]정보가 정확하지 않습니다.", dataset.rest_api);
                    result = false;
                }
            break;
            default:
                console.warn("요청하신 데이터셋의 [data_type]이 정확하지 않습니다.", dataset.data_type);
                //데이터 타입이 정확하지 않음
                result = false;
        }

        return result;
    }

    private static validationDataType( dataType: string ):boolean{
        let result = true;
        switch( dataType ){
            case Dataset.DATA_TYPE.SQL:
            case Dataset.DATA_TYPE.REST:
            case Dataset.DATA_TYPE.TIM:
            break;
            default:
            console.warn("요청하신 데이터셋의 [data_type]이 정확하지 않습니다.", dataType);
            //데이터 타입이 정확하지 않음
            result = false;
        }

        return result;
    }

    private static validationDeliveryType( deliveryType: string ):boolean{
        let result = true;
        switch( deliveryType ){
            case Dataset.DELIVERY_TYPE.POLLING:
            case Dataset.DELIVERY_TYPE.PUSH:
            break;
            default:
            console.warn("요청하신 데이터셋의 [delivery_type]이 정확하지 않습니다.", deliveryType);
            //데이터 타입이 정확하지 않음
            result = false;
        }

        return result;
    }

}

/**
 * 실제 데이터셋 호출을 수행하는 객체, 사용자가 Dataservice객체를 통해 call 호출시마다 생성됨.
 * clear, resume, pause 메소드 제공
 * resoponded, success, error 이벤트 전송
 * query, tim은 dataset.do를 호출하고, rest api는 ajax를 호출함
 * */
export class DatasetWorker extends EventDispatcher{
    public static readonly PAUSE: string = "pause";//일시 정지시
    public static readonly RESUME: string = "resume";//다시 요청 시
    public static readonly BEFORE_CLEAR: string = "before_clear";//해제되기 전
    public static readonly CLEAR: string = "clear";//해제된 후

    private timer: number;
    private item:DatasetInstDTO = null;
    private param: any = {};
    private isActive: boolean = false;
    constructor( item:DatasetInstDTO ){
        super();
        this.item = item;
        this.start();
    }


    /*데이터셋 clear, clear된 worker는 재실행할 수 없음*/
    public clear(){
        this.dispatchEvent(new DatasetEvent( DatasetEvent.STATE_CHANGE, this, this.getEventInfo( DatasetWorker.BEFORE_CLEAR ) ));
        this.pause();
        this.item = null;
        let info = this.getEventInfo( DatasetWorker.CLEAR );
        this.dispatchEvent(new DatasetEvent( DatasetEvent.STATE_CHANGE, this, info ));
    }

    /*데이터 통신을 시작하는 메소드*/
    private start(){
        this.isActive = true;
        if( !this.item ){return;}
        switch( this.item.delivery_type ){
            case Dataset.DELIVERY_TYPE.POLLING:
                if( this.item.delay > 0 ){
                    this.timer = setTimeout(()=>{
                        this.requestPollingData();
                    }, this.item.delay * 1000 );
                }else{
                    this.requestPollingData();
                }
            break;
            case Dataset.DELIVERY_TYPE.PUSH:
            console.warn("push 서비스는 준비 중 입니다.");
            break;
        }
    }

    /*데이터 통신 일시 정지 resume시 재실행*/
    public pause(){
        if( !this.item ){return;} //clear상태
        this.isActive = false;
        //clearInterval(this.timer);
        clearTimeout(this.timer);
        this.dispatchEvent(new DatasetEvent( DatasetEvent.STATE_CHANGE, this, this.getEventInfo( DatasetWorker.PAUSE ) ));
    }

    /*데이터셋을 재실행*/
    public resume(){
        if( !this.item ){return;}
        this.isActive = true;
        this.dispatchEvent(new DatasetEvent( DatasetEvent.STATE_CHANGE, this, this.getEventInfo( DatasetWorker.RESUME ) ));
        //resume 이벤트 발생 후 request 이벤트 발생
        this.requestPollingData();
    }

    private getEventInfo( type:string, data=null){
        let eventInfo:any = {
            type: type,
            info: this.info
        }

        return eventInfo;
    }

    public get info(){
        if( !this.item ){return;}
        return $.extend(true, {}, this.item);
    }

    /*외부에서 on으로 이벤트 바인딩하기 위해 제공*/
    public on(eventType:string, handler:Function):DatasetWorker{
        if( !this.item ){return this;}
        super.addEventListener(eventType, handler);
        return this;
    }

    public off(eventType:string, handler:Function){
        if( !this.item ){return this;}
        super.removeEventListener( eventType, handler );
        return this;
    }


    /* 데이터 통신을 polling하는 재귀함수 
     * 데이터통신 성공/실패 여부 상관없이 응답받은 시점에서 폴링 주기로 다시 실행됨
     */
    private requestPollingData(){
        clearTimeout(this.timer);
        if( !this.isActive || !this.item ){return;}
        let interval = this.item.interval * 1000;        
        
        let requestHandler = this.getRequestHandler().bind(this);
        if( interval != 0 ){
            requestHandler().then(()=>{
                this.timer = setTimeout(()=>{
                    this.requestPollingData();
                }, interval );
            }).catch((error)=>{        
                this.timer = setTimeout(()=>{
                    this.requestPollingData();
                }, interval );
            })
        }else{
            requestHandler().then(()=>{               
            }).catch((error)=>{   
            });
        }
    }
    
    /*data_type에 따라 request Handler가 다름*/
    private getRequestHandler(){
        if(this.item.data_type != Dataset.DATA_TYPE.REST ){
            return this.http;
        }else{
            return this.callRestApi;
        }
    }

    /**
     * data_type이 restapi인 경우 호출되는 request handler
     * get타입일때 파라메터를 object타입으로, post타입일경우 파라메터 타입을 string으로 보냄
     */
    private callRestApi():Promise<any>{
        return new Promise((resolve, reject) => {
             if( !this.isActive || !this.item ){
                reject();
            }else{
                let options;
                
                try{	
                    options = JSON.parse(this.item.rest_api);//사용자가 입력하여 저장된 rest_api 데이터 구문
                    options.method = options.method || "GET";

                    if(this.item.param){
                        options.data = this.item.param;
                    }                
                
                    if(options.data && options.method.toLowerCase() == "post" && typeof options.data == "object"){
                        options.data = JSON.stringify( options.data );
                        if( options.data == "{}" ){
                            delete options.data;
                        }
                    }                
                
                }catch(error){
                    reject();
                    return false;
                }

                
                $.ajax( options )
                .done((result, textStatus, jqXHR)=>{	
                    if( !this.isActive || !this.item){
                        reject();
                    }else{
                        let info: any = this.getEventInfo(DatasetEvent.SUCCESS, result );
                        this.dispatchEvent(new DatasetEvent(DatasetEvent.SUCCESS, this, info, result));
                        this.dispatchEvent(new DatasetEvent(DatasetEvent.RESPONDED, this, info, result ));
                        resolve();
                    }
                }).fail(()=>{                        
                    if(  !this.isActive || !this.item ){
                        //
                    }else{
                        //통신 장애
                        let info = this.getEventInfo(DatasetEvent.ERROR, "error");
                        this.dispatchEvent(new DatasetEvent(DatasetEvent.ERROR, info));
                        this.dispatchEvent(new DatasetEvent(DatasetEvent.RESPONDED, info));
                    }      
                    reject();
                })	
            }
        });
    }

    /**
     * data_type이 query, tim일 경우 실행되는 request handler
     */
    private http():Promise<any>{
        return new Promise((resolve, reject) => {
            if( !this.isActive || !this.item ){
                reject();
            }else{
                let params:any = {
                      dataset_id: this.item.id,
                      data_type: this.item.data_type,
                      params: this.item.param,
                      query_type: this.item.query_type
                };

                if (this.item.datasource) {
                      params.datasource = this.item.datasource;
                      params.dbType = this.item.dbType;
                }

                api.post(window.wemb.configManager.serverUrl + "/dataset.do",{
                    id: 'dataSetService.getPollingData',
                    params: params
                }).then((result) => {
                    if( !this.isActive || !this.item){
                        reject();
                    }else{
                        let info: any;
                        if(result && result.data && result.data.result == "ok"){
                            //정상 응답
                            //console.log("@@ response", result, result.data.data);
                            let rowData = result.data.data;
                            info = this.getEventInfo(DatasetEvent.SUCCESS, result.data.data);
                            this.dispatchEvent(new DatasetEvent(DatasetEvent.SUCCESS, this, info, rowData, rowData));
                            this.dispatchEvent(new DatasetEvent(DatasetEvent.RESPONDED, this, info, rowData, rowData));
                            resolve();
                        }else if( result && result.data && result.data.result != "ok" ){
                            ///error code가 있는 경우
                            info = this.getEventInfo(DatasetEvent.ERROR, result.data.errorCode);
                            this.dispatchEvent(new DatasetEvent(DatasetEvent.ERROR, this, info));
                            this.dispatchEvent(new DatasetEvent(DatasetEvent.RESPONDED, this, info));
                            reject();
                        }else{
                            ///error code가 없는 경우
                            info = this.getEventInfo(DatasetEvent.ERROR, "error");
                            this.dispatchEvent(new DatasetEvent(DatasetEvent.ERROR, this, info));
                            this.dispatchEvent(new DatasetEvent(DatasetEvent.RESPONDED, this, info));
                            reject();
                        }
                    }
                }, (error) => {
                    if(  !this.isActive || !this.item ){
                        //
                    }else{
                        //통신 장애
                        let info = this.getEventInfo(DatasetEvent.ERROR, error);
                        this.dispatchEvent(new DatasetEvent(DatasetEvent.ERROR, info));
                        this.dispatchEvent(new DatasetEvent(DatasetEvent.RESPONDED, info));
                    }      

                    reject();              
                })
            }
        });        
    }

    private socket(){
        if( !this.item ){return;}
    }
}



