import WVComponent from "../../core/component/WVComponent";
import WVComponentPropertyManager from "../../core/component/WVComponentPropertyManager";

import {BackgroundLayer} from "../layer/BackgroundLayer";
import {ViewerTwoLayer} from "../layer/ViewerTwoLayer";
import {WORK_LAYERS} from "../layer/Layer";
import {IMetaProperties} from "../../core/display/interfaces/DisplayInterfaces";
import {IWVComponentProperties} from "../../core/component/interfaces/ComponentInterfaces";
import {ViewerThreeLayer} from "../layer/ViewerThreeLayer";
import InstanceManagerOnPageViewer from "../managers/InstanceManagerOnPageViewer";
import {PageMetaProperties} from "./PageMetaProperties";
import PageComponentCore from "./page/PageComponentCore";

/*


- 레이어 관린하는 컴포넌트 (컨테이너가 아님)
- 컨테이너가 아니지만 컨테이너 역할을 어느정도 하게됨.

- 해당 layer에 컴포넌트 추가, 삭제, 찾기



 */
export default class ViewerPageComponent extends PageComponentCore{
      // WeMB에서 isPage()를 호출해 해당 컴포넌트가 페이지 컴포넌트인지 아닌지 판단하는 속성으로 사용됨.
      // 페이지 컴포넌트 인터페이스 확인을 위해
      public static readonly NAME: string = "ViewerPageComponent";
      private _bgLayer: BackgroundLayer;
      private _threeLayer: ViewerThreeLayer;
      private _twoLayer: ViewerTwoLayer;
      private _drawLayer: ViewerTwoLayer;
      private _masterLayer: ViewerTwoLayer;


      // 인스턴스를 WScript를 통해 외부에서 접속할 수 있게 인스턴스 명을 만들어주는 기능.
      private _instanceManager:InstanceManagerOnPageViewer;
      public  get instanceManager(){
            return this._instanceManager;
      }
      get bgLayer(){
            return this._bgLayer;
      }

      get twoLayer(){
            return this._twoLayer;
      }
      get threeLayer(){
            return this._threeLayer;
      }

      get drawLayer(){
            return this._drawLayer;
      }

      get masterLayer(){
            return this._masterLayer;
      }

      private layerMap: Map<String, object> = new Map();

      constructor() {
            super();
      }


      protected _onDestroy() {
            $(this.element).empty();


            this._bgLayer.destroy();
            this._threeLayer.destroy();
            this._twoLayer.destroy();
            this._drawLayer.destroy();
            this._masterLayer.destroy();


            this._bgLayer = null;
            this._threeLayer = null;
            this._twoLayer = null;
            this._masterLayer = null;


            this._instanceManager.clear();
            this._instanceManager = null;

            super._onDestroy();
      }


      /*
      프로퍼티 정보를 모두 업데이트 해야하는 경우에 사용함.
      call : EditProxy

       */
      public resetProperties(metaProperties:IMetaProperties){
            let newProperties:IWVComponentProperties = this._createProperties(metaProperties);
            if(newProperties){
                  this._properties = newProperties;
                  // 페이지 정보가 즉, 페이지가 다시 열릴때마다 페이지 인스턴스 관리자 생성
                  this._instanceManager.create((metaProperties as PageMetaProperties).id, this);

                  // properties에 따라 화면을 다시 업데이터 해야함.
                  this.immediateUpdateDisplay();
            }else {
                  console.log("경고 컴포넌트 프로퍼티 스타일이 맞지 않습니다.");
            }
      }

      /*
      1. layer를 생성 한후
      2. 수동으로 layer를 붙여야 함.
      3. layer를 붙인 후 layer의 parent를 ViewerPageComponent로 적용해야함.
       */
      protected _onCreateElement(template:string=null) {
            this._createLayer();

            this.addLayer(WORK_LAYERS.BG_LAYER,  this._bgLayer, false);
            this.addLayer(WORK_LAYERS.THREE_LAYER,  this._threeLayer);
            this.addLayer(WORK_LAYERS.TWO_LAYER,  this._twoLayer);
            this.addLayer(WORK_LAYERS.DRAW_LAYER,  this._drawLayer);
            this.addLayer(WORK_LAYERS.MASTER_LAYER,  this._masterLayer);

            /*
            아직은 three 레이어가 하나이지만 추후 여러 개 증가할 수 있음.
            이를 위해 mainThreeLayer를 선택할 수 있는 기능을 추가해 놓음.
             */
            this.setMainThreeLayer(this._threeLayer);
      }

      private _createLayer() {
            ////////////
            this._bgLayer = new BackgroundLayer();
            this._bgLayer.create({
                  id: "_bgLayer",
                  name: "_bgLayer",
                  x:0,
                  y:0
            });

            if(window.wemb.configManager.using3D) {
                  this._threeLayer = new ViewerThreeLayer();

                  this._threeLayer.create({
                        id: "_threeLayer",
                        name: "_threeLayer",
                        x:0,
                        y:0,
                        visible: true
                  });
            }


            this._twoLayer = new ViewerTwoLayer();
            this._twoLayer.create({
                  id: "_twoLayer",
                  name: "_twoLayer",
                  x:0,
                  y:0,
                  visible: true
            });


            this._drawLayer = new ViewerTwoLayer();
            this._drawLayer.create({
                  id: "_drawLayer",
                  name: "_drawLayer",
                  x:0,
                  y:0,
                  visible: true
            });



            this._masterLayer = new ViewerTwoLayer();
            this._masterLayer.create({
                  id: "_masterLayer",
                  name: "_masterLayer",
                  x:0,
                  y:0,
                  visible: true
            });
            ////////////

            this.layerMap.set(WORK_LAYERS.BG_LAYER, this._bgLayer);
            this.layerMap.set(WORK_LAYERS.TWO_LAYER, this._twoLayer);
            this.layerMap.set(WORK_LAYERS.MASTER_LAYER, this._masterLayer);
            this.layerMap.set(WORK_LAYERS.DRAW_LAYER, this._drawLayer);
      }

      ////////////////////////////////////////////////////////////



      public _onImmediateUpdateDisplay(){
            if(this._instanceManager==null){
                  this._instanceManager = new InstanceManagerOnPageViewer();
            }

      }

      ////////////////////////////////////////////////////////////////////////












      ////////////////////////////////////////////////////////////////////////
      /*
      ViewerAreaMainContainer.noti_openPage()에서 noti에서 호출됨.

       */
      public openPage(){
            this.setLayerVisible();
            this.setActiveAllLayer();
      }

      public clearPage(){
            super.clearPage();
            // 생성한 레이어 내용 삭제(단, two, three 내용만 삭제한다)
            // 기본적으로 3D 레이어는 숨겨지게 만들기 숨겨지게 되는 경우 화면에서 안보임과 동시에
            // 렌더러가 동작하기 않게됨.
            if(window.wemb.configManager.using3D) {
                  this._threeLayer.visible=false;
                  // 카메라 위치 초기화
                  this.resetCameraPosition();
            }

            // 인스턴스 관리자에서 기존 등록 정보를 모두 지운다.
            this._instanceManager.clear();

      }

      public setLayerVisible(){
            this._masterLayer.visible = this.properties.setter.masterLayer.visible;
            this._twoLayer.visible = this.properties.setter.twoLayer.visible;
            if(window.wemb.configManager.using3D){
                  this._threeLayer.visible = this.properties.setter.threeLayer.visible;
            }
      }

      public setActiveAllLayer(){
            this._drawLayer.setMouseEventEnabled(true);
            this._masterLayer.setMouseEventEnabled(this.properties.setter.masterLayer.visible);
            this._twoLayer.setMouseEventEnabled(this.properties.setter.twoLayer.visible);
            if(window.wemb.configManager.using3D) {
                  this._threeLayer.setMouseEventEnabled(this.properties.setter.threeLayer.visible);
            }
      }

      public setActiveThreeLayer(value: boolean){
            this._threeLayer.setMouseEventEnabled(value);
      }


      /*
	레이어에 컴포넌트 추가.
	 */
      public addComInstance(comInstance: WVComponent) {
            let result = super.addComInstance(comInstance);
            if(result==true) {
                  this._instanceManager.register(comInstance, comInstance.layerName);
            }

            return result;
      }


      /*
      Mater, Two, Three Layer 숨기기
       */
      public hiddenMTTLayer(){
            /*
            visivility의 경우 부모/자식간의 독립적으로 처리되기 때문에 (즉, 부모가 hidden되어 있다해도 자식이 visible인 경우 활성화됨)
            visivility 사용하기 어려움.

            */
            /*
            if(this._masterLayer) this._masterLayer.visible=false;
            if(this._twoLayer) this._twoLayer.visible=false;
            if(this._threeLayer) this._threeLayer.visible=false;
            */
            // 2018.11.17(ckkim) 화면을 뒤덮는 로딩 효과 숨기기
            if(this._masterLayer) $(this._masterLayer.element).css("opacity",0);
            if(this._twoLayer) $(this._twoLayer.element).css("opacity",0);
            if(this._drawLayer) $(this._drawLayer.element).css("opacity",0);
            if(this._threeLayer) $(this._threeLayer.element).css("opacity",0);
      }

      public showMTTLayer(){
            // 2018.11.17(ckkim) 화면을 뒤덮는 로딩 효과 숨기기
            if(this._masterLayer) $(this._masterLayer.element).css("opacity",1);
            if(this._twoLayer) $(this._twoLayer.element).css("opacity",1);
            if(this._drawLayer) $(this._drawLayer.element).css("opacity",1);
            if(this._threeLayer) $(this._threeLayer.element).css("opacity",1);

      }


      /*
      2018.09.20(ckkim)
            - 페이지에 저장된 카메라 위치로 이동 처리,
            - 애니메이션 없이 바로
       */
      public resetCameraPosition(){
            try {
                  window.wemb.mainControls.reset();

                  // 카메러 원위치 시키기
                  window.wemb.mainControls.transitionFocusInTarget(
                        window.wemb.mainControls.position0.clone(),
                        window.wemb.mainControls.target0.clone(),
                        0
                  );
            }catch(error){
                  console.warn("viewerPageCOmponent camera reset error ", error);
            }
      }


      public cameraTransitionOut(duration:number=1){
            // 카메러 원위치 시키기
            window.wemb.mainControls.transitionFocusInTarget(
                  window.wemb.mainControls.position0.clone(),
                  window.wemb.mainControls.target0.clone(),
                  duration
            );
      }
      public cameraTransitionIn(instance, duration:number=1) {
            if(!instance){
                  return;
            }
            if(!this._threeLayer){
                  return;
            }


            // instance의 스케일 정보를 초기 벡터에 반영
            let posZ = instance.size.z + 50;
            let posY = Math.max(40, instance.size.y + 40);
            let offset = new THREE.Vector3(0, posY * instance.elementSize.y / instance.size.y, posZ * instance.elementSize.z / instance.size.z);
            offset = offset.applyMatrix4(instance.appendElement.matrixWorld);

            window.wemb.mainControls.transitionFocusInTarget(
                  offset,
                  instance.appendElement.position.clone().add({ x: 0, y: 0, z: 0 }),
                  duration
            );


      }
      ////////////////////////////////////////////////////////////////








      ////////////////////////////////////////////////////////////////

}
WVComponentPropertyManager.attach_default_component_infos(ViewerPageComponent, {name: ViewerPageComponent.NAME});

ViewerPageComponent["default_properties"] = {
      primary:{
            id: "",
            name: "",
            type:"page",
            secret:"N"
      },
      info:{
            category:"Page",
            version:"1.0.0",
            componentName:"ViewerPageComponent"
      },
      setter: {
            visible: true,
            width: 1920,
            height: 1080,
            borderRadius: {
                  topLeft: 0,
                  topRight: 0,
                  bottomLeft: 0,
                  bottomRight: 0
            },
            masterLayer: {
                  visible: true
            },
            mapLayer: {
                  visible: false
            },
            twoLayer: {
                  visible: true
            },
            threeLayer: {
                  visible: false,
                  using_shadow: false,
                  using_light: false,
                  camera: {
                        x: 0,
                        y: 100,
                        z: 100.00000000000001
                  },
                  cameraView: {
                        save: false,
                        cameraMatrix: "",
                        target: {
                              x: 0,
                              y: 0,
                              z: 0
                        }
                  },
                  grid: {
                        x: 100,
                        z: 100
                  }
            },
            background: {
                  using: false,
                  color: "#ffffff",
                  path: "",
                  image: ""
            },
            event: false,
            description: ""
      },
      events: {
            beforeLoad: "",
            loaded: "",
            beforeUnLoad: "",
            unLoaded: "",
            ready: ""
      },
      script: {
            fileName: ""
      }
};


ViewerPageComponent["property_panel_info"] = [];
