

/*


- 레이어 관린하는 컴포넌트 (컨테이너가 아님)
- 컨테이너가 아니지만 컨테이너 역할을 어느정도 하게됨.

- 해당 layer에 컴포넌트 추가, 삭제, 찾기



 */
import WVDOMComponent from "../../core/component/2D/WVDOMComponent";
import WVComponentPropertyManager from "../../core/component/WVComponentPropertyManager";
import Vue from "vue";
import {PopupManagerEvent} from "../managers/PopupManager";
import WVEvent from "../../core/events/WVEvent";
import PageComponent from "./PageComponent";
import {PageComponentEvent} from "../events/PageComponentEvent";
import WVComponent from "../../core/component/WVComponent";



export interface IPopupWindow {
      setGlobalBus($globalBus:Vue);
      closed():void;
}

export type PopupWindowOptions={
      width:number,
      height:number,
      x:number,
      y:number,
      borderRadius:object,
      customStyle:false,
      header:string,
      close:string,
      className:string,
      params:any,
      popupId:string,
      contentOverflow:string,
      usingProgress:boolean,
      memory:boolean,
      title:string
}

export default class PopupWindow extends WVDOMComponent implements IPopupWindow {
      public static readonly NAME: string = "PopupWindow";
      public static  readonly DEFAULT_WINDOW_CLASS_NAME="renobit-popup-view";

      // 클로즈 이벤트 처리를 위해. (PopupManager 에서 생성된 Vue객체)
      private _$globalBus:Vue;
      private _$element:any;
      private _$closedButton:any;
      private _$header:any;
      private _$body:any;
      private _pageComponent: PageComponent;

      private _func_pageLoadedHandler:Function;
      public get pageComponent() {
            return this._pageComponent;
      }

      public get innerPage() {
            if(this._pageComponent){
                  return this._pageComponent.pageElementComponent;
            }

            return null;

      }



      public get className():string{
            let _name = this.getGroupPropertyValue("styleInfo", "className")
            if(_name==null || _name.length==0){
                  return PopupWindow.DEFAULT_WINDOW_CLASS_NAME;
            }
            return _name;
      }

      public get title():string{
            return this.getGroupPropertyValue("setter", "title")
      }
      public set title(value:string){
            this.setGroupPropertyValue("setter", "title", value);
      }

      public get contentOverflow():string{
            return this.getGroupPropertyValue("setter", "contentOverflow");
      }


      public get memory():boolean{
            return this.getGroupPropertyValue("setter", "memory");
      }


      constructor() {
            super();
      }


      protected  _onDestroy(){
            console.log("PopupWIndow _onDestroy 시작");

            this._$element.find('.header').off('mousedown');

            this._$closedButton.off();
            this._$closedButton = null;

            if(this._$closedButton){
                  this._$closedButton.off();
                  this._$closedButton = null;
            }

            if(this._$header){
                  this._$header.off();
                  this._$header = null;
            }


            if(this._pageComponent) {
                  this._pageComponent.removeEventListener(PageComponentEvent.LOADED, this._func_pageLoadedHandler)
                  this._func_pageLoadedHandler = null;

                  this._pageComponent.destroy();
                  this._pageComponent = null;
            }

            if(this._$body) {
                  this._$body.empty();
                  this._$body.off();
                  this._$body = null;
            }

            /*
            if(this._$element) {
                  this._$element = null;
            }
            */

            console.log("PopupWIndow _onDestroy 종료");
            super._onDestroy();
      }

      protected _createElement(template:string=""){
            if(this.isEditorMode)
                  return false;

            //Normal Popup
            if (!this.setter.customStyle) {
                  template = `
                  <div class="${PopupWindow.DEFAULT_WINDOW_CLASS_NAME} ${this.className}">
                        <div class="header">
                              <h4>Window</h4>
                              <a role="button" class="close-modal-btn"><i class="el-icon-close"></i></a>
                        </div>
                        <div class="body" style="overflow:${this.contentOverflow}">
                        </div>
                  </div>
                  `;
            } else {
                  //Custom Popup
                  template = `
                  <div class="${PopupWindow.DEFAULT_WINDOW_CLASS_NAME} ${this.className}">
                        <div class="body" style="overflow:${this.contentOverflow}">
                        </div>
                  </div>
                  `;
            }


            this._$element =$(template);


            /* 초기 나올 당시 숨겨진 상태에서 등장*/
            this._$element.css({visibility:"hidden"});
            this._element = this._$element[0];
            this._element.style.position = "absolute";
            this._element.style.transformOrigin = "0 0";


            if (!this.setter.customStyle) {
                  // 닫기 버튼 처리
                  this._$closedButton = this._$element.find(".close-modal-btn");
                  if (this._$closedButton) {
                        this._$closedButton.click(() => {
                              this.closed();
                        })

                        this._$closedButton.css("display", "none");
                  }

                  // 헤더 처리
                  this._$header = this._$element.find(".header");
                  if (this._$header) {
                        this._$element.draggable({handle: ".header"});
                  }
            }


            // body 처리
            this._$body = this._$element.find(".body");

            // 뷰어 모드 일때만 생성(popupwindow는 특별한 경우가 아닌한 viewer에서만 생성)
            if(this.isViewerMode) {
                  this._pageComponent = new PageComponent();

                  let pageComponentMetaProperties = this.getGroupProperties("pageComponentMetaProperties") || {};


                  let initProperties = {
                        props:{
                              pageInfo:{
                                    usingPopupPage:true,
                                    usingProgress:false
                              },

                              createLayerInfo:{
                                    usingThreeLayer:false
                              }

                        }
                  }
                  $.extend(true, initProperties.props, pageComponentMetaProperties);

                  this._pageComponent.executeViewerMode();
                  this._pageComponent.create(initProperties);

                  // 페이지가 로드된 후 closed 버튼 활성화 처리를 위한 handler추가
                  this._func_pageLoadedHandler = this._onLoadedPage.bind(this);
                  /*
                  페이지의 모든 컴포넌트가 로드(페이지 붙고, 리소스까지 모두 읽어진 상태)될때 이벤트 발생.
                  주의:
                  W스크립트 이벤트가 아닌 일반 이벤트임.
                   */
                  this._pageComponent.addEventListener(PageComponentEvent.LOADED, this._func_pageLoadedHandler);


                  // 동적으로 body 영역에 컴포넌트를 추가.
                  this._$body.append(this._pageComponent.appendElement);
                  // this.hide();
            }
      }


      ////////////////////////////////////////////////////////////

      /*
      _immedateUpdateDisplay에서
            _validateSize() 호출됨.
       */
      protected _validateSize(): void {
            let domElement: HTMLElement = this.element as HTMLElement;

            /*
                  페이지 크기에 header 크기 추가해서 팝업 크기 만들기.

             */

            let headerHeight = 0;
            if(this._$header){
                  /*
                  2018.05.16(ddandongne)
                  popupManager에서 height 크기를 구해서 사용하기 때문에 반드시 스타일에서 설정해줘야 함.
                  popup.css에 반드시 header의 height 값을 설정해줘야 함.
                   */
                  headerHeight =this._$header.outerHeight();
            }

            let borderRadius = `${this.borderRadius.topLeft}px ${this.borderRadius.topRight}px ${this.borderRadius.bottomLeft}px ${this.borderRadius.bottomRight}px`;

            $(domElement).css({
                  width: this.width,
                  height: this.height+headerHeight,
                  borderRadius: borderRadius
            })

      }


      protected _onImmediateUpdateDisplay(){
            if(this.isViewerMode) {
                  // 이벤트를 날려 실제 화면을 활성화 시킨다.
                  if(this._pageComponent)
                        this._pageComponent.dispatchEvent(new WVEvent(WVEvent.ADDED, this));

                  /* 초기 나올 당시 숨겨진 상태에서 등장했기 때문에 다시 보이기로 처리*/
                  setTimeout(()=>{
                        this._$element.css({visibility:"visible"})
                  },100);

                  this._validateTitle();
                  this._$element.find('.header').on('mousedown', function(){
                        //대전제 : PopupWindow는 master에 올라가 있는 컴포넌트들의 z-index보다 작을 수가 없다. 스크립트로 팝업을 띄우던, 뭔가를 클릭해서 띄우던
                        //항상 모든 페이지의 컴포넌트들이 다 로딩 되고 나서의 일이기 때문이다.

                        //master에 올라가 있는 컴포넌트들
                        let masterEntries : Array<WVComponent> = Array.from(wemb.mainPageComponent.masterLayer.componentInstanceListMap.values());

                        //가장 큰 z-index가 몇인지 찾아본다
                        let maxZIndex = masterEntries.reduce((past : WVComponent, now : WVComponent)=> past.depth > now.depth ? past : now).depth;

                        //지금 선택된 것이 가장 큰 z-index를 갖게 한다.
                        this.depth = maxZIndex + 1;

                  }.bind(this))
            }
      }

      protected _onCommitProperties() {
            if (this._updatePropertiesMap.has("setter.title") == true) {
                  this.validateCallLater(this._validateTitle);
            }
      }


      public _validateTitle(){
            if(this._$header){
                  let pageInfo = this.getGroupPropertyValue("pageComponentMetaProperties", "pageInfo");
                  if (this.title && this.title.length>0)
                        this._$header.find("h4").html(this.title);
                  else
                        this._$header.find("h4").html(pageInfo.pageName);

            }
      }

      ////////////////////////////////////////////////////////////////////////







      ////////////////////////////////////////////////////////////////////////
      // window 닫기 처리
      public setGlobalBus($globalBus:Vue){
            this._$globalBus = $globalBus;
      }
      public closed():void {

            /*
                  실행 순서가 중요함.
                  this.dispatchComponentEvent() 후
                  this._$globalBus.$emit를 해줘야 함. 아닌 경우 에러 발생.

             */


            this.dispatchComponentEvent(PopupManagerEvent.CLOSED_POPUP_EVENT, this);

            // PopupManager._initGlobalBusEvent()에 on이 존재
            this._$globalBus.$emit(PopupManagerEvent.CLOSED_POPUP_EVENT, this);
      }

      /*
      페이지가 closed()되었는데 memory가 true인 경우 숨기기만 하기
      중요!!
            반ㄷ싀 popup.css에 hidden이 있어야함.
       */
      public hide(){
            if(this.element) {
                  let $element = $(this.element);
                  $element.addClass("hidden");
            }
      }

      public show(){
            if(this.element) {
                  let $element = $(this.element);
                  $element.removeClass("hidden");
            }
      }

      ////////////////////////////////////////////////////////////////




      /*
      페이지 정보 부터해서 리소스가 모드 로드되면 실행되는 메서드
       */
      public _onLoadedPage(){
            if (this.setter.customStyle && this.setter.header) {
                  this._$header = this._$element.find("."+this.setter.header);
                  if (this._$header) {
                        //wv-component 클래스의 pointer-events 때문에 drag 가 안됨
                        //해당 클래스에 별다른 내용이 없기 때문에 class 삭제해도 무방해 보임
                        this._$header.removeClass('wv-component');
                        this._$element.draggable({handle: "." + this.setter.header});
                  }
            }

            if (this.setter.customStyle && this.setter.close) {
                  this._$closedButton = this._$element.find("." + this.setter.close);
                  if (this._$closedButton) {
                        this._$closedButton.click(() => {
                              this.closed();
                        })
                        this._$closedButton.css("opacity", 0);
                  }
            }

            if(this._$closedButton){
                  if(this.setter.customStyle){
                        this._$closedButton.css("opacity", 1);
                  }else{
                        this._$closedButton.css("display", "block");
                  }
            }
      }



      ////////////////////////////////////////////////////////////////

}
WVComponentPropertyManager.attach_default_component_infos(PopupWindow, {name: PopupWindow.NAME});

PopupWindow["default_properties"] = {

      primary:{
            id: "",
            name: "",
            layerName:""
      },

      setter: {
            width: 1920,
            height: 1080,
            borderRadius:{
                  topLeft:0,
                  topRight:0,
                  bottomLeft:0,
                  bottomRight:0
            },
            customStyle: false,
            header: '',
            close: '',
            x:0,
            y:0,
            depth:0,
            title:"",
            contentOverflow:"hidden",
            memory:false
      },

      styleInfo:{
            className:"renobit-popup-view"
      },

      pageComponentMetaProperties:{
            pageInfo:{
                  pageName:"",
                  usingPopupPage:true,
                  usingProgress:false
            },
            createLayerInfo:{
                  usingThreeLayer:false
            },
            params:{
            }
      },


      events: {
            "register": ""
      },
      info:{
            category:"2D",
            version:"1.0.0",
            componentName:"PopupWindow"
      }
};


PopupWindow["property_panel_info"] = [];
