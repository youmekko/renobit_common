import Vue from "vue";
import { dto } from '../../../../editor/windows/datasetManager/model/dto/DatasetDTO';
import DatasetItemDTO = dto.DatasetItemDTO;
import datasetApi from '../../../../common/api/datasetApi';


export default class DatasetPreviewService {
    private static _instance:DatasetPreviewService = null;


    constructor() {
    }

    public static getInstance():DatasetPreviewService{
        if( DatasetPreviewService._instance == null ){
            DatasetPreviewService._instance = new DatasetPreviewService();
        }

        return DatasetPreviewService._instance;
    }

    getPreviewData(sendData) {
        return datasetApi.getPreviewData(sendData).then((result) => {
           return result;
        }, (error) => {
            //console.log("error");
            return error;
        })

    }

    getDatasetList() {
        return datasetApi.getDatasetList('').then((result) => {
             return result;
        }, (error) => {
            //console.log("error");
            return error;
        });
    }


    validationDataset(id) {
        return datasetApi.getDatasetList('').then((result) => {
            if (result.result == "ok") {
                let dataset = result.data.find((x) => x.dataset_id == id) || false;
                return dataset;
            } else {
                return false;
            }
        }, (error) => {
            //console.log("error");
            return error;
        });
    }

    validationColumns(dataset, columns) {
        return this.getPreviewData(dataset).then((result) => {
            if (result) {
                let row = result[0];
                let keys = [];
                for (let key in row) {
                    keys.push(key);
                }

                for (let i = 0, len = columns.length; i < len; i++) {
                    if (keys.indexOf(columns[i]) == -1) {
                        return false;
                    }
                }

                /* //console.log({
                    keys: keys,
                    data: result
                });*/
                return {
                    keys: keys,
                    data: result
                }
            } else {
                return false;
            }
        }, (error) => {
            return false;
        });
    }

}
