import {IMetaProperties} from "../../../../core/display/interfaces/DisplayInterfaces";
import {ILayer} from "../../../layer/interfaces/LayerInterfaces";
import WVComponent from "../../../../core/component/WVComponent";
import {IThreeExtensionElements} from "../../../../core/component/interfaces/ComponentInterfaces";
import WVDisplayObjectContainer from "../../../../core/display/WVDisplayObjectContainer";
import {ThreeLayerCore} from "../../../layer/ThreeLayerCore";

export default interface IPageElement {
      __PAGE__:string;
}



export interface IWVPageComponent {
      cameraView();
      camera()
      grid();
      readonly layerListMap:Map<string,ILayer|WVDisplayObjectContainer>;
      destroy()
      clearPage():void;
      resetProperties(pageInfo:IMetaProperties);

      addLayer(layerName:string, layerInstance:ILayer|WVDisplayObjectContainer);
      removeLayer(layerName:string);
      addComInstance(comInstance:WVComponent);
      removeComInstance(comInstance:WVComponent);

      readonly  threeElements:IThreeExtensionElements;
      setMainThreeLayer(layer:ThreeLayerCore);

      isLoaded:boolean;
      readonly isLoading:boolean;
      params:any;
      readonly popupOwner:any|WVComponent;
}


/*

ComponentInstanceLoader를 사용하는 곳에서
컴포넌트 인스턴스 생성 후
proxy 또는 PageComponent로 생성한 한 인스턴스를 추가할때
사용하는 인터페이스

주로, 에디터 proxy, 뷰어 proxy, PageComponent에서 사용하게 됨.
 */
export interface IComponentInstanceLoaderContainer {
      // 인스턴스 생성 후 컴포넌트가 로드 될때
      addComInstance(comInstance:WVComponent):boolean;
      // 모든 컴포넌트가 로드 된 후
      loadCompleted():void;

      getThreeExtensionElements():IThreeExtensionElements;

}
