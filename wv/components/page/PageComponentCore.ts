import WVComponent, {GROUP_NAME_LIST} from "../../../core/component/WVComponent";
import WVDOMComponent from "../../../core/component/2D/WVDOMComponent";
import {IMetaProperties, IWVDisplayObjectContainer} from "../../../core/display/interfaces/DisplayInterfaces";
import {IWVComponentProperties} from "../../../core/component/interfaces/ComponentInterfaces";
import MeshManager from "../../../core/component/3D/manager/MeshManager";
import {WVPageComponentScriptEvent} from "../../../core/events/WVComponentEventDispatcher";
import {ILayer} from "../../layer/interfaces/LayerInterfaces";
import WVDisplayObjectContainer from "../../../core/display/WVDisplayObjectContainer";
import {IWVPageComponent} from "./interfaces/PageComponentInterfaces";
import {ThreeLayerCore} from "../../layer/ThreeLayerCore";
import {IEditorPageObject} from "../../managers/InstanceManagerOnPageViewer";
import {ArrayUtil} from "../../../utils/Util";
declare var THREE;

/*
- 레이어 관린하는 컴포넌트 (컨테이너가 아님)
- 컨테이너가 아니지만 컨테이너 역할을 어느정도 하게됨.
- 해당 layer에 컴포넌트 추가, 삭제, 찾기
 */


export default class PageComponentCore extends WVDOMComponent implements IWVPageComponent, IEditorPageObject{
      // WeMB에서 isPage()를 호출해 해당 컴포넌트가 페이지 컴포넌트인지 아닌지 판단하는 속성으로 사용됨.
      public __PAGE__:string="PAGE_TYPE";
      public static readonly NAME: string = "PageComponentCore";
      private _invalidateCamera = false;
      private _invalidateCameraView = false;
      protected _invalidateCameraFar=false;

      public _isLoading:boolean=false;
      public _isLoaded:boolean=false;
      /*
      중요 프로퍼티
      포함된 컴포넌트에서 초기 시작시 컴포넌트가
      로드되는 시점인지 유무 확인 하는 속성으로 사용됨.
      WVComponent
      isCompleted(){
      }
      에서 사용됨.
       */
      public get isLoading():boolean{
            return this._isLoading;
      }



      public get isLoaded():boolean{
            return this._isLoaded;
      }
      public set isLoaded(value:boolean){
            this._isLoaded = value;
      }



      set cameraView(value){
            if (this._checkUpdateGroupPropertyValue("setter.threeLayer", "cameraView", value)) {
                  this._invalidateCameraView = true;
                  this._invalidateCameraFar =true;
            }
      }

      get cameraView(){
            return this.getGroupPropertyValue("setter.threeLayer", "cameraView");
      }

      set camera(value){
            if (this._checkUpdateGroupPropertyValue("setter.threeLayer", "camera", value)) {
                  this._invalidateCamera = true;
            }
      }

      get camera(){
            return this.getGroupPropertyValue("setter.threeLayer", "camera");
      }

      get grid(){
            return this.getGroupPropertyValue("setter.threeLayer", "grid");
      }

      get event(){
            return this.getGroupPropertyValue("setter", "event");
      }

      get description(){
            return this.getGroupPropertyValue("setter", "description");
      }

      private _layerListMap: Map<string, ILayer|WVDisplayObjectContainer> = new Map();
      get layerListMap():Map<string, ILayer|WVDisplayObjectContainer>{
            return this._layerListMap;
      }

      // clear명령시 실행되는 레이어 목록
      private _clearLayerListMap: Map<string, ILayer|WVDisplayObjectContainer> = new Map();

      private _mainThreeLayer:ThreeLayerCore;
      public getMainThreeLayer():ThreeLayerCore{
            return this._mainThreeLayer;
      }

      public setMainThreeLayer(layer:ThreeLayerCore){
            this._mainThreeLayer = layer;
      }

      public get threeElements(){
            if(this._mainThreeLayer)
                  return this._mainThreeLayer.threeElements;
            else
                  return null;
      }


      // 전체 컴포넌트 리스트
      private _comInstanceList:WVComponent[] = [];
      public get comInstanceList(){
            return this._comInstanceList;
      }

      /*
      instance id를 가진 컴포넌트 구하기
       */
      public getComInstanceById(instanceId:string){
            console.log("@@ param = this._comInstanceList ", this._comInstanceList);
            return this._comInstanceList.find((comInstance:WVComponent)=>{

                  if(comInstance.id==instanceId){
                        return true;
                  }

                  return false;
            })
      }
      /*
      instance id를 가진 컴포넌트 구하기
       */
      public getComInstanceByName(instanceName:string){
            return this._comInstanceList.find((comInstance:WVComponent)=>{

                  if(comInstance.name==instanceName){
                        return true;
                  }

                  return false;
            })
      }

      public getComInstanceByComponentName(componentName : string){

            var isExistComponent = this._comInstanceList.find((comInstance:WVComponent)=> {
                  if (comInstance.componentName === componentName) {
                        return true;
                  }
            });
            if(isExistComponent)
                  return true;
            else
                  return false;

      }


      ////////////
      /* params 내용은 주입됨

            call : popupManager에 의해서
            openPageByName, ById() 메서드 호출시 파라메터가 넘어오는 경우
       */
      private _params:any;
      set params(value:any){
            this._params = value;
      }

      get params() {
            return this._params;
      }


      get popupOwner(){
            if(this._params==null)
                  return null;


            if(this._params.hasOwnProperty("popupOwner")==false) {
                  return null;
            }

            return this._params.popupOwner;

      }
      ////////////


      constructor() {
            super();
      }



      protected _onDestroy(): void {
            if(this.isViewerMode){
                  this.dispatchBeforeUnLoadScriptEvent();
            }
            if(this._layerListMap){

                  this._layerListMap.forEach((layerInstance:ILayer)=>{
                        if(layerInstance)
                              layerInstance.destroy();
                  })


                  this._layerListMap.clear();
                  this._layerListMap = null;

                  this._clearLayerListMap.clear();
                  this._clearLayerListMap = null;
            }

            $(this._element).empty();


            this._comInstanceList= null;

            if(this.isViewerMode){
                  this.dispatchUnLoadedScriptEvent();
            }

            this._params = null;
            super._onDestroy();
      }

      public setLoadingState(state:boolean){
            this._isLoading=state;
      }




      /*
	레이어는 살려둔 상태에서 컴포넌트만 제거하기
	 */
      public clearPage(){
            try {
                  this._clearLayerListMap.forEach((layerInstance: ILayer) => {
                        layerInstance.clear();
                  })
            }catch(error){
                  console.log("PageCompoenntCore ", error);
            }

            this._comInstanceList= [];

      }


      /*
      PageElementComponent에서는 위치를 기본으로 0,0으로 사용
      만약 페이지를 읽어들때마다 페이지 위치인 0,0으로 이동하게 되는 경우
      애니메이션 효과에 문제점이 생김.
      즉, PageElementComponent가 특정 페이지의 요소로 사용되는 경우
      초기에는 0,0위에 있고
      이후는 특정 상황에 맞게 밖에서 조절해서 사용하면 됨.
       */
      protected _validatePosition(){

      }
      /*
      프로퍼티 정보를 모두 업데이트 해야하는 경우에 사용함.
      call : EditProxy

       */
      public resetProperties(metaProperties:IMetaProperties){

            let newProperties:IWVComponentProperties = this._createProperties(metaProperties);
            if(newProperties){
                  this._properties = newProperties;
                  // 2. 신규 프로퍼티를 기반으로 화면 다시 생성
                  this.immediateUpdateDisplay();

            }else {
                  console.log("경고 컴포넌트 프로퍼티 스타일이 맞지 않습니다.");
            }

      }

      public addLayer(layerName:string, layerInstance:ILayer, clearLayer:boolean=true):boolean{
            let $element = $(this._element);
            if(layerInstance && this._layerListMap.has(layerName)==false) {
                  this._layerListMap.set(layerName, layerInstance);
                  $element.append(layerInstance.appendElement);


                  // clearLayer인 경우에만 추가.
                  if(clearLayer==true)
                        this._clearLayerListMap.set(layerName, layerInstance)
                  return true;
            }

            return false;
      }

      public removeLayer(layerName:string):boolean{

            if(this._layerListMap.has(layerName)==false) {
                  this._layerListMap.delete(layerName);
                  this._clearLayerListMap.delete(layerName);


                  let layerInstance:ILayer = this._layerListMap.get(layerName)as ILayer;
                  $(layerInstance.appendElement).remove();
                  layerInstance.destroy();

                  return true;
            }

            return false;
      }

      // 생성된 레이어를 모두 지운다.
      public removeAllLayer(){
            this._layerListMap.forEach((layerInstance:ILayer)=>{
                  $(layerInstance.appendElement).remove();
                  layerInstance.destroy();
            })

            this._layerListMap.clear();
            this._clearLayerListMap.clear();
      }
      ////////////////////////////////////////////////////////////



      protected _validateSize(): void {
            let domElement: HTMLElement = this.element as HTMLElement;
            let properties = this.getGroupProperties(GROUP_NAME_LIST.SETTER);

            let borderRadius = `${properties.borderRadius.topLeft}px ${properties.borderRadius.topRight}px ${properties.borderRadius.bottomLeft}px ${properties.borderRadius.bottomRight}px`;

            $(domElement).css({
                  width: properties.width,
                  height: properties.height,
                  borderRadius: borderRadius
            });

            this._layerListMap.forEach((layerInstance:ILayer)=>{
                  layerInstance.updateSize(this.width, this.height, this.borderRadius);
            })
      }



      protected  _commitProperties():void{
            super._commitProperties();




            // 카메라 속성 변경 유무
            if(this._invalidateCamera){
                  this.validateCallLater(this._validateCamera);
                  this._invalidateCamera=false;

            }

            if(this._invalidateCameraView){
                  this.validateCallLater(this._validateCameraView);
                  this._invalidateCameraView=false;


            }

            if(this._invalidateCameraFar){
                  this.validateCallLater(this._validateCameraFar);
                  this._invalidateCameraFar=false;
            }
      }




      protected _validateCamera():void {
            if(this.threeElements==null)
                  return;

            let value = this.getGroupPropertyValue("setter.threeLayer","camera");


            this.threeElements.camera.position.copy(value);
            this.threeElements.mainControls.target.x = 0;
            this.threeElements.mainControls.target.y = 0;
            this.threeElements.mainControls.target.z = 0;
            this.threeElements.mainControls.update();

      }


      protected _validateCameraFar():void{

            if(this.threeElements==null)
                  return;
                        /*
                        var axis = this.threeElements.camera.position.clone();
                        let gridHelper = new THREE.GridHelper( Math.max(parseInt(this.grid.x), parseInt(this.grid.z) ), 1); //MeshManager.gridHelper(parseInt(this.grid.x), 1, parseInt(this.grid.z), 1);

                        let box = new THREE.Box3().setFromObject(gridHelper);
                        const boundingSphere = box.getBoundingSphere();
                        var newDist = boundingSphere.radius / Math.sin((45 * (Math.PI / 180)) / 2);

                        axis.sub(boundingSphere.center);
                        axis.setLength(newDist);
                        axis.add(boundingSphere.center);


                        // grid크기가 변경되는 경우 far 위치 재 정의
                        this.threeElements.camera.far = axis.length() * 1.6;
                        this.threeElements.camera.near = this.threeElements.camera.far / 1000 > 10 ? 10 : this.threeElements.camera.far / 1000;
                        this.threeElements.camera.updateProjectionMatrix();
                        this.threeElements.mainControls.minDistance = 0;
                        this.threeElements.mainControls.maxDistance = axis.length() * 1.5;
                        this.threeElements.mainControls.update();

                        box = null;

            gridHelper.material.dispose();
            gridHelper.geometry.dispose();
            gridHelper = null;
            */
      }


      protected _validateCameraView():void {
            if(this.threeElements==null)
                  return;

            let value = this.cameraView;
            if(value.save==true) {
                  this.threeElements.mainControls.target.x = value.target.x;
                  this.threeElements.mainControls.target.y = value.target.y;
                  this.threeElements.mainControls.target.z = value.target.z;
                  this.threeElements.mainControls.update();

                  this.threeElements.camera.matrix.fromArray(JSON.parse(value.cameraMatrix));
                  this.threeElements.camera.matrix.decompose(this.threeElements.camera.position, this.threeElements.camera.quaternion, this.threeElements.camera.scale);

            }else {

                  /*
                  주의!
                        this.cameraView로 값을 직접 변경하면 안됨.(무한 루프 발생)
                   */

                  this._properties.setter.threeLayer.cameraView={
                        "save" : false,
                        "cameraMatrix": "[1, 0, -0, 0, -0, 0.8944271909999159, -0.447213595499958, 0, 0, 0.447213595499958, 0.8944271909999159, 0, 0, 100, 100, 1]",
                        "target": {
                              "x": 0,
                              "y": 0,
                              "z": 0
                        }
                  }
                  value =  this._properties.setter.threeLayer.cameraView;
                  this.threeElements.mainControls.target.x = value.target.x;
                  this.threeElements.mainControls.target.y = value.target.y;
                  this.threeElements.mainControls.target.z = value.target.z;
                  this.threeElements.mainControls.update();

                  this.threeElements.camera.matrix.fromArray(JSON.parse(value.cameraMatrix));
                  this.threeElements.camera.matrix.decompose(this.threeElements.camera.position, this.threeElements.camera.quaternion, this.threeElements.camera.scale);

            }
            // 카메라 현재 상태 저장
            this.threeElements.mainControls.saveState();
      }

      /*
      주의 : immediateUpdateDisplay() 내부에서 onImmediateUpdateDisplay()를 호출하기 때문에
      아래와 같이 super.immediateUpdateDisplay()를 늦게 호출해야줘함.
       */
      public immediateUpdateDisplay():void {
            this._layerListMap.forEach((layerInstance:ILayer)=>{
                  layerInstance.immediateUpdateDisplay();
            })


            this._validateCamera();
            this._validateCameraView();
            this._validateCameraFar();

            super.immediateUpdateDisplay();

      }


      public updateSize(width: number, height: number, borderRadius: object) {
            this.width = width;
            this.height = height;
            this.borderRadius = borderRadius;
      }


      ////////////////////////////////////////////////////////////////////////












      ////////////////////////////////////////////////////////////////////////

      /*
	레이어에 컴포넌트 추가.
	 */
      public addComInstance(comInstance: WVComponent):boolean {

            if(this._layerListMap.has(comInstance.layerName)==false){
                  console.log("#### 경고, ", comInstance.layerName + "를 찾을 수 없어 추가하지 못했음.1")
                  return false;
            }

            let layer: WVDisplayObjectContainer = (this._layerListMap.get(comInstance.layerName) as WVDisplayObjectContainer);


            if (layer) {
                  this._comInstanceList.push(comInstance);
                  layer.addChild(comInstance);
                  comInstance.setMouseEventEnabled(true);

                  return true;
            } else {
                  console.log("#### 경고, ", comInstance.layerName + "를 찾을 수 없어 추가하지 못했음.2")
                  return false;
            }
      }

      public removeComInstance(comInstance: WVComponent) {
            let layer: IWVDisplayObjectContainer = (this._layerListMap.get(comInstance.layerName) as IWVDisplayObjectContainer);
            if (layer) {
                  ArrayUtil.delete(this._comInstanceList, "id", comInstance.id);
                  layer.removeChild(comInstance);
                  return true;
            } else {
                  console.log("#### 경고, ", comInstance.layerName + "를 찾을 수 없어 추가하지 못했음.")
                  return false;
            }
      }

      ////////////////////////////////////////////////////////////////


      ////////////////////////////////////////////////////////////////






      ////////////////////////////////////////////////////////////////

      public dispatchBeforeUnLoadScriptEvent(){
            this._componentEventDispatcher.dispatchEvent(WVPageComponentScriptEvent.BEFORE_UNLOAD)
      }


      public dispatchUnLoadedScriptEvent(){
            this._componentEventDispatcher.dispatchEvent(WVPageComponentScriptEvent.UNLOADED);
      }



      public dispatchBeforeLoadScriptEvent(){
            this._componentEventDispatcher.dispatchEvent(WVPageComponentScriptEvent.BEFORE_LOAD)
      }

      public dispatchReadyScriptEvent(){
            this._componentEventDispatcher.dispatchEvent(WVPageComponentScriptEvent.READY)
      }


      public dispatchLoadedScriptEvent(){
            this._componentEventDispatcher.dispatchEvent(WVPageComponentScriptEvent.LOADED)
      }






      /*
      override해서 내부 내용이 실행되지 않게 만들기.
      여기에서는 사용하지 않음.
       */
      public syncLocationSizeElementPropsComponentProps(){

      }

}
