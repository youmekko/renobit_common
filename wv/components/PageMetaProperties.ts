export type PageMetaProperties={
      id:string
      name:string
      secret:string
      type:string,
      master:string,
      version:string
      props:{

      }
}
