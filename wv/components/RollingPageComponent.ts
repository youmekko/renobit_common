import WVComponentPropertyManager from "../../core/component/WVComponentPropertyManager";

import WVDOMComponent from "../../core/component/2D/WVDOMComponent";


import PageElementComponent from "./page/PageElementComponent";
import {ObjectUtil} from "../../utils/Util";
import WVEvent from "../../core/events/WVEvent";

import {OpenPageData} from "../page/Page";
import {IProgressbar, default as LoadingProgressbar} from "./LoadingProgressbar";
import IPageComponent from "./page/interfaces/PageComponentInterfaces";
import {EXE_MODE} from "../data/Data";
import {WVComponentScriptEvent, WVPageComponentScriptEvent} from "../../core/events/WVComponentEventDispatcher";
import {WVCOMPONENT_METHOD_INFO} from "../../core/component/interfaces/ComponentInterfaces";
import Vue from "vue";
import WVComponent from "../../core/component/WVComponent";

import axios from 'axios';
import {ComponentInstanceLoader2} from "../managers/ComponentInstanceLoader";
import {ComponentResourceLoader, ComponentResourceLoaderEvent} from "../helpers/ComponentResourceLoader";
import {PageComponentEvent} from "../events/PageComponentEvent";



declare var THREE;
declare function html2canvas(any,any);
/*

- 레이어 관린하는 컴포넌트 (컨테이너가 아님)
- 컨테이너가 아니지만 컨테이너 역할을 어느정도 하게됨.

- 해당 layer에 컴포넌트 추가, 삭제, 찾기

 */
export default class RollingPageComponent extends WVDOMComponent  implements IPageComponent {
      // WeMB에서 isPage()를 호출해 해당 컴포넌트가 페이지 컴포넌트인지 아닌지 판단하는 속성으로 사용됨.
      public __PAGE__:string="PAGE_TYPE";
      public static readonly NAME: string = "RollingPageComponent";
      private static readonly ICON: string = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAkCAYAAACaJFpUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MkIxRTY0QjMzRDJCMTFFODlFMkRFMjQyRkRCMjQwMDEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MkIxRTY0QjQzRDJCMTFFODlFMkRFMjQyRkRCMjQwMDEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyQjFFNjRCMTNEMkIxMUU4OUUyREUyNDJGREIyNDAwMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyQjFFNjRCMjNEMkIxMUU4OUUyREUyNDJGREIyNDAwMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PqwCd5cAAAJBSURBVHja7JdPSFRRFMbf0wlLk4IicBFii4iBQjOUqKhAXLhwIa5qURCRkCWCuxZtqpWCiyyCIq1NEYT2B2EWYS100zJdKkFQuQpKR03Hfie+gYfSzNyZ+1x14MfAvLnve/fOOd85L7w7Nv09CILdQfERwmvohq/RC1fbk5t+nIBqmIX3UO4gtA7b4AR0wHa4Al9yLTLBMkjxND0bLw69msknamsHoQ5aYBguw1yuBRY7ijzOStgLn+EGHIcROJhPsJSwe/yCfrgOx7TTZFyCFhVKnkfQC/XwmL/kcFyCWVGLB3ANTGwY0fq4BKNhO+3SsY4g2uBbcA2WNnz3BM5DDTzM7tSHYEb1uEtZu1NUUWov+eyDQxJtTHjI0GqVQQpWlTzZOl6VQdiDNMK9UgVX4C381E1DCURtL6MyOQlNpQou6796oRvnOvYB2F+qoO1mUWyKqHlzvCtxlsW/fNtJ8DQ0+/DBQp/ulhLEPHNf3ILlqq09sq130OnYP52dxhrsMzinrHuq7Ey6CLsmjdXVuJrtEJyBKbWlmmjR++yHtmZelmWjxSTcgefQpqP3KhiNCYnchlPwBi7krQ3HI42G+ehROKKOYaPGgi/BUFZm40SV5hdLoIvwTcOUeeYnX4IZZWoTXJJYGu7bOAEfC7YbhybbCme1btTajbLUzd8K3F1aSfZBc0uqaEMtIH7DTRX4uHYbxCkYSMjLELul8V8wNsH0Fmj91Qh5A16UB7q+kLpO5jai1CY0Ux4QccePPwIMACFgfcSkSv7EAAAAAElFTkSuQmCC';
      private _isLoadingPage:boolean=false;

      // 로딩된 페이지가 존재하는지 확인용으로 사용하는 변수
      // 스크린샷을 찍기위한 용도로 사용.
      // 즉, 페이지 한번이라도 로딩된 페이지가 존재하는 경우
      private _isPrevPage:boolean=false;
      public get isLoadingPage(){
            return this._isLoadingPage;
      }

      private _isShowFirstPage:boolean=false;
      public get isShowFirstPage(){
            return this._isShowFirstPage;
      }

      /*
      롤링 처리를 위한 페이지 컴포넌트 2개
       */
      private _pageElementComponent1:PageElementComponent;
      private _pageElementComponent2:PageElementComponent;


      // 현재 보여지고 있는 페이지 컴포넌트
      private _currentElementComponent:PageElementComponent=null;
      public get currentElementComponent():PageElementComponent{
            return this._currentElementComponent;
      }
      // 이전 페이지 컴포넌트(화면에 보이지 않는 경우 모두 clear()처리 됨..
      private _prevElementComponent:PageElementComponent=null;


      private _progressBar:IProgressbar = new LoadingProgressbar();
      private _rollingPageNavigator:RollingPageNavigator = null;

      private _realDataProvider:RollingPageInfo[] = [];


      public get currentIndex():number{
            return this._rollingPageNavigator.currentIndex;
      }

      public get totalPageCount():number{
            if(this._realDataProvider){
                  return this._realDataProvider.length;
            }

            return 0;
      }

      public get autoPlaying():boolean{
            return this._rollingPageNavigator.autoPlaying;
      }

      get usingThreeLayer(){
            /*
            2018.12.28(ckkim)
            PageELementComponent를 사용하는 페이지 컴포넌트 류에서는
            threeLayer를 사용하지 못하게 처리함.

            만약 일반  페이지 컴포넌트에서도 threeLayer를 사용하고자 한다면
            테스트를 좀 더 해야함.
            return this.getGroupPropertyValue("createLayerInfo", "usingThreeLayer");
            */

            return false;
      }

      get progressBar(){
            return this._progressBar;
      }

      //중요!: page정보 로딩 취소 용
      private _cancelSource = null;

      /*
      ComponentInstanceLoader2와 ComponentInstanceLoader와의 차이점
      ComponentInstanceLoader2에는
            - ajax을 통한 페이지 정보 읽기 취소
            - resource 로딩 읽기 취소 등의 기능이 추가되어 있음.
       */
      private _comInstanceLoader2:ComponentInstanceLoader2 = new ComponentInstanceLoader2();
      private _componentResourceLoader:ComponentResourceLoader = null;



      constructor() {
            super();

            this._progressBar = new LoadingProgressbar();
            this._rollingPageNavigator = new RollingPageNavigator(this);

      }

      protected _onDestroy() {

            if(this._comInstanceLoader2){
                  this._comInstanceLoader2.destroy();
                  this._comInstanceLoader2 = null;
            }

            // 롤링 페이지 컴포넌트가 특정 페이지를 로딩하고 있는 시점에서 페이지가 변경되는 경우
            if(this._cancelSource){
                  this._cancelSource.clear();
                  this._cancelSource = null;
            }


            if(this._pageElementComponent1) {
                  this._pageElementComponent1.destroy();
                  this._pageElementComponent1 = null;
            }

            if(this._pageElementComponent2){
                  this._pageElementComponent2.destroy();
                  this._pageElementComponent2 = null;
            }

            this._currentElementComponent = null;
            this._prevElementComponent = null;




            if(this._rollingPageNavigator){
                  this._rollingPageNavigator.destroy();
                  this._rollingPageNavigator = null;
            }


            this._progressBar = null;
            this._realDataProvider = null;

            super._onDestroy();
      }

      getExtensionProperties(){
            return true;
      }

      public setMouseEventEnabled(enabled:boolean){
            if(this._pageElementComponent1) {
                  this._pageElementComponent1.setMouseEventEnabled(false);
            }

            if(this._pageElementComponent2) {
                  this._pageElementComponent2.setMouseEventEnabled(false);
            }

            super.setMouseEventEnabled(enabled);
      }


      public play(){
            if(this.isViewerMode){
                  this._rollingPageNavigator.play();
            }
      }

      public stop(){
            if(this.isViewerMode){
                  this._rollingPageNavigator.stop()   ;
            }
      }


      public prevPage(){
            if(this.isViewerMode){
                  this._rollingPageNavigator.prevPage();
            }
      }

      public nextPage(){
            if(this.isViewerMode){
                  this._rollingPageNavigator.nextPage()   ;
            }
      }

      public showPageAt(index:number){
            if(this.isViewerMode){
                  this._rollingPageNavigator.showPageAt(index)   ;
            }
      }




      public get currentPageIndex():number{
            return this._rollingPageNavigator.currentIndex;
      }



      /*
      1. layer를 생성 한후
      2. 수동으로 layer를 붙여야 함.
      3. layer를 붙인 후 layer의 parent를 RollingPageComponent로 적용해야함.
       */
      protected _onCreateElement(template:string=null) {
            let $element = $(this._element);
            if(this.isViewerMode) {


                  this._pageElementComponent2 = new PageElementComponent();
                  this._pageElementComponent2.executeViewerMode();
                  this._pageElementComponent2.create({
                        id: ObjectUtil.generateID(),
                        name: "1",
                        x: 0,
                        y: 0,
                        visible:false
                  })
                  $element.append(this._pageElementComponent2.appendElement);

                  // 수동으로 붙였기 때문에 이벤트를 발생시켜 줘야함.
                  this._pageElementComponent2.dispatchEvent(new WVEvent(WVEvent.ADDED, this._pageElementComponent2));


                  // 롤링 처리를 위해 PageElementComponent 객체를 2개 생성
                  this._pageElementComponent1 = new PageElementComponent();

                  this._pageElementComponent1.executeViewerMode();
                  this._pageElementComponent1.create({
                        id: ObjectUtil.generateID(),
                        name: "2",
                        x: 0,
                        y: 0
                  })
                  $element.append(this._pageElementComponent1.appendElement);

                  // 수동으로 붙였기 때문에 이벤트를 발생시켜 줘야함.
                  this._pageElementComponent1.dispatchEvent(new WVEvent(WVEvent.ADDED, this._pageElementComponent1));



                  // _pageElementComponent1을 기본으로 설정하기
                  this._currentElementComponent= this._pageElementComponent1;


            }else{
                  $element.attr("data-compname", "page-component");
                  $element.append('<div style="width:100%;height:100%;display:flex;justify-content:center;align-items:center;"><img style="max-width:28px;width:100%;max-height:36px;height:100%;" src='+RollingPageComponent.ICON+' /></div>');
            }
      }


      ////////////////////////////////////////////////////////////
      protected  _onCommitProperties():void{
            if(this._updatePropertiesMap.has("extension")){
                  // this._isLoadingPage=false;
                  //
                  //
                  // //console.log("##페이지 컴포넌트, 1-2. 로딩전 레이어 숨기기");
                  // // 로드할 내용을 모두 지운다.
                  // // 인스턴스만 살리고 페이지 관련 내용을 모두 삭제
                  // this._currentElementComponent.resetPage();
                  // // visible처리시 자식 요소를 숨길 수 없음
                  // this._currentElementComponent.visible=false;
                  // // 레이어 숨기는 작업은 따로 해줘야 함.
                  // this._currentElementComponent.hiddenAllLayer();
                  //
                  //
                  // /*
                  // unload 되기 전 (before) 이벤트 발생
                  // //console.log("1-3. 페이지 정보 로딩 전 unload wscript event 발생");
                  // */
                  // //console.log("## RollingPageComponent, 1-3. 페이지 정보 로딩 전 unload wscript event 발생");
                  //
                  //
                  // // 로더 초기호
                  // this._resetLoader();


                  this.validateCallLater(this._validatePage);
            }


      }


      /*
      페이지 데이터가 변경되는 경우 실행

            1. editor모드에서는 실행되지 않음.
            2. provider 값 구하기
            3. 유효성 검사, provider에서 실제 페이지가 존재하는 내용만 추려 내기
            4. navigator에 provider 적용하기
            5. 오토 플레이 유무 처리
       */
      private _validatePage(){
            //1. editor모드에서는 실행되지 않음.
            if(this.isEditorMode==true)
                  return;


            //2. provider 값 구하기
            let datas:any[]=this.getGroupPropertyValue("extension", "dataProvider");

            //3. 유효성 검사, provider에서 실제 페이지가 존재하는 내용만 추려 내기
            this._realDataProvider= [];
            datas.forEach((data)=>{
                  let pageInfo:any = window.wemb.pageManager.getPageInfoBy(data.id);

                  if(pageInfo){
                        let rollingPageInfo:RollingPageInfo = new RollingPageInfo();
                        rollingPageInfo.pageName = pageInfo.name;
                        rollingPageInfo.pageID = data.id;
                        // 기본값 설정
                        if(!data.duration)
                              data.duration = 1;

                        rollingPageInfo.duration = parseInt(data.duration)*1000;

                        this._realDataProvider.push(rollingPageInfo);
                  }
            })


            //4. navigator에 provider 적용하기
            this._rollingPageNavigator.setDataProvider(this._realDataProvider);


            //5. 오토 플레이 유무 처리
            let autoPlay:boolean = this.getGroupPropertyValue("extension", "autoPlay");
            this._rollingPageNavigator.showFirstPageAndAutoPlay(autoPlay);
      }



      /*
      컴포넌트가 container에 추가되면 자동으로 호출
       */
      public _onImmediateUpdateDisplay(): void {
            this._validatePage();

      }

      ////////////////////////////////////////////////////////////////////////









      ////////////////////////////////////////////////////////////////
      /*
      페이지를 로드하기 전에 페이지 내용 삭제하기
       */
      private _resetLoader():void{


            if(this._comInstanceLoader2){
                  this._comInstanceLoader2.stopClearInstanceLoader();
            }


            //2. ajax을 이용한 페이지 정보 로딩 멈추기
            if(this._cancelSource){
                  this._cancelSource.clear();
                  this._cancelSource = null;
            }
      }

      public async showPageAt(index:number){
            if(this._rollingPageNavigator) {
                  return await this._rollingPageNavigator.showPageAt(index);
            }

            return false;
      }


      public async openPageByName(pageName:string):Promise<boolean> {
            let pageId=window.wemb.pageManager.getPageIdByName(pageName);
            return await this._openPageById(pageId);
      }



      /*
      페이지 로딩 순서 .

      1. 에디터 모드에서만 실행..
      2. 로딩 중인경우에는 다음 페이지 로딩 불가능
      3. 기존 열린 페이지가 존재하는 경우


       */
      private async  _openPageById(pageId:string):Promise<boolean> {
            if(this.isEditorMode){
                  //console.log("editor 모드에서는 실행 수 없습니다.");
                  return false;
            }


            var locale_msg = Vue.$i18n.messages.wv;

            if(pageId==null){
                  alert("RollingComponentError : " + locale_msg.page.noPage);
                  return false;
            }

            if(this.isLoadingPage){
                  //console.log("editor 모드에서는 실행 수 없습니다.");
                  return false;
            }



            // currentpage, prevpage 선택하기
            if(this._isPrevPage) {
                  this._prevElementComponent = this._currentElementComponent;

                  if(this._currentElementComponent==this._pageElementComponent1){
                        this._currentElementComponent = this._pageElementComponent2;
                  }else {
                        this._currentElementComponent = this._pageElementComponent1;
                  }
            }



            // 페이지 로딩 상태를 true로 만들기(중요!)
            this._isLoadingPage=true;


            //console.log("##페이지 컴포넌트, 1-2. 로딩전 레이어 숨기기");
            // 로드할 내용을 모두 지운다.
            // 인스턴스만 살리고 페이지 관련 내용을 모두 삭제
            this._currentElementComponent.resetPage();
            // visible처리시 자식 요소를 숨길 수 없음
            this._currentElementComponent.visible=false;
            // 레이어 숨기는 작업은 따로 해줘야 함.
            this._currentElementComponent.hiddenAllLayer();


            /*
	     unload 되기 전 (before) 이벤트 발생
	     //console.log("1-3. 페이지 정보 로딩 전 unload wscript event 발생");
		*/
            //console.log("## RollingPageComponent, 1-3. 페이지 정보 로딩 전 unload wscript event 발생");


            // 로더 초기호
            this._resetLoader();


            // 페이지 정보 읽기
            let pageData:OpenPageData = null;
            try {
                  // 페이지 정보 로딩을 도중에 취소하기 위해 토큰 발생.
                  this._cancelSource = axios.CancelToken.source();
                  //this._cancelSource.cancel()을 이용해 취소 가능.


                  // 페이지 정보 읽기
                  pageData = await window.wemb.pageManager.loadPageDataById(pageId);
                  //console.log("## RollingPageComponent, 2-2. 페이지 정보 읽기 완료");
            }catch(error){
                  //console.log("## RollingPageComponent",error);
                  alert(locale_msg.common.errorLoad+"An error occurred while reading the page information from the page component. Please check the page information.");
                  return;
            }


            try{
                  //페이지 정보 로딩이 정상적으로 완료된 경우 null 처리하기
                  this._cancelSource = null;

                  /*
		     resetProperties() 내부에서는 다음 내용이 호출됨.
			     immedateUpdateDisplay()
			     onAddedOnctainer()호출

		     알림:
			    ViewerPageComponent, EditorPageComponent처럼 따로 배경설정을 할 필요는 없음.
		     */

                  //console.log("## RollingPageComponent 2-3. 페이지 정보 설정하기, ");
                  this._currentElementComponent.resetProperties(pageData.page_info);

                  //console.log("## RollingPageComponent 2-4. beforeload wscript event 발생");
                  this._currentElementComponent.dispatchBeforeLoadScriptEvent();



                  //console.log("## RollingPageComponent 3-1. 리소스 로딩바 활성화"); // _comInstanceLoader2내부에서 로딩바 활성화 처리
                  //console.log("## RollingPageComponent 3-2. 화면에 붙일 컴포넌트 인스턴스 정보 생성, 시작");
                  // 레이어별 컴포넌트 정보 생성
                  let comInstanceInfoList: Array<any> = [];
                  comInstanceInfoList = comInstanceInfoList.concat(pageData.content_info.two_layer);

                  // threeLayer를 사용하는 경우에만 컴포넌트 정보 읽기
                  if(this.usingThreeLayer)
                        comInstanceInfoList = comInstanceInfoList.concat(pageData.content_info.three_layer);

                  //console.log("## RollingPageComponent 3-3, 화면에 붙일 컴포넌트 인스턴스 정보 생성 완료, 개수 = ", comInstanceInfoList.length);

                  // 프로그래스 처리를 위해 최대 length 설정
                  this.progressBar.setMaxLength(comInstanceInfoList.length);





                  /* □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□ */
                  // 컴포넌트 인스턴스와 하기 시작
                  //console.log("## RollingPageComponent 3-4. 화면에 인스턴스 붙이기, 시작!");
                  let success = await this._comInstanceLoader2.startInstanceLoader(comInstanceInfoList, this.progressBar, this._currentElementComponent, this.exeMode as EXE_MODE)
                  if(success) {
                        // 정상적으로 이벤트가 로드된 경우
                        //console.log("## RollingPageComponent 3-5. ready wscript event 발생 시작");
                        this._currentElementComponent.dispatchReadyScriptEvent();

                        this._startResourceLoading();
                  }else {
                        //console.log("컴포넌트 생성이 정상적으로 이뤄지지 않았습니다.")
                  }

                  /* □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□ */


            }catch(error){
                  //console.log("## RollingPageComponent",error);
                  alert(locale_msg.common.errorLoad+"An error occurred while creating the component in the page component. Please check the page information.");
            }
      }




      /*
      페이지가 바뀔때 실행.
       */
      private _dispatchChangeScriptEvent(){
            this._componentEventDispatcher.dispatchEvent(RollingPageComponentEvent.CHANGE,{
                  currentIndex:this.currentIndex,
                  totalPageCount:this.totalPageCount,
                  pageInfo:this._realDataProvider[this.currentIndex]
            })
      }
      /*
      컴포넌트 리소스 로딩 시작.
       */
      private _startResourceLoading() {
            let comInstanceList: Array<WVComponent> = this._currentElementComponent.comInstanceList;


            this._componentResourceLoader = new ComponentResourceLoader();
            this._componentResourceLoader.$on(ComponentResourceLoaderEvent.COMPLETED_ALL, () => {
                  this._completedLoadAllResource();
            })

            //console.log("## PageComponent 4-1. PageComponent _startResourceLoading");
            this._componentResourceLoader.startResourceLoading(comInstanceList);
      }
      private _completedLoadAllResource(){

            //console.log("## PageComponent 4-2. 컴포넌트 리소스 읽기, 완료");

            // 리로스 로더 파괴
            if(this._componentResourceLoader) {
                  this._componentResourceLoader.destroy();
                  this._componentResourceLoader = null;
            }


            this._currentElementComponent.isLoaded=true;
            //console.log("## RollingPageComponent 4-3. 화면에 배치된 모든 컴포넌트에 complete wscript 이벤트 발생");
            //console.log("## RollingPageComponent 4-4. 화면에 배치된 모든 컴포넌트의 onLoadPage 메서도 호출");
            let comInstanceList:Array<WVComponent> = this._currentElementComponent.comInstanceList;
            comInstanceList.forEach((instance:WVComponent)=>{
                  instance.dispatchWScriptEvent(WVComponentScriptEvent.COMPLETED);
                  // onLoadPage를 가지고 있는 컴포넌트만 onLoadPage()메서드 호출
                  if(instance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]){
                        instance[WVCOMPONENT_METHOD_INFO.ON_LAOAD_PAGE]();
                  }
            })


            // 최종 페이지 이벤트 발생
            //console.log("## RollingPageComponent 4-5. 페이지 이벤트 : LOADED 이벤트 발생");
            this._currentElementComponent.dispatchLoadedScriptEvent();


            this._isPrevPage = true;


            this._currentElementComponent.showAllLayer();
            this._currentElementComponent.visible=true;

            this.dispatchEvent(new PageComponentEvent(PageComponentEvent.LOADED,this))
            this.dispatchWScriptEvent(WVComponentScriptEvent.COMPLETED);
            this._isLoadingPage=false;
            ////////////////////////////////////////////////////




            ////////////////////////////////////////////////////
            //old 페이지 초기화 처리하기
            if(this._prevElementComponent){
                  this._prevElementComponent.visible=false;
                  this._prevElementComponent.hiddenAllLayer();
                  /*
                unload 되기 전 (before) 이벤트 발생
                //console.log("1-3. 페이지 정보 로딩 전 unload wscript event 발생");
	            */
                  //console.log("## RollingPageComponent, 1-3. 페이지 정보 로딩 전 unload wscript event 발생");
                  this._prevElementComponent.dispatchBeforeUnLoadScriptEvent();

                  // 레이어만 남겨두고 모든 내용을 삭제한다..
                  this._prevElementComponent.resetPage();

                  //console.log("## RollingPageComponent, 1-5. 페이지 닫은 후 unloaded wscript event 발생  ");
                  this._prevElementComponent.dispatchUnLoadedScriptEvent();
            }

            // 페이지 변경 이벤트 발생.
            this._dispatchChangeScriptEvent();


            //console.log("## RollingPageComponent 4-4. 페이지 로딩, 리소스 로딩, 모든 처리 완료!");
      }
}


class RollingPageInfo{

      public pageName:string;
      public pageID:string;
      public duration:number=1000;
      public isFirstPage:boolean=false;
      constructor(){

      }
}



class RollingPageNavigator {

      private _currentIndex:number=0;
      public get currentIndex(){
            return this._currentIndex;
      }

      private _currentRollingPageInfo:RollingPageInfo;
      private get currentRollingPageInfo():RollingPageInfo{
            return this._currentRollingPageInfo;
      }


      private _firstRollingPageInfo:RollingPageInfo;
      private get firstRollingPageInfo():RollingPageInfo{
            return this._firstRollingPageInfo;
      }

      private _dataProvider:RollingPageInfo[];

      private _currentTimerID:number = -1;


      private _isAutoPlaying:boolean=false;

      private _loadedPageHandler = null;
      public get autoPlaying():boolean{
            return this._isAutoPlaying;
      }

      constructor(private _rollingPageComponent:RollingPageComponent){
            this._dataProvider=[];

            /*
            페이지가 열린 후
             */

            this._loadedPageHandler = this._onLoadedPage.bind(this);
            this._rollingPageComponent.addEventListener(PageComponentEvent.LOADED,this._loadedPageHandler);
      }


      private _onLoadedPage(){
            if(this._isAutoPlaying){
                  this.play();
            }
      }
      public destroy(){
            this._rollingPageComponent.removeEventListener(PageComponentEvent.LOADED, this._loadedPageHandler);
            this._clearAutoPlay();
      }

      public setDataProvider(data:RollingPageInfo[]) {
            this._clearAutoPlay();
            this._dataProvider = data;
      }

      // 첫 번째 롤링 페이지 정보 찾기
      private _updateFirstRollingPageInfo(){

            this._firstRollingPageInfo =null;
            if(this._dataProvider==null || this._dataProvider.length<=0){
                  //console.log("롤링 페이지 컴포넌트에서 데이터가 존재하지 않음.")
            }


            let check:boolean=false;

            for(let i=0;i<this._dataProvider.length;i++){
                  let info:RollingPageInfo = this._dataProvider[i];
                  if(info.isFirstPage){
                        this._firstRollingPageInfo = info;
                        this._currentIndex=i;
                        check=true;
                        break;
                  }
            }

            if(check==false){
                  this._firstRollingPageInfo = this._firstRollingPageInfo[0];
                  this._currentIndex=0;
            }
      }


      /* 첫 번째 페이지를 보여줌과 동시에 autoPlay */
      public async showFirstPageAndAutoPlay(autoPlay:boolean=false){
            if(this._dataProvider==null || this._dataProvider.length<=0){
                  //console.log("롤링 페이지 컴포넌트에서 읽어들일 페이지 정보가 존재하지 않습니다.");
                  return;
            }

            this._currentIndex =0;
            this._isAutoPlaying = autoPlay;
            let open = await this.showPageAt(this._currentIndex);



            return open;

      }


      public play(){
            this._isAutoPlaying= true;

            if(this._rollingPageComponent.isLoadingPage==true)
            //console.log("페이지 로딩 중에는 play() 기능을 실행 할 수 없습니다.");


                  this._startAutoPlay();
      }

      public stop(){
            this._isAutoPlaying= false;
            if(this._rollingPageComponent.isLoadingPage==true)
            //console.log("페이지 로딩 중에는 stop() 기능을 실행 할 수 없습니다.");

                  this._clearAutoPlay();
      }




      public nextPage(){
            if(this._rollingPageComponent.isLoadingPage==true){
                  //console.log("현재 페이지를 로딩하고 있습니다.");
                  return;
            }

            let nIndex = this._currentIndex+1;
            if(nIndex>=this._dataProvider.length)
                  nIndex =0;

            this.showPageAt(nIndex);
      }


      public prevPage(){
            if(this._rollingPageComponent.isLoadingPage==true){
                  //console.log("현재 페이지를 로딩하고 있습니다.");
                  return;
            }

            let nIndex = this._currentIndex-1;
            if(nIndex<0) {
                  /*
                  2019.02.24(ckkkim)
                  오류 수정
                   */
                  nIndex = this._dataProvider.length - 1;
            }

            this.showPageAt(nIndex);
      }


      public async showPageAt(index:number) {
            if(this._dataProvider==null || this._dataProvider.length==0){
                  //console.log("데이터가 존재하지 않습니다.");
                  return;
            }

            // 0에서 length-1만큼 index 처리
            if(index<0){
                  index=0;
            }

            if(index>=this._dataProvider.length){
                  index = this._dataProvider.length-1;
            }

            // index에 해당하는 롤링 페이지 정보 구하기.
            this._currentIndex = index;
            this._currentRollingPageInfo = this._dataProvider[index];

            let isOpen: boolean = await this._rollingPageComponent.openPageByName(this._currentRollingPageInfo.pageName);


            return isOpen;
      }




      private _clearAutoPlay(){
            clearTimeout(this._currentTimerID);
            this._currentTimerID=-1;
      }

      private _startAutoPlay(){

            this._clearAutoPlay();
            this._currentTimerID=setTimeout(()=>{

                  this.nextPage();
            }, this._currentRollingPageInfo.duration)
      }

}
export class RollingPageComponentEvent {
      public static readonly CHANGE: string = "change";
}



/*
{
                pageId:"a051a4d1-0a67-4b5d-9a13-29472a6dd6be",
                pageName:"asset1",
                duration:3000,
                isFirstPage:true

          },{
                pageId:"41543b36-68f5-420c-972e-982f2e843121",
                pageName:"asset2",
                duration:5000,
                isFirstPage:false

          }
 */

WVComponentPropertyManager.attach_default_component_infos(RollingPageComponent, {
      "info":{
            "version":"1.0.0",
            "componentName":"RollingPageComponent"
      },

      "setter": {
            "width": 600,
            "height": 400
      },

      "extension":{
            "dataProvider":[],
            "autoPlay":true
      },

      "style": {
            "border": "1px solid #000000",
            "backgroundColor": "rgba(255, 255, 255, 0)",
            "borderRadius": 1,
            "overflow":"hidden"
      }
});

// 이벤트 정보
WVComponentPropertyManager.add_event(RollingPageComponent, {
      name: "change",
      label: "change",
      description: "페이지가 변경될때 발생하는 이벤트",
      properties: []
});


// 프로퍼티 패널에서 사용할 정보 입니다.
RollingPageComponent["property_panel_info"] = [{
      template: "primary"
},
      {
            template: "pos-size-2d"
      }, {
            template: "border"
      }, {
            label: "Style",
            template: "vertical",
            children: [{
                  owner: "style",
                  name: "overflow",
                  label: "Overflow",
                  type: "select",
                  options: {
                        items: [
                              {label: "hidden", value: "hidden"},
                              {label: "auto", value: "auto"},
                              {label: "scroll", value: "scroll"}
                        ]
                  },
                  writable: true,
                  show: true,
                  description: "overflow"
            }]
      }, {
            label: "Auto Play",
            template: "vertical",
            children: [{
                  owner: "extension",
                  name: "autoPlay",
                  label: "Use",
                  type: "checkbox",
                  writable: true,
                  show: true,
                  description: "autoplay"
            }]
      }
];



WVComponentPropertyManager.remove_property_group_info(RollingPageComponent, "label");
