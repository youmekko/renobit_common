
      /*
      전체 페이지 정보 읽어올 때 사용.
      */
      /*
	export class MasterPageData {
            public page_list:Array<PageInfo> = [];
            public master_info ={
                  master_layer: [],
                  background:new BackgroundInfo()
            }
      }
*/


      export class ProjectInfoData {
            public page_list:Array<PageInfo> = [];
            public stage_info ={
                  background:new BackgroundInfo()
            }
            public master_info={
                  master_layer: [],
                  background:new BackgroundInfo(),

                  /* 2018.09.09 ckkim
                        script관련 기능 추가.
                   */
                  scripts:{
                        global:"var test1=10;"
                  }
            }

      }







      /*
      페이지 저장시에 사용
       */
      export class SavePageData {
            public data ={
                  page_info:{
                        id:"",
                        name:""
                  },
                  master_info: {
                        background:new BackgroundInfo(),
                        master_layer: [],
                        scripts:{}
                  },
                  stage_info: {
                      background: new BackgroundInfo()
                  },
                  content_info:{
                        two_layer:[],
                        three_layer:[]
                  }
            }

            getJSON(){
                  return JSON.stringify(this.data);
            }


            // 페이지 저장용 json 구하기 (type:two_three인 경우 2d, 3d 내용 만
            getPageToJSON(){
                  let temp  = $.extend(true, {}, this.data);
                  delete temp.master_info;
                  delete temp.stage_info;
                  return JSON.stringify({data:temp});

            }

            getMasterPageInfoToJSON(){

                  let temp  = $.extend(true, {}, this.data);

                  delete temp.page_info;
                  delete temp.content_info;
                  delete temp.stage_info;
                  return JSON.stringify({type:"master", data:temp});
            }

      }
      ////////////////////////////////////





      export type OpenPageData ={
            page_info:PageInfo
            content_info:ContentInfo
      }




      ////////////////////////////////////
      // 기본 페이지 정보
	export type PageInfo ={
            type:string;
            master:string;
		version?:string;
		title?:string;
		id:string;
		name:string;
		regDate?:string;
		secret?:SECRET_TYPE;
		props:{
		      setter:any,
                  background:any,
                  events:any,
                  script:any
            }
	}



	export type ContentInfo= {
		two_layer:Array<any>;
		three_layer:Array<any>;
            master_layer:Array<any>;
	}

	export enum SECRET_TYPE {
		SECRET_YES="Y",
		SECRET_NO="N"
	}

	export class StageBackgroundInfo {
		public background={
		      color:"",
                  path:"",
                  image:""
            }
	}

      export class MasterBackgroundInfo {
        public background= {
              color: "",
              path: "",
              image: ""
        }
      }
      export class BackgroundInfo {

            public color:string="";
            public path:string="";
            public image:string="";
            public using:boolean=false;

      }
