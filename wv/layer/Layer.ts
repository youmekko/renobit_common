import {IWVEvent} from "../../core/events/interfaces/EventInterfaces";
import WVComponent from "../../core/component/WVComponent";
import WVDisplayObjectContainer from "../../core/display/WVDisplayObjectContainer";
import {EXE_MODE} from "../data/Data";
import {ILayer} from "./interfaces/LayerInterfaces";

export enum WORK_LAYERS {
      BG_LAYER="bgLayer",
      MASTER_LAYER ="masterLayer",
      TWO_LAYER="twoLayer",
      DRAW_LAYER="drawLayer",
      THREE_LAYER="threeLayer",
      NONE_LAYER="noneLayer",
      PAGE_CONTAINER="pageContainer",
      CONTROL_LAYER="controlLayer",
      DRAG_AREA_LAYER = "dragAreaLayer"
}


export class LayerEvent implements IWVEvent {
      public static readonly COMPONENT_MOUSE_DOWN:string="LayerEvent.componentMouseDown";
      public static readonly COMPONENT_DOUBLE_CLICK:string="LayerEvent.componentDoubleClick";
      public static readonly LAYER_MOUSE_DOWN:string="LayerEvent.layerMouseDown";
      public static readonly DRAG_SELECT:string ="LayerEvent.dragSelectIn";

      public type:string;
      public target:any;
      public originalEvent:any;
      public data?:{
            targetInstance:any
      };

      constructor(type:string, target:any, originalEvent?:any, data?:any){
            this.type = type;
            this.target = target;
            this.originalEvent = originalEvent;
            this.data = data;
      }
}



/*
EditorProxy에 적용.
 */
export class LayerInfo {
      name:WORK_LAYERS = WORK_LAYERS.NONE_LAYER;
      instanceList:Array<WVComponent> =[];
      startZIndex:number=0;
      instanceName:string="";
      injectElements?:any= null;	// 컴포넌트에 주입할 정보

      /*
      배치 컴포넌트를 클래스별로 그룹핑 시키기
       */
      get instanceListGroup():Array<ComponentGroup>{
            let groupListMap = new Map();

            this.instanceList.forEach((comInstance:WVComponent)=>{
                  let name = comInstance.componentName;
                  let items = new Array();


                  if(groupListMap.has(name)==true){
                        items = groupListMap.get(name);
                  }

                  items.push(comInstance);
                  groupListMap.set(name, items);

            })

            let groupList:Array<ComponentGroup> = [];
            groupListMap.forEach((value:Array, key:string)=>{
                  groupList.push({
                        name:key,
                        items:value
                  })
            })

            groupList.sort((a,b)=>{
                  let nameA = a.name.toUpperCase();
                  let nameB = b.name.toUpperCase();
                  if(nameA < nameB)
                        return -1;

                  if(nameA > nameB)
                        return 1;

                  return 0;
            });


            groupList.forEach((group:ComponentGroup)=>{
                  group.items.sort((a,b)=>{
                        let nameA = a.name.toUpperCase();
                        let nameB = b.name.toUpperCase();
                        if(nameA < nameB)
                              return -1;

                        if(nameA > nameB)
                              return 1;

                        return 0;
                  })
            })


            return groupList;
      }


}

export class ComponentGroup {
      public name:string="";
      public items:Array<String | Boolean> = [];
}


export abstract class LayerCore extends WVDisplayObjectContainer implements ILayer {
      protected _exeMode:string=(EXE_MODE.EDITOR as string);
      public get exeMode(){
            return this._exeMode;
      }


      public get isEditorMode(){
            return this._exeMode == EXE_MODE.EDITOR;
      }

      public get isViewerMode(){
            return this._exeMode  == EXE_MODE.VIEWER;
      }

      public abstract updateSize(width: number, height: number, borderRadius: object): void;
      public abstract clear(): void;

      protected constructor(){
            super();
      }


      /*
       layer에 이벤트 기능 추가
       */
      public on(eventName:string, listener:Function){
            if(this.element){
                  $(this.element).on(eventName, listener)
            }
      }

      public off(eventName:string, listener:Function){
            if(this.element){
                  $(this.element).off(eventName, listener)
            }
      }
}
