import WVComponent from "../../core/component/WVComponent";
import WVComponentEvent from "../../core/events/WVComponentEvent";
import EditorStatic from "../../../editor/controller/editor/EditorStatic";
import {IMetaProperties, IWVDisplayObject} from "../../core/display/interfaces/DisplayInterfaces";
import {LayerCore, LayerEvent, WORK_LAYERS} from "./Layer";




/*
레이어 기본
 */
export abstract class TwoLayerCore extends LayerCore{

      private componentInstanceListMap:Map<any, WVComponent> = new Map();
      protected constructor(lockClear:boolean=false) {
            super();
      }


      protected _onDestroy(){
            this.clear();


            $(this._element).off();

            this.componentInstanceListMap = null;
            super._onDestroy();
      }

      /*
      레이어는 그대로 둔 상태에서 요소만을 제거하기
       */
      public clear(){
            this.removeChildAll();
            this.componentInstanceListMap.clear();
            $(this._element).empty();
      }




      /*
            컴포넌트에서 발생하는 이벤트를 하나의 이벤트 리스너로 받기 위해서
            컨테이너인 부모에 이벤트를 등록하기 위해
            createElement를 override 처리

       */
      protected _createElement(template:string=null){
            this._element = $(template)[0] || document.createElement("div");
            this._element.style.position = "absolute";
            // 2d,3d 레이어 컴포넌트에서 발생하는 mousedown 이벤트를 하나에서 처리
            $(this._element).on("mousedown", ".wv-component", this._onComponentMouseDown.bind(this));
      }

      protected _createProperties(properties: IMetaProperties):any{
            try {

                  // 기본 값 생성하기
                  let newProperties: any = $.extend(true, {}, this._getDefaultProperties());
                  // 기본 값 + 입력 값 조합하기
                  $.extend(true, newProperties, properties);

                  return newProperties;
            } catch (error) {
                  console.log("경고 컴포넌트 프로퍼티 스타일이 맞지 않습니다.", error);
                  return null;
            }
      }









      private _getDefaultProperties():any {
            if (this.constructor.hasOwnProperty("default_properties")) {
                  return this.constructor["default_properties"];
            }
            return null;
      }



      /*
      컴포넌트에서 마우스 down이 눌렸는지 유무 처리
      컴포넌트가 선택되면
       */
      private _onComponentMouseDown(event){
            let comInstance:WVComponent = this.componentInstanceListMap.get(event.target as WVComponent);
            if(comInstance){
                  //console.log("컴포넌트에서 mousedown 이벤트 발생!!!! COMPONENT_MOUSE_DOWN이벤트 발생");
                  event.stopPropagation();
                  event.preventDefault();
                  this.notifyComponentEvent(new LayerEvent(LayerEvent.COMPONENT_MOUSE_DOWN, this,  event.originalEvent,{
                        targetInstance:comInstance
                  }));
            }
      }



      /*
      크기 처리
      EditorPageComponent에서 실행.
       */
      public updateSize(width: number, height: number, borderRadius: object): void {
            this.width = width;
            this.height =height;
            this.borderRadius = borderRadius;
      }


      /*
      마우스 이벤트 활성화/비활성화 처리
       */
      public setMouseEventEnabled(enabled:boolean){
            $(this.element).css("pointer-events", "none");
            for(let i=0;i<this.numChildren;i++){
                  this.getChildAt(i).setMouseEventEnabled(enabled);
            }

      }


      /*
      컨테이너에 신규 자식이 추가된 후 자동으로 실행되는 템플릿 요소 메서드
       */
      public onAddedChild(comInstance:any){
            // element클릭시 element에 해당하는 컴포넌트 인스턴스를 찾기 위해 저장하기
            this.componentInstanceListMap.set(comInstance.element, (comInstance as WVComponent));

            comInstance.addEventListener(WVComponentEvent.SELECTED, this.onSelected.bind(this));
            comInstance.addEventListener(WVComponentEvent.UN_SELECTED, this.onUnSelected.bind(this));
      }

      private  onSelected(event:WVComponentEvent){
            let comInstance:WVComponent = event.target as WVComponent;
            if(comInstance.layerName!=WORK_LAYERS.THREE_LAYER)
                  $(comInstance.element).addClass(EditorStatic.SELECTED_CLASS_NAME);
            else {
                  console.log("#### alert 클래스를 추가할 수 없음.")
            }
      }

      private onUnSelected(event:WVComponentEvent){

            let comInstance:WVComponent = event.target as WVComponent;
            if(comInstance.layerName!=WORK_LAYERS.THREE_LAYER)
                  $(comInstance.element).removeClass(EditorStatic.SELECTED_CLASS_NAME);
            else {
                  console.log("#### alert 클래스를 제거할 수 없음.")
            }
      }


      /*
      엘리먼트로 추가되는 기능
       */
      public getChildByElement(element:any){
            let comps = this._childs.filter((component:IWVDisplayObject)=>{
                  return component.element == element;
            });
            if( comps.length ){
                  return comps[0];
            }
            return null;
      }

}
