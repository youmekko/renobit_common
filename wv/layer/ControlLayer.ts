import WVTransformToolMG from "../../core/freeTransform/WVTransformToolMG";
import {TwoLayerCore} from "./TwoLayerCore";
import TransformEvent from "../../core/freeTransform/event/TransformEvent";
import EditorStatic from "../../../editor/controller/editor/EditorStatic";

export class ControlLayer extends TwoLayerCore {
      public static readonly NAME="ControlLayer"
      public transformMG:WVTransformToolMG = null;


      constructor() {
            super();
            this.transformMG = WVTransformToolMG.getInstance();
      }

      _createElement(){
            super._createElement();
            this.transformMG.draw(this.element);
            this.transformMG.addEventListener(TransformEvent.DOUBLE_CLICK, (event:TransformEvent)=>{
                  if(event.data.select!=null) {
                        window.wemb.editorFacade.sendNotification(EditorStatic.CMD_SHOW_EXTENSION_POPUP, event.data.select);
                        if(event.data.select.openTemplate && typeof event.data.select.openTemplate === "function"){
                              event.data.select.openTemplate();
                        }
                  }
            });
      }

      // 트렌스폼 툴 리셋 처리
      public resetTransformToolMG(){
            this.transformMG.target=[];
      }

}

ControlLayer["default_properties"] = {
      "category": "2D",
      "lock": false,
      "x": 0,
      "y": 0,
      "borderRadius": {
            topLeft:0,
            topRight:0,
            bottomLeft:0,
            bottomRight:0
      },
      "width": 100,
      "height": 100,
      "id": "",
      "name": "",
      "depth": 1,
      "visible": true
}

