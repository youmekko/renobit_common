export interface ILayer {
      name:string;
      updateSize(width: number, height: number, borderRadius: number): void;
      setMouseEventEnabled(enabled:boolean):void;
      immediateUpdateDisplay():void;
      clear():void;
      readonly appendElement;
      readonly numChildren;
      visible:boolean;
      destroy();


}
