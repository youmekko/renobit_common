import {EXE_MODE} from "../data/Data";
import {TwoLayerCore} from "./TwoLayerCore";

export class ViewerTwoLayer extends TwoLayerCore {
      public static readonly NAME="ViewerTwoLayer";

      constructor() {
            super();
            this._exeMode=EXE_MODE.VIEWER;

      }
}

ViewerTwoLayer["default_properties"] = {
      "category": "2D",
      "lock": false,
      "x": 0,
      "y": 0,

      "width": 100,
      "height": 100,
      "id": "",
      "name": "",
      "depth": 1,
      "visible": true
}

