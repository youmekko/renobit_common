import {TwoLayerCore} from "./TwoLayerCore";

export class DragAreaLayer extends TwoLayerCore {
      public static readonly NAME="DragAreaLayer"
      constructor() {
            super();
      }

      public dragView:HTMLDivElement;
      protected _createElement(template:string=null) {
            super._createElement(template);
            this.dragView = document.createElement("div");
            /*
            2018. 5.11(ddandongne)
            35000은 dragview의 z-index값
            masterLayer에 컴포넌트는 30000부터 시작함.
            추후 35000 값을 환경변수 값으로 변경해야함.

             */
            $(this.dragView).css({
                  "userSelect":"none",
                  "pointer-events":"none",
                  "position":"absolute",
                  "z-index":35000
            })
            $(this.element).append(this.dragView);
      }
}

DragAreaLayer["default_properties"] = {
      "category"  : "2D",
      "lock"      : false,
      "x": 0,
      "y": 0,
      "borderRadius": {
            topLeft:0,
            topRight:0,
            bottomLeft:0,
            bottomRight:0
      },
      "width": 100,
      "height": 100,
      "id": "",
      "name": "",
      "depth": 1,
      "visible": true
}

