export default class WSocket {
    private handlers: any = {};
    private socket: WebSocket;

    constructor(url) {
        this.socket = new WebSocket(url);
        this.socket.onopen = (message)=> {
            console.log('\t__START 1, RENOBIT data 관련 socket 연결', message);
        };
        //웹 소켓이 닫혔을 때 호출되는 이벤트
        this.socket.onclose = (message)=> {
            console.log(message);
        };
        //웹 소켓이 에러가 났을 때 호출되는 이벤트
        this.socket.onerror = (message)=> {
            console.log(message);
        };

        this.socket.onmessage = this.onMessage.bind(this);
    }

    private onMessage(msg) {
        try {
            /*
            {
                "event":"asset/data",
                "data":{
                    "list":[]
                }
            }
            */
            var args = JSON.parse(msg.data);   
            this.emit(args.event, args.data);
        } catch (error) {
            console.log(error);
        }
    }

    private emit(evt,arg) {
        if(this.handlers[evt]) {
            for (var i=0;i<this.handlers[evt].length;i++) {
                try {
                    this.handlers[evt][i](arg);
                } catch(err) {
                    console.log(err);
                }
            }
        }
    }

    public send(msg) {
        this.socket.send(JSON.stringify(msg));
    }

    public on(evt, func) {
        this.handlers[evt] = this.handlers[evt]||[];
        this.handlers[evt].push(func);
    }

    public off(evt, func) {
        var handler = this.handlers[evt];
        if(handler) {
            for(var i=0; i<handler.length;i++) {
                if(handler[i] === func) {
                    handler.splice(i,1);
                    return;
                }
            }
        }
    }
}
