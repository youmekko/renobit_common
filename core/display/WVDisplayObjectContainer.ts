import CPNotification from "../utils/CPNotification";
import WVDisplayObject from "./WVDisplayObject";
import {IMetaProperties, IWVDisplayObject, IWVDisplayObjectContainer} from "./interfaces/DisplayInterfaces";
import WVEvent from "../events/WVEvent";
import WV2DComponent from "../component/2D/WV2DComponent";
import {CPLogger} from "../utils/CPLogger";




export default abstract class WVDisplayObjectContainer extends WVDisplayObject implements IWVDisplayObjectContainer {


      get x(){
            return this._properties.x;
      }
      set x(value:number){
            if(this._properties.x!=value){
                  this._properties.x=value;
                  this.invalidatePosition=true;
                  this.invalidateProperties();
            }
      }

      get y(){
            return this._properties.y;
      }
      set y(value:number){
            if(this._properties.y!=value){
                  this._properties.y=value;
                  this.invalidatePosition=true;
                  this.invalidateProperties();
            }
      }



      get width(){
            return this._properties.width;
      }
      set width(value:number){

            if(this._properties.width!=value){
                  this._properties.width=value;
                  this.invalidateSize=true;
                  this.invalidateProperties();
            }
      }

      get height(){
            return this._properties.height;
      }
      set height(value:number){
            if(this._properties.height!=value){
                  this._properties.height=value;
                  this.invalidateSize=true;
                  this.invalidateProperties();
            }
      }

      get visible():boolean{
            return this._properties.visible;
      }
      set visible(value:boolean){

            if(this._properties.visible!=value){
                  this._properties.visible=value;
                  this.invalidateVisible=true;
                  this.invalidateProperties();
            }
      }










      protected _childs: Array<IWVDisplayObject>;
      protected _ignoreDepthChange: boolean;

      constructor(opt?: any) {
            super(opt);
            this._childs = [];
            this._ignoreDepthChange = false;
      }












      ////////////////////////////////////////////////
      protected _validateAttribute(): void {
            try {
                  // 스타일 클래스에 인스턴스 이름과 컴포넌트명을 추가
                  this.element.classList.add(this.name);
                  this.element.classList.add(this.constructor.name);


                  // element id를 id 속성 값으로 설정
                  this.element.id = this.id;
                  this.element["data-instanceName"] = this.name;
                  this.element["data-id"] = this.id;


            }
            catch (error) {
                  console.log("WVDisplayObjectContainer. validateAttribute error", error);
            }
      }

      protected _validatePosition(): void {
            let domElement: HTMLElement = this.element as HTMLElement;
            $(domElement).css({
                  left: this._properties.x,
                  top: this._properties.y
            })
      }

      protected _validateSize(): void {
            let domElement: HTMLElement = this.element as HTMLElement;
            $(domElement).css({
                  width: this._properties.width,
                  height: this._properties.height
            })
      }

      protected _validateVisible(): void {
            let visible = this._properties.visible;
            this.element.style.display = (visible==false || visible == null) ?"none" :  "" ;
      }


/*
삭제 예정
      public syncLocationSizeElementPropsComponentProps(){
            this._properties.x=parseInt(this.element.style.x);
            this._properties.y=parseInt(this.element.style.y);
            this._properties.width=parseInt(this.element.style.width);
            this._properties.height=parseInt(this.element.style.height);
      }
      */
      ////////////////////////////////////











      ////////////////////////////////////
      get numChildren(): number {
            return this._childs.length
      }


      // 리소스 초기화
      protected _onDestroy(): void {
            this.removeChildAll();
            super._onDestroy();
      }


      addChild(component: IWVDisplayObject): IWVDisplayObject {
            let zIndex = this.numChildren;
            if (!this.hasChild(component)) {
                  this.addChildAt(component, zIndex);
                  return component;
            }
            return this.setChildIndex(component, zIndex);
      }

      addChildAt(component: IWVDisplayObject, index: number): IWVDisplayObject {
            try {
                  if (!this.hasChild(component)) {

                        this.callLaterDispatch(component as WVDisplayObject, new WVEvent(WVEvent.ADDED, this));
                  }
                  //this._childs.splice(index, 0, component);

                  /*
			2018.01.31
			ddandongne
			이름이 없는 경우만 컴포넌트 명 + counter가 들어가야함.
			 */
                  this._childs.push(component);
                  /*
                  임시
                  20108.04.27
                  2D인 경우
                  화면에 붙이기 전에 보이지 않게 처리
                  실제 보이는 처리는 컴포넌트의 visible에서  보이게 처리됨.
                   */
                 // $(component.appendElement).css("display", "none");
                  $(this.element).append(component.appendElement);
                  this.onAddedChild(component);



                  return component;
            } catch (e) {
                  CPNotification.getInstance().throwErrorWithMessage("addChildAt" + e.toString());
            }
            return null;
      }

      removeChild(component: IWVDisplayObject): IWVDisplayObject {
            //let nIndex = this._childs.indexOf(component);
            let nIndex = this._childs.findIndex(function(d) { return d.id === component.id});
            if (nIndex > -1) {
                  return this.removeChildAt(nIndex);
            }
            console.log("제거 할 수 없습니다. 포함되지 않은 컴포넌트 입니다.");
            return null;
      }

      /*
      http://115.21.12.9:8080/share.cgi?ssid=0kkmmqr
            1. displayobjectonainer(layer)에 추가된 appendElement를 jQuery이용해서 삭제 (공통, 화면에 나타나지 않게 됨)
            2. 컴포넌트.destroy() 실행
                  - 컴포넌트._onDestroy() 실행
                        : 주로 컴포넌트에서 작성한 element를 jQuery를 이용해 제거 기능
                  - 부모._onDestory()  실행


       */
      removeChildAt(index: number): IWVDisplayObject {
            try {
                  let comInstance: WVDisplayObject = this._childs.splice(index, 1)[0] as WVDisplayObject;

                  comInstance.appendElement.parentNode.removeChild( comInstance.appendElement );
                  this.dispatchEvent(new WVEvent(WVEvent.REMOVE, comInstance));

                  try {
                        comInstance.destroy();
                  }catch (e) {
                        CPNotification.getInstance().throwErrorWithMessage("removeChildAt 22" + e.toString());
                  }
                  return comInstance;
            } catch (e) {
                  CPNotification.getInstance().throwErrorWithMessage("removeChildAt 33" + e.toString()+"  "+this.constructor.name);
            }
            return null;
      }

      removeChildAll(): void {
            this._ignoreDepthChange = true;
            console.log("컨테이너에서 자식 객체 삭제 시작, 개수 = ", this._childs.length);
            let index =0;
            while (this._childs.length) {
                  this.removeChildAt(0);
                  index++;
            }

            console.log("컨테이너에서 자식 객체 완료", this._childs.length);
            this._ignoreDepthChange = false;
      }

      swapChildren(componentA: IWVDisplayObject, componentB: IWVDisplayObject): void {
            /*
	    let indexA:number = this.getChildIndex(componentA);
	    let indexB:number = this.getChildIndex(componentB);
	    if( indexA > -1 && indexB > -1 ) {
		  let tempDepth:number = componentB.depth;
		  componentB.depth = componentA.depth;
		  componentA.depth = tempDepth;
	    }
	    */
      }

      getChildByName(name: string): IWVDisplayObject {
            let comps = this._childs.filter((component: IWVDisplayObject) => {
                  return component.name == name;
            });
            if (comps.length) {
                  return comps[0];
            }
            return null;
      }

      getChildAt(nIndex: number): IWVDisplayObject {
            if (this._childs.length > nIndex) {
                  return this._childs[nIndex];
            } else {
                  console.log("지정된 인덱스가 범위에 속하지 않습니다. 다시 확인");
            }
            return null;
      }

      getChildIndex(component: IWVDisplayObject): number {
            return this._childs.indexOf(component);
      }

      setChildIndex(component: IWVDisplayObject, index: number): IWVDisplayObject {
            if (!this.hasChild(component)) {
                  CPNotification.getInstance().throwErrorWithMessage("목록에 포함되지 않은 컴포넌트 입니다" + component.toString());
            }
            return this.addChildAt(component, index);
      }

      hasChild(component: IWVDisplayObject): boolean {
            if (this._childs.indexOf(component) > -1) {
                  return true;
            }
            return false;
      }

      _updateChildIndex(startIndex: number): void {
            /*
	    if(!this._ignoreDepthChange){
		  this._childs.sort((compA:IWVDisplayObject, compB:IWVDisplayObject)=>{
			return compA.depth > compB.depth ? 1 : -1;
		  });
		  let i:number;
		  let component:IWVDisplayObject;

		  for( i = startIndex ; i<this.numChildren; i++ ){
			component = this._childs[i];
			component.depth = i;
		  }
	    }
	    */
      }

      protected callLaterDispatch(target: WVDisplayObject, event: WVEvent): void {
            setTimeout(() => {
                  if(target)
                        target.dispatchEvent(event);
            }, 0)
      }

      onAddedChild(component: IWVDisplayObject) {

      }

      ////////////////////////////////////////////////

}
