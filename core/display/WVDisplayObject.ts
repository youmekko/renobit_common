import WVEvent from "../events/WVEvent";
import {
      IDisplayObjectProperties, IMetaProperties, IProperties, IWVDisplayObject, IWVDisplayObjectContainer,
      MethodQueueElement
} from "./interfaces/DisplayInterfaces";
import EventDispatcher from "../events/EventDispatcher";
import {IWVEvent} from "../events/interfaces/EventInterfaces";
import CPNotification from "../utils/CPNotification";
import {CPLogger} from "../utils/CPLogger";


export default abstract class WVDisplayObject implements IWVDisplayObject {

      protected _dispatcher:EventDispatcher
      protected _updatePropertiesMap: Map<string, any> = new Map();
      protected _extraProperties: Map<string, any> = new Map();

      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       public 속성 선언
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
            // 프로퍼티 생성
      protected _successCreateProperties: boolean = false;

      protected _initialize: boolean;

      protected _element: any;
      protected _properties: any;
      protected _parent?: IWVDisplayObjectContainer;
      public tempUpdateData: string = "";


      //////////////////////////////////////////
      // 성공적으로 프로퍼티가 생성되있는지 유무
      get successCreateProperties():boolean{
            return this._successCreateProperties;
      }


      // 초기와 완료(element, properties, element에 기초 properties가 적용되었는지 까지)
      get initialize(): boolean {
            return this._initialize;
      }


      get parent(): IWVDisplayObjectContainer {
            return this._parent;
      }


      get properties(): any {
            return this._properties;
      }



      get element(): any {
            return this._element;
      }

      // 화면에 붙일 최상위 엘리먼트 반환
      get appendElement():any {
            return this._element;
      }

      set id(value:string) {
            if (value != this._properties.id) {
                  this._properties.id = value;
            }
      }

      get id():string {
            return this._properties.id;
      }

      set name(value:string){
            this._properties.name=value;
      }

      get name():string{
            return this._properties.name;
      }

      set master(value:string){
            this._properties.master=value;
      }

      get master():string{
            return this._properties.master;
      }

      set type(value:string){
            this._properties.type=value;
      }

      get type():string{
            return this._properties.type;
      }


      public setMouseEventEnabled(enabled:boolean){
            //CPNotification.getInstance().notifyMessage("WVDiaplYObject.setMouseEventEnabled() 컴포넌트 내부에 추가되는 요소는 여기를 오버라이드해서 사용해 주세요");
      }

      /////////////////////////////////////////////////////

      protected constructor(opt?: any) {
            this._dispatcher = new EventDispatcher();
            this._properties = $.extend({}, opt);
            this._methodQueue = [];

            this._extraProperties = new Map<string, any>();
            this.addEventListener(WVEvent.ADDED, this.onAdded);
            this.addEventListener(WVEvent.BEFORE_ADDED, this.onBeforeAdded);
            this.addEventListener(WVEvent.REMOVE, this.onRemove);

      }
      addEventListener(eventType: string, handler: Function): boolean {
            return this._dispatcher.addEventListener(eventType, handler);
      }

      removeEventListener(eventType: string, handler: Function): boolean {
            return this._dispatcher.removeEventListener(eventType, handler);
      }

      hasEventListener(eventType: string, handler?:Function ): boolean {
            return this._dispatcher.hasEventListener(eventType, handler );
      }

      dispatchEvent(event:WVEvent ): void {
            if(this._dispatcher)
                  this._dispatcher.dispatchEvent(event, this );
      }














      /////////////////////////////////////////////////////
      /*
	1. 프로퍼티 목록 생성 하기 (createElement에서 프로퍼티.template을 사용하기 때문에 properties를 먼저 생성해야함.
	2. element 생성하기
	3. 프로퍼티를 element에 적용하기
	 */
      create(initProperties: IMetaProperties, template: string = null): boolean {
            // INIT_BEFORE 이벤트 발생.
            this.notifyComponentEvent(new WVEvent(WVEvent.BEFORE_CREATE, this));


            // 속성 생성하기
            this._properties = this._createProperties(initProperties);
            if (this._properties) {
                  // 속성 추가 처리
                  this._onCreateProperties();
            }

            if (this._validateInitProperties(this._properties)) {
                  this._successCreateProperties = true;
                  // element 생성 하기
                  this._createElement(template);
                  this._initWScriptEvent();
                  this._completedCreate();
                  return true;
            } else {
                  console.log("#### Core properties do not exist");
                  this.notifyComponentEvent(new WVEvent(WVEvent.FAIL_CREATE, this, "Core properties do not exist"));
                  return false;
            }
      }

      protected abstract _createProperties(properties: IMetaProperties): any;

      /*
      필요한 이벤트 처리 하기
       */
      protected _initWScriptEvent(){
            //CPNotification.getInstance().notifyMessage("_initWScriptEvent() 컴포넌트 내부에 추가되는 요소는 여기를 오버라이드해서 사용해 주세요", this.name);
      }


      /* 기본 프로퍼티 유효성 검사. */
      protected _validateInitProperties(initProperties: IProperties): boolean{
            let properties:IDisplayObjectProperties = initProperties as IDisplayObjectProperties;
            if (properties.hasOwnProperty("id") == false) {
                  return false;
            }

            if (properties.hasOwnProperty("name") == false) {
                  return false;
            }

            return true;
      }


      /* element 생성 */
      protected abstract _createElement(template: string );
      //public onCreateElement() {}

      protected _completedCreate() {
            this._initialize = true;
            this.notifyComponentEvent(new WVEvent(WVEvent.CREATED, this));
      }




      /////////////////////////////////////////////////////////////////////






      /////////////////////////////////////////////////////////////////////
      onBeforeAdded(event: IWVEvent): void {
            this.notifyComponentEvent(new WVEvent(WVEvent.BEFORE_ADDED_CONTAINER, this));
      }

      onAdded(event: IWVEvent): void {
            setTimeout(()=>{
                  this.setParent(event.target);
                  this.immediateUpdateDisplay();
                  this.notifyComponentEvent(new WVEvent(WVEvent.ADDED_CONTAINER, this));
            }, 0);
      }

      setParent(parent:IWVDisplayObjectContainer){
            this._parent = parent
      }


      onRemoveBefore(): void {

      }

      onRemove(event: IWVEvent): void {

      }

      onRemoveDocument(event: IWVEvent): void {

      }

      onRegisterBefore(): void {

      }

      onRegist(event: IWVEvent): void {

      }

      onDestroyBefore(): void {

      }

      protected _onDestroy(): void {}

      public destroy():void{
            this._onDestroy();

            this._clearDeferred();
            this._properties = null;
            this._methodQueue = [];
            this._extraProperties.clear();
            this._extraProperties = null;
            this._updatePropertiesMap.clear();
            this._updatePropertiesMap = null;
            this.removeEventListener(WVEvent.BEFORE_ADDED, this.onBeforeAdded);
            this.removeEventListener(WVEvent.ADDED, this.onAdded);
            this.removeEventListener(WVEvent.REMOVE, this.onRemove);
            this._dispatcher.destroy();
            this._dispatcher        = null;
            this._element           = null;
            this._parent            = null
            //console.log("com destroy  end = ", this.constructor.name);

      }

      protected notifyComponentEvent(event: WVEvent): void {
            this.dispatchEvent(event);
      }
      ////////////////////////////////////////////////////////////

























      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       지연처리를 위한 IDeferredAble 인터페이스 구현
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
      protected _deferredID: number;
      invalidateProperty: boolean;
      invalidateAttribute: boolean;
      invalidateSize: boolean=false;
      invalidatePosition: boolean=false;
      invalidateVisible:boolean=false;



      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       지연 업데이트 처리 관련 시작
       _commitProperties 컴포넌트 속성 갱신에 따라 필요한 요청을
       validateCallLater를 통해 큐에 저장하여 한 번에 실행시킴
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
      public invalidateProperties(): void {
            this._clearDeferred();
            this._deferredID = setTimeout(() => {
                  this._validateProperties();
                  this._deferredID = null;
            }, 0);
      }

      public invalidateImmediateProperties(): void {
            this._clearDeferred();
            this._validateProperties();
      }


      /*
	외부에서 조건에 상관없이 화면을 다시 그리기
	 */
      public immediateUpdateDisplay(event: boolean = false): void {
            this._clearDeferred();
            this._validateAttribute();
            this._validatePosition();
            this._validateSize();
            this._validateVisible();
            this._onImmediateUpdateDisplay();
      }

      /*하위에서 override*/
      protected _onImmediateUpdateDisplay() { }


      /*
		프로퍼티 유효 처리
		메모 : 모든 field(setter)는 값 변경 후 validatePropeties를 호출해줘야함.
	 */
      protected _validateProperties(): void {
            /*
            2018.04.05 임시적으로 풀어 둔 상태
            EditorPageComponent를 container로 상속받은 후 변경할 예정
             */
            //if (this._parent) {

                  //1. 실행할 validate 메서드를 큐에 넣기
                  this._commitProperties();
                  //2. queue에 저장한 메서드를 일괄적으로 호출하기
                  this._dispatchCallLater();
                  //3. validate 함수 호출 후 처리할 내용이 있는경우사용
                  this.onUpdate();


                  this.dispatchEvent(new WVEvent(WVEvent.UPDATE_PROPERTY, this,this._updatePropertiesMap));


                  // validate 변수 모두 초기화
                  this._clearInvalidate();


		//}
      }



      /**
       // 컴포넌트 속성 갱신 처리
       속성을 갱신하며 컴포넌트에 속성에 따른 필요 요청을 결정한다.
       ex ) width, height, => redraw요청
       lineWidth, storke, opacity => style갱신 요청
       기본속성
       x, y, width, height, style관련만 처리 나머지는 상속한 객체에서 담당
       */
      protected _commitProperties(): void {

            if(this.invalidateAttribute){
                  this.validateCallLater(this._validateAttribute);
            }

            if(this.invalidatePosition){
                  this.validateCallLater(this._validatePosition);
            }


            if(this.invalidateSize){
                  this.validateCallLater(this._validateSize);
            }

            if(this.invalidateVisible){
                  this.validateCallLater(this._validateVisible);
            }

            this._onCommitProperties();
      }

      protected abstract _validateAttribute():void;
      protected abstract _validatePosition(): void;
      protected abstract _validateSize(): void;
      protected abstract _validateVisible(): void;

      protected _onCommitProperties(){

      }




      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       지연처리를 위해 큐에 기억한 함수 실행처리
       _methodQueue를 복사 후 바로 초기화 하여 비워줌.
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       */
      private _dispatchCallLater(): void {
            let queues: Array<MethodQueueElement> = this._methodQueue.concat();
            this._methodQueue = [];
            while (queues.length) {
                  let queue: MethodQueueElement = queues.shift();
                  queue.method.apply(this, queue.args);
            }
      }





      /*
	2018.02.04
	ddandongne
		추가적으로 처리해야할 내용
		예) 정렬 등
	 */
      protected onUpdate() {

      }



      protected _clearInvalidate() {
            this.invalidateAttribute=false;
            this.invalidatePosition=false;
            this.invalidateSize=false;
            this.invalidateVisible=false;
            this._updatePropertiesMap.clear();
      }




      protected _clearDeferred(): void {
            if (this._deferredID) {
                  clearTimeout(this._deferredID);
                  this._deferredID = null;
            }
      }


      private _methodQueue: Array<MethodQueueElement>;

      validateCallLater<T>(method: Function, args?: T[]): void {
            let i;
            let queueElement: MethodQueueElement;
            // 중복 유무 체크
            for (i = this._methodQueue.length - 1; i > 0; i--) {
                  queueElement = this._methodQueue[i];
                  if (queueElement.method == method) {
                        return;
                  }
            }
            this._methodQueue.push({method: method, args: args});
      }



      protected deferredCall<T>(method: Function, args?: T[]): void {
            if(method==null)
                  return;

            setTimeout(() => {
                  try {
                        method.apply(this, args);
                  }catch(error){
                        console.log("WVDisplayObject에서 호출할 수 없습니다.");
                  }
            }, 0);
      }





      ///////////////////////////////////////////











      ///////////////////////////////////////////
      toString(): string {
            try {
                  return JSON.stringify(this);
            }catch(error){
                  return "";
            }
      }

      ///////////////////////////////////////////











      ///////////////////////////////////////////
      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
      프로퍼티 처리 관련 부분
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */

      /*
	@groupName : properties를 적용할 그룹 이름
	@properties : 적용할 프로퍼티 목록
	 */
      public setGroupProperties(groupName: string, properties: any) :boolean{

            if (properties == null)
                  return false;

            let propertyNames: Array<any> = Object.keys(properties);
            if (propertyNames.length == 0)
                  return false;

            let isChanged = false;
            let groupOwner: any = this._call_getGroup(groupName);
            if (groupOwner) {
                  propertyNames.forEach((propertyName: string) => {
                        let value = properties[propertyName];
                        isChanged = this._call_setGroupPropertyValue(groupName, propertyName, value);
                  })

                  return isChanged;
            }
            return false;
      }


      /*
      groupname에 해당하는 프로퍼티 정보 구하기

      @groupName: 구할 그룹 이름.

       */
      public getGroupProperties(groupName: string): any {
            return this._call_getGroup(groupName);
      }


      /*
      groupName에 propertyName의 속성 값 변경하기

      @groupName : 그룹 이름
      @propertyName : 프로퍼티 명
      @vlaue: 적용할 값
	 */
      public setGroupPropertyValue(groupName: string, propertyName: string, value: any):boolean {

            return this._call_setGroupPropertyValue(groupName, propertyName, value);
      }


      /*
      동적으로 그룹 생성하기
      call : 현재는 AssetListPanel에서 사용
       */
      public setDynamicGroupPropertyValue(groupName: string, propertyName: string, value: any) {
            if(this._call_getGroup(groupName)==null){
                  this._properties[groupName] ={

                  }
            }

            this._properties[groupName][propertyName] = value;
      }


      /*
      groupName에 propertyName의 속성 값 구하기

      @groupName : 그룹 이름
      @propertyName : 프로퍼티 명

       */
      public getGroupPropertyValue(groupName: string | object, propertyName: string): any {
            if (typeof(groupName)==='string') {
                  if (groupName.indexOf('.') != -1) {
                        let path_arr = groupName.split('.');
                        return path_arr.reduce(function (accumulator, current, index, arr) {
                              if (index === path_arr.length - 1) {
                                    return accumulator[current][propertyName];
                              }
                              return accumulator[current]
                        }, this._properties);
                  } else {
                        let groupOwner = this._call_getGroup(groupName as string);
                        if (groupOwner != null) {
                              return groupOwner[propertyName];
                        }
                  }
            }else{
                  return groupName[propertyName];
            }
            return null;
      }


      ///////////////////////////////////////////////////////////////
      /*
	그룹 오브젝트 구하기.
	@groupName : 그룹 이름
	 */
      protected _call_getGroup(groupName: string): any {
            try{
                  if(groupName.indexOf('.') != -1){
                        let path_arr = groupName.split('.');
                        return path_arr.reduce(function (accumulator, currentValue, index, arr) {
                              if (index === path_arr.length-1) {
                                    return accumulator[currentValue];
                              }
                              return accumulator[currentValue]
                        }, this._properties);
                  }else{
                        if(this._properties.hasOwnProperty(groupName)){
                              return this._properties[groupName];
                        }
                  }
            } catch (e){
                  CPLogger.log("call_getGroup속성 에러", [CPLogger.getCallerInfo(this, "_call_getGroup"), "groupName", groupName,  this._properties ]);
            }
            return null;
      }


      /*
	기존 프로퍼티 값과 신규 프로퍼티 값 비교해서 업데이트 유무 확인하기
	call : 주로 setter에서 실행

	실행순서
	1. 그룹 owner 구하기
	2. 기존 값과 비교
	3. 프로퍼티 값 변경
	4. invalidate 속성 적용 및 invalidateProperties() 메서드 호출
	 */
      protected _call_setGroupPropertyValue(groupName: string, propertyName: string, value: any) {
            //1. 그룹 owner 구하기
            let groupOwner = this._call_getGroup(groupName);
            //2. 기존 값과 비교
            if (this._call_checkSameNewOldValue(groupOwner, propertyName, value) == true) {
                  //3. 프로퍼티 값 변경
                  groupOwner[propertyName] = value;
                  //4. invalidate 속성 적용 및 invalidateProperties() 메서드 호출
                  this._call_setInvalidateGroupPropertyValue(groupName, propertyName, value);
                  return true;
            }

            return false;
      }


      /*
	신규 값과 기존 값 비교
	 */
      protected _call_checkSameNewOldValue(groupOwner: object, propertyName: string, value: any) {
            if (groupOwner) {

                  let _oldValue = this.getGroupPropertyValue(groupOwner, propertyName);
                  if (_oldValue != value) {
                        return true;
                  }
            }

            return false;
      }

      /*
      _updatePropertiesMap에 groupName.propertyName을 키로 업데이트 히스토리 남기기
       */
      protected _call_setInvalidateGroupPropertyValue(groupName: string, propertyName: string, value: any) {
            this._updatePropertiesMap.set(`${groupName}`, true);
            this._updatePropertiesMap.set(`${groupName}.${propertyName}`, value);
            this.invalidateProperty = true;
            this.invalidateProperties();
      }



      ////////////////////////////////////////////////////////////////////////////////////








      ////////////////////////////////////////////////////////////////////////////////////
      protected _registExtraProperty(key:string, value:any ):void {
            let pValue:any = this._extraProperties.get(key);
            if( pValue != value ){
                  this._extraProperties.set(key, value);
            }
      }

      protected _restoreExtraProperty(key:string ):any {
            let pValue:any;
            if(this._extraProperties.has(key)){
                  pValue = this._extraProperties.get(key);
            }
            return pValue;
      }







      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
            hook 메서드 관련 시작
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       * */
      /*프로퍼티 생성 후 추가 내용*/
      protected _onCreateProperties() {}

      //element생성 직후 호출 되는 메서드
      protected _onCreateElement() {}



}




