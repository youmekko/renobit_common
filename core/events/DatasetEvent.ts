import {IWVEvent} from "./interfaces/EventInterfaces";

export default class DatasetEvent implements IWVEvent{

	//public static readonly REQUEST: string = "request";
	public static readonly RESPONDED: string = "responded";
	public static readonly SUCCESS: string = "success";	
    public static readonly ERROR: string = "error";

	public static readonly STATE_CHANGE: string ="state_change";
												//pause, resume, done, before_clear, clear


	type: string;
	target: any;
	data?: any;
	rowData?: any;
	response?:any;
	
	constructor(type: string, target: any, data?: any, response:any=null, rowData:any=null) {
		this.type = type;
		this.target = target;
		this.data = data;
		this.rowData = rowData;
		this.response = response;
	}
}