
export interface IEventDispatcher {
      addEventListener(eventType: string, handler: Function): boolean;
      removeEventListener(eventType: string, handler: Function): boolean;


      dispatchEvent(event: IWVEvent, scope?: any): void;
      hasEventListener(eventType: string, handler?: Function): boolean;

}

export interface IWVEvent {
      type:string;
      target:any;
      data?:any;
      originalEvent?:any;
}

export type EventListenerTarget = {
      type: string
      handler: Function
}
