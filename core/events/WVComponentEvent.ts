import {IWVEvent} from "./interfaces/EventInterfaces";

export default class WVComponentEvent implements IWVEvent {
      public static readonly LOADED_RESOURCE: string = "WVComponentEvent.LOADED_RESOURCE";
      public static readonly SELECTED: string = "WVComponentEvent.SELECTED";
      public static readonly UN_SELECTED: string = "WVComponentEvent.UN_SELECTED";

      public static readonly MOVE:string              = "WVComponentEvent.MOVE";
      public static readonly RESIZE:string            = "WVComponentEvent.RESIZE";
      public static readonly TOGGLE_VISIBLE:string    = "WVComponentEvent.TOGGLE_VISIBLE";

      public static readonly REMOVE:string           = "WVComponentEvent.REMOVE";
      public static readonly REMOVE_DOCUMENT:string  = "WVComponentEvent.REMOVE_DOCUMENT";

      public static readonly INIT:string              = "WVComponentEvent.INIT";

      public static readonly ADDED:string            = "WVComponentEvent.ADDED";
      public static readonly ADDED_DOCUMENT:string   = "WVComponentEvent.ADDED_DOCUMENT";


      public static readonly CREATED:string          = "WVComponentEvent.CREATED";
      public static readonly DESTROYED:string        = "WVComponentEvent.DESTROYED";

	public static readonly LOCKED: string = "WVComponentEvent.LOCKED";
	public static readonly UN_LOCKED: string = "WVComponentEvent.UN_LOCKED";



      public static readonly SELECT_ITEM:string       = "WVComponentEvent.SELECT_ITEM";



	// transform크기를 element(width,height) 크기와 동일하게 만들기위한 이벤트
      public static readonly SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE: string = "WVComponentEvent.SYNC_TRANSFROM_SIZE_TO_ELEMENT_SIZE";


      type:string;
      target:any;
      data?:any;

      constructor( type:string, target:any, data?:any ){
            this.type = type;
            this.target = target;
            this.data = data;
      }
}

