import {IWVComponent} from "../component/interfaces/ComponentInterfaces";
import WScriptEventWatcher from "./WScriptEventWatcher";
import vm from 'vm-browserify'



export class WVComponentScriptEvent {
      public static readonly REGISTER: string = "register";
      public static readonly DESTROY:string        = "destroy";

      public static readonly CLICK: string = "click";
      public static readonly DBL_CLICK: string = "dblClick";
      public static readonly MOUSE_OVER: string = "mouseOver";
      public static readonly MOUSE_OUT: string = "mouseOut";

      public static readonly RESOURCE_LOADED:string = "resourceLoaded";
      public static readonly COMPLETED: string = "completed";
}


export class WVPageComponentScriptEvent {
      public static readonly BEFORE_UNLOAD: string = "beforeUnLoad";
      public static readonly UNLOADED: string = "unLoaded";

      public static readonly BEFORE_LOAD: string = "beforeLoad";
      public static readonly READY: string = "ready";
      public static readonly LOADED: string = "loaded";
}

export class WScriptEventVO{
      public script:string="";
      public eventData:WScriptEventData = null;
}

export class WScriptEventData {
      public name:string;
      public target:any;
      public instance_id:string;
      public data:any;
}


/*
주로 viewer에서 컴포넌트가 실행되는 경우에 사용하며
eventName에 해당하는 컴포넌트 이벤트를 발생 및 실행하는 기능.
1. eventName에 해당하는 컴포넌트 이벤트프로퍼티의 스크립트 문자열 정보를 구한다.
2. 스크립트 문자열 정보를 $eventWatcherbus 즉, WScriptEventWatcher에 보낸다.
3. WScriptEventWatcher에서 스크립트를 파생해서 실행한다.
 */
export default class WVComponentEventDispatcher {



      constructor(private _instance:IWVComponent ){
      }
      public getInstance(){
            return this._instance;
      }

      test(){

      }

      findErr(err){
            var index = err.stack.search(/\n\s*at Script.runInContext/);
            err.stack = err.stack.slice(0, index).split('\n').slice(0,-1).join('\n');
            var stack = err.stack.split(/\r?\n/);

            var line = 0;
            var errorMessage;
            if (stack.length > 0) {
                  while (
                        line < stack.length &&
                        (
                              stack[line].indexOf("ReferenceError") !== 0 &&
                              stack[line].indexOf("TypeError") !== 0 &&
                              stack[line].indexOf("EvalError") !== 0 &&
                              stack[line].indexOf("InternalError") !== 0 &&
                              stack[line].indexOf("RangeError") !== 0 &&
                              stack[line].indexOf("SyntaxError") !== 0 &&
                              stack[line].indexOf("URIError") !== 0
                        )
                        )
                  {

                        line++;
                  }
                  if (line < stack.length) {
                        errorMessage = stack[line];
                        var m = /:(\d+):(\d+)/.exec(stack[line+1]);
                        if (m) {
                              var lineno = Number(m[1])-1;
                              var cha = m[2];
                              errorMessage += " (line "+(lineno + 1)+", col "+cha+")";
                              return errorMessage
                        }
                  }
            }
            return null
      }
      dispatchEvent(eventName:string, vo={}){
            if(this._instance==null)
                  return;


            /*let eventVO = {
                  script: this._getEventScriptString(eventName),
                  eventData: {
                        name: eventName,
                        target: this._instance,
                        instance_id: this._instance.id,
                        data: vo,
                  }
            };*/

            // var res = null;
            // try {
            //       // console.log(this._instance);
            //       var context = {};
            //       $.extend(true, context, window);
            //       $.extend(true, context, this._instance);
            //       vm.runInNewContext(this._getEventScriptString(eventName), context);
            //
            //       let eventVO:WScriptEventVO = {
            //             script: this._getEventScriptString(eventName),
            //             eventData: {
            //                   name: eventName,
            //                   target: this._instance,
            //                   instance_id: this._instance.id,
            //                   data: vo,
            //             }
            //       };
            //
            //       /*
            //       WScript에 등록된 이벤트 실행.
            //        */
            //       WScriptEventWatcher.getInstance().$eventBus.$emit("__com_event__", eventVO);
            //
            //       // 외분에서 이벤트 이름으로 이벤트를 등록시켜 실행할 수 있기 $emit 이벤트 발생
            //       /*
            //             WScript이벤트가 발생하는 경우
            //             컴포넌트에 등록되어 있는 이벤트를 실행시키기 위해
            //
            //        */
            //       this._instance.emitWScriptEvent(eventName, eventVO.eventData);
            //       context = null;
            // }catch (e) {
            //       //컨텍스트 검사 해서 에러 나면
            //       console.error(this.findErr(e) +", " + "PAGE NAME : " + this._instance.name + ", EVENT NAME : " + eventName);
            //       context = null;
            //
            // }

            let eventVO:WScriptEventVO = {
                  script: this._getEventScriptString(eventName),
                  eventData: {
                        name: eventName,
                        target: this._instance,
                        instance_id: this._instance.id,
                        data: vo,
                  }
            };

            /*
            WScript에 등록된 이벤트 실행.
             */
            WScriptEventWatcher.getInstance().$eventBus.$emit("__com_event__", eventVO);
            this._instance.emitWScriptEvent(eventName, eventVO.eventData);
      }


      /*
      eventName에 해당하는 스킯트 문자열 가져오기.
       */
      private _getEventScriptString(eventName) {
            try {
                  let eventProperties = this._instance.getGroupProperties("events");
                  let script = eventProperties[eventName] || "";
                  return script;
            } catch (error) {
                  return error;
            }
      }

}

