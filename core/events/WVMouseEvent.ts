import {IWVEvent} from "./interfaces/EventInterfaces";

export default class WVMouseEvent implements IWVEvent {

      static readonly MOUSE_ENTER: string       = "WVMouseEvent.MOUSE_ENTER";

      static readonly DOUBLE_CLICK: string      = "WVMouseEvent.DOUBLE_CLICK";
      static readonly CLICK: string             = "WVMouseEvent.CLICK";
      static readonly ROLL_OVER: string         = "WVMouseEvent.ROLL_OVER";
      static readonly ROLL_OUT: string          = "WVMouseEvent.ROLL_OUT";

      static readonly ITEM_ROLL_OVER: string    = "WVMouseEvent.ITEM_ROLL_OVER";
      static readonly ITEM_ROLL_OUT: string     = "WVMouseEvent.ITEM_ROLL_OUT";
      static readonly ITEM_CLICK: string        = "WVMouseEvent.ITEM_CLICK";
      static readonly ITEM_DOUBLE_CLICK: string = "WVMouseEvent.ITEM_DOUBLE_CLICK";

      type: string;
      target: any;
      data?: any;

      constructor(type: string, target: any, data?: any) {
            this.type = type;
            this.target = target;
            this.data = data;
      }

}
