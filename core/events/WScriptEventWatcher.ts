import Vue from "vue";
import {WScriptEventVO} from "./WVComponentEventDispatcher";

export default class WScriptEventWatcher {
      private static _instance:WScriptEventWatcher;

      public static getInstance(){
            if( WScriptEventWatcher._instance == null ){
                  WScriptEventWatcher._instance = new WScriptEventWatcher();
            }

            return WScriptEventWatcher._instance;
      }


      private _$eventBus:Vue;
      get $eventBus(){
            return this._$eventBus;
      }


      constructor(){
            this._$eventBus = new Vue();
            this._initEvent();
      }


      _initEvent(){
            /*
            eventData :
           export class WScriptEventVO{
                  public script:string="";
                  public eventData:WScriptEventData = null;
            }

            export class WScriptEventData {
                  public name:string;
                  public target:any;
                  public instance_id:string;
                  public data:any;
            }


             */
            this._$eventBus.$on("__com_event__", (eventVO:WScriptEventVO)=>{

                  try {
                        let context = eventVO.eventData.target;
                        let func = new Function("event", eventVO.script);
                        func.call(context, eventVO.eventData);
                  }catch(error){
                        console.log("\n\n\n##### WScript ERROR #####");
                        console.log("실행한 스크립트", eventVO.script);
                        console.log("스크립트 실행 중 error", error);
                        console.log("##### WScript ERROR #####\n\n\n");
                        console.error(this.findErr(error) +", " + "PAGE NAME : " + eventVO.eventData.target.name + ", EVENT NAME : " + eventVO.eventData.name);
                        let errMsg = this.findErr(error);
                        Vue.$alert(errMsg  +",<br/>" + "PAGE NAME : " + eventVO.eventData.target.name + ",<br/> EVENT NAME : " + eventVO.eventData.name,'Script Editor Error!', {
                              confirmButtonText: 'OK',
                              type: "error",
                              dangerouslyUseHTMLString: true
                        });

                  }

            })
      }

      findErr(err){
            var stack = err.stack.split(/\r?\n/);

            var line = 0;
            var errorMessage;
            if (stack.length > 0) {
                  while (
                        line < stack.length &&
                        (
                              stack[line].indexOf("ReferenceError") !== 0 &&
                              stack[line].indexOf("TypeError") !== 0 &&
                              stack[line].indexOf("EvalError") !== 0 &&
                              stack[line].indexOf("InternalError") !== 0 &&
                              stack[line].indexOf("RangeError") !== 0 &&
                              stack[line].indexOf("SyntaxError") !== 0 &&
                              stack[line].indexOf("URIError") !== 0
                        )
                        )
                  {

                        line++;
                  }
                  if (line < stack.length) {
                        errorMessage = stack[line];
                        var m = /:(\d+):(\d+)/.exec(stack[line+1]);
                        if (m) {
                              var lineno = Number(m[1])-1;
                              var cha = m[2];
                              errorMessage += " (line "+(lineno)+", col "+cha+")";
                              return errorMessage
                        }
                  }
            }
            return null
      }

}
