
export default class WVEvent {

	public static readonly BEFORE_CREATE: string    = "WVEvent.BEFORE_CREATE";
	public static readonly CREATED: string          = "WVEvent.CREATED";
	public static readonly DESTROYED: string        = "WVEvent.DESTROYED";
	public static readonly FAIL_CREATE: string      = "WVEvent.FAIL_CREATE";

	public static readonly MOVE: string             = "WVEvent.MOVE";
	public static readonly RESIZE: string           = "WVEvent.RESIZE";
	public static readonly TOGGLE_VISIBLE: string   = "WVEvent.TOGGLE_VISIBLE";
	public static readonly ROTATION: string         = "WVEvent.ROTATION";


	public static readonly REMOVE: string = "WVEvent.REMOVE";
	//public static readonly REMOVE_DOCUMENT: string = "WVEvent.REMOVE_DOCUMENT";


	public static readonly BEFORE_ADDED: string = "WVEvent. BEFORE_ADDED";
	public static readonly ADDED: string = "WVEvent.ADDED";
	//public static readonly ADDED_DOCUMENT: string = "WVEvent.ADDED_DOCUMENT";





	public static readonly BEFORE_ADDED_CONTAINER: string = "WVEvent.BEFORE_ADDED_CONTAINER";
	public static readonly ADDED_CONTAINER: string  = "WVEvent.ADDED_CONTAINER";
      public static readonly REMOVE_CONTAINER: string  = "WVEvent.REMOVE_CONTAINER";


      // 속성 값이 변경되는 경우 실행
      public static readonly UPDATE_PROPERTY:string ="WVEvent.UPDATE_PROPERTY";


	//public static readonly TOGGLE_VISIBLE:string   = "WVEvent.TOGGLE_VISIBLE";

	type: string;
	target: any;
	data?: any;

	constructor(type: string, target: any, data?: any) {
		this.type = type;
		this.target = target;
		this.data = data;
	}
}
