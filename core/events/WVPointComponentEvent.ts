import WVComponentEvent from "./WVComponentEvent";

export class WVPointComponentEvent extends WVComponentEvent {

    public static readonly INSERT_POINT:string = "WVPointComponentEvent.INSERT_POINT";
    public static readonly REMOVE_POINT:string = "WVPointComponentEvent.REMOVE_POINT";

    constructor( type:string, target:any, data?:any ){
        super( type, target, data );
    }
}
