import Vue from "vue";
import IFacade = puremvc.IFacade;
import {IWVExtension} from "./IWVExtension";

export interface IExtensionPanel extends  IWVExtension{
      readonly  $element:any;
      setParentView(parentView:any):void;      // 패널 부모에 현재 패널 붙이기

}
