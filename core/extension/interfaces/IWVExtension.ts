import Vue from "vue";
import IFacade = puremvc.IFacade;

export interface IWVExtension {
      readonly notificationList:string[];
      handleNotification(note:any):void;
      create():void;
      setFacade(facade:IFacade):void;
      sendNotification(notificationName:string, data:any);

}
