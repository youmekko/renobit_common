import {Matrix} from "../../core/geom/Matrix";
import Point from "../../core/geom/Point";
import {IWVTransformTool, TransformCursor, TransformCursorType} from "./interfaces/WVTransformInterfaces";
import  RectControlCursor from "./controls/html/RectControlCursor";
import TranslateCursor from "./controls/html/TranslateCursor";
import Transform from "../../core/geom/Transform";
import TransformEvent from "./event/TransformEvent";
import CircleControlCursor from "./controls/html/CircleControlCursor";
import EventDispatcher from "../../core/events/EventDispatcher";
import WV2DComponent from "../component/2D/WV2DComponent";
import {$} from "../utils/reference";
import {CPLogger} from "../utils/CPLogger";

export default class WVTransformTool extends EventDispatcher implements IWVTransformTool {

      static readonly CURSOR_SIZE     : number                = 8;
      static readonly CURSOR_OFFSET   : number                = ((WVTransformTool.CURSOR_SIZE)/2)*-1;

      get container(): any {
            return this._container;
      }

      protected _target: WV2DComponent;
      private parentOffset:any;

      private targetMatrix: Matrix;
      private targetInvertMatrix: Matrix;
      private targetStartMatrix: Matrix;
      private globalMatrix: Matrix;
      private globalInvertMatrix: Matrix;

      private moveLoc: Point;
      private mouseLoc: Point;
      private startMouseLoc: Point;

      private innerMouseLoc: Point;
      private startInnerMouseLoc: Point;

      private pivotLoc: Point;
      private innerPivotLoc: Point;

      private rotatePivotLoc: Point;
      private innerRotatePivotLoc: Point;

      private cursors: Object;
      private startRotate: number;              // 시작 시 rotation 참조
      private selectControl: TransformCursor;   // 선택 컨트롤 참조
      private option: any;
      private _container: any;  // 컨테이너 참조
      private _onMouseMove_Handler: EventListenerObject;
      private _onMouseUp_Handler: EventListenerObject;
      private _onMouseDown_Handler: Function;
      private _onDoubleClick_Handler:Function;

      constructor(opt?: any) {
            super();
            this.option = $.extend({
                  template: '<div></div>'
            }, opt);
            this._preInitialize();
      }

      protected _preInitialize(): void {
            this.globalMatrix = new Matrix();
            this.globalInvertMatrix = this.globalMatrix.clone();
            this.globalInvertMatrix.invert();
            this.mouseLoc = new Point();
            this._container = $(this.option.template).get(0);
            $(this._container).css({position: "absolute"});
            this._onMouseMove_Handler = this._onMouseMove_Document.bind(this);
            this._onMouseUp_Handler = this._onMouseUp_Document.bind(this);
            this._onMouseDown_Handler = this._onMouseDown_Cursor.bind(this);
            this._onDoubleClick_Handler = this._onDoubleClick_Cursor.bind(this);
            this.cursors = {};
      }


      private getPivotCursorByType( type:string ):TransformCursor {
            let cursor:TransformCursor = null;
            switch (type){
                  case TransformCursorType.TOP_LEFT_SCALE:
                        cursor = this.getCursorByType(TransformCursorType.BOTTOM_RIGHT_SCALE);
                        break;
                  case TransformCursorType.TOP_MIDDLE_SCALE:
                        cursor = this.getCursorByType(TransformCursorType.BOTTOM_MIDDLE_SCALE);
                        break;
                  case TransformCursorType.TOP_RIGHT_SCALE:
                        cursor = this.getCursorByType(TransformCursorType.BOTTOM_LEFT_SCALE);
                        break;
                  case TransformCursorType.MIDDLE_LEFT_SCALE:
                        cursor = this.getCursorByType(TransformCursorType.MIDDLE_RIGHT_SCALE);
                        break;
                  case TransformCursorType.MIDDLE_RIGHT_SCALE:
                        cursor = this.getCursorByType(TransformCursorType.MIDDLE_LEFT_SCALE);
                        break;

                  case TransformCursorType.BOTTOM_LEFT_SCALE:
                        cursor = this.getCursorByType(TransformCursorType.TOP_RIGHT_SCALE);
                        break;

                  case TransformCursorType.BOTTOM_MIDDLE_SCALE:
                        cursor = this.getCursorByType(TransformCursorType.TOP_MIDDLE_SCALE);
                        break;

                  case TransformCursorType.BOTTOM_RIGHT_SCALE:
                        cursor = this.getCursorByType(TransformCursorType.TOP_LEFT_SCALE);
                        break;

                  case TransformCursorType.ROTATE:
                        cursor = this.getCursorByType(TransformCursorType.REGIST);
                        break;
            }
            return cursor;
      }

      private createCursorByType( type:string ):TransformCursor {

            let cursorSize: number = WVTransformTool.CURSOR_SIZE;
            let cursorOffset: number = WVTransformTool.CURSOR_OFFSET;
            let cursor: TransformCursor;
            let style :any;
            switch (type) {
                  case TransformCursorType.ROTATE:
                        style = {backgroundColor:"#ffcc00"};
                        cursor = new CircleControlCursor(type, cursorSize, cursorOffset, style);
                        break;
                  case TransformCursorType.REGIST:
                        style = {opacity:"0.3"};
                        cursor = new CircleControlCursor(type, cursorSize, cursorOffset, style);
                        break;
                  case TransformCursorType.TRANSLATE:
                        cursor = new TranslateCursor(type, cursorSize, cursorOffset );
                        break;
                  case TransformCursorType.TOP_LEFT_SCALE:
                  case TransformCursorType.TOP_RIGHT_SCALE:
                  case TransformCursorType.BOTTOM_LEFT_SCALE:
                  case TransformCursorType.BOTTOM_RIGHT_SCALE:
                  case TransformCursorType.MIDDLE_LEFT_SCALE:
                  case TransformCursorType.MIDDLE_RIGHT_SCALE:
                  case TransformCursorType.TOP_MIDDLE_SCALE:
                  case TransformCursorType.BOTTOM_MIDDLE_SCALE:
                        cursor = new RectControlCursor(type, cursorSize, cursorOffset);
                        break;
            }

            // 커서에 핸들러 함수 지정
            switch (cursor.type) {
                  case TransformCursorType.TOP_LEFT_SCALE:
                  case TransformCursorType.TOP_RIGHT_SCALE:
                  case TransformCursorType.BOTTOM_LEFT_SCALE:
                  case TransformCursorType.BOTTOM_RIGHT_SCALE:
                        cursor.onMoveHandler = this._onMoveScaleXYCursor.bind(this);
                        break;

                  case TransformCursorType.MIDDLE_LEFT_SCALE:
                  case TransformCursorType.MIDDLE_RIGHT_SCALE:
                        cursor.onMoveHandler = this._onMoveScaleXCursor.bind(this);
                        break;

                  case TransformCursorType.TOP_MIDDLE_SCALE:
                  case TransformCursorType.BOTTOM_MIDDLE_SCALE:
                        cursor.onMoveHandler = this._onMoveScaleYCursor.bind(this);
                        break;
                  case TransformCursorType.ROTATE:
                        cursor.onMoveHandler = this._onMoveRotateCursor.bind(this);
                        break;
                  case TransformCursorType.TRANSLATE:
                        cursor.onMoveHandler = this._onMoveTranslateCursor.bind(this);
                        break;
            }


            return cursor
      }


      protected _registEventCursor(cursor: TransformCursor, eventType: string, handler: Function): void {
            cursor.addEventListener(eventType, handler);
      }

      protected _removeEventCursor(cursor: TransformCursor, eventType: string, handler: Function): void {
            cursor.removeEventListener(eventType, handler);
      }


      protected _initMatrixReference(): void {
            this.targetMatrix = this.target.transform.matrix.clone();
            this.targetStartMatrix = this.targetMatrix.clone();
            this.targetInvertMatrix = this.targetMatrix.clone();
            this.targetInvertMatrix.invert();
      }


      private globalToLocal( px:number, py:number ):Point {
            let off:Point = new Point( px-this.parentOffset.left, py-this.parentOffset.top);
            return off;
      }

      protected _initPositionReference(mouseInfo: any): void {
            this.parentOffset = this.container.parentNode.getBoundingClientRect();
            this.mouseLoc = this.globalToLocal( mouseInfo.px, mouseInfo.py );
            this.startMouseLoc = this.mouseLoc.clone();
            this.innerMouseLoc = this.targetInvertMatrix.transformPoint(this.mouseLoc);
            this.startInnerMouseLoc = this.innerMouseLoc.clone();
      }


      protected _initPivotReference(): void {
            this.rotatePivotLoc = this.getCursorByType(TransformCursorType.REGIST).position;
            this.innerRotatePivotLoc = this.targetInvertMatrix.transformPoint(this.rotatePivotLoc);
            if (this.selectControl.pivotCursor) {
                  this.pivotLoc       = this.selectControl.pivotCursor.position;
                  this.innerPivotLoc  = this.targetInvertMatrix.transformPoint(this.pivotLoc);
            }
      }


      protected _getRotateReference(): number {
            let gMouse: Point = this.globalMatrix.transformPoint(this.mouseLoc);
            let gPivot: Point = this.globalMatrix.transformPoint(this.rotatePivotLoc);
            let diff: Point   = gMouse.subtract(gPivot);
            return Math.atan2(diff.y, diff.x);
      }

      protected _onMouseDown_Cursor(event: TransformEvent): void {
            let cursor:TransformCursor = event.target as TransformCursor;
            this.selectControl = cursor;
            this._initMatrixReference();
            this._initPositionReference(event.data);
            this._initPivotReference();
            this.startRotate = this._getRotateReference();
            this.dispatchEvent(new TransformEvent(TransformEvent.START_TRANSFORM, this));
            this._removeDocumentEvent();
            this._registDocumentEvent();
      }

      protected _updateMousePosition(event:MouseEvent): void {
            this.mouseLoc = this.globalToLocal(event.pageX, event.pageY).subtract(this.selectControl.offsetPoint);
            this.innerMouseLoc = this.targetInvertMatrix.transformPoint(this.mouseLoc);
            this.moveLoc = this.mouseLoc.subtract(this.startMouseLoc);
      }

      protected _onDoubleClick_Cursor( event:any ):void{
            this.dispatchEvent(new TransformEvent(TransformEvent.DOUBLE_CLICK, this, {select:this.target}));
      }

      protected _onMouseMove_Document(event: MouseEvent): void {
            // 매트릭 초기화
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            this.targetMatrix = this.targetStartMatrix.clone();
            this._updateMousePosition(event);
            if (this.selectControl != null && this.moveLoc.x != 0 && this.moveLoc.y != 0 ) {
                  this.selectControl.onMoveHandler();
                  if (this.selectControl.type != TransformCursorType.ROTATE &&
                        this.selectControl.type != TransformCursorType.TRANSLATE) {
                        this._limitScale();
                        this._adjustTranslateValue();
                  }
                  this._updateControls();
                  this._applyTargetMatrix();
            }
      }

      protected _limitScale(): void {

            let minScale: number = this.target.transformMinimumScale();
            let cloneMatrix = this.targetMatrix.clone();
            let flipX       = this.targetMatrix.a <= 0 ? -1 : 1;
            let flipY       = this.targetMatrix.d <= 0 ? -1 : 1;
            let startFlipX  = this.targetStartMatrix.a <= 0 ? -1 : 1;
            let startFlipY  = this.targetStartMatrix.d <= 0 ? -1 : 1;
            let angle;
            let force   = false;
            angle       = this.targetStartMatrix.getRotateX();

            if (cloneMatrix.getScaleX() < minScale || flipX != startFlipX ) {
                  cloneMatrix.a = Math.cos(angle) * minScale;
                  cloneMatrix.b = Math.sin(angle) * minScale;
                  force = true;
            }

            angle           = this.targetStartMatrix.getRotateY();
            if (cloneMatrix.getScaleY() < minScale || flipY != startFlipY) {
                  cloneMatrix.d = Math.cos(angle) * minScale;
                  cloneMatrix.c = Math.sin(angle) * minScale;
                  force = true;
            }

            if (force) {
                  this.targetMatrix = cloneMatrix.clone();
            }

      }


      /**
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       커서 움직임에 따른 핸들러 함수 구현 시작
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       * */
      protected _onMoveScaleXYCursor(): void {
            this._calculateScaleX();
            this._calculateScaleY();
      }

      protected _onMoveScaleXCursor(): void {
            this._calculateScaleX();
      }

      protected _onMoveScaleYCursor(): void {
            this._calculateScaleY();
      }

      protected _onMoveTranslateCursor(): void {
            this.targetMatrix.translate(this.moveLoc.x, this.moveLoc.y);
      }

      protected _onMoveRotateCursor(): void {
            let angle = this._getRotateReference() - this.startRotate;
            this.targetMatrix.rotate(angle);
            let currentRotatePivotLoc:Point = this.rotatePivotLoc.subtract(this.targetMatrix.transformPoint(this.innerRotatePivotLoc));
            this.targetMatrix.tx += currentRotatePivotLoc.x;
            this.targetMatrix.ty += currentRotatePivotLoc.y;
      }



      private _applyTargetMatrix(): void {

            let sX:number = this.targetMatrix.getScaleX();
            let sY:number = this.targetMatrix.getScaleY();
            let nWidth: number = sX * this.target.transform.width;
            let nHeight: number = sY * this.target.transform.height;
            let nRotate: number = this.targetMatrix.getRotateX() * Matrix.TO_DEGREE;
            let tRotate: number = this.targetMatrix.getRotateX() - this.startRotate;
            let nX: number = this.targetMatrix.tx;
            let nY: number = this.targetMatrix.ty;
            let changeInfo: any = {
                  x: (nX - this.target.x),
                  y: (nY - this.target.y),
                  width: (nWidth - this.target.width),
                  height: (nHeight - this.target.height),
                  rotation: (nRotate - this.target.rotation),
                  trotation: tRotate,
                  scaleX: sX-this.targetStartMatrix.getScaleX(),
                  scaleY: sY-this.targetStartMatrix.getScaleY(),
                  type: this.selectControl ? this.selectControl.type : "",
                  pivotType: ""
            };
            // 선택 컨트롤이 있고 컨트롤에 축 컨트롤 속성이 설정되 있으면 변경 정보에 축 타입 설정
            if (this.selectControl != null && this.selectControl.pivotCursor ) {
                  changeInfo.pivotType = this.selectControl.pivotCursor.type;
            }
            //console.log("변경 값", changeInfo.scaleX, changeInfo.scaleY);
            this.target.transform.matrix = this.targetMatrix.clone();

            // 선택 컨트롤이 있다면 그룹 업데이트를 위해 이벤트 발생
            if (this.selectControl) {
                  this.dispatchEvent(new TransformEvent(TransformEvent.UPDATE_TRANSFORM, this, changeInfo));
            }

      }

      private _calculateScaleX(): void {
            let horizontalMovePoint = new Point(this.innerMouseLoc.x, this.startInnerMouseLoc.y);
            let distance: number    = this.startInnerMouseLoc.x - this.innerPivotLoc.x;
            let offset: Point       = this._getDistortOffset(horizontalMovePoint, distance);
            this.targetMatrix.a += offset.x;
            this.targetMatrix.b += offset.y;
      }

      private _calculateScaleY(): void {
            let verticalMovePoint = new Point(this.startInnerMouseLoc.x, this.innerMouseLoc.y);
            let distance: number = this.startInnerMouseLoc.y - this.innerPivotLoc.y;
            let offset: Point = this._getDistortOffset(verticalMovePoint, distance);
            this.targetMatrix.c += offset.x;
            this.targetMatrix.d += offset.y;
      }

      private _adjustTranslateValue(): void {
            let currentPivotLoc: Point = this.targetMatrix.transformPoint(this.innerPivotLoc);
            let off: Point = this.pivotLoc.subtract(currentPivotLoc);
            this.targetMatrix.tx += off.x;
            this.targetMatrix.ty += off.y;
      }

      private _getDistortOffset(offsetPoint: Point, base: number): Point {
            let w: number = this.target.transform.width;
            let h: number = this.target.transform.height;
            let ratioW: number = w / base;
            let ratioH: number = h / base;
            let movePoint: Point = this.targetStartMatrix.transformPoint(offsetPoint).subtract(this.startMouseLoc);
            // targetMatrix에 적용된 scale값을 초기화 하기 위해 다시 w, h로 나눔
            movePoint.x *= ratioW / w;
            movePoint.y *= ratioH / h;
            movePoint = (movePoint);
            return movePoint;
      }

      // 대상 상태에 따른 커서 상태 변경
      private _updateCursorState():void{
            let cursorList:Array<string> = this.target.handleTransformList();
            let cursorType;
            let cursor:TransformCursor;
            for ( cursorType in this.cursors ) {
                  cursor      = this.getCursorByType(cursorType);
                  let idx:number =cursorList.indexOf(cursor.type);
                  if(idx <= -1) {
                        cursor.reDraw({visible:false});
                  } else {
                        cursor.reDraw({visible:true});
                  }
            }
      }

      /**
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       커서 움직임에 따른 핸들러 함수 구현 끝
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       * */
      protected _onMouseUp_Document(event: MouseEvent): void {

            event.stopPropagation();
            event.stopImmediatePropagation();
            this._removeDocumentEvent();
            this._updateCursorState();
            this.dispatchEvent(new TransformEvent(TransformEvent.COMPLETE_TRANSFORM, this));
            this.target.transform.reset(); // 트랜스 폼이 끝나면 정보를 초기화
            this.selectControl = null;

      }

      protected _registDocumentEvent(): void {
            document.addEventListener("mousemove", this._onMouseMove_Handler);
            document.addEventListener("mouseup", this._onMouseUp_Handler);
      }

      protected _removeDocumentEvent(): void {
            document.removeEventListener("mousemove", this._onMouseMove_Handler);
            document.removeEventListener("mouseup", this._onMouseUp_Handler);
      }


      private _reDrawCursorByInfo( type:string, info:any ){
            let cursor:TransformCursor = this.getCursorByType(type);
            if(cursor){
                  cursor.reDraw(info);
            }
      }

      protected _updateControls(): void {
            let transform: Transform = this.target.transform as Transform;
            let w: number = transform.width;
            let h: number = transform.height;

            let tl: Point = new Point(0, 0);
            let tr: Point = new Point(w, 0);
            let bl: Point = new Point(0, h);
            let br: Point = new Point(w, h);
            let cp = new Point(w / 2, h / 2);

            tl = this.targetMatrix.transformPoint(tl);
            tr = this.targetMatrix.transformPoint(tr);
            bl = this.targetMatrix.transformPoint(bl);
            br = this.targetMatrix.transformPoint(br);
            cp = this.targetMatrix.transformPoint(cp);

            let tm = tl.interpolate(tr, 0.5);
            let ml = tl.interpolate(bl, 0.5);
            let mr = tr.interpolate(br, 0.5);
            let bm = bl.interpolate(br, 0.5);

            let rp = cp.clone();
            rp.y -= ((h * this.targetMatrix.getScaleY()) / 2) + 20;
            rp = rp.transformPoint(cp, this.targetMatrix.getRotateX());

            let rDegree = this.targetMatrix.getScaleX() * Matrix.TO_DEGREE;
            if(this.selectControl != null && this.selectControl.type != TransformCursorType.ROTATE ){
                  rDegree = this.startRotate * Matrix.TO_DEGREE;
            }

            this._reDrawCursorByInfo(TransformCursorType.TOP_LEFT_SCALE, {position: tl, rotation:rDegree} );
            this._reDrawCursorByInfo(TransformCursorType.TOP_RIGHT_SCALE, {position: tr, rotation:rDegree} );

            this._reDrawCursorByInfo(TransformCursorType.BOTTOM_LEFT_SCALE, {position: bl, rotation:rDegree} );
            this._reDrawCursorByInfo(TransformCursorType.BOTTOM_RIGHT_SCALE, {position: br, rotation:rDegree} );

            this._reDrawCursorByInfo(TransformCursorType.MIDDLE_LEFT_SCALE, {position: ml, rotation:rDegree} );
            this._reDrawCursorByInfo(TransformCursorType.MIDDLE_RIGHT_SCALE, {position: mr, rotation:rDegree} );

            this._reDrawCursorByInfo(TransformCursorType.TOP_MIDDLE_SCALE, {position: tm, rotation:rDegree} );
            this._reDrawCursorByInfo(TransformCursorType.BOTTOM_MIDDLE_SCALE, {position: bm, rotation:rDegree} );

            this._reDrawCursorByInfo(TransformCursorType.REGIST, {position: cp, rotation:rDegree} );
            this._reDrawCursorByInfo(TransformCursorType.ROTATE, {position: rp } );

            let nWidth: number = this.targetMatrix.getScaleX() * this.target.transform.width;
            let nHeight: number = this.targetMatrix.getScaleY() * this.target.transform.height;
            let nRotate: number = this.targetMatrix.getRotateX() * Matrix.TO_DEGREE;
            let nX: number = this.targetMatrix.tx;
            let nY: number = this.targetMatrix.ty;
            this._reDrawCursorByInfo(TransformCursorType.TRANSLATE, {x:nX, y:nY, rotate:nRotate, width: nWidth, height: nHeight} );
      }

      protected getCursorByType(type: string): TransformCursor {
            return this.cursors[type] as TransformCursor;
      }


      // 타깃에 따른 커서 초기화
      protected _initCursor(): void {

            let cursorList: Array<string> = this.target.handleTransformList();
            let cursor: TransformCursor;
            let pivotCursor:TransformCursor;
            let cursorType:any;
            for ( cursorType in cursorList ) {
                  cursor      = this.createCursorByType( cursorList[cursorType] );
                  // 생성하며 객체참조 만듬
                  if( cursor && this.cursors[cursor.type] == null ){
                        this.cursors[cursor.type] = cursor;
                  }
            }

            for ( cursorType in this.cursors ) {
                  cursor      = this.getCursorByType(cursorType);
                  pivotCursor = this.getPivotCursorByType(cursor.type);
                  if(pivotCursor){cursor.pivotCursor = pivotCursor;}
                  // 중심점이 아니면 이벤트 등록
                  if ( cursorType != TransformCursorType.REGIST  ) {
                        this._registEventCursor(cursor, TransformEvent.DOWN_CURSOR, this._onMouseDown_Handler);
                  }
                  cursor.draw(this);
                  if( cursorType == TransformCursorType.TRANSLATE){
                        $(cursor.element).on("dblclick", this._onDoubleClick_Handler );
                  }
            }

      }

      protected _init(): void {
            this.target.selected = true;
            this._initCursor();
            this._initMatrixReference();
            this._updateControls();
            this.syncFromTarget();
            this._updateCursorState();

      }

      set target(objTarget: WV2DComponent) {
            if (this._target != objTarget) {
                  if (this._target) {
                        this.resetTargetResource();
                  }
                  if (objTarget != null) {

                        this._target = objTarget;
                        this._init();
                  }
            }
      }

      get target(): WV2DComponent {
            return this._target;
      }

      syncFromTarget():void{
            // 트랜스 폼이 끝나면 정보를 초기화
            this.target.transform.reset();
            let cAngle:number = this.targetMatrix.getRotateX()*Matrix.TO_DEGREE;
            this._initMatrixReference();
            /*
            // 이동처리
            let tx:number = this.target.x - this.targetMatrix.tx;
            let ty:number = this.target.y - this.targetMatrix.ty;
            this.targetMatrix.tx += tx;
            this.targetMatrix.ty += ty;
            this.target.transform.matrix = this.targetMatrix.clone();

            //this.translate( this.targetMatrix.tx+tx, this.targetMatrix.ty+ty);

            //rotate처리
            this._initMatrixReference();
            let cAngle:number = this.targetMatrix.getRotateX()*Matrix.TO_DEGREE;
            cAngle = this.target.rotation - cAngle
            console.log("cAngle", cAngle );
            this.rotate(cAngle);

            //스케일 처리
            this._initMatrixReference();
            let csx:number = (this.target.width/this.target.transform.width) - this.targetMatrix.getScaleX();
            let csy:number = (this.target.height/this.target.transform.height) - this.targetMatrix.getScaleY();
            this.scale( csx, csy );
            */
            this._updateControls();
            this._applyTargetMatrix();
            // 싱크 후 모든 커서 보이게 처리
            let cursor:TransformCursor;
            for ( let cursorType in this.cursors ) {
                  cursor = this.getCursorByType(cursorType);
                  cursor.reDraw({visible:true, rotation:cAngle});
            }

      }

      toggleCursor(bValue:boolean):void{
            this._updateCursorState();
      }

      initGroupSync():void {
            this._initMatrixReference();
      }



      protected transformTypeToPivotPoint(strTrasnformType: string): Point {
            let pivot: Point = new Point();
            let transform: Transform = this.target.transform as Transform;
            let w: number = transform.width;
            let h: number = transform.height;
            switch (strTrasnformType) {

                  case TransformCursorType.BOTTOM_RIGHT_SCALE:
                        pivot.x = w;
                        pivot.y = h;
                        break;

                  case TransformCursorType.BOTTOM_MIDDLE_SCALE:
                        pivot.x = w / 2;
                        pivot.y = h;
                        break;

                  case TransformCursorType.BOTTOM_LEFT_SCALE:
                        pivot.x = 0;
                        pivot.y = h;
                        break;

                  case TransformCursorType.MIDDLE_LEFT_SCALE:
                        pivot.x = 0;
                        pivot.y = h / 2;
                        break;

                  case TransformCursorType.MIDDLE_RIGHT_SCALE:
                        pivot.x = w;
                        pivot.y = h / 2;
                        break;

                  case TransformCursorType.TOP_RIGHT_SCALE:
                        pivot.x = w;
                        pivot.y = 0;
                        break;

                  case TransformCursorType.TOP_MIDDLE_SCALE:
                        pivot.x = w / 2;
                        pivot.y = 0;
                        break;

                  case TransformCursorType.TOP_LEFT_SCALE:
                        pivot.x = 0;
                        pivot.y = 0;
                        break;

                  case TransformCursorType.REGIST:
                        pivot.x = w / 2;
                        pivot.y = h / 2;
                        break;

            }
            return pivot;
      }

      /**
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       ITransformToolCalculator함수 구현
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       * */
      scale(sx: number, sy: number, type?: string): void {
            this.targetMatrix = this.targetStartMatrix.clone();
            let minScale:number = this.target.transformMinimumScale();
            let rotate:number = this.targetMatrix.getRotateX();
            let iPivot:Point = this.transformTypeToPivotPoint(type||TransformCursorType.TOP_LEFT_SCALE);
            let gPivot:Point = this.targetMatrix.transformPoint(iPivot);
            let scaleP:Point = new Point(this.targetMatrix.getScaleX() + sx, this.targetMatrix.getScaleY() + sy);
            this.targetMatrix.concat(this.targetInvertMatrix);
            this.targetMatrix.scale( Math.max( scaleP.x, minScale), Math.max( scaleP.y, minScale));
            this.targetMatrix.rotate(rotate);
            let off:Point = gPivot.subtract(this.targetMatrix.transformPoint(iPivot));
            this.targetMatrix.tx +=off.x;
            this.targetMatrix.ty +=off.y;
            this._updateControls();
            this._applyTargetMatrix();
      }

      rotate(angle: number): void {
            let iPivot:Point = this.transformTypeToPivotPoint(TransformCursorType.REGIST);
            let gPivot:Point = this.targetMatrix.transformPoint(iPivot);
            this.targetMatrix.rotateDeg(angle);
            let off:Point = gPivot.subtract( this.targetMatrix.transformPoint(iPivot));
            this.targetMatrix.tx += off.x;
            this.targetMatrix.ty += off.y;
            this._updateControls();
            this._applyTargetMatrix();
      }

      translate(tx: number, ty: number): void {
            this.targetMatrix.tx = tx;
            this.targetMatrix.ty = ty;
            this._updateControls();
            this._applyTargetMatrix();
      }

      protected clearCursor():void{
            let cursor:TransformCursor;
            for ( let cursorType in this.cursors ) {
                  cursor = this.getCursorByType(cursorType);
                  if ( cursor.type != TransformCursorType.REGIST  ) {
                        this._removeEventCursor(cursor, TransformEvent.DOWN_CURSOR, this._onMouseDown_Handler);
                  }
                  cursor.clear();
                  this.cursors[cursorType] = null;
            }
      }

      protected resetTargetResource():void{
            this.clearCursor();
            this._target.selected = false;
            this._target = null;
      }
      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       IDrawable 인터페이스 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       */
      draw(parent: any): void {
            $(parent).append(this.container);
            this.parentOffset = parent.getBoundingClientRect();
      }

      clear(): void {
            this.resetTargetResource();
            this.targetMatrix = null;
            this.targetInvertMatrix = null;
            this.targetStartMatrix  = null;
            this.globalMatrix       = null;
            this.globalInvertMatrix = null;

            this.moveLoc            = null;
            this.mouseLoc           = null;
            this.startMouseLoc      = null;
            this.innerMouseLoc      = null;
            this.startInnerMouseLoc = null;

            this.pivotLoc           = null;
            this.innerPivotLoc      = null;
            this.rotatePivotLoc     = null;
            this.innerRotatePivotLoc= null;


            this.startRotate        = null;
            this.selectControl      = null;
            this.option             = null;

            this.cursors                = null;
            this._removeDocumentEvent();
            this._onMouseMove_Handler   = null;
            this._onMouseUp_Handler     = null;
            this._onMouseDown_Handler   = null;
            this._onDoubleClick_Handler = null;
            $(this._container).remove();
            this._container = null;
      }
}
