import Point from "../../../../core/geom/Point";
import SVGControlCursor from "./SVGControlCursor";
import CPUtil from "../../../utils/CPUtil";

export default class SVGPointControlCursor extends SVGControlCursor {

    get position():Point {
        let convertInt:Function = CPUtil.getInstance().convertInt;
        let px:number = convertInt($(this.element).attr("cx")),
            py:number = convertInt($(this.element).attr("cy"));
        return new Point(px, py);
    }

    constructor( type:string, size:number, offset:number, style?:any, ){
        super( type, size, offset, style );
    }

    protected _createCursorElement( style:any ):SVGElement{
        return CPUtil.getInstance().createSVGElement("circle", style );
    }


}
