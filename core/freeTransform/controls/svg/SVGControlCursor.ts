import Point from "../../../../core/geom/Point";
import EventDispatcher from "../../../../core/events/EventDispatcher";
import TransformEvent from "../../event/TransformEvent";
import {TransformCursor, TransformCursorType} from "../../interfaces/WVTransformInterfaces";
import CPUtil from "../../../utils/CPUtil";
import {IWVEvent} from "../../../events/interfaces/EventInterfaces";
import CPNotification from "../../../utils/CPNotification";

export default class SVGControlCursor implements TransformCursor {

    pivotCursor?:TransformCursor;
    onMoveHandler:Function;
    protected _element:SVGElement;
    protected _offset: number;
    protected _position:Point;
    protected _type: string;
    protected _size: number;

    protected _style:any;

    get type(): string {
        return this._type;
    }

    get position():Point {
        let convertInt:Function = CPUtil.getInstance().convertInt;
        let px:number = convertInt($(this.element).attr("cx")),
            py:number = convertInt($(this.element).attr("cy"));
        return new Point(px, py);
    }

    get size(): number {
        return this._size;
    }

    get offset(): number {
        return this._offset;
    }

    get element():SVGElement{
        return this._element;
    }


    private _dispatcher:EventDispatcher;
    constructor( type:string, size:number, offset:number, style?:any ){
        this._dispatcher = new EventDispatcher();
        this._type      = type;
        this._size      = size;
        this._offset    = offset;
        this._style     = $.extend({
            stroke        :"black",
            "stroke-width"  :1,
            "fill-opacity"  :1,
            fill            :"white",
            r               : this.size,
            cx              : this.offset,
            cy              : this.offset
        }, style );

    }


    /**
     ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
     커서 사용 메서드 구현
     ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
     */
    protected registEventListener():void{
        $(this.element).on("mousedown", this._onMouseDown_Control.bind(this) );
    }


    protected _onMouseDown_Control( event:any ):void {
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();
        this.dispatchEvent( new TransformEvent(TransformEvent.DOWN_CURSOR, this, {
            px:event.pageX,
            py:event.pageY
        }) );
    }




    /**
     ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
     IEventDispatcher구현
     ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
     */
    addEventListener(eventType: string, handler: Function): boolean {
        return this._dispatcher.addEventListener(eventType, handler);
    }

    removeEventListener(eventType: string, handler: Function): boolean {
        return this._dispatcher.removeEventListener(eventType, handler );
    }

    hasEventListener(eventType: string, handler?: Function): boolean {
        return this._dispatcher.hasEventListener(eventType, handler);
    }

    dispatchEvent(event: IWVEvent, scope?: any): void {
        this._dispatcher.dispatchEvent(event, scope);
    }


    protected _createCursorElement( style?:any ):SVGElement {
        CPNotification.getInstance().notifyMessage("커서 표현을 위한 엘리먼트 생성 처리를 구현해 주세요");
        return null;
    }
    /**
     ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
     IDrawable인터페이스 구현
     ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
     */
    draw( parent:SVGElement, style?: any): void {
        this._element = this._createCursorElement(this._style);
        $(this._element).css({ cursor:"pointer",  pointerEvents:"auto" })
            .addClass(`${TransformCursorType.DEFAULT} ${this.type}`);
        parent.appendChild( this.element );
        this.registEventListener();
    }

    clear(): void {
        $(this.element).off("mousedown");
        $(this.element).remove();
        this._dispatcher.destroy();
        this._type          = null;
        this._size          = null;
        this._offset        = null;
        this._dispatcher    = null;
        this._element       = null;
    }

    reDraw(info: any): void {
        let jQueryEle = $(this.element);
        if (info.x) {
            jQueryEle.attr({cx: info.x});
        }
        if (info.y) {
            jQueryEle.attr({cy: info.y});
        }
        (info.visible) ? jQueryEle.attr({visibility: "visible"}) : jQueryEle.attr({visibility: "hidden"});
    }

}
