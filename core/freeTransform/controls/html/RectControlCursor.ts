import ControlCursor from "./ControlCursor";
import {TransformCursor} from "../../interfaces/WVTransformInterfaces";

export default class RectControlCursor extends ControlCursor {

      constructor(type: string, size: number, offset: number, style?: any, pivotCursor?: TransformCursor) {
            super(type, size, offset, style, pivotCursor);
      }

      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       IDrawable인터페이스 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       */
      protected _createCursorElement(style: any): HTMLElement {
            let element: HTMLElement = document.createElement("div");
            $(element).css(style);
            return element;
      }

      reDraw(info: any): void {
            let r: number = info.rotation || 0;
            let jQueryEle: any = $(this.element);
            jQueryEle.css({"transform": ""});
            super.reDraw(info);
            jQueryEle.css({"transform": "translate(-50%, -50%) rotate(" + r + "deg)"});
      }


}
