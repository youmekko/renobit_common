import {IWVEvent} from "../../../core/events/interfaces/EventInterfaces";

export default class TransformEvent implements IWVEvent {

    static readonly DOWN_CURSOR:string          = "TransformEvent.DOWN_CURSOR";
    static readonly MOVE_CURSOR:string          = "TransformEvent.MOVE_CURSOR";
    static readonly MOVE_END_CURSOR:string      = "TransformEvent.MOVE_END_CURSOR";
    static readonly DOUBLE_CLICK:string         = "TransformEvent.DOUBLE_CLICK";
    static readonly START_TRANSFORM:string     = "TransformEvent.START_TRANSFORM";
    static readonly UPDATE_TRANSFORM:string     = "TransformEvent.UPDATE_TRANSFORM";
    static readonly COMPLETE_TRANSFORM:string   = "TransformEvent.COMPLETE_TRANSFORM";

    type:string;
    target:any;
    data?:any;
    constructor( type:string, target:any, data?:any ){
        this.type = type;
        this.target = target;
        this.data = data;
    }

}
