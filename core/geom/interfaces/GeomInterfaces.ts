import {Matrix} from "../Matrix";
import Point from "../Point";
import Transform from "../Transform";
import {IWV2DComponent} from "../../component/interfaces/ComponentInterfaces";



// 트랜스 툴을 이용하는 컴포넌트가 구현해야 할 인터페이스
export interface ITransformClient {
    readonly transform:Transform;
    createTransform():void              // transform객체 생성 함수
    transformMinimumScale():number      // 최소 스케일 사이즈 반환 기본값은 20px
    handleTransformList():Array<string> // 사용 할 트랜스폼 커서 목록 반환
}


export interface ITransform {
    readonly x:number
    readonly y:number
    readonly width:number
    readonly height:number
    readonly ownerScale:Point;
    readonly owner:ITransformClient | IWV2DComponent
    matrix:Matrix
}


export type Rectangle = {
    x:number
    y:number
    width:number
    height:number
}


