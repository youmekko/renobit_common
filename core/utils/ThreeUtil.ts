import {$, THREE} from "./reference";
import CPUtil from "./CPUtil";

export default class ThreeUtil {

      static getTwoPositionToThreePosition(x,y){
            var rect = window.wemb.renderer.domElement.getBoundingClientRect();
            let point = {
                  x:0,
                  y:0
            }
            point.x = ( (x - rect.left) / rect.width ) * 2 - 1;
            point.y = - ( (y- rect.top) / rect.height ) * 2 + 1;                                          //z
            window.wemb.raycaster.setFromCamera( point, window.wemb.camera );


            let position = new THREE.Vector3(0,0,0);
            var intersects =  window.wemb.raycaster.intersectObject(  window.wemb.scene.getObjectByName("gridHelper") );
            if ( intersects.length > 0 ) {
                  var intersect = intersects[ 0 ];

                  position.copy(intersect.point);
            }

            return {x:position.x, y:position.y, z:position.z};
      }


      static getBestPowerOf2( value ):number {
            let MAX_SIZE = 4096;
            var p = 1;
            while (p < value)
                  p <<= 1;

            if (p > MAX_SIZE)
                  p = MAX_SIZE;
            return p;
      }

      static convertVector3To2DFromArea( position, rect:any ):any {
            let cam     = window.wemb.camera;
            let vector = position.project(cam);
            vector.x = (vector.x+1)/2*rect.width;
            vector.y = (-vector.y+1)/2*rect.height;
            return vector;
      }

      static getThreePositionTo2D( position, camera?:any, renderer?:any ):any {
            let rect    = renderer ? renderer.domElement : window.wemb.renderer.domElement;
            let cam     = camera   ? camera : window.wemb.camera;
            let vector = position.project(cam);
            vector.x = (vector.x + 1)/2*$(rect).width();
            vector.y = (-vector.y+1)/2*$(rect).height();

            return vector;
      }


      static _sizeVector:THREE.Vector3 = new THREE.Vector3;
      static _boxHelper:THREE.Box3     = new THREE.Box3;
      static getObjectSize( object:THREE.Object3D ):THREE.Vector3{
            return ThreeUtil.getObjectBox(object).getSize(this._sizeVector).clone();
      }

      static getObjectBox( object:THREE.Object3D ):THREE.Box3 {
            return ThreeUtil._boxHelper.setFromObject(object);
      }

      static getLabelTexture( info:any ){
            return CanvasLabelBuilder.build(info);
      }

}


const CanvasLabelBuilder = {

      _drawRoundRect: function(ctx, x, y, w, h, r) {
            ctx.beginPath();
            ctx.moveTo(x + r, y);
            ctx.lineTo(x + w - r, y);
            ctx.quadraticCurveTo(x + w, y, x + w, y + r);
            ctx.lineTo(x + w, y + h - r);
            ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
            ctx.lineTo(x + r, y + h);
            ctx.quadraticCurveTo(x, y + h, x, y + h - r);
            ctx.lineTo(x, y + r);
            ctx.quadraticCurveTo(x, y, x + r, y);
            ctx.closePath();
            ctx.fill();
            ctx.stroke();
      },

      _getTextHeight: function(text, fonts, fontWeight) {
            let d = document.createElement("span");
            $(d).css({"font": fonts});
            if (fontWeight) {
                  $(d).css({"font-weight": fontWeight});
            }
            d.textContent = text;
            document.body.appendChild(d);
            let emHeight = (d.offsetHeight) + 1;
            document.body.removeChild(d);
            return emHeight;
      },


      /**
       * canvas에 텍스트 표현
       * @param ctx canvas2dcontext 참조
       * @param sx  x축 마진 값
       * @param sy  y축 마진 값
       * @param message 표현 텍스트 정보
       * @param color 텍스트 컬러
       * @param textInfo context에 적용할 텍스트 속성정보 객체 fontName, fontWeight, fontSize ...
       * */
      _fillText: function(ctx: any, sx: number, sy: number, message: Array<string>, color: any, textInfo: any) {

            ctx.save();
            let fontSize = textInfo.fontSize;
            let fontName = textInfo.fontName;
            let fontWeight = textInfo.fontWeight;
            let fonts = fontWeight + " " + fontSize + "px " + fontName;
            //let hasLabelInfo =  textInfo.labelInfo ? true : false;
            ctx.font = fonts;
            ctx.fillStyle = color;
            ctx.textAlign = "left";
            ctx.textBaseline = "top";
            let i;
            let w = 0;
            let h = 0;
            let text;
            let x = sx;
            let y = sy;
            let max = message.length;
            let textHeight;
            ctx.beginPath();
            for (i = 0; i < max; i++) {
                  text = message[i];
                  textHeight = CanvasLabelBuilder._getTextHeight(text, fontSize + "px " + fontName, fontWeight);
                  let diff = textHeight - fontSize;
                  y += diff;
                  ctx.fillText(text, x, y);
                  y += fontSize;
                  w = Math.max(ctx.measureText(text).width, w);
            }
            // width, height에 margin값 적용
            w += (sx*2)+2;
            h += (y+sy+2);
            return {w: w, h:h};

      },


      clearContext:function( ctx, w, h ){
            ctx.clearRect(0, 0, w, h);
      },

      /**
       * info정보로 구성한 라벨 texture를 반환
       * @param info 라벨에 스타일 정보를 가진 객체
       * */
      build:function( info:any ){

            let _dummyCanvas: HTMLCanvasElement = document.createElement("canvas");
            let _dummyCtx: any      = _dummyCanvas.getContext("2d");
            let _drawCanvas: HTMLCanvasElement = document.createElement("canvas");
            let _drawCtx: any       = _drawCanvas.getContext("2d");

            let message:Array<string> = info.text.split("<br>");
            let color    = CPUtil.getInstance().parseColor(info.color).hex;
            let marginTop          = info.marginW;
            let marginBottom       = info.marginH;
            CanvasLabelBuilder.clearContext(_drawCtx,  _drawCanvas.width, _drawCanvas.height);
            CanvasLabelBuilder.clearContext(_dummyCtx, _dummyCanvas.width, _dummyCanvas.height);

            let textVO = CanvasLabelBuilder._fillText( _dummyCtx, marginTop, marginBottom, message, color, info );
            _dummyCtx.restore();
            _drawCanvas.width  = textVO.w;
            _drawCanvas.height = textVO.h;
            // 백그라운드 사용 시 처리
            if(info.useBackGround){
                  let border      = info.borderWidth;
                  marginTop    += border;
                  marginBottom += border;
                  _drawCanvas.width  += border*2;
                  _drawCanvas.height += border*2;
                  _drawCtx.save();
                  _drawCtx.fillStyle = info.bgColor;
                  _drawCtx.strokeStyle = info.borderColor;
                  _drawCtx.lineWidth = border;
                  CanvasLabelBuilder._drawRoundRect( _drawCtx, border/2, border/2, textVO.w+border/2, textVO.h+border/2, info.cornerRadius );
                  _drawCtx.restore();
            }
            //_drawCtx.save();
            CanvasLabelBuilder._fillText( _drawCtx, marginTop, marginBottom, message, color, info );
            _drawCtx.restore();
            /*let img:HTMLImageElement = document.createElement("img");
            img.src = _drawCanvas.toDataURL("image/png");*/
            let w = ThreeUtil.getBestPowerOf2(_drawCanvas.width);
            let h = ThreeUtil.getBestPowerOf2(_drawCanvas.height);
            let texture = new THREE.Texture( _drawCanvas );
            texture.needsUpdate           = true;
            texture.anisotropy            = 16;
            return {texture:texture, w:w, h:h};
      }

}
