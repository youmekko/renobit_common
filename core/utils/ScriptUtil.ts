class ScriptUtil {
      // from: https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/eval
      public static eval(obj){
        return Function('"use strict";return (' + obj + ')')();
  }
}
export default ScriptUtil;
