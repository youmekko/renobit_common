import {CPLogger} from "./CPLogger";


export default class CPNotification {
      private static _instance:CPNotification;
      private constructor(){
            if( !CPNotification._instance ){
                 CPNotification._instance = this;
            }
            return CPNotification._instance;
      }

      public static getInstance():CPNotification {
            return CPNotification._instance || new CPNotification();
      }

      throwErrorWithMessage( message:string ): never {
            throw new Error( message );
      }

      notifyMessage( message?:string, args?:any ) : void {
            CPLogger.LOG_TYPE = CPLogger.WARN;
            CPLogger.log( message, [args] );
      }

}
