import CommonStatic from "../../../editor/controller/common/CommonStatic";

export default class ComponentCounter {

      public static shouldBeOneComponent(componentName:string){
            return CommonStatic.SHOULD_BE_ONE_COMPONENT_NAMES.some(name => name === componentName);
      }

      public static positionedComponent(componentName:string){
            return window.wemb.mainPageComponent.getComInstanceByComponentName(componentName);
      }

      public static shouldBeOneAndPositionedComponent(componentName:string){
            return ComponentCounter.shouldBeOneComponent(componentName) && ComponentCounter.positionedComponent(componentName);
      }
}
