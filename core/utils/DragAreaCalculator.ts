import Point from "../geom/Point";
import {Rectangle} from "../geom/interfaces/GeomInterfaces";

export default class DragAreaCalculator {

      private static _instance:DragAreaCalculator;
      private constructor(){
            if( !DragAreaCalculator._instance ){
                  DragAreaCalculator._instance = this;
            }
            return DragAreaCalculator._instance;
      }

      public static getInstance():DragAreaCalculator {
            return DragAreaCalculator._instance || new DragAreaCalculator();
      }

      _startPoint:Point;
      setStartPoint( point:Point ):void{
            this._startPoint = point;
      }

      getDragArea( point:Point ):Rectangle {
            let diff = point.subtract(this._startPoint);
            let rectangle:Rectangle ={x:this._startPoint.x, y:this._startPoint.y, width:0, height:0};
            if( diff.x < 0 ){ rectangle.x = point.x; }
            if( diff.y < 0 ){ rectangle.y = point.y; }
            rectangle.width = Math.abs(diff.x);
            rectangle.height = Math.abs(diff.y);
            return rectangle;
      }

      reset():void{
            this._startPoint = null;
      }


}
