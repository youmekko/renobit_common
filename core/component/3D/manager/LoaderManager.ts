import ThreeUtil from "../../../utils/ThreeUtil";

declare var THREE;
export default class LoaderManager {


      /*[ MESH, TEXTURE 를 재사용하기 위한 풀 ]*/
      static compLoaderPool: Map<String, any> = new Map();

      /*[ obj 로더 ]*/
      static objLoader: any = new THREE.OBJLoader();

      /*[ texture 로더 ]*/
      static textureLoader: any = new THREE.TextureLoader();

      /*[ font 로더 ]*/
      static fontLoader: any = new THREE.FontLoader();

      /*[ json 로더 ]*/
      static JSONLoader: any = new THREE.JSONLoader();

      static getLoaderPool(compName: string, type: string) {

            var oPoolInfo = null;
            var oPoolObj = null;
            var oPoolSize = null;

            if (LoaderManager.compLoaderPool.has(compName)) {

                  oPoolInfo = LoaderManager.compLoaderPool.get(compName)[type];

                  if (type === "OBJECT") oPoolObj = LoaderManager.clone(LoaderManager.getCompType(oPoolInfo.MESH), oPoolInfo.MESH);

                  if (type === "JSON_OBJECT") oPoolObj = oPoolInfo.MESH.clone();

                  oPoolSize = oPoolInfo.SIZE;
                  oPoolObj.scale.set(1 / oPoolSize.x, 1 / oPoolSize.y, 1 / oPoolSize.z);
                  oPoolObj.updateMatrixWorld();


            } else {

                  console.log("compLoaderPool에 해당 컴포넌트에 대한 리소스가 없습니다.");

            }

            return {MESH: oPoolObj, SIZE: oPoolSize};

      }


      static hasLoaderPool( compName:string ):boolean {
            return (LoaderManager.compLoaderPool.get(compName) !== null) ? true : false;
      }

      /**
       * Mesh 복사 하기 전 타입 추출
       **/
      static getCompType(obj: any) {

            let emptyObj;

            if (obj.type === "Group") {

                  emptyObj = new THREE.Group();

            } else if (obj.type === "Mesh") {

                  emptyObj = new THREE.Mesh();

            }

            return emptyObj;

      }

      /**
       * Mesh생성 함수
       **/
      static async setLoaderObj(info: any) {

            var obj: any;

            /* compLoaderPool에 해당 MESH가 있을경우 풀에서 추출 */
            if (LoaderManager.compLoaderPool.has(info.compName)) {

                  obj = LoaderManager.compLoaderPool.get(info.compName)["OBJECT"];

            } else {

                  let temp: any = await LoaderManager.objLoading(info);
                  obj = temp["OBJECT"]

            }

            var mesh = obj.MESH;
            var size = obj.SIZE;
            obj = LoaderManager.clone(LoaderManager.getCompType(mesh), mesh);
            obj.scale.set(1 / size.x, 1 / size.y, 1 / size.z);
            obj.updateMatrixWorld();
            return {MESH: obj, SIZE: size};

      }

      /**
       * Texture 생성 함수
       **/
      static async setLoaderTexture(info: any) {

            return await LoaderManager.textureLoading(info);
      }

      /**
       * Font 생성 함수
       **/
      static async setLoaderFont(info: any) {

            return await LoaderManager.fontLoading(info);

      }


      /**
       * Json 생성 함수
       **/
      static async setLoaderJson(info: any) {

            var jsonObj: any;

            if (LoaderManager.compLoaderPool.has(info.compName)) {

                  jsonObj = LoaderManager.compLoaderPool.get(info.compName)["JSON_OBJECT"];

            } else {

                  let temp: any = await LoaderManager.jsonLoading(info);

                  jsonObj = temp["JSON_OBJECT"];

            }

            let oResult = await LoaderManager.jsonLoading(info);

            var mesh = jsonObj.MESH;

            var size = jsonObj.SIZE;

            jsonObj = mesh.clone();

            jsonObj.scale.set(1 / size.x, 1 / size.y, 1 / size.z);

            return {MESH: jsonObj, SIZE: size};

      }

      /**
       * 반환된 Mesh를 재사용하기 위한 깊은 복사 함수
       **/
      static clone(object: any, copyObject: any) {

            if (object === undefined) object = new THREE.Object3D();

            object.name = copyObject.name;

            object.up.copy(copyObject.up);

            if (copyObject.material !== undefined && copyObject.geometry !== undefined) {

                  if (copyObject.material instanceof Array) {

                        object.material = [];

                        for (let i = 0; i < copyObject.material.length; i++) {

                              object.material.push(copyObject.material[i].clone());

                        }

                  } else {

                        object.material = copyObject.material.clone();
                  }


                  object.geometry = copyObject.geometry.clone();

            }

            object.position.copy(copyObject.position);
            object.quaternion.copy(copyObject.quaternion);
            object.scale.copy(copyObject.scale);
            object.renderDepth = copyObject.renderDepth;
            object.rotationAutoUpdate = copyObject.rotationAutoUpdate;
            object.matrix.copy(copyObject.matrix);
            object.matrixWorld.copy(copyObject.matrixWorld);
            object.matrixAutoUpdate = copyObject.matrixAutoUpdate;
            object.matrixWorldNeedsUpdate = copyObject.matrixWorldNeedsUpdate;
            object.type = copyObject.type;
            object.visible = copyObject.visible;
            object.castShadow = copyObject.castShadow;
            object.receiveShadow = copyObject.receiveShadow;
            object.frustumCulled = copyObject.frustumCulled;
            object.layers = copyObject.layers;
            object.renderOrder = copyObject.renderOrder;
            object.userData = JSON.parse(JSON.stringify(copyObject.userData));

            if (copyObject.children.length > 0) {

                  let childSize = copyObject.children.length;

                  for (var i = 0; i < childSize; i++) {

                        var child = LoaderManager.clone(new THREE.Mesh(), copyObject.children[i]);

                        object.add(child);

                  }

            }

            return object;

      }

      /**
       * OBJ Loading에 사용되는 함수 ( LoaderManager.compLoaderPool( Key : MESH ) 을 참조 )
       * @param info{ compName: 컴포넌트 이름,
     *              objPath : .obj 경로,
     *              mapPath : texture 경로,
     *              recycleYn : compLoaderPool에 저장 여부 ( true / false ),
     *              loaderFunc : Cumstom OBJ Load 함수
     *             }
       **/
      static async objLoading(info: { compName: String, objPath: String, mapPath: String, recycleYn: boolean, loaderFunc: Function }) {

            /* 커스텀 함수가 있을경우 */
            if (info.loaderFunc) {

                  let object = await info.loaderFunc.call(LoaderManager, info);
                  var obj = {
                        OBJECT: {
                              MESH: object,
                              SIZE: ThreeUtil.getObjectSize(object)
                        }
                  };

                  if (info.recycleYn) {

                        if (LoaderManager.compLoaderPool.has(info.compName)) {

                              //LoaderManager.compLoaderPool.set(info.compName, Object.assign(LoaderManager.compLoaderPool.get(info.compName), obj));

                        } else {

                              LoaderManager.compLoaderPool.set(info.compName, obj);
                        }

                  }

                  return obj;

            } else {

                  /* 커스텀 함수가 없을경우 기본 로더 */
                  let loader = new Promise((resolve, reject) => {

                        LoaderManager.objLoader.load(window.wemb.configManager.serverUrl + info.objPath, object => {

                              let size = object.children.length;

                              for (let i = 0; i < size; i++) {
                                    let child = object.children[i];
                                    if (child instanceof THREE.Mesh) {
                                          child.material = new THREE.MeshBasicMaterial();
                                          child.material.side = THREE.DoubleSide;
                                          if (info.mapPath) {

                                                child.material.map = LoaderManager.textureLoader.load(window.wemb.configManager.serverUrl + info.mapPath + "/" + child.name + ".png");
                                                //object.children[i]["material"]["map"] = LoaderManager.textureLoader.load(window.wemb.configManager.serverUrl + info.mapPath + "/" + object.children[i].name + ".png");

                                                /* 투명 처리  */
                                                if (child.name.includes("_A") || child.name.includes("area") || child.name.includes("base")) {

                                                      child.material.transparent = true;

                                                }

                                          }

                                          /* 그림자 처리 */
                                          //object.children[i].castShadow = true;

                                    }

                              }

                              var obj = {
                                    OBJECT: {
                                          MESH: object,
                                          SIZE: ThreeUtil.getObjectSize(object)
                                    }
                              };

                              if (info.recycleYn) {

                                    if (LoaderManager.compLoaderPool.has(info.compName)) {

                                          //LoaderManager.compLoaderPool.set(info.compName, Object.assign(LoaderManager.compLoaderPool.get(info.compName), obj));

                                    } else {

                                          LoaderManager.compLoaderPool.set(info.compName, obj);
                                    }

                              }

                              resolve(obj);

                        });

                  });

                  return await loader;

            }

      }

      /**
       * TEXTURE Loading에 사용되는 함수 ( LoaderManager.compLoaderPool( Key : TEXTURE ) 을 참조 )
       * @param info{ compName: 컴포넌트 이름,
     *              texturePath : texture 경로,
     *              recycleYn : compLoaderPool에 저장 여부 ( Y/N )
     *             }
       **/
      static textureLoading(info) {

            return new Promise((resolve, reject) => {

                  if (LoaderManager.compLoaderPool.has(info.compName)) {

                        resolve(LoaderManager.compLoaderPool.get(info.compName)["TEXTURE"]);

                  } else {

                        LoaderManager.textureLoader.load(window.wemb.configManager.serverUrl + info.mapPath + "/" + info.mapNameList + ".png", (texture) => {

                              if (info.recycleYn) {

                                    if (LoaderManager.compLoaderPool.has(info.compName)) {

                                          LoaderManager.compLoaderPool.set(info.compName, Object.assign(LoaderManager.compLoaderPool.get(info.compName), {TEXTURE: texture}));

                                    } else {

                                          LoaderManager.compLoaderPool.set(info.compName, {TEXTURE: texture})
                                    }

                              }

                              resolve(texture);

                        });

                  }

            });

      }

      /**
       * FONT Loading에 사용되는 함수 ( LoaderManager.compLoaderPool( Key : FONT_JSON ) 을 참조 )
       * @param info{ compName: 컴포넌트 이름,
     *              fontPath : texture 경로,
     *              recycleYn : compLoaderPool에 저장 여부 ( Y/N )
     *             }
       **/
      static fontLoading(info) {

            // return new Promise((resolve, reject) => {

            //     if (LoaderManager.config.compLoaderPool.has(info.compName)) {

            //         resolve(LoaderManager.config.compLoaderPool.get(info.compName)["FONT_JSON"]);

            //     } else {

            //         LoaderManager.config.fontLoader.load("static/assets/3Dcomponent/" + info.fontPath, (font) => {

            //             if (info.recycleYn) {

            //                 LoaderManager.config.compLoaderPool.set(info.compName, { FONT_JSON: font })

            //             }

            //             resolve(font);

            //         });

            //     }

            // });

      }

      /**
       * JSON Loading에 사용되는 함수 ( LoaderManager.compLoaderPool( Key : JSON_MESH ) 을 참조 )
       * @param info{ compName: 컴포넌트 이름,
     *              jsonPath : json 경로,
     *              texturePath : texture 경로,
     *              recycleYn : compLoaderPool에 저장 여부 ( Y/N )
     *             }
       **/
      static async jsonLoading(info) {

            var loader = new Promise((resolve, reject) => {

                  LoaderManager.JSONLoader.load(window.wemb.configManager.serverUrl + info.jsonPath, (geometry) => {

                        geometry.computeFaceNormals();
                        geometry.computeVertexNormals();

                        resolve(geometry);

                  });

            });

            var geometry = await loader;

            var material = new THREE.MeshBasicMaterial({skinning: true, map: new THREE.Texture()});

            material.side = THREE.DoubleSide;
            material.map = LoaderManager.textureLoader.load(window.wemb.configManager.serverUrl + info.texturePath + "/" + info.compName + ".png");

            var mesh = new THREE.SkinnedMesh(geometry, material);

            mesh.rotation.set(-Math.PI / 2, 0, 0);

            var obj = {
                  JSON_OBJECT: {
                        MESH: mesh,
                        SIZE: ThreeUtil.getObjectSize(mesh)
                  }
            };

            if (info.recycleYn) {

                  if (LoaderManager.compLoaderPool.has(info.compName)) {

                        //LoaderManager.compLoaderPool.set(info.compName, Object.assign(LoaderManager.compLoaderPool.get(info.compName), obj));
                        //LoaderManager.compLoaderPool.set(info.compName, Object.assign(LoaderManager.compLoaderPool.get(info.compName), obj));

                  } else {

                        LoaderManager.compLoaderPool.set(info.compName, obj);
                  }

            }

            return obj;

      }

      static async generateTexture(color) {

            var canvas = document.createElement('canvas');
            canvas.width = 32;
            canvas.height = 32;

            var context = canvas.getContext('2d');

            context.fillStyle = color || "#ffffff";
            context.fillRect(0, 0, 32, 32);

            return canvas;

      }

      static async loadImage(src, material) {

            var image = document.createElement('img');

            material.wireframe = false;
            material.opacity = 1;

            material.map.image = image;
            image.onload = function () {
                  material.map.needsUpdate = true;
            };

            image.src = src;

            return material;

      }

}
