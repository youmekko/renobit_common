import WVComponentPropertyManagerCore from "../../WVComponentPropertyManagerCore";

export default class WV3DComponentPropertyManager extends WVComponentPropertyManagerCore {


// 기본 프로퍼티 정보입니다.
      static default_properties = {
            "primary": {
                  "id": "",
                  "name": "",
                  "layerName": ""
            },

            "setter": {
                  "lock": false,
                  "visible": true,
                  "position": {x: 0, y: 0, z: 0},
                  "size":     {x: 1, y: 1, z: 1},
                  "rotation": {x: 0, y: 0, z: 0},
                  "opacity": 100,
                  "shadow": false,
                  "originSize": false,
                  "mouseEnable": true,
                  "outlineEnable": true,
                  "outlineColor":"#00ff00",
                  "selectedOutlineColor":"#00ff00",
                  "color": "#ffffff",
                  "emissive":"#000000"
            },

            "label": {
                  "label_using": "Y",
                  "label_text": "BoxComponent",
                  "label_color": "#ffffff",
                  "label_opacity": 1,
                  "label_font_size": 15,
                  "label_background_color": "#141366",
                  "label_font_type": "inherit",
                  "label_border": "1px none #000000",
                  "label_font_weight": "normal",
                  "line_connect": true,
                  "x": 0,
                  "y": 20
            },

            "info": {
                  "componentName": "",
                  "version": "1.0.0",
                  "category": "3D"
            },

            "events": {
                  "click": "",
                  "dblclick": "",
                  "register": ""
            },
            "editorMode": {
                  "lock": false,
                  "visible": true
            }

      };

// 프로퍼티 패널에서 사용할 정보 입니다.
      static property_panel_info = [{
            label: "일반 속성",
            template: "primary",
            children: [{
                  owner: "primary",
                  name: "name",
                  type: "string",
                  label: "이름",
                  writable: true,
                  show: true,
                  description: "컴포넌트 아이디"
            }]
      }, {
            name:"display",
            label: "위치 크기 정보",
            template: "pos-size-3d",
            children: [{
                  owner: "setter",
                  name: "lock",
                  type: "checkbox",
                  label: "잠금",
                  show: true,
                  writable: true,
                  description: "잠금 유무"
            }, {
                  owner: "setter",
                  name: "visible",
                  type: "checkbox",
                  label: "잠금",
                  show: true,
                  writable: true,
                  description: "잠금 유무"
            }, {
                  owner: "setter",
                  name: "position",
                  type: "object",
                  label: "위치",
                  show: true,
                  writable: true,
                  description: "위치 값"
            }, {
                  owner: "setter",
                  name: "size",
                  type: "object",
                  label: "크기",
                  show: true,
                  writable: true,
                  description: "크기 값"
            }, {
                  owner: "setter",
                  name: "rotation",
                  type: "object",
                  label: "회전",
                  show: true,
                  writable: true,
                  description: "회전 값"
            }]
      }, {
            label: "label",
            template: "label-3d",
            children: [{
                  owner: "label",
                  name: "label",
                  label: "label",
                  type: "string",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            }]
      },
            {
                  label: "Original Size",
                  template: "vertical",
                  children: [{
                        owner: "setter",
                        name: "originSize",
                        type: "checkbox",
                        label: "Use",
                        show: true,
                        writable: true,
                        description: "원본 크기 전환"
                  }]
            },
            {
                  label: "color",
                  template: "color-3d",
                  children: [{
                        owner: "setter",
                        name: "color",
                        type: "color",
                        label: "color",
                        show: true,
                        writable: true,
                        description: "color"
                  },{
                        owner: "setter",
                        name: "emissive",
                        type: "color",
                        label: "emissive",
                        show: true,
                        writable: true,
                        description: "emissive"
                  }]
            },
            {
                  label: "Opacity",
                  template: "vertical",
                  children: [{
                        owner: "setter",
                        name: "opacity",
                        type: "number",
                        label: "Opacity",
                        show: true,
                        writable: true,
                        description: "opacity",
                        options: {
                              min: 0,
                              max: 100
                        }
                  }]
            }
      ];

      static property_info = [{
            name: "normal",
            label: "일반 속성",
            children: [{
                  owner: "setter",
                  name: "name",
                  type: "string",
                  label: "name",
                  writable: false,
                  show: true,
                  description: "인스턴스명"
            }, {
                  owner: "setter",
                  name: "visible",
                  type: "boolean",
                  label: "visible",
                  writable: true,
                  show: true,
                  defaultValue: "true",
                  description: "visible 상태"
            }, {
                  owner: "setter",
                  name: "color",
                  type: "color",
                  label: "color",
                  writable: true,
                  show: true,
                  defaultValue: "#ffffff",
                  description: "컴포넌트 컬러"
            }, {
                  owner: "setter",
                  name: "opacity",
                  type: "number",
                  label: "Opacity",
                  writable: true,
                  show: true,
                  defaultValue: "100",
                  description: "컴포넌트 알파"
            }, {
                  owner: "setter",
                  name: "outlineEnable",
                  type: "boolean",
                  label: "use",
                  writable: true,
                  show: true,
                  defaultValue: true,
                  description: "마우스 호버 이벤트 시 boudingBox 활성화"
            }]
      }, {
            name :"display",
            label: "위치 크기 정보",
            children: [{
                  name: "position",
                  type: "vector3",
                  show: true,
                  writable: true,
                  defaultValue: "{x:0,y:0,z:0}",
                  description: "컴포넌트 position을 설정합니다."
            }, {
                  name: "size",
                  type: "vector3",
                  show: true,
                  writable: true,
                  defaultValue: "{x:10,y:10,z:10}",
                  description: "컴포넌트 size를 설정합니다."
            }, {
                  name: "rotation",
                  type: "vector3",
                  show: true,
                  writable: true,
                  defaultValue: "{x:0,y:0,z:0}",
                  description: "컴포넌트 rotate값 설정합니다."
            }]
      }, {
            label: "레이블 속성",
            children: [
                  {
                        owner: "label",
                        name: "label_using",
                        type: "string",
                        show: true,
                        writable: true,
                        defaultValue: "'Y'",
                        description: "라벨 사용 여부 Y/N값 입니다."
                  }, {
                        owner: "label",
                        name: "label_text",
                        type: "string",
                        show: true,
                        writable: true,
                        defaultValue: "'component-label'",
                        description: "라벨영역에 노출할 텍스트 문자열 입니다."
                  }
            ]
      }
      ];


//  추후 추가 예정
      static method_info = [];


// 이벤트 정보
      static event_info = [{
            name: "click",
            label: "클릭이벤트",
            description: "클릭이벤트입니다.",
            properties: [],

      }, {
            name: "dblclick",
            label: "더블클릭이벤트",
            description: "더블클릭이벤트입니다.",
            properties: [],

      }, {
            name: "register",
            label: "컴포넌트 등록 완료",
            description: "컴포넌트 등록 완료 이벤트 입니다.",
            properties: []
      }];


      /*
      컴포넌트 기본 속성, 프로퍼티 패널 정보, 이벤트 정보, 메서드 정보를 생성하는 기능
      일반적으로 컴포넌트 내부에서 호출됨.
       */
      static attach_default_component_infos(ComponentClass: any, default_info: any = {}) {
            ComponentClass.default_properties = WV3DComponentPropertyManager.clone_default_properties();
            ComponentClass.default_properties.setter.componentName = ComponentClass.name;
            $.extend(true, ComponentClass.default_properties, default_info);

            ComponentClass.property_panel_info = WV3DComponentPropertyManager.clone_default_property_panel_info();
            ComponentClass.property_info = WV3DComponentPropertyManager.clone_default_property_info();
            ComponentClass.method_info = WV3DComponentPropertyManager.clone_default_method_info();
            ComponentClass.event_info = WV3DComponentPropertyManager.clone_default_event_info();
      }


      static clone_default_properties() {
            return JSON.parse(JSON.stringify(WV3DComponentPropertyManager.default_properties));
      }

      /*
            기존 방식 slice이용.
            -컴포넌트 별 속성 조정 시 다른 클래스에도 동일한 적용이 생김.
            -다른 컴포넌트 영향이 없도록 json방식을 이용해 복제 적용
      */
      static clone_default_property_panel_info() {
            return JSON.parse(JSON.stringify(WV3DComponentPropertyManager.property_panel_info));
      }

      static clone_default_property_info() {
            return JSON.parse(JSON.stringify(WV3DComponentPropertyManager.property_info));
      }

      static clone_default_method_info() {
            return WV3DComponentPropertyManager.method_info.slice(0);
      }

      static clone_default_event_info() {
            return WV3DComponentPropertyManager.event_info.slice(0);
      }


}
