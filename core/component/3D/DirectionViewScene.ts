import PersonViewControls from "./PersonViewControls";
import {THREE} from "../../utils/reference";
import MeshManager from "./manager/MeshManager";
import {CPLogger} from "../../utils/CPLogger";

export default class DirectionViewScene {

    public static readonly OBJ_NAME = "obj_orientation";
    private mainCamera:THREE.Camera;
    private controls: any;
    private objOrientation: any;
    private enabled:boolean;
    private prevCamera:THREE.Camera;
    private prevTarget: any;
    private opt:any;
    private iContainer:any;
    private iRenderer:any;
    private iScene:any;
    private iCamera:any;
    private workHolder:any;
    private objHolder:any;


    constructor( mainCamera:any, controls:any, opt:any ){
        this.mainCamera = mainCamera;
        this.controls = controls;

        this.objOrientation = null;
        this.enabled = true;

        this.prevCamera = null;
        this.prevTarget = null;

        this.opt = Object.assign({
            width: 110,
            height:110,
            distance:10
        }, opt );

        this.iContainer = this.opt.container || document.createElement("div");
        this.iContainer.style.position = "absolute";

        this.iContainer.style.width  = this.opt.width+"px";
        this.iContainer.style.height = this.opt.height+"px";

        this.iRenderer = new THREE.WebGLRenderer({alpha:true});
        this.iRenderer.setClearColor(0x000000, 0);
        this.iRenderer.setSize(this.opt.width, this.opt.height);
        this.iContainer.appendChild( this.iRenderer.domElement );

        this.iScene = new THREE.Scene();
        this.iCamera = new THREE.PerspectiveCamera( 60, this.opt.width/this.opt.height, 0.1, 1000 );
        this.iCamera.up = this.mainCamera.up; // 메인 카메라의 업벡터 동기화
        var ambientLight = new THREE.AmbientLight( 0xffffff );
        this.iScene.add(ambientLight);

        this.workHolder =  this.opt.workHolder;

        this.objHolder = new THREE.Object3D();
        this.objHolder.position.set(0, 0, 0);
        this.objHolder.scale.set(5,5,5);

        this.iScene.add(this.objHolder);

          /*
	    2018.09.20
		  초기 시작시 활성화 유무 처리
	     */
          this.setVisible(wemb.configManager.viewerDirectionView)

    }

    setEnabled(value:boolean){
        this.enabled=value;
    }
    getEnabled(){
        return this.enabled;
    }

    setControls( objControls:any ){
        this.controls = objControls;
    };

    setTextureUrl( objURL:string=null, textureUrl:string=null ){
        //console.log("setTextureUrl Vue.$configManager.serverUrl = ", wemb.configManager.serverUrl);
        var newTextureUrl = 'client/common/assets/orientation/orientation.png';
        var newObjUrl =  'client/common/assets/orientation/orientation_ET.obj';
        var image = document.createElement("img");
        image.src =  newTextureUrl;
        image.onload = (event)=>{
              let loader = new THREE.OBJLoader();
              let texture = new THREE.Texture(image);
              texture.needsUpdate = true;
              loader.load(
                    newObjUrl,
                    ( objObject )=>{
                          objObject.traverse( function ( child ) {
                                if ( child instanceof THREE.Mesh ) {
                                      (child.material as THREE.MeshBasicMaterial).map = texture;
                                }
                          } );
                          objObject.position.set(0, 0, 0);
                          objObject.name = DirectionViewScene.OBJ_NAME;
                          this.objHolder.add(objObject);
                          this.objOrientation = objObject;
                    }, undefined, (error)=>{
                          CPLogger.log("error-objLoad", [error.toString()] );
                    }
              );
        }

    };



    render(){
        if( this.enabled && this.objOrientation != null ){
              this.iCamera.rotation.copy(this.mainCamera.rotation);
              this.iCamera.position.copy(this.mainCamera.position);
              if( (this.controls instanceof PersonViewControls ) == false ){
                    this.iCamera.position.sub(this.controls.target);
                    this.iCamera.position.x*=-1;
              } else {
                    this.objHolder.rotation.set(0, this.controls.rotateY, 0);
                    this.objOrientation.rotation.set(this.controls.rotateX, 0, 0 );
              }
              this.iCamera.position.setLength(this.opt.distance);
              this.iCamera.lookAt( this.iScene.position );

        }
        this.iRenderer.render( this.iScene, this.iCamera );
    }


    destroy(){
          this.enabled = false;

          this.iRenderer.dispose();
          for( let i=0; i <  this.iScene.children.length; i++){
                let obj =  this.iScene.children[i];
                MeshManager.disposeMesh(obj);
                this.iScene.remove(obj);
          }

          this.iCamera   = null;
          this.iRenderer = null;
          this.iScene    = null;
          this.mainCamera = null;
          this.controls = null;
    }

      /*
	2018.09.20 (ckkim)
	  환경설정에 따른 활성화 비활성화 처리
	 */
      setVisible(value:boolean){
            //console.log("@@ wemb.configManager.viewerDirectionView ", wemb.configManager.viewerDirectionView, this.iContainer)
            if(value==true){
                  this.iContainer.style.display  = "block";
            }else {
                  this.iContainer.style.display  = "none";
            }
      }

};
