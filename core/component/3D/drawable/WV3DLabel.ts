import {$, THREE} from "../../../utils/reference";
import MeshManager from "../manager/MeshManager";

export default class WV3DLabel {

      static LINE_NAME:string = "labelLine";
      label:any = null;
      parent:any;
      line:THREE.Line = null;
      container:THREE.Object3D;


      constructor(){
            this._createElements();
      }
      public destroy(){
            if(this.label)
                  this.clear();
      }
      protected _createElements(){
            this.container = new THREE.Object3D;
            this.label = new THREE.CSS2DObject(document.createElement('div'));
            this.label.element.className = "label";
            $(this.label.element).css({
                  "position" : "absolute",
                  "display":"inline-block",
                  "pointer-events":"none",
                  "background-color":"#0022ff",
                  "color" :"#ffffff"
            });
            this.container.add(this.label);
      }

      public createLine(lineColor){
            let geometry:THREE.Geometry = MeshManager.getGeometry('Geometry').clone(); // pool에서 가져오기
            geometry.vertices.push(new THREE.Vector3(this.labelPosition.x, this.labelPosition.y, this.labelPosition.z));
            geometry.vertices.push(new THREE.Vector3(0, 0, 0 ));
            let material = new THREE.LineBasicMaterial({ color: lineColor, transparent:true});
            this.line = new THREE.Line(geometry, material);
            this.line.name = WV3DLabel.LINE_NAME;
            this.line.userData.mouseEnabled = false;
            this.container.add(this.line);
      }


      public draw(parent:any): void {
            this.parent = parent;
            parent.appendElement.add(this.container);
      }

      public lineClear() : void {
            if(this.line !== null){
                  this.container.remove(this.line);
                  this.line.material.dispose();
                  this.line.geometry.dispose();
                  this.line = null;
            }
      }
      public clear(): void {
            this.parent.appendElement.remove(this.container);
            this.container.remove(this.label);

            if(this.line !== null){
                  this.container.remove(this.line);
                  this.line.material.dispose();
                  this.line.geometry.dispose();
                  this.line = null;
            }

            this.label = null;

            MeshManager.disposeMesh(this.container);
            this.container = null;
            this.parent  = null;
      }
      labelShow():void {
            $(this.label.element).show();
      }
      //라벨을 보이게 한다.
      public show():void{
            if(this.label.element)
                  $(this.label.element).show();
            if(this.line !== null)
                  this.line.visible = true;
      }
      //라벨을 보이지 않게만 한다.
      public notShow() : void {
            $(this.label.element).hide();
            if(this.line !== null)
                  this.line.visible =false;
      }

      public css( styleInfo:any ):void {
            $(this.label.element).css(styleInfo);
            let bgColor = $(this.label.element).css("background-color");
            let opacity = +$(this.label.element).css("opacity");
            if(bgColor){
                  if(this.line !== null)
                        this.line.material.color.set(bgColor);
            }
            if(opacity){
                  if(this.line !== null)
                        this.line.material.opacity = opacity;
            }
      }

      set text( str ){
            $(this.label.element).html(str);
      }

      get text():String {
            return $(this.label.element).text();
      }


      set lineConnect( bValue:boolean ){
            if(this.line !== null)
                  this.line.visible = bValue;
      }

      get lineConnect():boolean {
            if(this.line !== null)
                  return this.line.visible;
            else
                  return false;
      }

      set labelPosition( value:THREE.Vector3 ){
            this.label.position.copy(value);
            if(this.line !== null){
                  this.line.geometry.vertices[0].copy( this.label.position );
                  this.line.geometry.verticesNeedUpdate=true;
            }
      }

      get labelPosition():THREE.Vector3 {
            return this.label.position;
      }

      set position( value:THREE.Vector3 ){
            this.container.position.copy(value);
      }

      get position():THREE.Vector3 {
            return this.container.position;
      }

}

