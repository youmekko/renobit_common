import {$, THREE} from "../../../utils/reference";
import MeshManager from "../manager/MeshManager";

export default class WV3DIconLabel {

      protected _parent:any;
      protected _container:any;
      protected _data:any;

      constructor(){
            this._container = new THREE.CSS2DObject(document.createElement("div"));
            let label     = $(document.createElement("p"));
            let icon      = $(document.createElement("img"));
            $(this._container.element).append(label);
            $(this._container.element).append(icon);
      }

      draw(parent:any){
            this._parent = parent;
            this._parent.appendElement.add(this._container);
      }

      clear(){
            this._parent.appendElement.remove(this._container);
      }

      destroy(){
            this.clear();
            this._parent = null;
            $(this.element).off();
            MeshManager.disposeMesh(this._container);
      }


      get size(){
            return this.element.getBoundingClientRect();
      }

      set iconPath( value:string ){
            if(Boolean(value)){
                  this.icon.attr("src", value );
            }
      }

      get iconPath():string {
            return this.icon.attr("src");
      }

      set text( value:string ){
            this.label.html(value);
      }

      get text(){
            return this.label.text();
      }

      get icon():any {
            return $(this._container.element).find("img");
      }

      get label():any {
            return $(this._container.element).find("p");
      }

      set data( data:any ){
            this._data = data;
      }

      get data():any{
            return this._data;
      }

      set position( value:any ){
            this._container.position.set( value.x, value.y, value.z );
      }

      get position(){
            return this._container.position;
      }

      get element():any {
            return this._container.element;
      }

      set visible( value:boolean ) {
            this._container.visible = value;
            value ? $(this.element).show() : $(this.element).hide() ;
      }

      get visible():boolean {
            return this._container.visible;
      }


      get appendElement():any{
            return this._container;
      }
}
