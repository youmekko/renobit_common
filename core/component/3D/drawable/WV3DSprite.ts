import {$, THREE} from "../../../utils/reference";
import WVMouseEvent from "../../../events/WVMouseEvent";
import MeshManager from "../manager/MeshManager";
export default class WV3DSprite {

      protected _container:THREE.Sprite;
      protected _parent:any;
      protected _onClickHandler:Function;
      protected _onDoubleClickHandler:Function;
      protected _onOverHandler:Function;
      protected _onOutHandler:Function;

      constructor(){
            this._onClickHandler          =this.onClick.bind(this);
            this._onDoubleClickHandler    =this.onDoubleClick.bind(this);
            this._onOverHandler           =this.onOver.bind(this);
            this._onOutHandler            =this.onOut.bind(this);
      }

      protected _preventEvent( event ){
            event.stopPropagation();
            event.origDomEvent.preventDefault();
            event.origDomEvent.stopPropagation();
            event.origDomEvent.stopImmediatePropagation();
      }

      protected onClick( event ){
            this._preventEvent(event);
            this.appendElement.dispatchEvent({type:WVMouseEvent.CLICK} );
      }

      protected onDoubleClick( event ){
            this._preventEvent(event);
            this.appendElement.dispatchEvent({type:WVMouseEvent.DOUBLE_CLICK} );
      }

      protected onOver( event ){
            this._preventEvent(event);
            this.appendElement.dispatchEvent({type:WVMouseEvent.ROLL_OVER} );
      }

      protected onOut( event ){
            this._preventEvent(event);
            this.appendElement.dispatchEvent({type:WVMouseEvent.ROLL_OUT} );
      }


      draw(parent:any){
            this._container = new THREE.Sprite;
            this._parent = parent;
            this._parent.appendElement.add(this.appendElement);
            wemb.threeElements.domEvents.addEventListener(this.appendElement, "mouseover", this._onOverHandler, false );
            wemb.threeElements.domEvents.addEventListener(this.appendElement, "mouseout", this._onOutHandler, false );
            wemb.threeElements.domEvents.addEventListener(this.appendElement, "click", this._onClickHandler, false );
            wemb.threeElements.domEvents.addEventListener(this.appendElement, "dblclick", this._onDoubleClickHandler, false );
      }

      clear(){
            this._parent.appendElement.remove(this.appendElement);
      }

      get appendElement(){
            return this._container;
      }

      makeTexture( source:any ){
            let texture = new THREE.Texture(source.texture);
            texture.needsUpdate     =     true;
            let material = new THREE.SpriteMaterial({map:texture});
            if(this._container.material.map){
                  this._container.material.map.dispose();
            }
            this._container.material.dispose();
            this._container.material = material;
            this._container.material.needsUpdate=true;
            this._container.scale.set(source.w, source.h, 1);
      }

      loadSpriteSource( path:string ):Promise<any>{
            return new Promise((resolve, reject)=>{
                  let img = document.createElement("img");
                  img.onload = () => {
                        document.body.appendChild(img);
                        let w = img.offsetWidth;
                        let h = img.offsetHeight;
                        document.body.removeChild(img);
                        resolve([null, {texture:img, w:w, h:h}]);
                  };
                  img.onerror =(event)=>{
                        resolve([event, null]);
                  }
                  img.src = path;
            });
      }

      initialize( source:any ){
            if( typeof source == "string" ){
                  async()=>{
                        const [error, spriteSource] = await this.loadSpriteSource(source);
                        if(error){
                              console.log("loadSpriteSource-error-", error);
                              return;
                        }
                        this.makeTexture(spriteSource);
                  }
            } else {
                  this.makeTexture(source);
            }
      }

      destroy(){
            wemb.threeElements.domEvents.removeEventListener(this.appendElement, "mouseover", this._onOverHandler, false );
            wemb.threeElements.domEvents.removeEventListener(this.appendElement, "mouseout", this._onOutHandler, false );
            wemb.threeElements.domEvents.removeEventListener(this.appendElement, "click", this._onClickHandler, false );
            wemb.threeElements.domEvents.removeEventListener(this.appendElement, "dblclick", this._onDoubleClickHandler, false );
            this._onClickHandler          =null;
            this._onDoubleClickHandler    =null;
            this._onOverHandler           =null;
            this._onOutHandler            =null;
            this._parent.appendElement.remove(this._container);
            this._parent                  =null;
            MeshManager.disposeMesh(this._container);
      }

      set color( value ){
            this.appendElement.material.color.set(value);
      }

      get color(){
            return this.appendElement.material.color.getHexString();
      }

      set scale( value:any ){
            this.appendElement.scale.set( value.x, value.y, value.z );
      }

      get scale():any {
            return this.appendElement.scale;
      }

      set position( value:any ){
            this.appendElement.position.set( value.x, value.y, value.z );
      }

      get position():any {
            return this.appendElement.position;
      }



}
