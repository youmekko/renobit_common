import {THREE} from "../../utils/reference";

export default class TopViewPanel {

      opt: any;
      scene: THREE.Scene;
      renderer: THREE.WebGLRenderer;
      sourceView: any;
      container: HTMLDivElement;
      camera: THREE.Camera;
      hasIcon: boolean;


      constructor(scene:THREE.Scene, sourceView:any, container:HTMLDivElement, opt?:any ) {

            this.opt = Object.assign({
                  top: 10,  // y위치
                  left: 10, // x위치
                  width: 300, // width
                  height: 200, // height
                  startY: 1000,
                  mapSize: {
                        w: 1920,  // 맵 크기정보
                        h: 1280   // 맵 높이정보
                  },
                  zIndex: 12000,
                  syncCamera: false
            }, opt);

            this.scene = scene;
            this.sourceView = sourceView;
            this.renderer = new THREE.WebGLRenderer({alpha: true});
            this.renderer.setClearColor(0x00000000, 0);
            this.renderer.setSize(this.opt.width, this.opt.height);
            this.container = container;
            this.container.setAttribute("id", "top-view-panel");
            this.container.style.position = "absolute";
            this.container.style.top = this.opt.top + "px";
            this.container.style.left = this.opt.left + "px";
            this.container.style.width = this.opt.width + "px";
            this.container.style.height = this.opt.height + "px";
            this.container.style.zIndex = this.opt.zIndex;
            this.container.appendChild(this.renderer.domElement);

            let w = window.innerWidth * .5 || this.opt.mapSize.w / 2;
            let h = window.innerHeight * .5 || this.opt.mapSize.h / 2;
            this.camera = new THREE.OrthographicCamera(-w * 2, w * 2, h * 2, -h * 2, 1, 5000);
            this.camera.position.y = this.opt.startY;
            this.camera.rotation.x = -1 * Math.PI / 2;
            this.camera.rotation.y = 0;
            this.camera.rotation.z = 0;
            this.hasIcon = this.sourceView.icon != null ? true : false;
      }

      render() {

            if (this.opt.syncCamera) {
                  this.camera.position.x = this.sourceView.position.x;
                  this.camera.position.z = this.sourceView.position.z;
            }

            if (this.hasIcon){
                  this.sourceView.icon.scale.set(4, 4, 4);
                  this.sourceView.icon.position.set(0, 50, 0);
                  this.sourceView.icon.visible = true;
            }

            this.renderer.render(this.scene, this.camera);

            if (this.hasIcon){
                  this.sourceView.icon.visible = false;
                  this.sourceView.icon.position.set(0, 0, 0);
                  this.sourceView.icon.scale.set(1, 1, 1);
            }

      }

      destroy() {
            $(this.container).remove();
            this.renderer.dispose();
            this.camera = null;
            this.renderer = null;
      }

}
