import MeshManager from "./manager/MeshManager";
import {THREE, TweenMax} from "../../utils/reference";

export default class PersonViewControls {

      private camera:THREE.Camera;
      private controls:any;
      private posy:number;
      private _enabled:boolean;
      private moveForward:boolean;
      private moveBackward:boolean;
      private moveLeft:boolean;
      private moveRight:boolean;
      private moveUp:boolean;
      private moveDown:boolean;
      private prevTime:number;
      private velocity:THREE.Vector3;
      private direction:THREE.Vector3;
      private startPos:any;
      private startYaw:number;
      private startPitch:number;

      private _icon:THREE.Mesh;
      private _person:THREE.Object3D

      private _cameraOffset:THREE.Vector3;
      private onMouseDownHandler:any;
      private onMouseUpHandler:any;
      private onMouseMoveHandler:any;
      private onKeyDownHandler:any;
      private onKeyUpHandler:any;

      private _transitionInfo:any;
      private transitionTween:any;
      private _rotation:any;

      constructor( camera:THREE.Camera, controls:any, icon?:THREE.Mesh ) {

            this.camera       = camera;
            this.controls     = controls;
            this.posy         = 0;
            this._enabled      = false;
            this.moveForward  = false;
            this.moveBackward = false;
            this.moveLeft     = false;
            this.moveRight    = false;
            this.moveUp       = false;
            this.moveDown     = false;
            this.prevTime     = performance.now();
            this.velocity     = new THREE.Vector3();
            this.direction    = new THREE.Vector3();

            this.startPos     = 0;
            this.startPitch   = 0;

            if(!icon){
                  var geometry = new THREE.SphereGeometry(5, 16, 16);
                  icon = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({color: 0xff0000}));
                  icon.name = "icon";
            }
            this._icon          = icon;
            this._icon.visible  = false;
            this._person        = new THREE.Object3D();
            this._person.name   = "person";
            this._person.add(this._icon);

            this._cameraOffset = new THREE.Vector3(0, 0, 100);
            this.onMouseDownHandler = this.onMouseDown.bind(this);
            this.onMouseUpHandler = this.onMouseUp.bind(this);
            this.onMouseMoveHandler = this.onMouseMove.bind(this);
            this.onKeyDownHandler = this.onKeyDown.bind(this);
            this.onKeyUpHandler  = this.onKeyUp.bind(this);

            this._transitionInfo = {
                  prevCamera:null,
                  prevControlTarget:null
            };
            this.transitionTween = null;
            this._rotation = new THREE.Vector3();

      }

      get person(){
            return this._person;
      }

      get icon(){
            return this._icon;
      }

      set icon( objIcon:THREE.Mesh ) {
            if(this._icon != objIcon ){
                  objIcon.add(this.camera);
                  objIcon.name = "icon";
                  if(this._icon != null){
                        MeshManager.disposeMesh(this._icon);
                  }
                  this._icon = objIcon;
            }
      }

      set cameraOffset( offset:THREE.Vector3 ){
            this._cameraOffset = offset;
      }

      get cameraOffset():THREE.Vector3 {
            return this._cameraOffset;
      }


      tweenTo( value:number, target:number, rate:number ):number {
            return value + ( target - value ) * rate;
      }

      resetTransitionTween():void {
            if(this.transitionTween != null && this.transitionTween.isActive()){
                  this.transitionTween.pause();
                  this.transitionTween.kill();
            }
      }

      activeHelper( focusInfo:any, duration:number, completeCallback?:Function ) {
            this.controls.enabled = false;
            this.resetTransitionTween();

            let tweenObj = {t:0};
            let startQuaternion = this.camera.quaternion.clone();
            let dummyCamera = this.camera.clone();
            let startCamera = this.camera.clone();
            let startPos    = this.position.clone();
            let startRote   = this.rotation.clone();
            let time        = duration;
            dummyCamera.rotation.set( focusInfo.rotation.x, focusInfo.rotation.y, focusInfo.rotation.z);

            // 변경 전 카메라 및 매쉬 위치 기억
            this._transitionInfo.prevControlTarget = this.controls.target.clone();
            this._transitionInfo.prevCamera = startCamera.clone();
            this._transitionInfo.prevPerson = this.person.clone();

            this.transitionTween = TweenMax.to(tweenObj, time, {
                  t : 1,
                  onUpdate:() => {
                        this.position.set(
                              this.tweenTo( startPos.x, focusInfo.position.x, tweenObj.t ),
                              this.tweenTo( startPos.y, focusInfo.position.y, tweenObj.t ),
                              this.tweenTo( startPos.z, focusInfo.position.z, tweenObj.t )
                        );
                        this.rotateX = this.tweenTo( startRote.x, focusInfo.rotation.x, tweenObj.t );
                        this.rotateY = this.tweenTo( startRote.y, focusInfo.rotation.y, tweenObj.t );


                        this.camera.position.set(
                              this.tweenTo( startCamera.position.x, focusInfo.position.x+this.cameraOffset.x, tweenObj.t ),
                              this.tweenTo( startCamera.position.y, focusInfo.position.y+this.cameraOffset.y, tweenObj.t ),
                              this.tweenTo( startCamera.position.z, focusInfo.position.z+this.cameraOffset.z, tweenObj.t )
                        );

                        this.controls.target.set(
                              this.tweenTo( this._transitionInfo.prevControlTarget.x, 0, tweenObj.t ),
                              this.tweenTo( this._transitionInfo.prevControlTarget.y, 0, tweenObj.t ),
                              this.tweenTo( this._transitionInfo.prevControlTarget.z, 0, tweenObj.t )
                        );

                        // 쿼터리온 값을 카메라에 적용
                        THREE.Quaternion.slerp(startQuaternion, dummyCamera.quaternion, this.camera.quaternion, tweenObj.t );

                  },
                  onComplete:() => {
                        if(completeCallback){
                              completeCallback.apply( null, []);
                        }
                  }
            });


      }

      deActiveHelper( duration:number, completeCallback?:Function ){
            this.enabled=false;
            this.resetTransitionTween();
            if(this._transitionInfo.prevCamera){
                  let tweenObj = {t:0};
                  let focusInfo       = this._transitionInfo.prevPerson;
                  let cameraInfo      = this._transitionInfo.prevCamera;
                  let startQuaternion = this.camera.quaternion.clone();
                  let dummyCamera = this.camera.clone();
                  let startCamera = this.camera.clone();
                  let startPos    = this.position.clone();
                  let startRote   = this.rotation.clone();
                  let startTarget = this.controls.target.clone();
                  let time        = duration;
                  dummyCamera.rotation.set( cameraInfo.rotation.x, cameraInfo.rotation.y, cameraInfo.rotation.z);
                  // 변경 전 카메라 및 매쉬 위치 기억
                  this.transitionTween = TweenMax.to(tweenObj, time, {
                        t : 1,
                        onUpdate:() => {
                              this.position.set(
                                    this.tweenTo( startPos.x, focusInfo.position.x, tweenObj.t ),
                                    this.tweenTo( startPos.y, focusInfo.position.y, tweenObj.t ),
                                    this.tweenTo( startPos.z, focusInfo.position.z, tweenObj.t )
                              );
                              this.rotateX = this.tweenTo( startRote.x, focusInfo.rotation.x, tweenObj.t );
                              this.rotateY = this.tweenTo( startRote.y, focusInfo.rotation.y, tweenObj.t );


                              this.camera.position.set(
                                    this.tweenTo( startCamera.position.x, cameraInfo.position.x, tweenObj.t ),
                                    this.tweenTo( startCamera.position.y, cameraInfo.position.y, tweenObj.t ),
                                    this.tweenTo( startCamera.position.z, cameraInfo.position.z, tweenObj.t )
                              );

                              this.controls.target.set(
                                    this.tweenTo( startTarget.x, this._transitionInfo.prevControlTarget.x, tweenObj.t ),
                                    this.tweenTo( startTarget.y, this._transitionInfo.prevControlTarget.y, tweenObj.t ),
                                    this.tweenTo( startTarget.z, this._transitionInfo.prevControlTarget.z, tweenObj.t )
                              );

                              // 쿼터리온 값을 카메라에 적용
                              THREE.Quaternion.slerp(startQuaternion, dummyCamera.quaternion, this.camera.quaternion, tweenObj.t );

                        },
                        onComplete:() => {

                              this._transitionInfo.prevCamera = null;
                              this._transitionInfo.prevControlTarget = null;
                              this.controls.enabled = true;
                              if(completeCallback){
                                    completeCallback.apply( null, []);
                              }
                        }
                  });
            } else {

                  this.controls.enabled = true;
                  if(completeCallback){
                        completeCallback.apply( null, []);
                  }

            }


      }


      destroy() {
            this.resetTransitionTween();
            this.enabled = false;
            if(this.icon){
                  if(this.icon.geometry){this.icon.geometry.dispose();}
                  if(this.icon.material){this.icon.material.dispose();}
                  this.person.remove(this._icon);
            }
      }


      toggleEventListener(){
            if(this.enabled){
                  document.addEventListener('mousedown', this.onMouseDownHandler, false);
                  document.addEventListener("keydown", this.onKeyDownHandler, false);
                  document.addEventListener("keyup", this.onKeyUpHandler, false);
            } else {
                  document.removeEventListener('mousedown', this.onMouseDownHandler, false);
                  document.removeEventListener("keydown", this.onKeyDownHandler, false);
                  document.removeEventListener("keyup", this.onKeyUpHandler, false);
            }
      }


      registDocumentMouseEvent(){
            document.addEventListener('mousemove', this.onMouseMoveHandler, false);
            document.addEventListener("mouseup", this.onMouseUpHandler, false);
      }

      removeDocumentMouseEvent(){
            document.removeEventListener('mousemove', this.onMouseMoveHandler, false);
            document.removeEventListener("mouseup", this.onMouseUpHandler, false);
      }


      set rotateX( value:number ) {
            this._rotation.x = value;
            this._icon.rotation.x = this._rotation.x;
      }

      get rotateX():number{
            return this._rotation.x;
      }


      set rotateY( value:number ){
            this._rotation.y = value;
            this.person.rotation.y = this._rotation.y;
      }

      get rotateY():number{
            return this._rotation.y;
      }

      set rotation( value:THREE.Vector3 ){
            this.rotateX = value.x || this._rotation.x;
            this.rotateY = value.y || this._rotation.y;
      }

      get rotation():THREE.Vector3{
            return this._rotation;
      }

      set position( value:THREE.Vector3 ){
            this.person.position.set(value.x, value.y, value.z);
      }

      get position():THREE.Vector3 {
            return this.person.position;
      }

      set enabled( bValue:boolean ){
            if(this.enabled != bValue ){
                  this._enabled = bValue;
                  this.toggleEventListener();
            }
      }

      get enabled():boolean {
            return this._enabled;
      }


      onMouseDown( event:MouseEvent ) {
            if (this.enabled === false) return;

            this.startPos = {x:event.pageX, y:event.pageY};
            this.startYaw = this.rotation.y;
            this.startPitch = this.rotation.x;
            this.registDocumentMouseEvent();
      }

      onMouseUp( event:MouseEvent ){
            this.removeDocumentMouseEvent();
      }

      onMouseMove( event:MouseEvent ){
            if (this.enabled === false) return;
            let movementX:number = (event.pageX - this.startPos.x);
            let movementY:number = (event.pageY - this.startPos.y);
            this._rotation.y   = this.startYaw + movementX * 0.002;
            this._rotation.x   = this.startPitch  + movementY * 0.002;
            this._rotation.x   = Math.max(-Math.PI/2, Math.min(Math.PI/2, this._rotation.x));

            this.person.rotation.y  = this._rotation.y;
            this._icon.rotation.x   = this._rotation.x;

      }


      onKeyDown( event:KeyboardEvent ){
            if(this.enabled == false ) return;
            switch (event.keyCode) {
                  case 49: // yaw y up
                        this.moveUp = true;
                        break;
                  case 50: // yaw y down
                        this.moveDown = true;
                        break;

                  case 38: // up
                  case 87: // w
                        this.moveForward = true;
                        break;

                  case 37: // left
                  case 65: // a
                        this.moveLeft = true;
                        break;

                  case 40: // down
                  case 83: // s
                        this.moveBackward = true;
                        break;

                  case 39: // right
                  case 68: // d
                        this.moveRight = true;
                        break;

                  case 32: // space
                        break;

            }
      }

      onKeyUp( event:KeyboardEvent ) {
            if(this.enabled == false ) return;
            switch (event.keyCode) {
                  case 49: // yaw y up
                        this.moveUp = false;
                        break;
                  case 50: // yaw y down
                        this.moveDown = false;
                        break;
                  case 38: // up
                  case 87: // w
                        this.moveForward = false;
                        break;

                  case 37: // left
                  case 65: // a
                        this.moveLeft = false;
                        break;

                  case 40: // down
                  case 83: // s
                        this.moveBackward = false;
                        break;

                  case 39: // right
                  case 68: // d
                        this.moveRight = false;
                        break;
            }
      };

      update() {

            var time = performance.now();
            var delta = (time - this.prevTime) / 1000;
            this.velocity.x -= this.velocity.x * 10.0 * delta;
            this.velocity.z -= this.velocity.z * 10.0 * delta;
            this.velocity.y -= this.velocity.y * 10.0 * delta;

            this.direction.z = Number(this.moveForward) - Number(this.moveBackward);
            this.direction.x = Number(this.moveLeft) - Number(this.moveRight);
            this.direction.y = Number(this.moveUp) - Number(this.moveDown);
            this.direction.normalize(); // this ensures consistent movements in all directions
            if (this.moveForward || this.moveBackward) this.velocity.z -= this.direction.z * 1000.0 * delta;
            if (this.moveLeft || this.moveRight) this.velocity.x -= this.direction.x * 1000.0 * delta;
            if (this.moveUp || this.moveDown )  this.velocity.y -= this.direction.y * 1000.0 * delta;
            this.person.translateX(this.velocity.x * delta);
            this.person.translateY(this.velocity.y * delta);
            this.person.translateZ(this.velocity.z * delta);
            if (this.person.position.y < this.posy) {
                  this.velocity.y = 0;
                  this.person.position.y = this.posy;
            }
            this.prevTime = time;

      }

}
