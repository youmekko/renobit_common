import WVComponent, {GROUP_NAME_LIST} from "../WVComponent";
import MeshManager from "./manager/MeshManager";
import {
      EventVO,
      IExtensionElements,
      IStyleManager,
      IThreeExtensionElements,
      IWV3DComponent,
      IWVVector,
      WV3D_PROPERTIES, WVCOMPONENT_EDITOR_POPERTY_NAMES
} from "../interfaces/ComponentInterfaces";

import WVDOMComponentStyleManager from "../2D/manager/WVDOMComponentStyleManager";
import {THREE} from "../../utils/reference";
import WVComponentEvent from "../../events/WVComponentEvent";
import {ArrayUtil} from "../../../utils/Util";


export default abstract class WV3DComponent extends WVComponent implements IWV3DComponent{

      protected _container:any = null;
      protected _elementSize:WVVector = new WVVector();
      protected invalidateRotation:boolean = false;
      protected _domEvents:any = null;

      protected _threeElements:IThreeExtensionElements;


      private _func_call_click_handler:Function= null;
      private _func_call_dblclick_handler:Function = null;

      protected constructor() {
		super();

		this._func_call_click_handler = this._onClickHandler.bind(this);
            this._func_call_dblclick_handler = this._onDblClickHandler.bind(this);


      }



      protected _createElement(template) {

            //this._container = new THREE.Object3D();
            this._container = new THREE.Mesh(MeshManager.getGeometry('BoxGeometry').clone(), MeshManager.getMaterial('MeshBasicMaterial').clone());
            this._container.material.visible = false;

            this._element = new THREE.Mesh(MeshManager.getGeometry('BoxGeometry').clone(), MeshManager.getMaterial('MeshBasicMaterial').clone());
            this._container.add(this._element);
            //let box = new THREE.Box3().setFromObject(this._element);
            //let offset = box.getCenter();
            //this._container.position.set(offset.x, offset.y, offset.z);
            this._onCreateElement();

      }

      protected toggleDomEvents( bValue:boolean) {
            if(bValue){
                  this._domEvents.addEventListener(this.appendElement, "click", this._func_call_click_handler, false);
                  this._domEvents.addEventListener(this.appendElement, "dblclick", this._func_call_dblclick_handler, false);
            } else {
                  this._domEvents.removeEventListener(this.appendElement, "click", this._func_call_click_handler, false);
                  this._domEvents.removeEventListener(this.appendElement, "dblclick", this._func_call_dblclick_handler, false);
            }

      }


      protected _onDestroy(){

            this._threeElements = null;
            if(this._domEvents) {
                  this._domEvents.removeEventListener(this.appendElement, "click", this._func_call_click_handler, false);
                  this._domEvents.removeEventListener(this.appendElement, "dblclick", this._func_call_dblclick_handler, false);

                  this._eventListenerList.forEach((obj)=>{
                        this._domEvents.removeEventListener(this.appendElement, obj.eventName, obj.listener, false);
                  });

                  this._eventListenerList = null;

                  this._domEvents = null;

                  /*
                  2018.05.06
                  domEvents는 공유하기 때문에 주의 아래 내용을 실행하면 안됨.
                  this._domEvents.destroy();
                   */


            }
            MeshManager.disposeMesh(this.appendElement);
            this._func_call_click_handler = null;
            this._func_call_dblclick_handler = null;
            this._container = null;
            //this._element = null;
            super._onDestroy();
      }


      private _eventListenerList:Array<EventVO> = [];
      public $on(eventName:string, listener:Function) {
            if(this.appendElement && this._domEvents){
                  this._domEvents.addEventListener(this.appendElement, eventName, listener,false);

                  this._eventListenerList.push({
                        eventName,
                        listener
                  })
            }
      }

      public $off(eventName:string, listener:Function) {
            if(this.appendElement){
                  this._domEvents.removeEventListener(this.appendElement, eventName, listener, false);
                  ArrayUtil.delete(this._eventListenerList, "listener", listener);
            }
      }


      public setExtensionElements(extension:IExtensionElements){
            super.setExtensionElements(extension);
            this._threeElements = extension as IThreeExtensionElements;
            this._domEvents = this._threeElements.domEvents;

      }


	public createStyleManager():IStyleManager{

	      // 3D 스타일관리자로 변경해야함.
            this._styleManager = new WVDOMComponentStyleManager(this);
            return this._styleManager;
      }

      get appendElement():any {
            return this._container;
      }

	set position(value:WVVector){
		if(this._checkUpdateGroupVectorPropertyValue(GROUP_NAME_LIST.SETTER, WV3D_PROPERTIES.POSITION, value)){
                  this.invalidatePosition=true;
            }
	}
	get position():WVVector{
		return this.setter.position;
	}

	set size(value:WVVector){
            if(this._checkUpdateGroupVectorPropertyValue(GROUP_NAME_LIST.SETTER, WV3D_PROPERTIES.SIZE, value)){
                  this.invalidateSize=true;
            }
      }


	get size():WVVector{
            return this.setter.size;
	}


	set rotation(value:WVVector){
            if(this._checkUpdateGroupVectorPropertyValue(GROUP_NAME_LIST.SETTER, WV3D_PROPERTIES.ROTATION, value)){
                  this.invalidateRotation = true;
            }
	}

	get rotation():WVVector{
            return this.setter.rotation;
	}
      /////////////



      /////////////
      /* 커스텀 렌더링 처리

      컴포넌트 자체에서 렌더링을 사용하는 경우
      1. usingRender()를 true 값을 리턴한다.
      2. renderer()를 구현한다.
      */


      // 렌더러를 사용하는 컴포넌트 인경우 true를 리턴후 render() 메서드를 재정의 하세요.
      get usingRenderer() {
            return false;
      }
      public render(){

      }
      /////////////



      public immediateUpdateDisplay(event:boolean=false):void{
		super.immediateUpdateDisplay(event);
            this._validateRotation();
            this._validateLabel();
            this._validateLock();

	}


      protected _commitProperties():void {
	      super._commitProperties();

	      if(this.invalidateRotation){
                  this.validateCallLater(this._validateRotation);
            }

            if(this._updatePropertiesMap.has("label")){
                  this.validateCallLater(this._validateLabel);
            }

      }

      protected _validateAttribute(): void {

      }


      protected _validateLock(): void {
            /*
            lock처리는 editor 모드에서만 실행되어야 함.
             */
            if(this.isEditorMode) {
                  if (this.lock) {
                        this.dispatchEvent(new WVComponentEvent(WVComponentEvent.LOCKED, this, {}));
                  } else {
                        //$(this.element).removeClass("wv-component-lock");
                        this.dispatchEvent(new WVComponentEvent(WVComponentEvent.UN_LOCKED, this, {}));
                  }
            }
      }


      protected _validatePosition(): void {
            this.appendElement.position.set(this.position.x,this.position.y,this.position.z);

      }

      protected _validateSize(): void {

            let value = this.size;
            this.element.scale.set(value.x, value.y, value.z);
            this.element.updateMatrixWorld();
            var label = this.getGroupProperties("label");
            this.element.geometry.computeBoundingBox();
            this.element.geometry.computeBoundingSphere();

            if(label.label_using == "Y" && this._threeElements.labelList.get(this.appendElement.uuid) != undefined){

                  this._disposeLabelLine();
                  this._createLabelLine();
                  this._updateLabel();

            }

      }

      protected _validateRotation(): void {
            let value = this.rotation;
            //degree => radian으로 변경
            this.element.rotation.set(((value.x*3.1415926535)/180),((value.y*3.1415926535)/180),((value.z*3.1415926535)/180));
      }

      protected _validateVisible(): void {
            if(this.isViewerMode) {
                  this.element.visible = this._properties.setter.visible;
            }
      }


      /*
      editor 모드에서만 실행
       */
      protected _validateEditorModeVisible():void {

            if(this.isEditorMode){

                  let visible:boolean = this.getGroupPropertyValue(GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_EDITOR_POPERTY_NAMES.VISIBLE);
                  this.element.visible=visible;

            }
      }


      protected _validateLabel() : void {

            var value = this.getGroupProperties("label");

            var oContainer = $(".label-container").get(0);

            if(value.label_using == "Y"){

                  if(this._threeElements.labelList.get(this.appendElement.uuid) == undefined){
                        this._createLabelLine();
                  }

                  this._updateLabel();

            }else if(value.label_using == "N"){

                  if (this._threeElements.labelList.get(this.appendElement.uuid) != undefined) {
                        this._disposeLabelLine();
                  }
            }
      }

      private _createLabelLine() : void{

            var value = this.getGroupProperties("label");
            var oContainer = $(".label-container").get(0);
            var text = this._createTextLabel(value);
            oContainer.appendChild(text.elementLabel);

            /*[ 라벨 리스트 풀에 넣기 ]*/
            this._threeElements.labelList.set(this.appendElement.uuid, text);

            var geometry = MeshManager.getGeometry('Geometry').clone(); // pool에서 가져오기
            geometry.vertices.push(new THREE.Vector3(0, 0, 0));
            geometry.vertices.push(new THREE.Vector3(0, 1, 0));

            var material = new THREE.MeshPhongMaterial({ //shininess: 40,
                  specular: 0x00e0fb,
                  emissive: 0x00e0fb,
                  color: 0x00e0fb,
            });

            var line = new THREE.Line(geometry, material);

            line.name = 'labelLine';

            this.appendElement.add(line);

            text.setParent(line);

      }

      private _disposeLabelLine() : void{

            var oLabelLine = this.appendElement.getObjectByName("labelLine");

            MeshManager.disposeMesh(oLabelLine);

            this.appendElement.remove(oLabelLine);

            $(this._threeElements.labelList.get(this.appendElement.uuid).elementLabel).remove();

            this._threeElements.labelList.delete(this.appendElement.uuid);

      };

      private _updateLineSize() : void{

            var value = this.getGroupProperties("label");

            var size = value.label_line_size;

            var oLabelLine = this.appendElement.getObjectByName("labelLine");

            oLabelLine.scale.set(1, size, 1);

      }

      private _updateLabel() : void {

            let oLabel = this._threeElements.labelList.get(this.appendElement.uuid);

            let oProp = this.getGroupProperties("label");

            oLabel.setText(this.getTranslateText(oProp.label_text));

            oLabel.setStyle('opacity', oProp.label_opacity);

            oLabel.setStyle('color', oProp.label_color);

            oLabel.setStyle('background-color', oProp.label_background_color);

            oLabel.setStyle('font-size', oProp.label_font_size + "px");

            oLabel.setStyle('font-family', oProp.label_font_type);

            oLabel.setStyle('font-weight', oProp.font_weight);

            oLabel.setStyle('border', oProp.border);

            this._updateLineSize();

      }

      private getTranslateText(str){
            return wemb.localeManager.translatePrefixStr(str);
      }


      private _createTextLabel(value) {
            var oContainer = document.createElement('div');
            oContainer.className = "label";
            oContainer.style.cssText = "position : absolute; width: 100; height: 100; top:-1000; left:-1000;";

            var that = this;

            return {
                elementLabel: oContainer,
                parent: null,
                temp: null,
                target: null,
                position: new THREE.Vector3(0, 0, 0),
                setText: function(text) {
                    this.elementLabel.textContent = text;
                },
                setParent: function(parent) {
                    this.parent = parent;
                },
                setStyle: function(type, value) {
                    this.elementLabel.style[type] = value;
                },
                updatePosition: function() {

                  if (!this.parent || !this.parent.parent) return false;

                  this.position.copy(this.parent.position.clone().applyMatrix4(this.parent.matrixWorld));
                  this.position.y = this.position.y + this.parent.scale.y;

                  let camera = that._threeElements.camera;

                  let coords2d = this.get2DCoords(this.position, camera);

                  var labelWidth:any = this.elementLabel.clientWidth/2;
                  this.elementLabel.style.left = parseInt(coords2d.x) - parseInt(labelWidth) + 'px';
                  this.elementLabel.style.top = parseInt(coords2d.y) + 'px';
                },
                get2DCoords: function(position, camera) {

                  let oContainer = that._threeElements.renderer.domElement;

                  var vector = position.project(camera);
                  vector.x = (vector.x + 1) / 2 * oContainer.clientWidth;
                  vector.y = -(vector.y - 1) / 2 * oContainer.clientHeight;

                  return vector;
                }
            };
        }

      protected _clearInvalidate(){
	      super._clearInvalidate();
	      this.invalidateRotation=false;
      }

      /*
      일반적으로 controller에 의해서 움직인 값을 setter에 적용할 때 사용.
       */
	syncLocationSizeElementPropsComponentProps(){
            this._properties.setter.position.x =  parseInt(this.appendElement.position.x);
            this._properties.setter.position.y =  parseInt(this.appendElement.position.y);
            this._properties.setter.position.z =  parseInt(this.appendElement.position.z);

            this._properties.setter.rotation.x =  (this.element.rotation.x*180 / 3.1415926535).toFixed(0);
            this._properties.setter.rotation.y =  (this.element.rotation.y*180 / 3.1415926535).toFixed(0);
            this._properties.setter.rotation.z =  (this.element.rotation.z*180 / 3.1415926535).toFixed(0);

            this._properties.setter.size.x =  parseInt(this.element.scale.x);
            this._properties.setter.size.y =  parseInt(this.element.scale.y);
            this._properties.setter.size.z =  parseInt(this.element.scale.z);

	}


	/*
	vector 요소 변경 유무
	 */
      protected _checkUpdateGroupVectorPropertyValue(groupName:string, propertyName:string, value:IWVVector){
            let _oldValue = this.getGroupPropertyValue(groupName, propertyName);
            _oldValue = new WVVector(_oldValue);
            console.log("_oldValue.equal(value) ", groupName, propertyName, _oldValue.equal(value))
            if(_oldValue.equal(value)==false){
                 let groupOwner = this._call_getGroup(groupName);
                 if(groupOwner==null)
                       return false;

                  let targetVector =groupOwner[propertyName];
                  if(targetVector==null){
                        return false;
                  }

                  targetVector.x = value.x;
                  targetVector.y = value.y;
                  targetVector.z = value.z;

                  this._call_setInvalidateGroupPropertyValue(groupName, propertyName, value);

                  return true;
            }


            return false;
      }
      ///////////////////////////////////////////////////////










      ///////////////////////////////////////////////////////
      // 이벤트 처리
      private _onClickHandler(event){
            event.stopPropagation();
            this.dispatchWScriptEvent("click");
      }

      private _onDblClickHandler(event){
            event.stopPropagation();
            this.dispatchWScriptEvent("dblclick");
      }

      protected _onInitWScriptEvent():void{
            if(this.events){
                  let eventInfos = this.getEventDescription();
                  if(eventInfos) {
                        eventInfos.forEach((eventInfo) => {
                              let eventName = eventInfo.name;
                              if (eventName == "click") {
                                    this._domEvents.addEventListener(this.appendElement, "click", this._func_call_click_handler, false);
                              }
                              if (eventName == "dblclick") {
                                    this._domEvents.addEventListener(this.appendElement, "dblclick", this._func_call_dblclick_handler, false);
                              }
                        });
                  }

            }
      }


}





export class WVVector implements  IWVVector{

	x:number=0;
	y:number=0;
	z:number=0;
	constructor(x=0,z=0,y=0){
	      if(arguments.length==1 && typeof arguments[0] =="object"){
	            let tempObject:any = x;
                  this.x=tempObject.x;
                  this.y=tempObject.y;
                  this.z=tempObject.z;
            }else {
                  this.x = x;
                  this.y = y;
                  this.z = z;
            }

	}
	equal(value:WVVector):boolean{
		return value.x==this.x && value.y==this.y && value.z==this.z;
	}

	copy(value:WVVector):void {
		this.x = value.x;
		this.y = value.y;
		this.z = value.z;
	}

	sub(value:WVVector):void {
		this.x = this.x-value.x;
		this.y = this.y-value.y;
		this.z = this.z-value.z;
	}
	add(value:WVVector):void {
		this.x = this.x+value.x;
		this.y = this.y+value.y;
		this.z = this.z+value.z;
	}

}


