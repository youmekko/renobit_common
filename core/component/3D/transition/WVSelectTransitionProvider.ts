import ISelectTransitionProvider = WV.Transition.ISelectTransitionProvider;
import ISelectTransitionProviderClient = WV.Transition.ISelectTransitionProviderClient;
import {CPLogger} from "../../../utils/CPLogger";
export default class WVSelectTransitionProvider implements ISelectTransitionProvider {

      protected _instances: Map<any, any>;

      constructor() {
            this._instances = new Map();
      }

      checkImplements( instance:any, methodName:string ):boolean {
            return (methodName in instance);
      }

      addItem(client: ISelectTransitionProviderClient): boolean {
            if (!this.hasClient(client)) {
                  if (!this.checkImplements(client, "transitionIn")) {
                        CPLogger.log(CPLogger.getCallerInfo(this, "addItem"), [client.constructor.name + "에서 transitionIn를 구현하지 않았습니다."]);
                        return false;
                  }
                  if (!this.checkImplements(client, "transitionOut")) {
                        CPLogger.log(CPLogger.getCallerInfo(this, "addItem"), [client.constructor.name + "에서 transitionOut를 구현하지 않았습니다."]);
                        return false;
                  }
                  this._instances.set(client.transitionTarget, client);
                  return true;
            }
            return false;
      }

      removeItem(client: ISelectTransitionProviderClient): boolean {
            if (this.hasClient(client)) {
                  this._instances.delete(client.transitionTarget);
                  return true;
            }
            return false;
      }

      hasClient(client: ISelectTransitionProviderClient): boolean {
            return this._instances.has(client.transitionTarget);
      }


      transitionInFromInstance(instance: ISelectTransitionProviderClient): void {
            this._instances.forEach((client: ISelectTransitionProviderClient, key: any) => {
                  if (client.selected) {
                        if (client != instance) {
                              client.selected = false;
                              client.transitionOut();
                        }
                  }
            });
            instance.transitionIn();
      }

      transitionOut(): void {
            this._instances.forEach((client: ISelectTransitionProviderClient, key: any) => {
                  if(client.selected) {
                        client.selected = false;
                        client.transitionOut();
                  }
            });
      }


      // 초기화 처리
      protected init(): void {
            this.registEventListener();
      }


      destroy(): void {
            // 삭제 처리
            this.removeEventListener();
            this._instances.clear();
            this._instances = null;
      }

      registEventListener(): void{}
      removeEventListener(): void{}

}

