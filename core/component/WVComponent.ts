import WVDisplayObject from "../display/WVDisplayObject";
import {
      IComponentMetaProperties,
      IExtensionElements,
      IStyleManager,
      IWVComponent,
      IWVComponentProperties,
      RESOURCE_STATE,
      WVCOMPONENT_EDITOR_POPERTY_NAMES,
      WVCOMPONENT_PROPERTY_NAMES,
      WVDISPLAY_PROPERTIES
} from "./interfaces/ComponentInterfaces";
import WVComponentEvent from "../events/WVComponentEvent";
import {IMetaProperties, IProperties} from "../display/interfaces/DisplayInterfaces";
import {IWVEvent} from "../events/interfaces/EventInterfaces";
import {EXE_MODE} from "../../wv/data/Data";
import WVComponentEventDispatcher, {WVComponentScriptEvent} from "../events/WVComponentEventDispatcher";
import {IEditorPageObject, IViewerPageObject} from "../../wv/managers/InstanceManagerOnPageViewer";
import Vue from "vue";
import CommonPropertyManager from "./property/CommonPropertyManager";


export default abstract class WVComponent extends WVDisplayObject implements IWVComponent{

      /*
      확장 요소
      3D인 경우 scene, render, 등의 정보가 넘어오게 됨.
       */
      private _extensionElements:IExtensionElements = null;
      public get extensionElements(){
            return this._extensionElements;
      }

      public setExtensionElements(extensionElements:IExtensionElements){
            this._extensionElements = extensionElements;
      }

      private _exeMode:string=(EXE_MODE.EDITOR as string);
      public get exeMode(){
            return this._exeMode;
      }

      public set exeMode(value) {
            this._exeMode = value;
      }


      public get isEditorMode(){
            return this._exeMode == EXE_MODE.EDITOR;
      }

      public get isViewerMode(){
            return this._exeMode == EXE_MODE.VIEWER;
      }



      // viwer 모드일때만 사용.
      private _$WScriptEventBus = null;




      // viwer 모드일때만 사용.
      protected _componentEventDispatcher:WVComponentEventDispatcher = null;
      public get componentEventDispatcher() {
            return this._componentEventDispatcher;
      }

      // viwer 모드일때만 사용.
      private _page:IViewerPageObject|IEditorPageObject;
      public set page(_page:IViewerPageObject|IEditorPageObject){
            this._page = _page;
      }

      public get page():IViewerPageObject|IEditorPageObject{
            return this._page;
      }



      /*
      페이지에 의해서 로드 되었는지 유무 확인
       */
      public get isCompleted(){
            return this.page.isLoaded;
      }


      // 화면에 붙일 최상위 엘리먼트 반환
      get appendElement():any { return null }

      get primary(){
            return this.getGroupProperties(GROUP_NAME_LIST.PRIMARY);
      }
      get setter() {
            return this.getGroupProperties(GROUP_NAME_LIST.SETTER);
      }

      get info(){
            return this.getGroupProperties(GROUP_NAME_LIST.INFO);
      }

      get events(){
            return this.getGroupProperties(GROUP_NAME_LIST.EVENTS);
      }
      //////////////////////////////////////////////////////////////




      //////////////////////////////////////////////////////////////
      get id(){
            return this.primary.id;
      }

      set id(value){

      }

      get name(){
            return this.primary.name;
      }

      set name(value){

      }


      get layerName():string {
            return this.primary.layerName;
      }

      set layerName(value:string){
            this.primary.layerName = value;
      }



      get componentName() {
            return this.info.componentName;
      }
      get category(): any {
            return this.info.category;
      }



      get lock():boolean{
            return this.getGroupPropertyValue(GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_PROPERTY_NAMES.LOCK );
      }
      set lock(value:boolean){
            this._checkUpdateGroupPropertyValue( GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_PROPERTY_NAMES.LOCK, value);
      }

      get depth():number {
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.DEPTH );
      }
      set depth(value:number){
            this._checkUpdateGroupPropertyValue( GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.DEPTH, value);
      }


      protected  _selected:boolean=false;
      set selected(value:boolean){
            if(value!=this._selected) {
                  this._selected = value;
                  if (this._selected)
                        this.notifyComponentEvent(new WVComponentEvent(WVComponentEvent.SELECTED, this, {}));
                  else
                        this.notifyComponentEvent(new WVComponentEvent(WVComponentEvent.UN_SELECTED, this, {}));
            }
      }

      get selected(){
            return this._selected;
      }


      get visible(){
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.VISIBLE );
      }

      set visible(value:boolean){
            if(this._checkUpdateGroupPropertyValue( GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.VISIBLE, value)){
                  this.invalidateVisible=true;
            }
      }




      /*
      editorMode에서는 visible대신 이 속성을 사용해 visible처리함.
       */
      get editorModeVisible(){
            return this.getGroupPropertyValue(GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_EDITOR_POPERTY_NAMES.VISIBLE);
      }
      /*
		set editorVisible(){
			this.getGroupPropertyValue(GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_EDITOR_POPERTY_NAMES.VISIBLE);
		}
	*/




      /*
      이벤트 등록하기
      // DOM,SVG(2D)는 jquery사용
      // 3D는 다르게 사용해야함.
       */
      public $on(eventName:string, func:Function) {
      }

      public $off(eventName:string, func:Function) {

      }

      protected constructor() {
            super();
      }

      /*
      뷰어 모드로 실행하기
      call : CompoenntInstanceFactory,
       */
      public executeViewerMode(){
            if(this._element) {
                  console.warn("컴포넌트 생성 전에 호출해야합니다.");
            }
            this._exeMode = EXE_MODE.VIEWER;
            this._$WScriptEventBus = new Vue();
      }




      // 기본 컴포넌트 프로퍼티 설정
      ///////////////////////////////////////////
      create(initProperties: IMetaProperties, template:string=null):boolean {
            if(super.create(initProperties, template)){
                  this.createStyleManager();
                  this.initCustomProperty();

                  return true;
            }

            return false;
      }

      private initCustomProperty() {
            let commonProperty = CommonPropertyManager.getInstance().getDefaultProperty(this);
            let propertyPanelInfo = this.constructor[COMPONENT_DESCRIPTION_NAME.PROPERTY_PANEL_INFO];

            commonProperty.forEach(item => {
                  if (!propertyPanelInfo.includes(item)) {
                        propertyPanelInfo.push(item);
                  }
            });
      }

      /*
      create내부에서 호출 됨. _initWScriptEvent
       */
      protected  _initWScriptEvent(){

            if(this._exeMode==(EXE_MODE.VIEWER as string)){
                  if(this._componentEventDispatcher==null)
                        this._componentEventDispatcher =  new WVComponentEventDispatcher(this);

                  this._onInitWScriptEvent();
            }
      }

      /*
           WScriptEvent 추가 기능
           // WVDOMComponent에서는 이 내용을 override해서 이벤트 처리를 자동으로 함.
       */
      protected _onInitWScriptEvent(){
            //CPNotification.getInstance().notifyMessage("WVComponent._onInitWScriptEvent() 컴포넌트 내부에 추가되는 요소는 여기를 오버라이드해서 사용해 주세요22 ");

      }

      public dispatchWScriptEvent(eventName, data=null) {
            if(this.componentEventDispatcher && this.isViewerMode ) {
                  this.componentEventDispatcher.dispatchEvent(eventName, data);
            }
      }










      protected _createProperties(metaProperties:IMetaProperties):any {
            try {
                  // component meta정보를 컴포넌트 프로퍼티 정보로 변경
                  let initProperties: IWVComponentProperties= this._convertMetaPropertiesToComponentProperties(metaProperties as IComponentMetaProperties);

                  // 기본 값 생성하기
                  let newProperties: IWVComponentProperties = <IWVComponentProperties>$.extend(true, {}, this.getDefaultProperties());

                  // 기본 값 + 입력 값 조합하기
                  $.extend(true, newProperties, initProperties);

                  return newProperties;
            } catch(error){
                  console.log("경고 컴포넌트 프로퍼티 스타일이 맞지 않습니다.", error);
                  return null;
            }
      }


      /*
      컴포넌트 메타 정보를 => 일반 컴포넌트 정보로 변경.
       */
      protected _convertMetaPropertiesToComponentProperties(properties:IComponentMetaProperties):IWVComponentProperties{
            let comProperties:IWVComponentProperties = {
                  "primary" :{
                        "id":properties.id,
                        "name":properties.name,
                        "type":properties.type,
                        "master":properties.master,
                        "updateDt": properties.update_dt,
                        "lastUser": properties.last_user,
                        "layerName":properties.layerName,

                  },
                  "info":{
                        "componentName":properties.componentName,
                        "version":properties.version,
                        "category":properties.category
                  }
            }
            if(properties.hasOwnProperty("props")){
                  $.extend(true, comProperties, properties.props);
            }

            return comProperties;
      }




      /*
      페이지 저장 전 호출 되는 정보
      이전 페이지 정보와 호완성을 위해
      laeryName을 layer
       */
      public serializeMetaProperties():IComponentMetaProperties{

            let properties:any = {
                  props:{}
            };
            $.extend(true, properties.props, this._properties);



            /*
            primary 정보를 properties 로 만들기
             */
            properties.id = this._properties.primary.id;
            properties.name = this._properties.primary.name;
            properties.type = this._properties.primary.type;
            properties.master = this._properties.primary.master;
            properties.updateDt = this._properties.primary.updateDt;
            properties.lastUser = this._properties.primary.lastUser;
            properties.layerName = this._properties.primary.layerName;
            properties.assetId = this.assetId || "";
            delete properties.props.primary;



            /*
            info 정보를 properties로 만들기
             */
            properties.componentName = this._properties.info.componentName;
            properties.version = this._properties.info.version;
            properties.category = this._properties.info.category;
            delete properties.props.info;

            // delete properties.props.resource;

            return properties;
      }




















      ///////////////////////////////////////////


      /*
    자바스크립트에서 문자열로 이벤트를 발생시켜야 하는 경우가 있음.
    이때 추가해야함.
     */
      public dispatchComponentEvent(eventName:string, data:any=null){
            let event:IWVEvent = new WVComponentEvent(eventName, this, data);
            this._dispatcher.dispatchEvent(event);
      }






      ///////////////////////////////////////////////////////////
      /*
      유효 프로퍼티 체크
      컴포넌트에 반드시 있어야 하는
      category, laeryName 추가

      만약 없는 경우 element 생성 부터 element 에 프로퍼티 설정이 실행되지 않음.

      call :
            WVDisplayCobject.create()에서 호출
       */
      protected  _validateInitProperties(initProperties:IProperties):boolean{
            let properties:IWVComponentProperties = initProperties as IWVComponentProperties;

            if (properties.hasOwnProperty(GROUP_NAME_LIST.PRIMARY) == false) {
                  return false;
            }

            if (properties.primary.hasOwnProperty("id") == false) {
                  return false;
            }

            if (properties.primary.hasOwnProperty("name") == false) {
                  return false;
            }

            if (properties.primary.hasOwnProperty("layerName") == false) {
                  return false;
            }




            if (properties.hasOwnProperty(GROUP_NAME_LIST.SETTER) == false) {
                  return false;
            }

            if (properties.hasOwnProperty(GROUP_NAME_LIST.INFO) == false) {
                  return false;
            }

            return true;
      }











      /*
	기존 프로퍼티 값과 신규 프로퍼티 값 비교해서 업데이트 유무 확인하기
	call : 주로 setter에서 실행
	 */
      protected _checkUpdateGroupPropertyValue(groupName:string, propertyName:string, value:any){
            return this._call_setGroupPropertyValue(groupName, propertyName, value);
      }

      public  setGroupProperties(groupName, properties):boolean{
            if(groupName==GROUP_NAME_LIST.STYLE){
                  return this._styleManager.setProperties(properties, false);
            }

            if(groupName==GROUP_NAME_LIST.SETTER){
                  return this._call_setSetterProperties(properties);
            }

            return super.setGroupProperties(groupName, properties);
      }

      public setGroupPropertyValue(groupName, propertyName, value):boolean{
            if(groupName==GROUP_NAME_LIST.STYLE){
                  return this._styleManager.setProperty(propertyName, value);
            }

            if(groupName==GROUP_NAME_LIST.SETTER){
                  return this._call_setSetterProperty(propertyName, value);
            }

            return super.setGroupPropertyValue(groupName, propertyName, value);
      }



      /*
	여러 setter 프로퍼티 설정
	 */
      private  _call_setSetterProperties(properties:any){
            let isChanged:boolean =false;
            let propertyNames: Array<any> = Object.keys(properties);
            propertyNames.forEach((propertyName:string)=>{
                  isChanged = this._call_setSetterProperty(propertyName, properties[propertyName]);
            });

            return isChanged;
      }

      /*
	단일 setter 프로퍼티 설정
	 */
      private _call_setSetterProperty(propertyName:string, value:any):boolean {
            let groupOwner:any = this._call_getGroup(GROUP_NAME_LIST.SETTER);
            if(groupOwner!=null){

                  try {
                        // 프로퍼티 설정하기, 만약 setter가 없는 경우 동적으로 값이 추가됨.
                        let propertyValue = this[propertyName];

                        // 인스턴스의 메서드(함수)와 프로퍼티 명이 같은 경우는 무시.
                        if (typeof propertyValue == "function") {
                              return false;

                        }
                        if (value == this[propertyName]) {
                              return false;
                        }

                        this[propertyName] = value;
                        return true;
                  }catch(error){
                        // 프로퍼티에 셋터가 선언되어 있지만  etter로 메서드가 만들어져 있지 않은 경우.
                        return super.setGroupPropertyValue(GROUP_NAME_LIST.SETTER, propertyName, value);

                  }
                  /*

                  2018.05.25 삭제 예정
                  setter 사용 유무를 알아 낼 수 없기 때문에 try catch()로 변경


                  // 프로퍼티 설정하기, 만약 setter가 없는 경우 동적으로 값이 추가됨.
                  let propertyValue = this[propertyName];


                  console.log("this.hasOwnProperty(propertyName) ", Object.getOwnPropertyDescriptor(this, propertyName), propertyName);

                  // 프로퍼티에 셋터가 선언되어 있고 setter로 메서드가 만들어진 경우
                  if(propertyValue!=null) {
                        // 인스턴스의 메서드(함수)와 프로퍼티 명이 같은 경우는 무시.
                        if (typeof propertyValue == "function") {
                              return false;

                        }
                        if (value == this[propertyName]) {
                              return false;
                        }

                        this[propertyName] = value;
                        return true;
                  }else {
                        // 프로퍼티에 셋터가 선언되어 있지만  etter로 메서드가 만들어져 있지 않은 경우.
                        return super.setGroupPropertyValue(GROUP_NAME_LIST.SETTER, propertyName, value);
                  }
                  */


            }

            return false;
      }

      protected  _commitProperties():void{
            super._commitProperties();


            if (this._styleManager.invalidateProperty) {
                  this.validateCallLater(this._validateStyle);
            }


            if(this._updatePropertiesMap.has("editorMode.lock")==true){
                  this.validateCallLater(this._validateLock);
            }


            if(this._updatePropertiesMap.has("editorMode.visible")==true){
                  this.validateCallLater(this._validateEditorModeVisible);
            }




      }

      protected _validateStyle(): void {
            this._styleManager.validateProperty();
      }


      protected _validateLock():void{

      }



      protected _clearInvalidate(){
            super._clearInvalidate();

            this._styleManager.clearInvalidate();
      }





      public immediateUpdateDisplay(event:boolean=false):void{
            this._clearDeferred();
            this._validateAttribute();
            this._validatePosition();
            this._validateSize();
            this._styleManager.immediateProperties(this.properties.style);
            this._validateVisible();


            /*
        삭제 예정.
        super.immediateUpdateDisplay(dispatchEvent);
        this._styleManager.setProperties(this.properties.style);*/


            if(this.isViewerMode){
                  this._componentEventDispatcher.dispatchEvent(WVComponentScriptEvent.REGISTER);
            }

            if(this.isEditorMode){
                  this._validateEditorModeVisible();
            }

            this._onImmediateUpdateDisplay();

      }

      protected abstract _validateEditorModeVisible();


      protected _onDestroy(){
            if(this._exeMode==(EXE_MODE.VIEWER as string)){
                  this._componentEventDispatcher.dispatchEvent(WVComponentScriptEvent.DESTROY);

                  if(this._$WScriptEventBus)
                        this._$WScriptEventBus.$off();
            }



            // style정보와 eventBus 초기화
            this._$WScriptEventBus = null;
            this._extensionElements = null;
            this._componentEventDispatcher = null;
            this._styleManager.destroy();
            this._styleManager = null;
            this._page = null;
            super._onDestroy();
      }





      ///////////////////////////////////////////////////////
      //스타일 처리
      protected _styleManager:IStyleManager;
      public get styleManager():IStyleManager{
            return this._styleManager;
      }
      ///////////////////////////////////////////////////////







      //////////////////////////// ///////////////////////////
      // 에디터에서만 사용됨.
      // 주로 프로퍼티 패널 또는 WScript 패널에서 사용됨.
      // 컴포넌트 별로 override해야함.
      public getEditableProperties():any{
            return this._properties;
      }
      public getDefaultProperties():any {
            if (this.constructor.hasOwnProperty(COMPONENT_DESCRIPTION_NAME.DEFAULT_PROPERTIES)) {
                  return this.constructor[COMPONENT_DESCRIPTION_NAME.DEFAULT_PROPERTIES];
            }
            return null;
      }

      public getExtensionProperties():any{
            return null;
      }

      public getPropertyPanelDescription():Array<any>{
            if (this.constructor.hasOwnProperty(COMPONENT_DESCRIPTION_NAME.PROPERTY_PANEL_INFO)) {
                  return this.constructor[COMPONENT_DESCRIPTION_NAME.PROPERTY_PANEL_INFO];
            }
            return null;
      }

      /*
      컴포넌트 기본 PROPERTY_PANEL_INFO의 children 정보만을 빼냄.
      컴포넌트 마다 한번만 구함.
       */
      public getPropertyDescription():Array<any>{
            if (this.constructor.hasOwnProperty(COMPONENT_DESCRIPTION_NAME.PROPERTY_INFO)) {
                  return this.constructor[COMPONENT_DESCRIPTION_NAME.PROPERTY_INFO];
            }
            return null;
      }
      public getMethodDescription():Array<any>{
            if (this.constructor.hasOwnProperty(COMPONENT_DESCRIPTION_NAME.METHOD_INFO)) {
                  return this.constructor[COMPONENT_DESCRIPTION_NAME.METHOD_INFO];
            }
            return null;
      }

      public getEventDescription():Array<any>{
            if (this.constructor.hasOwnProperty(COMPONENT_DESCRIPTION_NAME.EVENT_INFO)) {
                  return this.constructor[COMPONENT_DESCRIPTION_NAME.EVENT_INFO];
            }
            return null;
      }





      public abstract  createStyleManager():IStyleManager;

      ////////////////////////////////////////////////////////////////////////////////////
      // 추상 메서드
      abstract syncLocationSizeElementPropsComponentProps();








      ////////////////////////////////////////////////////////////////////////////////////
      // 리소스 관련 기능

      protected  _isResourceComponent:boolean=false;

      public get isResourceComponent():boolean{
            return this._isResourceComponent;
      }

      private _$resourceBus:Vue;
      public get resourceBus():Vue {
            return this._$resourceBus;
      }

      private _resourceState:RESOURCE_STATE = RESOURCE_STATE.READY;
      public get resourceState(){
            return this._resourceState;
      }

      public setResourceBus($eventBus){
            this._$resourceBus = $eventBus;
      }

      public startLoadResource(){}
      public stopLoadResource(){}


      /*
      2018.09.12
            - WScript 에디터 이벤트 처리용.
       */
      public onWScriptEvent(eventName:string, func:Function){

            if(this._$WScriptEventBus) {
                  this._$WScriptEventBus.$on(eventName, func);
            }
      }

      public offWScriptEvent(eventName:string, func:Function=null){
            if(this._$WScriptEventBus==null)
                  return;


            if(func==null){
                  this._$WScriptEventBus.$off(eventName);
                  return;
            }


            this._$WScriptEventBus.$off(eventName, func);
      }

      public emitWScriptEvent(eventName:string, data:any){
            if(this._$WScriptEventBus) {
                  this._$WScriptEventBus.$emit(eventName, data);
            }else {
                  console.warn("WARNNING WVComponent.emitWScriptEvent() ");
            }
      }

      public offWScriptEventAll(){
            this._$WScriptEventBus.$off();
      }


}


export enum COMPONENT_DESCRIPTION_NAME {
      DEFAULT_PROPERTIES="default_properties",
      PROPERTY_PANEL_INFO = "property_panel_info",
      PROPERTY_INFO = "property_info",
      METHOD_INFO = "method_info",
      EVENT_INFO="event_info"
}


export enum GROUP_NAME_LIST {
      PRIMARY="primary",
      SETTER="setter",
      STYLE="style",
      LABEL="label",
      INFO="info",
      EVENTS="events",
      EXTENSION="extension",
      EDITOR_MODE="editorMode"
}
