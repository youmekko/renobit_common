import {ArrayUtil} from "../../utils/Util";
export default class WVComponentPropertyManagerCore {


	// 모든 2D 컴포넌트의 기본 속성 객체에 그룹 속성을 추가하는 기능
	static  add_default_props(ComponentClass, groupName, value){
		ComponentClass.default_properties[groupName] = value;
	}

      static remove_default_props(ComponentClass, groupName){
            if(ComponentClass.default_properties.hasOwnProperty(groupName)) {
                  delete ComponentClass.default_properties[groupName];
            }
      }


      // 모든 2D 컴포넌트의 기본 속성 객체에 그룹 속성의 속성을 추가하는 기능
	static  add_default_prop(ComponentClass, groupName, propertyName, value){
		let groupOwner = ComponentClass.default_properties[groupName];
		groupOwner[propertyName] = value;
	}

      static remove_default_prop(ComponentClass, groupName, propertyName){
            if(ComponentClass.default_properties.hasOwnProperty(groupName)) {
                  let groupOwner = ComponentClass.default_properties[groupName];
                  if(groupOwner){
                        if(groupOwner.hasOwnProperty(propertyName)){
                              delete groupOwner[propertyName];
                        }
                  }
            }
      }


      /* 프로퍼티 API reference에  출력 그룹 정보 추가 */
	static add_property_group_info(ComponentClass, info){
	      // 배열이라는 점에 주의
		ComponentClass.property_info.push(info);
	}

	static remove_property_group_info(ComponentClass, groupName){
            ArrayUtil.delete(ComponentClass.property_info, "name", groupName);
      }


      /* 프로퍼티 패널에 출력되는 그룹 정보 추가 */
	static add_property_panel_group_info(ComponentClass, info){
	      // 배열이라는 점에 주의
		ComponentClass.property_panel_info.push(info);
	}

	static remove_property_panel_group_info(ComponentClass, groupName){
            ArrayUtil.delete(ComponentClass.property_panel_info, "name", groupName);
      }

      static remove_property_panel_group_info_by_label(ComponentClass, labelName ){
            ArrayUtil.delete(ComponentClass.property_panel_info, "label", labelName);
      }

      static removePropertyGroupByName( propertyArray, groupName, propertyName ){
	      ArrayUtil.delete(propertyArray, groupName, propertyName );
      }

      static removePropertyGroupChildrenByName( propertyArray, groupName, childItemName ){
            let idx=  ArrayUtil.getItemIndex( propertyArray, "name", groupName );
            let childrenInfo = propertyArray[idx].children;
            if(childrenInfo){
                  ArrayUtil.delete( childrenInfo, "name", childItemName );
            } else {
                  console.log("children이 존재하지 않습니다");
            }
      }


      static getPropertyGroupChildrenByName( propertyArray, groupName, childItemName ){
            let idx=  ArrayUtil.getItemIndex( propertyArray, "name", groupName );
            let childrenInfo  = propertyArray[idx].children;
            let childIdx = ArrayUtil.getItemIndex( childrenInfo, "name", childItemName );
            return childrenInfo[childIdx];
      }


      static get_property_index( propertyArray , groupName, propertyName ){
            return ArrayUtil.getItemIndex( propertyArray, groupName, propertyName );
      }


      static add_event(ComponentClass, info){
            ComponentClass.default_properties.events[info.name]="";
            ComponentClass.event_info.push(info);
      }

      static remove_event(ComponentClass, eventName:string){
            if(ComponentClass.default_properties.events.hasOwnProperty(eventName)) {
                  delete ComponentClass.default_properties.events[eventName];
                  ArrayUtil.delete(ComponentClass.event_info, "name", eventName);
            }
      }

      static add_method_info(ComponentClass, info){
            ComponentClass.method_info.push(info);
      }

      static remove_method_info(ComponentClass, methodName){
            ArrayUtil.delete(ComponentClass.method_info, "name", methodName);
      }

}
