import WV2DComponent from "./WV2DComponent";
import {IWVEvent} from "../../events/interfaces/EventInterfaces";
import CPUtil from "../../utils/CPUtil";
import CPNotification from "../../utils/CPNotification";
import {IStyleManager, WVCOMPONENT_EDITOR_POPERTY_NAMES} from "../interfaces/ComponentInterfaces";
import WVSVGComponentStyleManager from "./manager/WVSVGComponentStyleManager";
import WVComponentEvent from "../../events/WVComponentEvent";
import {GROUP_NAME_LIST} from "../WVComponent";
import {ObjectUtil} from "../../../utils/Util";

export default class WVSVGComponent extends WV2DComponent {


      private _appendElement: SVGElement;
      protected defs : SVGDefsElement;
      private linearGradient : SVGLinearGradientElement;
      private strokeLinearGradient : SVGLinearGradientElement;
      private radialGradient : SVGRadialGradientElement;
      private strokeRadialGradient : SVGRadialGradientElement;

      private dropShadow: SVGFilterElement;
      private feOffset: SVGFEOffsetElement;
      private feGaussianBlur: SVGFEGaussianBlurElement;
      private feColorMatrix: SVGFEColorMatrixElement;

      get appendElement(): SVGElement {
            return this._appendElement;
      }

      get stroke() {
            return this.getGroupProperties("strokeStyle");
      }

      get svgComponentInfo() {
            return this.getGroupProperties("info");
      }

      updateFunctionInfo() {
            this.updateBackground();
            this._validateStrokeStyle();
            this._validateShadow();
      }

      _onImmediateUpdateDisplay(){
            this._validateLock();
            this.updateFunctionInfo();

      }

      onLoadPage() {
            this.updateFunctionInfo();
      }

      _validateStrokeStyle() {
            let strokeModel = this.stroke;
            if(strokeModel.type === 'solid'){
                  $(this.element).css({
                        "stroke" : strokeModel.color1,
                        "stroke-width": strokeModel.width,
                        "stroke-dasharray" : 0,
                  })
            }else if(strokeModel.type === 'dashed') {
                  $(this.element).css({
                        "stroke" : strokeModel.color1,
                        "stroke-width": strokeModel.width,
                        "stroke-dasharray" : strokeModel.width,
                  })
            }else if(strokeModel.type === 'gradient') {
                  $(this.element).css({
                        "stroke" : '',
                        "stroke-width": strokeModel.width,
                        "stroke-dasharray" : 0,
                  });
                  strokeModel.direction !== 'radial' ? this.getLinearGradient(strokeModel) : this.getRadialGradient(strokeModel);
                  this.getSVGGradientColor(strokeModel);
            }
      }

      _onCommitProperties() {
            if(this._updatePropertiesMap.has("background")) {
                  this.validateCallLater(this.updateBackground);
            }

            if(this._updatePropertiesMap.has("strokeStyle")){
                  this.validateCallLater(this._validateStrokeStyle);
            }

            if (this._updatePropertiesMap.has('shadow')) {
                  this.validateCallLater(this._validateShadow);
            }
      }

      _onCreateProperties() {
            if(this.getGroupPropertyValue("background", "type") === "") {

                  this.setGroupPropertyValue("background", "type", "solid");
                  let color = this.getGroupPropertyValue("style", "fill");
                  this.setGroupPropertyValue("background", "color1", color);

            }

            if (this.getGroupPropertyValue("strokeStyle", "type") == "") {
                  this.setGroupPropertyValue("strokeStyle", "type", "solid");
                  let color = this.getGroupPropertyValue("strokeStyle", "color1");
                  this.setGroupPropertyValue("strokeStyle", "color1", color);
            }
      }

      protected _validateLock(): void {
            if (this.lock) {
                  $(this.element).addClass("wv-component-lock");
                  this.dispatchEvent(new WVComponentEvent(WVComponentEvent.LOCKED, this, {}));
            } else {
                  $(this.element).removeClass("wv-component-lock");
                  this.dispatchEvent(new WVComponentEvent(WVComponentEvent.UN_LOCKED, this, {}));
            }
      }

      protected _createElement() {
            this._appendElement = ($(this.properties.template)[0] || CPUtil.getInstance().createSVGElement("svg")) as  SVGElement;

            this.linearGradient = CPUtil.getInstance().createSVGElement("linearGradient") as SVGLinearGradientElement;
            this.linearGradient.id = ObjectUtil.generateID();

            this.strokeLinearGradient = CPUtil.getInstance().createSVGElement("linearGradient") as SVGLinearGradientElement;
            this.strokeLinearGradient.id = ObjectUtil.generateID() + "_2";

            this.radialGradient = CPUtil.getInstance().createSVGElement('radialGradient') as SVGRadialGradientElement;
            this.radialGradient.id = ObjectUtil.generateID();

            this.strokeRadialGradient = CPUtil.getInstance().createSVGElement('radialGradient') as SVGRadialGradientElement;
            this.strokeRadialGradient.id = ObjectUtil.generateID();

            this.createDropShadowElement();
            this.dropShadow.id = ObjectUtil.generateID();

            $(this._appendElement).css({
                  position: "absolute",
                  width: "100%",
                  height: "100%",
                  pointerEvents: "none"
            });
            this.appendElement.style.visibility = "hidden";
            this._validateProperty();
            this._onCreateElement();
            this.createDef();
            this.createTransform();
      }

      private _validateProperty() {
            var style = this.getGroupProperties('style');

            if (style && style.fill) {
                  var svgList = ['SVGRectComponent', 'SVGCircleComponent'];
                  this._call_setGroupPropertyValue('strokeStyle', 'color1', style.stroke);
                  this._call_setGroupPropertyValue('strokeStyle', 'width', style['stroke-width']);

                  if (svgList.includes(this.componentName)) {
                        this._call_setGroupPropertyValue('background', 'color1', style.fill);
                  }
            }
      }

      private createDropShadowElement() {
            this.feGaussianBlur = CPUtil.getInstance().createSVGElement('feGaussianBlur') as SVGFEGaussianBlurElement;
            this.feOffset = CPUtil.getInstance().createSVGElement('feOffset') as SVGFEOffsetElement;
            this.feColorMatrix = CPUtil.getInstance().createSVGElement('feColorMatrix') as SVGFEColorMatrixElement;
            let feBlend = CPUtil.getInstance().createSVGElement('feBlend') as SVGFEBlendElement;

            this.dropShadow = CPUtil.getInstance().createSVGElement('filter') as SVGFilterElement;
            this.dropShadow.id = ObjectUtil.generateID();

            this.feOffset.setAttribute("result", "shadow");
            this.feOffset.setAttribute("in", "shadow");

            this.feGaussianBlur.setAttribute("result", "shadow");
            this.feGaussianBlur.setAttribute("in", "SourceGraphic");

            this.feColorMatrix.setAttribute("in", "shadow");
            this.feColorMatrix.setAttribute("type", "matrix");
            this.feColorMatrix.setAttribute("result", "shadow");

            feBlend.setAttribute("in", "SourceGraphic");
            feBlend.setAttribute("in2", "shadow");
            feBlend.setAttribute("mode", "normal");

            this.dropShadow.setAttribute("x", "0");
            this.dropShadow.setAttribute("y", "0");
            this.dropShadow.setAttribute("width", "200%");
            this.dropShadow.setAttribute("height", "200%");

            this.setShadowProperties();

            this.dropShadow.appendChild(this.feGaussianBlur);
            this.dropShadow.appendChild(this.feOffset);
            this.dropShadow.appendChild(this.feColorMatrix);
            this.dropShadow.appendChild(feBlend);
      }

      private convertColor(rgba) {
            let color = rgba.replace('rgba(', '').replace('rgb(', '').replace(')', '');
            let result = '';
            let opacity = 1;

            if (!color.trim()) return '1 0 0 0 0  1 0 0 0 0  1 0 0 0 0  0 0 0 1 0';

            color = color.split(',');

            for (let i = 0; i < 3; i++) {
                  const value = color[i];
                  result += `${value / 255} 0 0 0 0 `;
            }

            if (color.lenth === 4) opacity = color[3];

            result += `0 0 0 ${opacity} 0`;

            return result;
      }

      private setShadowProperties() {
            let shadow = this.getGroupProperties('shadow');

            if (!shadow) return;

            this.convertColor(shadow.color);
            this.feOffset.setAttribute("dx", shadow.xPosition || 0);
            this.feOffset.setAttribute("dy", shadow.yPosition || 0);
            this.feGaussianBlur.setAttribute("stdDeviation", shadow.blur || 0);
            this.feColorMatrix.setAttribute("values", this.convertColor(shadow.color));
      }

      protected _onDestroy(): void {
            $(this._appendElement).remove();
            this._appendElement = null;
            super._onDestroy();
      }


      protected createDef(): void {
            CPNotification.getInstance().notifyMessage("def가 필요한 클래스는 onCreateDef 오버라이드 해 사용해야 됩니다.");

            this.defs = CPUtil.getInstance().createSVGElement("defs") as SVGDefsElement;
      }

      protected _onCreateElement(): void {
            CPNotification.getInstance().throwErrorWithMessage("사용을 위해 _onCreateElement 오버라이드가 필요합니다.");

      }


      protected _validatePosition(): void {
            CPNotification.getInstance().throwErrorWithMessage("사용을 위해 validatePosition 오버라이드 해야 합니다.");
      }

      protected _validateSize(): void {
            CPNotification.getInstance().throwErrorWithMessage("사용을 위해 validateSize 오버라이드 해야 합니다.");
      }

      protected _validateVisible(): void {
            if(this.isViewerMode) {
                  if (this.appendElement != null) {
                        this.appendElement.style.visibility = this.visible ? "visible" : "hidden";
                  }
            } else {
                  if (this.appendElement != null) {
                        this.appendElement.style.visibility = "visible";
                  }
            }
      }

      protected _validateRotation(): void {
            CPNotification.getInstance().throwErrorWithMessage("사용을 위해 validateSize 오버라이드 해야 합니다.");
      }

      protected _validateStyle(): void {
            this._styleManager.validateProperty();
      }

      protected _validateDepth():void {
            $(this.appendElement).css({zIndex:this.depth});
      }

      public syncLocationSizeElementPropsComponentProps() {
            this._properties.setter.x = parseInt(this.element.style.x);
            this._properties.setter.y = parseInt(this.element.style.y);
            this._properties.setter.width = parseInt(this.element.style.width);
            this._properties.setter.height = parseInt(this.element.style.height);
            this._properties.setter.rotation = CPUtil.getInstance().getCssRotateValue(this.element);
      }

      public createStyleManager(): IStyleManager {
            this._styleManager = new WVSVGComponentStyleManager(this);
            return this.styleManager;
      }

      protected _validateShadow(): void {
            $(this.element).attr('filter', `url(#${this.dropShadow.id})`);
            this.defs.appendChild(this.dropShadow);
            this.setShadowProperties();
      }

      protected updateBackground() {
            let bgData = this.getGroupProperties("background");

            if (!bgData) {
                  $(this.element).attr("fill", "none");
            } else if(bgData.type === 'solid') {
                  this.appendElement.appendChild(this.defs);
                  $(this.element).attr("fill", bgData.color1);
            }else if(bgData.type === 'gradient'){
                  bgData.direction !== 'radial' ? this.getLinearGradient(bgData) : this.getRadialGradient(bgData);
                  this.getSVGGradientColor(bgData);
            }
      }

      protected getLinearGradient(property) {

            if(property.info === 'background'){
                  let linearElement = document.getElementById(this.linearGradient.id);
                  if(linearElement) {
                        while(linearElement.hasChildNodes()){
                              linearElement.removeChild(linearElement.firstChild);
                        }
                        linearElement.parentNode.removeChild(linearElement);
                  }

                  this.appendElement.appendChild(this.element);
                  this.appendElement.appendChild(this.defs);

                  this.defs.appendChild(this.linearGradient);
                  this.defs.appendChild(this.strokeLinearGradient);

                  $(this.element).attr( "fill",  `url(#${this.linearGradient.id})`);

                  this.linearGradient.setAttribute('x1', this.linearGradient.x1.baseVal.valueAsString);
                  this.linearGradient.setAttribute('x2', this.linearGradient.x2.baseVal.valueAsString);
                  this.linearGradient.setAttribute('y1', this.linearGradient.y1.baseVal.valueAsString);
                  this.linearGradient.setAttribute('y2', this.linearGradient.y2.baseVal.valueAsString);

            }else if(property.info === 'stroke'){
                  let linearElement = document.getElementById(this.strokeLinearGradient.id);
                  if(linearElement) {
                        while(linearElement.hasChildNodes()){
                              linearElement.removeChild(linearElement.firstChild);
                        }
                        linearElement.parentNode.removeChild(linearElement);
                  }
                  this.appendElement.insertBefore(this.element, this.appendElement.childNodes[0]);
                  this.appendElement.insertBefore(this.defs, this.appendElement.childNodes[0]);
                  this.defs.appendChild(this.strokeLinearGradient);

                  $(this.element).attr( "stroke",  `url(#${this.strokeLinearGradient.id})`);

                  this.strokeLinearGradient.setAttribute('x1', this.strokeLinearGradient.x1.baseVal.valueAsString);
                  this.strokeLinearGradient.setAttribute('x2', this.strokeLinearGradient.x2.baseVal.valueAsString);
                  this.strokeLinearGradient.setAttribute('y1', this.strokeLinearGradient.y1.baseVal.valueAsString);
                  this.strokeLinearGradient.setAttribute('y2', this.strokeLinearGradient.y2.baseVal.valueAsString);
            }

      }

      protected getRadialGradient(property) {
            if(property.info === 'background'){
                  let radialElement = document.getElementById(this.radialGradient.id);
                  if(radialElement){
                        while(radialElement.hasChildNodes()){
                              radialElement.removeChild(radialElement.firstChild);
                        }
                        radialElement.parentNode.removeChild(radialElement);
                  }

                  this.appendElement.appendChild(this.element);
                  this.appendElement.appendChild(this.defs);
                  this.defs.appendChild(this.radialGradient);
                  $(this.element).attr( "fill",  `url(#${this.radialGradient.id})`);

            }else if(property.info === 'stroke') {

                  let radialElement = document.getElementById(this.strokeRadialGradient.id);
                  if(radialElement){
                        while(radialElement.hasChildNodes()){
                              radialElement.removeChild(radialElement.firstChild);
                        }
                        radialElement.parentNode.removeChild(radialElement);
                  }

                  this.appendElement.appendChild(this.element);
                  this.appendElement.appendChild(this.defs);
                  this.defs.appendChild(this.strokeRadialGradient);
                  $(this.element).attr( "stroke",  `url(#${this.strokeRadialGradient.id})`);
            }

      }

      protected getSVGGradientColor(bgData) {
            const GRADIENT_DIRECTION = {
                  left : {
                        fromOffset : '0%',
                        toOffset : '100%',
                        x1 : '0%',
                        y1 : '0%',
                        x2 : '100%',
                        y2 : '0%'
                  },
                  top : {
                        fromOffset : '40%',
                        toOffset : '100%',
                        x1 : '50%',
                        y1 : '0%',
                        x2 : '50%',
                        y2 : '80%'
                  },
                  diagonal1 : {
                        fromOffset : '30%',
                        toOffset : '90%',
                        x1 : '75%',
                        y1 : '75%',
                        x2 : '10%',
                        y2 : '10%'
                  },
                  diagonal2 : {
                        fromOffset: '30%',
                        toOffset: '90%',
                        x1 : '15%',
                        y1 : '65%',
                        x2 : '75%',
                        y2 : '10%'
                  },
                  radial : {
                        fromOffset : '30%',
                        toOffset : '100%'
                  }
            };

            let stops = [
                  {
                        "color" : bgData.color1,
                        "offset" : GRADIENT_DIRECTION[bgData.direction].fromOffset,
                  },{
                        "color" : bgData.color2,
                        "offset" : GRADIENT_DIRECTION[bgData.direction].toOffset
                  }
            ];
            for(let i = 0; i < stops.length; i++){

                  if(bgData.direction !== 'radial') {
                        if(bgData.info === 'background') {
                              this.linearGradient.setAttribute('x1', GRADIENT_DIRECTION[bgData.direction].x1);
                              this.linearGradient.setAttribute('y1', GRADIENT_DIRECTION[bgData.direction].y1);
                              this.linearGradient.setAttribute('x2', GRADIENT_DIRECTION[bgData.direction].x2);
                              this.linearGradient.setAttribute('y2', GRADIENT_DIRECTION[bgData.direction].y2);

                        }else if(bgData.info === 'stroke'){
                              this.strokeLinearGradient.setAttribute('x1', GRADIENT_DIRECTION[bgData.direction].x1);
                              this.strokeLinearGradient.setAttribute('y1', GRADIENT_DIRECTION[bgData.direction].y1);
                              this.strokeLinearGradient.setAttribute('x2', GRADIENT_DIRECTION[bgData.direction].x2);
                              this.strokeLinearGradient.setAttribute('y2', GRADIENT_DIRECTION[bgData.direction].y2);
                        }

                  }

                  let stop = CPUtil.getInstance().createSVGElement("stop");

                  stop.setAttribute("offset", stops[i].offset);
                  stop.setAttribute("stop-color", stops[i].color);

                  if(bgData.info === 'background'){
                        bgData.direction !== 'radial' ? this.linearGradient.appendChild(stop) : this.radialGradient.appendChild(stop);
                  }else if(bgData.info === 'stroke'){
                        bgData.direction !== 'radial' ? this.strokeLinearGradient.appendChild(stop) : this.strokeRadialGradient.appendChild(stop);
                  }
            }
      }



      /*
      editor 모드에서만 실행
       */
      protected _validateEditorModeVisible():void {

            if(this.isEditorMode){
                  let visible:boolean = this.getGroupPropertyValue(GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_EDITOR_POPERTY_NAMES.VISIBLE);
                  this.appendElement.style.visibility = (visible || visible == null) ? "visible" : "hidden";
            }
      }



      /*
      *     마우스 enabled처리
      * */
      public setMouseEventEnabled(enabled:boolean){
            $(this.element).css({
                  pointerEvents: enabled == true ? "auto" : "none"
            })
      }



}

