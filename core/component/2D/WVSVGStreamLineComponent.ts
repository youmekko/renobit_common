import WVPointComponent from "./WVPointComponent";
import CPUtil from "../../utils/CPUtil";
import {
      StreamLineDirection, StreamLineVO, StreamTweenVO, WVDISPLAY_PROPERTIES, WVSVG_PROPERTIES
} from "../interfaces/ComponentInterfaces";
import {GROUP_NAME_LIST} from "../WVComponent";
import {TweenMax} from "../../utils/reference";
import {BROWSER_TYPE} from "../../display/interfaces/DisplayInterfaces";
import {CPLogger} from "../../utils/CPLogger";


export default class WVSVGStreamLineComponent extends WVPointComponent {

      private invalidateTween:boolean;
      get preview(): boolean {
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.PREVIEW_MODE );
      }

      set preview(value: boolean) {
            if(this._checkUpdateGroupPropertyValue( GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.PREVIEW_MODE, value)){
                  this.invalidateTween = true;
            }
      }



      constructor(){super();}

      protected _onDestroy():void {
            this.stop();
            $(this.streamPath).remove();
            this.tween        = null;
            this._streamVO    = null;
            this._streamPath  = null;
            super._onDestroy();
      }

      protected _onCreateProperties() {
            super._onCreateProperties();
            this._streamVO      = {
                  stateInfo: this.properties.stateInfo,
                  tweenInfo: $.extend({
                              from: 0,
                              to:0,
                              value:0
                  }, this.properties.tweenInfo)
            }

      }

      protected _onCreateElement():void{
            this._element       = CPUtil.getInstance().createSVGElement("path") as SVGGraphicsElement;
            this._streamPath  = this.element.cloneNode(true) as SVGPathElement;
            this.appendElement.appendChild(this.element);
            this.appendElement.appendChild(this._streamPath);
      }


      _onCommitProperties():void{
            super._onCommitProperties();
            if( this.invalidatePoints ) {
                  this.validateCallLater(this._validateStreamPoints);
            }

            if(this._updatePropertiesMap.has("tweenInfo")){
                  this.streamVO.tweenInfo    = this.getGroupProperties("tweenInfo");
                  this.validateCallLater(this._validateStreamPoints);
            }

            if( this._updatePropertiesMap.has("stateInfo") ){
                  this.streamVO.stateInfo = this.getGroupProperties("stateInfo");
                  this.validateCallLater(this._validateStreamPoints);
            }

            if(this.invalidateTween ) {
                  this.validateCallLater(this._validateTween);
            }


      }

      _onImmediateUpdateDisplay():void{
            super._onImmediateUpdateDisplay();
            this._validateStreamPoints();
      }


      // 편집기에서 미리보기가 활성화 되어 있으면 재생
      protected _validateTween():void {
            this.invalidateTween = false;
            // viewer일 경우는 리턴
            if(!this.isEditorMode) {return;}
            // 에디터일 경우 preview값에 따른 play, stop처리
            if( this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVDISPLAY_PROPERTIES.PREVIEW_MODE) == true ) {
                  this.play();
            } else {
                  this.stop();
            }
      }

      protected _validateStreamPoints():void {
            let totalLength:number = (<SVGPathElement>this.element).getTotalLength();
            $(this.streamPath).attr({
                  "d"                  : this.element.getAttribute("d"),
                  "stroke-width"       : this.element.getAttribute("stroke-width"),
                  "stroke"             : this._streamVO.stateInfo[this._streamVO.stateInfo.state],
                  "stroke-dasharray"   :`${this.streamVO.tweenInfo.streamSize} ${totalLength}`,
                  "fill"               : "none",
                  "fill-opacity"       : 0
            });
            this._validateTween();
      }

      protected _validateStyle():void {
            super._validateStyle();
            $(this.streamPath).attr({
                  "stroke-width" :  parseInt(this.getGroupPropertyValue(GROUP_NAME_LIST.STYLE, WVSVG_PROPERTIES.STROKE_WIDTH)) - 1
            });
      }


      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       IWVStreamLine 인터페이스 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       * */
      protected _streamVO:StreamLineVO;
      get streamVO(): StreamLineVO {
            return this._streamVO;
      }


      protected _streamPath:SVGGraphicsElement;
      get streamPath(): SVGGraphicsElement {
            return this._streamPath;
      }


      protected getTweenValueByStreamType( strType:string, totalLength:number ):StreamTweenVO {
            let info:StreamTweenVO = this.streamVO.tweenInfo;
            switch ( strType ){
                  case StreamLineDirection.LEFT:
                        info.from  = info.streamSize;
                        info.to    = totalLength+info.streamSize*2;
                        break;

                  case StreamLineDirection.RIGHT:
                  case StreamLineDirection.BI_DIRECTIONAL:
                        info.from  = totalLength+info.streamSize*2;
                        info.to    = info.streamSize;
                        break;
            }
            info.value = info.from;
            return info;

      }



      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       IStreamTweenAble 인터페이스 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       * */
      protected tween:any;
      play(): void {
            this.stop();
            let tweenObj:StreamTweenVO = this.getTweenValueByStreamType(this.streamVO.stateInfo.direction, (<SVGPathElement>this.element).getTotalLength());
            this.tween = TweenMax.to(tweenObj, tweenObj.duration/1000, {
                  value : tweenObj.to,
                  repeat:-1,
                  yoyo  :this.streamVO.stateInfo.direction == StreamLineDirection.BI_DIRECTIONAL,
                  onUpdate:()=>{
                        this.refreshIESVGElement(this.appendElement);
                        $(this.streamPath).attr({"stroke-dashoffset":tweenObj.value});
                  }
            });
      }

      stop(): void {
            if(this.tween != null && this.tween.isActive()){
                  this.tween.pause();
                  this.tween.kill();
            }
      }



      /*
      *
      * */
      onLoadPage():void{

            if(this.isEditorMode){
                  this._validateTween();
            } else {
                  this.play();
            }
      }

}
