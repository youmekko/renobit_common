import Point from "../../geom/Point";
import WVSVGComponent from "./WVSVGComponent";
import {BROWSER_TYPE, IPointDisplay} from "../../display/interfaces/DisplayInterfaces";
import CPNotification from "../../utils/CPNotification";
import CPUtil from "../../utils/CPUtil";
import {IComponentMetaProperties, WVDISPLAY_PROPERTIES, WVSVG_PROPERTIES} from "../interfaces/ComponentInterfaces";
import {Matrix} from "../../geom/Matrix";
import {GROUP_NAME_LIST} from "../WVComponent";

export default class WVPointComponent extends WVSVGComponent implements IPointDisplay {


      protected _onDestroy(): void {
            if (this._startMarker) {
                  $(this._startMarker).remove();
                  this._startMarker = null;
            }
            if (this._endMarker) {
                  $(this._endMarker).remove();
                  this._endMarker = null;
            }
            this._points = null;
            super._onDestroy();
      }

      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       get set 프로퍼티 선언
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       * */
      protected invalidateMarker: boolean = false;

      set startMarker(value: boolean) {
            if (this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVSVG_PROPERTIES.START_MARKER, value)) {
                  this.invalidateMarker = true;
            }
      }

      get startMarker(): boolean {
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVSVG_PROPERTIES.START_MARKER);
      }

      set endMarker(value: boolean) {
            if (this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVSVG_PROPERTIES.END_MARKER, value)) {
                  this.invalidateMarker = true;
            }
      }

      get endMarker(): boolean {
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVSVG_PROPERTIES.END_MARKER);
      }

      /**
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       IPointDisplay 인터페이스 구현
       ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧
       * */
      get pointCount(): number {
            return this.getGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVSVG_PROPERTIES.POINT_COUNT);
      }

      set pointCount(value: number) {
            let count:number = Math.max(this.getDefaultProperties().setter.pointCount, value);
            if (this._checkUpdateGroupPropertyValue(GROUP_NAME_LIST.SETTER, WVSVG_PROPERTIES.POINT_COUNT, count)) {
                  let idx: number = 0;
                  if (count != this.points.length) {
                        if (count > this.points.length) { // 클 경우
                              count -= this.points.length;
                              while (idx < count) {
                                    this.insertPoint();
                                    idx++;
                              }
                        } else {
                              idx = this.points.length;
                              while (idx > count) {
                                    this.removePoint();
                                    idx--;
                              }
                        }
                  }
                  this.invalidatePoints = true;
                  this.invalidateProperties();
            }
      }

      protected _points: Array<Point>;
      protected invalidatePoints: boolean;
      protected _useAffineMatrix: boolean = false;

      get points(): Array<Point> {
            return this._points;
      }

      set points(value: Array<Point>) {
            if (this._points != value) {
                  this._points = value;
                  this.invalidatePoints = true;
                  this.invalidateProperties();
            }
      }

      get paths(): string {
            return CPNotification.getInstance().throwErrorWithMessage("서브 클래스에서 path값 생성처리가 필요합니다.");
      }

      protected _notifyInserPoint(point: Point): void {
            CPNotification.getInstance().throwErrorWithMessage("_notifyInserPoint 함수를 오버라이드 필요합니다.");
      }

      protected _notifyRemovePoint(point: Point): void {
            CPNotification.getInstance().throwErrorWithMessage("_notifyRemovePoint 함수를 오버라이드 필요합니다.");
      }

      insertPoint(point?: Point): Point {
            let lastIndex: number = this.points.length - 1;
            let prevIndex: number = lastIndex - 1;

            let prevPoint: Point = this.points[prevIndex];
            let endPoint: Point = this.points[lastIndex];
            let insert: Point = point || prevPoint.interpolate(endPoint, 0.5);
            this.points.splice(lastIndex, 0, insert);
            // 컨트롤 추가를 위한 훅 호출
            this._notifyInserPoint(insert);
            return insert;
      }

      removePoint(point?: Point): Point {
            let nIndex: number = -1;
            let rPoint: Point = point;
            if (point != null) {
                  nIndex = this.points.indexOf(point);
            } else {
                  nIndex = this.points.length - 2;
            }
            if (nIndex > 0) {
                  // pointTransformTool에서 참조를 하기 때문에
                  // 이벤트 전송 후 배열에서 제거 함.
                  this._notifyRemovePoint(this.points[nIndex]);
                  rPoint = this.points.splice(nIndex, 1)[0];
            }
            return rPoint
      }

      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       생성자
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
      constructor() {
            super();
      }

      protected _onCreateProperties(): void {
            super._onCreateProperties();
            if (this.properties.points) {
                  let restorePoints: Array<any> = JSON.parse(this.properties.points);
                  let point: any;
                  this._points = [];
                  for (let idx: number = 0; idx < restorePoints.length; idx++) {
                        point = restorePoints[idx];
                        this._points.push(new Point(point.x, point.y));
                  }
            } else {
                  this._points = this._createDefaultPoints();
            }
      }

      protected _createDefaultPoints(): Array<Point> {
            return CPNotification.getInstance().throwErrorWithMessage("사용을 위해 기본 _points값을 반환해 주세요");
      }

      handleTransformList(): Array<string> {
            return CPNotification.getInstance().throwErrorWithMessage("사용을 위한 handleTransformList 오버라이드 필요합니다.");
      }


      /*
	외부에서 조건에 상관없이 화면을 다시 그리기
	 */
      _onImmediateUpdateDisplay() {
            super._onImmediateUpdateDisplay();
            this._validateMarker();
            this._validatePoints();
            this._useAffineMatrix = true;
      }

      _onCommitProperties(): void {
            super._onCommitProperties();
            if (this.invalidateMarker) {
                  this.validateCallLater(this._validateMarker);
            }
            // 최종 포인트에 따른 드로잉 처리
            if (this.invalidatePoints) {
                  this.validateCallLater(this._validatePoints);
            }
      }


      protected _validateRotation(): void {
            CPNotification.getInstance().notifyMessage("라인 객체는 Rotate 속성을 사용하지 않습니다.!");
      }


      _clearInvalidate(): void {
            super._clearInvalidate();
            if (!this._useAffineMatrix) {
                  this._useAffineMatrix = true;
            }
      }

      protected _validatePoints(): void {
            $(this.element).attr({"d": this.paths});
            if(this._parent!=null){
                  let rect: SVGRect = (this.element as SVGGraphicsElement).getBBox();
                  // IE에서 svgrect의 속성 변경시 NoModificationAllowedError 에러발생.
                  // 직선처리 시 width,height변경을 위해 applyRect에 svgrect값 참조.
                  let applyRect = {width:rect.width, height:rect.height};
                  this._registExtraProperty(WVDISPLAY_PROPERTIES.OLD_X, rect.x);
                  this._registExtraProperty(WVDISPLAY_PROPERTIES.OLD_Y, rect.y);
                  this._registExtraProperty(WVDISPLAY_PROPERTIES.OLD_WIDTH, rect.width );
                  this._registExtraProperty(WVDISPLAY_PROPERTIES.OLD_HEIGHT, rect.height );
                  this.transform.matrix.scale(rect.width / this.transform.width, rect.height / this.transform.height);
                  this.transform.matrix.tx = rect.x;
                  this.transform.matrix.ty = rect.y;
                  if(rect.width == 0){
                        applyRect.width = 1;
                  };
                  if(rect.height == 0){
                        applyRect.height = 1;
                  };
                  this.properties.setter.x = rect.x;
                  this.properties.setter.y = rect.y;
                  this.properties.setter.width = applyRect.width;
                  this.properties.setter.height = applyRect.height;
            }
            this.invalidatePoints = false;
      }

      protected _validatePosition(): void {

            if (this._useAffineMatrix) {
                  let transX: number = this.x - this._restoreExtraProperty(WVDISPLAY_PROPERTIES.OLD_X);
                  let transY: number = this.y - this._restoreExtraProperty(WVDISPLAY_PROPERTIES.OLD_Y);
                  if (transX != 0 || transY != 0) {
                        this.points.forEach((p: Point) => {
                              p.x += transX;
                              p.y += transY;
                        });
                        this._validatePoints();
                  }
            }
            this._registExtraProperty(WVDISPLAY_PROPERTIES.OLD_X, this.properties.setter.x);
            this._registExtraProperty(WVDISPLAY_PROPERTIES.OLD_Y, this.properties.setter.y);
      }


      /**
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       라인의 경우 width, height 변경 시 전체 포인트 이동 처리를 해줘야 함.
       단 초기화 시에는 예외
       ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨
       */
      protected _validateSize(): void {
            if (this._useAffineMatrix) {
                  let sx: number = this.width / this._restoreExtraProperty(WVDISPLAY_PROPERTIES.OLD_WIDTH);
                  let sy: number = this.height / this._restoreExtraProperty(WVDISPLAY_PROPERTIES.OLD_HEIGHT);
                  // 0일 경우 0으로 수렴하게
                  if(sx == 0 ){sx = 0.000001;}
                  if(sy == 0 ){sy = 0.000001;}
                  // 0에서 변경될 경우 기존 점들을 이동 시키기 위해 강제로 포인트 위치를 변경
                  if(sx == Infinity ){
                        sx = this.width;
                        let step:number = 1/(this._points.length-1);
                        for( let i=1; i<this._points.length; i++){
                              this.points[i].x += step*i;
                        }
                  }
                  if(sy == Infinity ){
                        sy = this.height;
                        let step:number = 1/(this._points.length-1);
                        for( let i=1; i<this._points.length; i++){
                              this.points[i].y  += step*i;
                        }
                  }

                  let matrix: Matrix = new Matrix(1, 0, 0, 1, this.x, this.y);
                  matrix.concat(new Matrix(sx, 0, 0, sy));
                  let tlPoint: Point = new Point(this.x, this.y);
                  tlPoint = tlPoint.subtract(matrix.transformPoint(tlPoint));
                  this._points = this.points.map((p: Point) => {
                        p = matrix.transformPoint(p);
                        p.x = Math.floor(p.x + tlPoint.x);
                        p.y = Math.floor(p.y + tlPoint.y);
                        return p;
                  });
                  this._validatePoints();
            }
      }

      protected _validateMarker(): void {
            this.element.setAttribute("marker-start", "none");
            this.element.setAttribute("marker-end", "none");
            this.refreshIESVGElement(this.appendElement);
            if (this.startMarker) {
                  this.element.setAttribute("marker-start", "url(#" + this._startMarker.getAttribute("id") + ")");

            }
            if (this.endMarker) {
                  this.element.setAttribute("marker-end", "url(#" + this._endMarker.getAttribute("id") + ")");
            }
            this._validateMarkerStyle();
      }

      _validateProperty() {
            super._validateProperty();
            setTimeout(() => {
                  this._useAffineMatrix = false;
            })
      }

      protected lineTo(point: Point): Array<string | number> {
            return ["L", point.x, point.y];
      }

      protected moveTo(point: Point): Array<string | number> {
            return ["M", point.x, point.y];
      }

      protected curveTo(sp: Point, cp: Point): Array<string | number> {
            return ["Q", sp.x, sp.y, cp.x, cp.y];
      }


      protected _validateStyle(): void {
            this._styleManager.validateProperty();
            // 스타일 정보를 marker에도 적용 처리
            this._validateMarkerStyle();
      }

      protected _validateMarkerStyle():void{
            // 스타일 정보를 marker에도 적용 처리
            let fillValue = this._styleManager.getProperty(WVSVG_PROPERTIES.STROKE);
            $(this._startMarker.querySelector("path")).attr({fill: fillValue});
            $(this._endMarker.querySelector("path")).attr({fill: fillValue});
      }



      protected refreshIESVGElement( objElement:any ):void{
            if(CPUtil.getInstance().browserDetection() == BROWSER_TYPE.IE){
                  objElement.parentNode.insertBefore(objElement, objElement);
            }
      }

      /**
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       화살표 처리
       □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
       * */
      protected _startMarker: SVGMarkerElement;
      protected _endMarker: SVGMarkerElement;

      protected createDef(): void {
            let def: SVGDefsElement = this.defs = CPUtil.getInstance().createSVGElement("defs") as SVGDefsElement;
            let arrowDef: SVGGraphicsElement = CPUtil.getInstance().createSVGElement("path", {
                  d: "M4,0 0,2 4,4 2,2 4,0z",
                  stroke: "none"
            }) as SVGGraphicsElement;
            let endArrowDef: SVGGraphicsElement = arrowDef.cloneNode(true) as SVGGraphicsElement;
            $(endArrowDef).attr({"transform": "rotate(180 2 2)"})
            this._startMarker = CPUtil.getInstance().createSVGElement("marker") as SVGMarkerElement;
            $(this._startMarker).attr({
                  "markerWidth": "4",
                  "markerHeight": "4",
                  "refX": "2",
                  "refY": "2",
                  "orient": "auto"
            });
            let time = parseInt(performance.now().toString());
            this._endMarker = this._startMarker.cloneNode(true) as SVGMarkerElement;
            this._startMarker.setAttribute("id", "marker" + time + "_start");
            this._startMarker.appendChild(arrowDef);
            this._endMarker.setAttribute("id", "marker" + time + "_end");
            this._endMarker.appendChild(endArrowDef);
            def.appendChild(this._startMarker);
            def.appendChild(this._endMarker);
            this.appendElement.insertBefore(def, this.element);
      }

      serializeMetaProperties(): IComponentMetaProperties {
            let properties: any = super.serializeMetaProperties();
            properties.props.points = JSON.stringify(this.points);
            return properties;
      }

}
