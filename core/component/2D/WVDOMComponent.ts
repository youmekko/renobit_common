import {GROUP_NAME_LIST} from "../WVComponent";
import WVDOMComponentStyleManager from "./manager/WVDOMComponentStyleManager";
import {IStyleManager, WVCOMPONENT_EDITOR_POPERTY_NAMES} from "../interfaces/ComponentInterfaces";
import WV2DComponent from "./WV2DComponent";
import CPUtil from "../../utils/CPUtil";
import WVComponentEvent from "../../events/WVComponentEvent";

export default class WVDOMComponent extends WV2DComponent {
      private _labelElement: any;
      private _tempTimerID:number = 0;

      get appendElement(){
            return this._element;
      }

      constructor() {
            super();
            this._labelElement = null;
      }

      protected _onDestroy(){
            if(this._element){
                  let $element =$(this._element);
                  $element.empty();
                  $element.off();
                  this._element = null;

            }
            if (this._labelElement) {
                  let $labelElement =$(this._labelElement);
                  $labelElement.remove();
                  $labelElement.off();
                  this._labelElement = null;
            }

            super._onDestroy();
      }

      setMouseEventEnabled(enabled:boolean){
            if(enabled){
                  $(this.appendElement).css("pointer-events", "auto");

            }else {
                  $(this.appendElement).css("pointer-events", "none");
            }
      }



      protected _createElement(template) {
            this._element = $(template)[0] || document.createElement("div");
            this._element.style.position = "absolute";
            this._element.style.transformOrigin = "0 0";
            // $(this._element).on('contextmenu', function(){return false;}); cctv context text(do not erase);
      /*
      2018.05.02
            - 초기 화면에서 보이지 않게
            - 보이게 처리하는 부분은?
                  - WVComponent의 visible 값이 true인 경우 보이게됨.
       */
            //this._element.style.display="none";
            this._onCreateElement();
            this._createLabelElement();
            this.createTransform();

      }

      private _createLabelElement(): void {
            if (this.getGroupProperties("label")) {
                  // 라벨 엘리먼트 초기화
                  if (this._labelElement) {
                        $(this._labelElement).remove();
                        this._labelElement = null;
                  }

                  if (this._properties.label.label_using == 'Y') {

                        var $label = $(`<div>${this.getTranslateText(this._properties.label.label_text)}</div>`);
                        $label.css({
                              "white-space": "nowrap",
                              "position": "absolute"
                        });

                        $(this._element).append($label);
                        this._labelElement = $label[0];
                  }
            }
      }

      private getTranslateText(str){
            return wemb.localeManager.translatePrefixStr(str);
      }


      protected _commitProperties(): void {
            super._commitProperties();

            if (this._updatePropertiesMap.has("label")) {
                  this.deferredCall(this._validateLabelProperty);
            }
      }


      public immediateUpdateDisplay(event: boolean = false) {
            this._validateLock();
            super.immediateUpdateDisplay(event);
      }


      protected _validateLock(): void {
            /*
            lock처리는 editor 모드에서만 실행되어야 함.
             */
            if(this.isEditorMode) {
                  if (this.lock) {
                       $(this.element).addClass("wv-component-lock");
                        this.dispatchEvent(new WVComponentEvent(WVComponentEvent.LOCKED, this, {}));
                  } else {
                        $(this.element).removeClass("wv-component-lock");
                        this.dispatchEvent(new WVComponentEvent(WVComponentEvent.UN_LOCKED, this, {}));
                  }
            }
      }


      protected _validatePosition(): void {
            let domElement: HTMLElement = this.element as HTMLElement;
            let properties = this.getGroupProperties(GROUP_NAME_LIST.SETTER);
            if(!properties)
                  return;

            $(domElement).css({
                  left: properties.x,
                  top: properties.y
            })
      }

      protected _validateSize(): void {
            let domElement: HTMLElement = this.element as HTMLElement;
            let properties = this.getGroupProperties(GROUP_NAME_LIST.SETTER);
            if(!properties)
                  return;

            $(domElement).css({
                  width: properties.width,
                  height: properties.height
            });

            this._validateLabelProperty();


      }

      protected _validateVisible(): void {
            if(this.isViewerMode) {
                  let visible = this.visible;
                  this.element.style.display = (visible || visible == null) ? "" : "none";
            }
      }

      /*
      editor 모드에서만 실행
       */
      protected _validateEditorModeVisible():void {

            if(this.isEditorMode){
                  let visible:boolean = this.getGroupPropertyValue(GROUP_NAME_LIST.EDITOR_MODE, WVCOMPONENT_EDITOR_POPERTY_NAMES.VISIBLE);
                  this.element.style.display = (visible || visible == null) ? "" : "none";
            }
      }

      protected _validateRotation(): void {
            $(this._element).css({"transform": "rotate(" + this.rotation + "deg)"})
      }

      protected _validateDepth(): void {
            this._element.style.zIndex = this.depth.toString();
      }

      public syncLocationSizeElementPropsComponentProps() {
            this._properties.setter.x = parseInt(this.element.style.x);
            this._properties.setter.y = parseInt(this.element.style.y);
            this._properties.setter.width = parseInt(this.element.style.width);
            this._properties.setter.height = parseInt(this.element.style.height);
            this._properties.setter.rotation = CPUtil.getInstance().getCssRotateValue(this.element);
      }


      public createStyleManager(): IStyleManager {
            this._styleManager = new WVDOMComponentStyleManager(this);
            return this._styleManager;
      }



      public _validateLabelProperty(): void {
            this._createLabelElement();


            var labelProps = this.getGroupProperties("label");
            if(labelProps==null)
                  return;

            if (labelProps.label_using == 'N') {
                  return;
            }

            labelProps.label_font_weight = labelProps.label_font_weight || 'normal';
            labelProps.label_border_radius = labelProps.label_border_radius || 0;
            labelProps.label_padding_tb = labelProps.label_padding_tb || 0;
            labelProps.label_padding_lr = labelProps.label_padding_lr || 0;

            var left = 0;
            var top = 0;
            var $label = $(this._labelElement);
            if(this._tempTimerID!=0){
                  clearInterval(this._tempTimerID);
                  this._tempTimerID= 0;
            }

            let info = this._measureLabelSize(labelProps);
            let labelWidth = info.width;
            let labelHeight = info.height;

            switch (labelProps.label_position) {
                  case "LT":
                        left = 0;
                        top = 0;
                        break;
                  case "CT":
                        left = (this.width - labelWidth) / 2;
                        top = 0;
                        break;
                  case "RT":
                        left = (this.width - labelWidth);
                        top = 0;
                        break;
                  case "LC":
                        left = 0;
                        top = (this.height - labelHeight) / 2;
                        break;
                  case "CC":
                        left = (this.width - labelWidth) / 2;
                        top = (this.height - labelHeight) / 2;
                        break;
                  case "RC":
                        left = this.width - labelWidth;
                        top = (this.height - labelHeight) / 2;
                        break;
                  case "LB":
                        left = 0;
                        top = (this.height);
                        break;
                  case "CB":
                        left = (this.width - labelWidth) / 2;
                        top = (this.height);
                        break;
                  case "RB":
                        left = this.width - labelWidth;
                        top = (this.height);
                        break;
            }

            let background = this.styleManager.getLabelBackgroundStyle(labelProps);
            let padding = labelProps.label_padding_tb+"px "+labelProps.label_padding_lr+"px";

            $label.css({
                  color: labelProps.label_color,
                  opacity: labelProps.label_opacity,
                  fontSize: labelProps.label_font_size,
                  fontWeight: labelProps.label_font_weight,
                  fontFamily: labelProps.label_font_type,
                  padding: padding,
                  background: background,
                  border: labelProps.label_border,
                  borderRadius: labelProps.label_border_radius,
                  left: parseInt(labelProps.label_offset_x) + left,
                  top: parseInt(labelProps.label_offset_y) + top
            });

      }

      getPadding(){

      }

      _measureLabelSize(labelProps) {
            let $div = $(`<div>${this.getTranslateText(labelProps.label_text)}</div>`);
            let padding = labelProps.label_padding_tb+"px "+labelProps.label_padding_lr+"px";
            //font-weight는 width에 반영되지 않는 문제가 있음
            $div.css({
                  color: labelProps.label_color,
                  opacity: labelProps.label_opacity,
                  fontSize: labelProps.label_font_size,
                  fontFamily: labelProps.label_font_type,
                  fontWeight: labelProps.label_font_weight,
                  border: labelProps.label_border,
                  boxSizing: "border-box",
                  padding: padding,
                  visibility:"hidden",
                  position:"absolute"
            });

            $(document.body).append($div);

            let info = {
                  width:$div.outerWidth(),
                  height:$div.outerHeight()
            }

            $div.remove();

            return info;

      }

      getLabelSize(label, fontSize){
            let $div = $(`<div>${label}</div>`);
            $div.css({
                  fontSize: fontSize,
                  visibility:"hidden",
                  position:"absolute"
            });

            $(document.body).append($div);

            let info = {
                  width:$div.width(),
                  height:$div.height()
            }

            $div.remove();

            return info;
      }

}

