import WVComponentPropertyManagerCore from "./WVComponentPropertyManagerCore";
export default class WVComponentPropertyManager extends WVComponentPropertyManagerCore{


      // 기본 프로퍼티 정보입니다.
      static default_properties = {
            "primary":{
                  "id": "",
                  "name": "",
                  "layerName":""
            },
            "setter": {
                  "mouseEnabled":true,
                  "depth": 100,
                  "x": 0,
                  "y": 0,
                  "visible":true,
                  "opacity": 1,
                  "width": 100,
                  "height": 100,
                  "rotation":0
            },
            "label": {
                  "label_using":"N",
                  "label_text":"Frame Component",
                  "label_position":"CB",
                  "label_offset_x":0,
                  "label_offset_y":0,
                  "label_color":"#333333",
                  "label_font_type": "inherit",
                  "label_font_size":11,
                  "label_font_weight":'normal',
                  "label_border":"1px none #000000",
                  "label_background_color":"#eeeeee",//삭제 예정
                  "label_padding_tb": 0,
                  "label_padding_lr":0,
                  "label_background_type": "",
                  "label_background_direction": "left",//top, left, diagonal1, diagonal2, radial
                  "label_background_color1": "rgba(0,0,0,0)",
                  "label_background_color2": "rgba(0,0,0,1)",
                  "label_background_text": "",

                  "label_border_radius": 0,
                  "label_opacity":1
            },
            "style": {
                  "border":"1px none #000000",
                  "backgroundColor":"rgba(255,255,255,0)",
                  "borderRadius":0
            },

            "info":{
                  "componentName":"",
                  "version":"1.0.0",
                  "category":"2D"
            },
            "events": {
                  "click": "",
                  "dblclick": "",
                  "register": "",
                  "complete":""
            },
            "editorMode":{
                  "lock": false,
                  "visible":true
            },
            "tooltip": {
                  "description": "",
                  "border_radius": 0,
                  "border": "1px none #000000",
                  "type": "solid",
                  "direction": "left",
                  "color1": "rgba(0, 0, 0, 1)",
                  "color2": "rgba(0, 0, 0, 1)",
                  "text": "",
                  "font_type": "inherit",
                  "font_color": "#ffffff",
                  "font_weight": "none",
                  "font_size": "14",
                  "line_height": "14",
            }
      };

      // 프로퍼티 패널에서 사용할 정보 입니다.
      static property_panel_info = [{
            name:"primary",
            label: "일반 속성",
            template: "primary",
            children: [{
                  owner:"primary",
                  name: "name",
                  type: "string",
                  label: "이름",
                  writable: true,
                  show: true,
                  description: "인스턴스 이름"
            }]
      }, {
            name:"pos-size-2d",
            label: "위치 크기 정보",
            template: "pos-size-2d",
            children: [{
                  owner:"setter",
                  name: "lock",
                  type: "checkbox",
                  label: "잠금",
                  show: true,
                  writable: true,
                  description: "잠금 유무"
            },{
                  owner:"setter",
                  name: "visible",
                  type: "checkbox",
                  label: "잠금",
                  show: true,
                  writable: true,
                  description: "잠금 유무"
            }, {
                  owner:"setter",
                  name: "x",
                  type: "number",
                  label: "x",
                  tag: 'px',
                  show: true,
                  writable: true,
                  description: "x 위치 값 test..."
            }, {
                  owner:"setter",
                  name: "y",
                  type: "number",
                  label: "y",
                  tag: 'px',
                  show: true,
                  writable: true,
                  description: "y 위치 값"
            }, {
                  owner:"setter",
                  name: "width",
                  type: "number",
                  label: "width",
                  tag: 'px',
                  show: true,
                  writable: true,
                  description: "width  값"
            }, {
                  owner:"setter",
                  name: "height",
                  type: "number",
                  label: "height",
                  tag: 'px',
                  show: true,
                  writable: true,
                  description: "height 위치 값"
            }]
      }, {
            name:"label",
            label: "레이블 속성",
            template: "label",
            children: [{
                  owner:"label",
                  name: "label_using",
                  type: "boolean",
                  label: "using",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_text",
                  type: "string",
                  label: "text",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_position",
                  type: "string",
                  label: "position",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_offset_x",
                  type: "number",
                  label: "offsetY",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_offset_y",
                  type: "number",
                  label: "offsetY",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_color",
                  type: "color",
                  label: "color",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_font_size",
                  type: "number",
                  label: "fontSize",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_border",
                  type: "string",
                  label: "border",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_background_color",
                  type: "color",
                  label: "background_color",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_border_radius",
                  type: "number",
                  label: "border_radius",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            },{
                  owner:"label",
                  name: "label_opacity",
                  type: "number",
                  label: "Opacity",
                  writable: true,
                  show: true,
                  description: "레이블 속성입니다."
            }]
      }, {
            name:"background",
            label: "배경",
            template: "background",
            children: [{
                  owner:"style",
                  name: "backgroundColor",
                  type: "color",
                  label: "배경색",
                  show: true,
                  writable: true,
                  description: "배경색 값"
            }]
      }, {
            name: "border",
            label: "외각선",
            template: "border",
            children: [{
                  owner:"style",
                  name: "border",
                  type: "border",
                  label: "외각선",
                  show: true,
                  writable: true,
                  minValue: 0,
                  maxValue: 10,
                  description: "외각선"
            }]
      }];



      // 스크립트 에디터에서 사용할 정보 입니다.
      static property_info = [{
            name: "primary",
            label: "일반 속성",
            children: [{
                  owner:"setter",
                  name: "name",
                  type: "string",
                  label: "name",
                  writable: false,
                  show: true,
                  description: "인스턴스명"
            },{
                  owner:"setter",
                  name: "visible",
                  type: "boolean",
                  label: "visible",
                  writable: true,
                  show: true,
                  defaultValue: "true",
                  description: "visible 상태 값"
            }/*,{
                  owner:"setter",
                  name: "opacity",
                  type: "number",
                  label: "opacity",
                  writable: true,
                  show: true,
                  defaultValue: "1",
                  description: "opacity 값(0~1)"
            }*/]
      }, {

            name: "pos-size-2d",
            label: "위치 크기 정보",
            children: [{
                  owner:"",
                  name: "x",
                  type: "number",
                  label: "x",
                  show: true,
                  writable: true,
                  description: "x 위치 값"
            }, {
                  owner:"",
                  name: "y",
                  type: "number",
                  label: "y",
                  tag: 'px',
                  show: true,
                  writable: true,
                  description: "y 위치 값"
            }, {
                  owner:"",
                  name: "width",
                  type: "number",
                  label: "width",
                  tag: 'px',
                  show: true,
                  writable: true,
                  description: "width  값"
            }, {
                  owner:"",
                  name: "height",
                  type: "number",
                  label: "height",
                  tag: 'px',
                  show: true,
                  writable: true,
                  description: "height 위치 값"
            }]
      }, {
            name: "label",
            label: "레이블 속성",
            children: [{
				owner:"label",
				name: "label_using",
				type: "string",
				show: true,
				writable: true,
				defaultValue: "'Y'",
				description: "사용 여부 Y/N값 입니다."
			},{
				owner:"label",
				name: "label_text",
				type: "string",
				show: true,
				writable: true,
				defaultValue: "'component_label'",
				description: "라벨영역에 노출할 텍스트 문자열 입니다."
			}]
      }];

//  추후 추가 예정
      static method_info = [{
            name:"getGroupPropertyValue",
            description:"그룹 프로퍼티 값을 구하는 메서드 입니다.",
            params:[{
                  name:"groupName",
                  type:"string",
                  description:"그룹 이름입니다."
            },{
                  name:"propertyName",
                  type:"string",
                  description:"그룹내에 존재하는 프로퍼티 이름입니다."
            }]
      }
];


// 이벤트 정보
      static event_info = [
            {
                  name: "click",
                  label: "클릭이벤트",
                  description: "클릭이벤트입니다.",
                  properties: [],

            },{
                  name: "dblclick",
                  label: "더블클릭이벤트",
                  description: "더블클릭이벤트입니다.",
                  properties: [],

            }, {
                  name: "register",
                  label: "컴포넌트 등록 완료",
                  description: "컴포넌트 등록 완료 이벤트 입니다.",
                  properties: []
            },{
                  name: "completed",
                  label: "completed 이벤트",
                  description: "completed 이벤트 입니다.",
                  properties: []
            },{
                  name: "destroy",
                  label: "destroy 이벤트",
                  description: "destroy 이벤트 입니다.",
                  properties: []
            }
      ];


      static attach_default_component_infos(ComponentClass:any, default_info:any={}){
            ComponentClass.default_properties =WVComponentPropertyManager.clone_default_properties();
            $.extend(true, ComponentClass.default_properties, default_info);

            ComponentClass.property_panel_info = WVComponentPropertyManager.clone_default_property_panel_info();
            ComponentClass.property_info = WVComponentPropertyManager.clone_default_property_info();
            ComponentClass.method_info = WVComponentPropertyManager.clone_default_method_info();
            ComponentClass.event_info = WVComponentPropertyManager.clone_default_event_info();
      }


      static clone_default_properties(){
            return JSON.parse(JSON.stringify(WVComponentPropertyManager.default_properties));
      }


      static clone_default_property_panel_info(){
            return WVComponentPropertyManager.property_panel_info.slice(0);
      }

      static clone_default_property_info(){
            return WVComponentPropertyManager.property_info.slice(0);
      }

      static clone_default_method_info(){
            return WVComponentPropertyManager.method_info.slice(0);
      }

      static clone_default_event_info(){
            return WVComponentPropertyManager.event_info.slice(0);
      }


}
